# TubeGem

This application fetches the information of all the videos of a given YouTube channel using the YouTube V3 Data API in a regular interval (default 20 minutes), creates or updates the information in the database and calculates video performance for each of the videos. It also exposes an API endpoint for the saved videos with filters on tags and performance.

YouTube channel and the fetch interval are configurable by the environment variables in the `docker-compose.yml`.
Video performance = first hour (from when the video is created in the database) views divided by the channel's all videos' first hour views median.

## Top tools
- Docker, Compose
- Python 3.9
- Django 3.2, DRF
- Celery, RabbitMQ
- MySQL 8
- Pytest

## Running the project

Using docker-compose:
```bash
docker-compose up -d
```

Following services will be up and running by this:

- Django app server
- Celery worker and beat
- MySQL
- RabbitMQ

Database migrations will be auto applied during the start.
App server will be accessible through the `8000` port from the host machine.

## API endpoint

```
GET /api/v1/videos
```

Example request with all the available query params:

```
GET http://0.0.0.0:8000/api/v1/videos?performance_min=5&performance_max=10&tags=a16z,sf vc&limit=10&offset=20
```

Detailed documentation with interactive playground is available at [http://0.0.0.0:8000/docs](http://0.0.0.0:8000/docs).

## Running tests

```bash
docker-compose run --rm app pytest -v
```

## View logs

App server log:
```bash
docker-compose logs -f app
```

Worker log:
```bash
docker-compose logs -f worker
```

## Steps for fetching videos from multiple channels
1. Create a model `Channel` which will contain the unique YouTube channel ID
2. Add channel as a foreign key in the `Video` model
3. Create a new celery task
    * Point the celery beat schedule to this task
    * Retrieve all the channels
    * Trigger `videos.tasks.fetch_and_update_videos` for each of the channels
4. Take channel ID as an argument of this task `videos.tasks.fetch_and_update_videos`
5. Scale up the celery task processing
6. Have a channel filter on the videos endpoint

Caveat: `videos.services.YoutubeV3.get_all_videos` keeps all the fetched videos information in memory. While processing lots of channels with tons of videos, memory usage may become an issue. May need to process the fetched information in chunks to save memory when required.
