import requests

from datetime import datetime

from django.conf import settings


class YoutubeV3:
    BASE_URL = 'https://www.googleapis.com/youtube/v3'
    REQUEST_TIMEOUT = (3.05, 27)

    def _get_resources(self, path_with_filter, page_token=None, resources=None):
        """
        Recursively paginate through youtube api and return all resources
        """
        url = f'{self.BASE_URL}{path_with_filter}'
        params = {'key': settings.YOUTUBE_API_KEY, 'maxResults': 50}
        if page_token:
            params['pageToken'] = page_token

        response = requests.get(url, params=params, timeout=self.REQUEST_TIMEOUT)
        response.raise_for_status()
        data = response.json()

        if resources:
            resources += data['items']
        else:
            resources = data['items']
        page_token = data.get('nextPageToken')

        if not page_token:
            return resources
        return self._get_resources(path_with_filter, page_token, resources)

    def _get_playlist_id(self, channel_id):
        """
        Get the playlist ID of all the uploads of the given channel
        """
        path_with_filter = f'/channels?part=contentDetails&id={channel_id}'
        channels = self._get_resources(path_with_filter)
        playlist_id = channels[0]['contentDetails']['relatedPlaylists']['uploads']
        return playlist_id

    def _get_all_video_ids(self, playlist_id):
        """
        Get all the video IDs of the given playlist
        """
        path_with_filter = f'/playlistItems?part=contentDetails&playlistId={playlist_id}'
        playlist_items = self._get_resources(path_with_filter)
        video_ids = [item['contentDetails']['videoId'] for item in playlist_items]
        return video_ids

    def get_all_videos(self, channel_id):
        """
        Get all the video information of the given channel
        """
        playlist_id = self._get_playlist_id(channel_id)
        video_ids = self._get_all_video_ids(playlist_id)

        # divide the video ids into segments of at most 50 items per list since
        # youtube supports at most 50 video ids per api call to the videos endpoint
        ids_segments = [video_ids[i:i + 50] for i in range(0, len(video_ids), 50)]
        videos = []

        for ids_segment in ids_segments:
            ids = ','.join(ids_segment)
            path_with_filter = f'/videos?part=snippet,statistics&id={ids}'
            videos += self._get_resources(path_with_filter)

        formatted_videos = [
            {
                'youtube_id': video['id'],
                'title': video['snippet']['title'],
                'description': video['snippet']['description'],
                'thumbnail': video['snippet']['thumbnails']['default']['url'],
                'published_at': datetime.strptime(
                    video['snippet']['publishedAt'], '%Y-%m-%dT%H:%M:%SZ'
                ),
                'channel_id': video['snippet']['channelId'],
                'tags': video['snippet'].get('tags', []),
                'view_count': int(video['statistics']['viewCount']),
                'like_count': int(video['statistics']['likeCount']),
                'dislike_count': int(video['statistics']['dislikeCount']),
                'favorite_count': int(video['statistics']['favoriteCount']),
                'comment_count': int(video['statistics']['commentCount']),
            } for video in videos
        ]
        return formatted_videos
