from django.db import models


class Tag(models.Model):
    value = models.CharField(max_length=100, unique=True, db_index=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.value


class Video(models.Model):
    youtube_id = models.CharField(max_length=50, unique=True, db_index=True)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    thumbnail = models.URLField()
    published_at = models.DateTimeField()
    channel_id = models.CharField(max_length=50, db_index=True)
    tags = models.ManyToManyField(Tag, related_name='videos', blank=True)
    view_count = models.PositiveBigIntegerField()
    like_count = models.PositiveBigIntegerField()
    dislike_count = models.PositiveBigIntegerField()
    favorite_count = models.PositiveBigIntegerField()
    comment_count = models.PositiveBigIntegerField()
    first_hour_views = models.PositiveBigIntegerField(null=True, blank=True)
    performance = models.FloatField(null=True, blank=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.title
