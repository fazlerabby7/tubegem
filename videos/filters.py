from django_filters import rest_framework as filters

from .models import Video


class CharInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class VideoFilter(filters.FilterSet):
    tags = CharInFilter(field_name='tags__value', lookup_expr='in')
    performance = filters.RangeFilter()

    class Meta:
        model = Video
        fields = ['tags', 'performance']
