from rest_framework import generics

from .filters import VideoFilter
from .models import Video
from .serializers import VideoSerializer


class VideoList(generics.ListAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    filterset_class = VideoFilter
