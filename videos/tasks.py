from copy import deepcopy
from datetime import timedelta
from statistics import median

from celery import shared_task
from django.conf import settings
from django.utils import timezone

from .models import Tag, Video
from .services import YoutubeV3


youtube = YoutubeV3()


@shared_task(autoretry_for=(Exception,),
             retry_kwargs={'max_retries': 5},
             retry_backoff=True)
def fetch_and_update_videos():
    """
    Fetch all videos from youtube of the given channel in settings,
    create the videos and tags in the db, update if already exist.

    Calculate and update the first hour views and performance of the videos.

    This task is supposed to be invoked in regular intervals using celery beat.
    """
    fetched_videos = deepcopy(youtube.get_all_videos(settings.YOUTUBE_CHANNEL_ID))
    db_videos = []

    for fetched_video in fetched_videos:
        fetched_tags = fetched_video.pop('tags')
        db_tags = []
        for fetched_tag in fetched_tags:
            db_tag, _ = Tag.objects.get_or_create(value=fetched_tag)
            db_tags.append(db_tag)

        youtube_id = fetched_video.pop('youtube_id')
        db_video, _ = Video.objects.update_or_create(
            youtube_id=youtube_id,
            defaults=fetched_video
        )
        db_video.tags.set(db_tags)
        db_videos.append(db_video)

        # if the video is saved in the db within an hour,
        # update the first hour views
        if (timezone.now() - db_video.created_at) <= timedelta(hours=1):
            db_video.first_hour_views = db_video.view_count

    first_hour_views_median = median(
        [db_video.first_hour_views for db_video in db_videos]
    )

    for db_video in db_videos:
        db_video.performance = db_video.first_hour_views / first_hour_views_median

    Video.objects.bulk_update(db_videos, ['performance', 'first_hour_views'])
