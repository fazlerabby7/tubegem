from django.urls import path

from .views import VideoList


urlpatterns = [
    path('v1/videos', view=VideoList.as_view(), name='videos'),
]
