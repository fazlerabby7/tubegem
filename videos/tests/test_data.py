import datetime

from django.conf import settings


YOUTUBE_CHANNELS_RESPONSE = {
  "kind": "youtube#channelListResponse",
  "etag": "EBGxcszUcg3sNTyS2gejorCMuto",
  "pageInfo": {
    "totalResults": 1,
    "resultsPerPage": 50
  },
  "items": [
    {
      "kind": "youtube#channel",
      "etag": "G8SiILF8BOB5mO50vE8YRK3MUeU",
      "id": settings.YOUTUBE_CHANNEL_ID,
      "contentDetails": {
        "relatedPlaylists": {
          "likes": "",
          "favorites": "",
          "uploads": "UUIBgYfDjtWlbJhg--Z4sOgQ"
        }
      }
    }
  ]
}

YOUTUBE_PLAYLISTS_RESPONSE_1 = {
  "kind": "youtube#playlistItemListResponse",
  "etag": "q3o4_ANrLfW4giHis-7GUsjQml4",
  "nextPageToken": "CDIQAA",
  "items": [
    {
      "kind": "youtube#playlistItem",
      "etag": "QnmeSiL0KWycXxmS1vjkw-dThdM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkpiSGlwNHRqRWNn",
      "contentDetails": {
        "videoId": "JbHip4tjEcg",
        "videoPublishedAt": "2021-06-16T14:30:02Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "o-22kWhCdxOsNzF-cqncIqIkcZ8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmpvNThxRW9UaFNz",
      "contentDetails": {
        "videoId": "jo58qEoThSs",
        "videoPublishedAt": "2021-06-09T15:21:01Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "txb-gOjMAifhyMt7KELEwge_ni8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnlVZ1VzLU1PeEUw",
      "contentDetails": {
        "videoId": "yUgUs-MOxE0",
        "videoPublishedAt": "2021-06-01T14:30:08Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "ozpe6ZPh-9OCEU46TDncJovugos",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLk1KLThUU3JuMEpz",
      "contentDetails": {
        "videoId": "MJ-8TSrn0Js",
        "videoPublishedAt": "2021-05-26T14:30:02Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "wmgZqo7eNdIaKj-mmf-fJfOJSQw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlNsWThwN3FXZ3Nr",
      "contentDetails": {
        "videoId": "SlY8p7qWgsk",
        "videoPublishedAt": "2021-05-20T14:30:07Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Q6ypkSgiylM7nk3TU_ndrLt2kwY",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlVwYkdiS1FzVGpj",
      "contentDetails": {
        "videoId": "UpbGbKQsTjc",
        "videoPublishedAt": "2021-05-19T14:30:07Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Fg_IpJY8L4QRJWqaPMWxiDbVf2o",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLm1KN3otNUtqejQ0",
      "contentDetails": {
        "videoId": "mJ7z-5Kjz44",
        "videoPublishedAt": "2021-05-04T14:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "OPbDBK2c7xojMWSIeKj4t_a9LH8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlN3dm9SOW1MVFhN",
      "contentDetails": {
        "videoId": "SwvoR9mLTXM",
        "videoPublishedAt": "2021-04-22T14:30:05Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "QAtYgWPt_Sh6VD1a-42ojayXEiI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLng1WUFwam5URzEw",
      "contentDetails": {
        "videoId": "x5YApjnTG10",
        "videoPublishedAt": "2021-04-14T14:30:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "rL8xH808JDbyq-z1fLPJZmAqhcw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmVMZWxneTV6UnY0",
      "contentDetails": {
        "videoId": "eLelgy5zRv4",
        "videoPublishedAt": "2021-04-07T14:30:03Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "a3Pn9TSy7Tj-HvWnrebTkczvrLg",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjdIZHU0RGxuTElr",
      "contentDetails": {
        "videoId": "7Hdu4DlnLIk",
        "videoPublishedAt": "2021-03-31T14:30:04Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "MAgG2CET5wp70at3oywUfXI19cM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlNGUFFsS2NlUHU4",
      "contentDetails": {
        "videoId": "SFPQlKcePu8",
        "videoPublishedAt": "2021-03-23T14:30:16Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "soHFvG_4dmEFTY_5F5HxgJ_FyAo",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLktEMmJrZ294U1NJ",
      "contentDetails": {
        "videoId": "KD2bkgoxSSI",
        "videoPublishedAt": "2021-03-17T13:00:18Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "SCUlEdQwPZb3GBYQAfAzUv-rrz8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmFBYncxSUw5NlY4",
      "contentDetails": {
        "videoId": "aAbw1IL96V8",
        "videoPublishedAt": "2021-02-26T17:00:01Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "EioMHwyvHiRY-sx59gAJ1YV1RPc",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjVscy1GUUE1VWlz",
      "contentDetails": {
        "videoId": "5ls-FQA5Uis",
        "videoPublishedAt": "2021-02-26T15:30:14Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "FZdxDFUmortGhvHSknHBMHGc3OU",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnpHX0lYcjZOWEFn",
      "contentDetails": {
        "videoId": "zG_IXr6NXAg",
        "videoPublishedAt": "2021-02-22T15:30:14Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "ZqzimI7ObxQ2zETvHk6PkRGApxE",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmtoWTNSRU9zdC1B",
      "contentDetails": {
        "videoId": "khY3REOst-A",
        "videoPublishedAt": "2021-02-15T15:30:15Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "4KuflVrrHU9QsVOLFycPLUwptqo",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjZjVzJJZFk2SGhj",
      "contentDetails": {
        "videoId": "6cW2IdY6Hhc",
        "videoPublishedAt": "2021-02-06T15:30:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "uAP7FlU1wcNgLLOcysKG6mW4_P8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLm8yR3VPdk8tTW5z",
      "contentDetails": {
        "videoId": "o2GuOvO-Mns",
        "videoPublishedAt": "2021-01-30T15:30:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "agHIkTpOdMO7OdOxQzm8biUwhlI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnkyOVpsaHVFSXlj",
      "contentDetails": {
        "videoId": "y29ZlhuEIyc",
        "videoPublishedAt": "2021-01-26T15:30:30Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "2w5t6tJqYDwaFinpzBG3GHOKcuE",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkszQ3RjMVVOMGJR",
      "contentDetails": {
        "videoId": "K3Ctc1UN0bQ",
        "videoPublishedAt": "2021-01-20T15:30:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "5zayXqfj-glSsSJFNzfg3Ou3VF8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjhNaldDa2NMZjF3",
      "contentDetails": {
        "videoId": "8MjWCkcLf1w",
        "videoPublishedAt": "2021-01-18T15:30:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "THS4-7sL89fwT5UcWVXzkMHCzqg",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmNSRE9qNEVaOXFv",
      "contentDetails": {
        "videoId": "cRDOj4EZ9qo",
        "videoPublishedAt": "2021-01-12T15:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "fggJQW1WhuogRXh0Tx1MkP_oEnI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmtKNG8wT05IN1c4",
      "contentDetails": {
        "videoId": "kJ4o0ONH7W8",
        "videoPublishedAt": "2021-01-09T20:00:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "o2PLmsbJj5XlEHGgfWD5aWzXXt4",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRtdi1ub29ONDNV",
      "contentDetails": {
        "videoId": "dmv-nooN43U",
        "videoPublishedAt": "2020-12-29T15:00:27Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Z1z5sLFU7bM-Ir9v2S6A0t_Zwq8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjlOWXJzRV95VzY0",
      "contentDetails": {
        "videoId": "9NYrsE_yW64",
        "videoPublishedAt": "2020-12-22T17:45:31Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "8pfNt4UBLPtEijO6HIfP0uFEIrw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnZoVGljRUpUc3RB",
      "contentDetails": {
        "videoId": "vhTicEJTstA",
        "videoPublishedAt": "2020-12-21T18:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "rtlp_NETdAdgN9xQ976Aj-Fj05M",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmt0OVNjUXZUcWNV",
      "contentDetails": {
        "videoId": "kt9ScQvTqcU",
        "videoPublishedAt": "2020-12-18T15:00:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "RdrB1HoOt1FOey0P4F_oe_Sdriw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLk1iNUZrMUdYdUVN",
      "contentDetails": {
        "videoId": "Mb5Fk1GXuEM",
        "videoPublishedAt": "2020-12-15T15:00:12Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "NAcpuMSRsQJA-ZshSFZ27CJJnCI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnZpTlpLOVA0MGtZ",
      "contentDetails": {
        "videoId": "viNZK9P40kY",
        "videoPublishedAt": "2020-12-12T17:33:40Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "2HtWfq1crcwRZMZ8T9hyahZWkEA",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRlY0RJa0RiUzNR",
      "contentDetails": {
        "videoId": "decDIkDbS3Q",
        "videoPublishedAt": "2020-12-09T15:20:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "bXp0QUTuC2-4mCW4HvH1qU7Wf9M",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRCVUxPbzAzY3lz",
      "contentDetails": {
        "videoId": "dBULOo03cys",
        "videoPublishedAt": "2020-12-07T15:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "EC--NlNSmOeuHggj0XQE8Xldi0w",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlBCbVQzMGFVZDJz",
      "contentDetails": {
        "videoId": "PBmT30aUd2s",
        "videoPublishedAt": "2020-12-01T15:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "nkxPr4OivrQLN2vUdZsHzcroumQ",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlJPdzFHTUVQXzE0",
      "contentDetails": {
        "videoId": "ROw1GMEP_14",
        "videoPublishedAt": "2020-11-28T16:00:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "UzAj2CoLtxiY09uL5Fhe20zC1_c",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjBwenBoc3pFNzA0",
      "contentDetails": {
        "videoId": "0pzphszE704",
        "videoPublishedAt": "2020-11-19T15:00:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "mb3f2Om3fVKAiTezyfhzZxIYPIM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjNZS05yLUxpYmxJ",
      "contentDetails": {
        "videoId": "3YKNr-LiblI",
        "videoPublishedAt": "2020-11-14T15:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "OMutCCbVpD3YXjivZ5zgg4HOeEI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnFsUDRiblpfaXpr",
      "contentDetails": {
        "videoId": "qlP4bnZ_izk",
        "videoPublishedAt": "2020-11-09T15:00:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "omB3ozNc-j0oM9p_4V4VdKKEero",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnVRV2s3VDlWel9r",
      "contentDetails": {
        "videoId": "uQWk7T9Vz_k",
        "videoPublishedAt": "2020-11-05T15:00:13Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "IOlRwCiewqYXQD0O5tLsH8SzoHI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLk1udUZFdkk3RUd3",
      "contentDetails": {
        "videoId": "MnuFEvI7EGw",
        "videoPublishedAt": "2020-10-28T14:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "jHlaUT3R0H03QcU0VwZ-WgqirTg",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjVzaWdnV004ZmFZ",
      "contentDetails": {
        "videoId": "5siggWM8faY",
        "videoPublishedAt": "2020-10-27T14:00:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "6AZVlURnBy5Gv5CB9bsp1VnkQjc",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlAtSWdqMjdSaF9r",
      "contentDetails": {
        "videoId": "P-Igj27Rh_k",
        "videoPublishedAt": "2020-10-20T14:00:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "cd5AtKN9Mq4QJSJxzb9FOrKK0Cw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLm9wa0hKTFZBTTRB",
      "contentDetails": {
        "videoId": "opkHJLVAM4A",
        "videoPublishedAt": "2020-10-19T14:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "025hnfhz2gDP3dC0gnlBdguHFuI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnd4c0RuUUtjcTRZ",
      "contentDetails": {
        "videoId": "wxsDnQKcq4Y",
        "videoPublishedAt": "2020-10-13T14:40:30Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "WAvVYQRXfZFKU2_MTf1vPsWS_uA",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlYwZVkybkJ0c2p3",
      "contentDetails": {
        "videoId": "V0eY2nBtsjw",
        "videoPublishedAt": "2020-10-08T14:37:29Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "R9ery3x4UV0Tcx4nfz7fBBrXpQ4",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlhhNnpzYlhHUTVN",
      "contentDetails": {
        "videoId": "Xa6zsbXGQ5M",
        "videoPublishedAt": "2020-10-06T14:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Yd1EOQCWmmqgoquL_ShffB7HZLQ",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjhwRzZnb2xNSFBv",
      "contentDetails": {
        "videoId": "8pG6golMHPo",
        "videoPublishedAt": "2020-09-30T13:43:55Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "LQgGrhM7FR0muLGmV4hffJTqQ4Q",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnh5NFdkUmZEeVRz",
      "contentDetails": {
        "videoId": "xy4WdRfDyTs",
        "videoPublishedAt": "2020-09-23T14:00:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "HcDkLGwLiSxsDKiWlaPXc-dwSt0",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlJoWVpFQ1IyUnU4",
      "contentDetails": {
        "videoId": "RhYZECR2Ru8",
        "videoPublishedAt": "2020-09-09T14:00:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "vpdiEx2d5vpz2-3WrYIrtm1hvXI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRSQkR6LWZsS3Mw",
      "contentDetails": {
        "videoId": "dRBDz-flKs0",
        "videoPublishedAt": "2020-09-01T15:59:20Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "_s6E6VvIOnZGD_ZzRH20AZ67nVo",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRMaWNndDA0aEhZ",
      "contentDetails": {
        "videoId": "dLicgt04hHY",
        "videoPublishedAt": "2020-08-18T14:30:09Z"
      }
    }
  ],
  "pageInfo": {
    "totalResults": 88,
    "resultsPerPage": 50
  }
}

YOUTUBE_PLAYLISTS_RESPONSE_2 = {
  "kind": "youtube#playlistItemListResponse",
  "etag": "vVr3pztHR8UvPjPgPVgArSOViiw",
  "prevPageToken": "CDIQAQ",
  "items": [
    {
      "kind": "youtube#playlistItem",
      "etag": "_JBo1qIfw-S-CbxMYRbkYH_Jtyk",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmJPUmdNSTUtb1pF",
      "contentDetails": {
        "videoId": "bORgMI5-oZE",
        "videoPublishedAt": "2020-07-29T14:30:03Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "xeOWiwspaYZrIxE7N3zMiWB9KZU",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjZYVVpyV3pGbUlF",
      "contentDetails": {
        "videoId": "6XUZrWzFmIE",
        "videoPublishedAt": "2020-07-21T13:13:08Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "03PFWoTh2x6j2pqF8L9XewcaHmw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjk2SENDbmhiS1lN",
      "contentDetails": {
        "videoId": "96HCCnhbKYM",
        "videoPublishedAt": "2020-07-14T16:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "1kj_UUkrir9YhLqpoLsuWbcYZ80",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnpIWnhVUkZnSy00",
      "contentDetails": {
        "videoId": "zHZxURFgK-4",
        "videoPublishedAt": "2020-07-09T14:30:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "3B8YL_vpp3-8MxehNBPBGvyZgpM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkFWR2VBLTdXYmVR",
      "contentDetails": {
        "videoId": "AVGeA-7WbeQ",
        "videoPublishedAt": "2020-06-30T14:30:11Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "2Td0JUmgDQu06RbF938ScGxEdvo",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnI4aGh2dy0xYi1N",
      "contentDetails": {
        "videoId": "r8hhvw-1b-M",
        "videoPublishedAt": "2020-06-15T14:30:31Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "evlxRtpxFwAOOnR8YmIzBm4dM5I",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmFwUDYyYzhHSGlN",
      "contentDetails": {
        "videoId": "apP62c8GHiM",
        "videoPublishedAt": "2020-05-20T00:00:28Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "nd0aDc1GW1k8m3Vz5ydWxkfV8L8",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjM1VU9HWWxtN0Y4",
      "contentDetails": {
        "videoId": "35UOGYlm7F8",
        "videoPublishedAt": "2020-05-13T16:00:09Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "KdA8mRhtY0C9xJiNMKJ3ra7tND0",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmhPTVplYkRYUXNV",
      "contentDetails": {
        "videoId": "hOMZebDXQsU",
        "videoPublishedAt": "2020-05-06T17:55:14Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "sTu2maIUSNCckRBgv3PJ3mP0HZA",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLldLX1h3amhKbDNr",
      "contentDetails": {
        "videoId": "WK_XwjhJl3k",
        "videoPublishedAt": "2020-04-28T23:42:37Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "x2CCLBQWtNT0QGE9PKySch8D6uQ",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkYtcjZqTC1vRlhF",
      "contentDetails": {
        "videoId": "F-r6jL-oFXE",
        "videoPublishedAt": "2020-04-22T14:28:34Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "fw25iRmBBSkr6fq59v_1L1-E9as",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlpqVGFGQjNtbzV3",
      "contentDetails": {
        "videoId": "ZjTaFB3mo5w",
        "videoPublishedAt": "2020-04-14T14:15:00Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "bRBZEXRjBgoCVyS5a7YXzWhUlhI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjJnY2NBT3VHUmRV",
      "contentDetails": {
        "videoId": "2gccAOuGRdU",
        "videoPublishedAt": "2020-04-07T14:57:07Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "c1hP0rkqOFDnklxWyB3YROwjKuA",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjNwUDVSSi1kRTc0",
      "contentDetails": {
        "videoId": "3pP5RJ-dE74",
        "videoPublishedAt": "2020-04-01T14:56:06Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Ut0nja-iP2zHuc-RQiz4mVMmjzk",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmhkOURENHQ4NWZN",
      "contentDetails": {
        "videoId": "hd9DD4t85fM",
        "videoPublishedAt": "2020-03-25T15:48:26Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "xbLYbnffIIBLW3QW0dIP3C9AegI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkZjamJwWDdjbkVJ",
      "contentDetails": {
        "videoId": "FcjbpX7cnEI",
        "videoPublishedAt": "2020-03-19T02:46:53Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "vE_ZB1Axx3p2xCRlGOzQuYxaM1I",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmlkeWZIczNEV21j",
      "contentDetails": {
        "videoId": "idyfHs3DWmc",
        "videoPublishedAt": "2020-03-11T15:17:14Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "t7c7iv5XrDx9kgU6DA81Prc4Hig",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmRobnpCUWpzZ0dV",
      "contentDetails": {
        "videoId": "dhnzBQjsgGU",
        "videoPublishedAt": "2020-03-04T14:42:53Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "1UXduFSirJc7QMJoTivv6f8p_HM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkJKb183LVJuMXdj",
      "contentDetails": {
        "videoId": "BJo_7-Rn1wc",
        "videoPublishedAt": "2020-02-28T14:48:44Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "l5SUfsfyUR2pAfpbri3s_ISEixI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmJ0UkhtN3o2MW9v",
      "contentDetails": {
        "videoId": "btRHm7z61oo",
        "videoPublishedAt": "2020-02-21T14:34:24Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "agxiR449GgEQsCVZnVU-zG-6p_c",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmVtb2F1RmxoWEEw",
      "contentDetails": {
        "videoId": "emoauFlhXA0",
        "videoPublishedAt": "2020-02-12T13:45:01Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "uD-sbrhKGDNYTCL35jGCiB7Mbno",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnBHYld5bnpXYXVR",
      "contentDetails": {
        "videoId": "pGbWynzWauQ",
        "videoPublishedAt": "2020-02-05T15:00:25Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "37PvpGp135BdtS_HLUaJAN_gcqo",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmxJbGd1eld4RWlJ",
      "contentDetails": {
        "videoId": "lIlguzWxEiI",
        "videoPublishedAt": "2020-01-29T15:00:08Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "KzB9psDZH3ZaIC0ekxeoaSe-7-Y",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjBhM2VNN2xjVlRJ",
      "contentDetails": {
        "videoId": "0a3eM7lcVTI",
        "videoPublishedAt": "2020-01-20T21:10:21Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Ar-1gwUT_3LAAAiABfmsoe84cHw",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLk05M3hsUHhRQURF",
      "contentDetails": {
        "videoId": "M93xlPxQADE",
        "videoPublishedAt": "2020-01-13T14:54:15Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "GsVDe-VAwgs6-6g9qtoQowUKn68",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnQ0d214eDB4V1Aw",
      "contentDetails": {
        "videoId": "t4wmxx0xWP0",
        "videoPublishedAt": "2020-01-06T15:49:55Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Bfg-dfD81e-rArN2ghHsJvuSA_E",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmxiSm5NbWZINC1V",
      "contentDetails": {
        "videoId": "lbJnMmfH4-U",
        "videoPublishedAt": "2019-12-30T15:05:43Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "W1x4xw8Vf0xeXwpqnVmK_SN2qUI",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmR0bkcwRUxqdmNN",
      "contentDetails": {
        "videoId": "dtnG0ELjvcM",
        "videoPublishedAt": "2019-12-23T15:26:07Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "w4RP9IjGCo-ESIk7uwXwPC2cpWE",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjlGcElaM243UE1B",
      "contentDetails": {
        "videoId": "9FpIZ3n7PMA",
        "videoPublishedAt": "2019-12-19T19:20:51Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "ct2m0qt1eVZ7M-X-xxcH4Ni6X5Y",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmdYNHN4SFJvMTJV",
      "contentDetails": {
        "videoId": "gX4sxHRo12U",
        "videoPublishedAt": "2019-12-03T15:00:10Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "h2vaoRj2tiSOeJo5ZPWDBzlfllg",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLk5CZDZ5SkJ6eWlz",
      "contentDetails": {
        "videoId": "NBd6yJBzyis",
        "videoPublishedAt": "2019-11-14T08:29:34Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "z9XzqvcXT2S4sOc6hByJWOZhD_M",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkxNRFA2VEN2MXBJ",
      "contentDetails": {
        "videoId": "LMDP6TCv1pI",
        "videoPublishedAt": "2019-11-05T15:06:49Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "jRIhrqkSNCeAh8as_f2ESqpqZg0",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLlVXd3RYZmFsZGw0",
      "contentDetails": {
        "videoId": "UWwtXfaldl4",
        "videoPublishedAt": "2019-10-29T03:41:18Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "e4ENKPzXA7MIiNU0nDEbzE3yNeQ",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmJrLXIwekNQNTlN",
      "contentDetails": {
        "videoId": "bk-r0zCP59M",
        "videoPublishedAt": "2019-10-22T16:30:25Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "Vtp3pN4KOv7KlrOv9HPRZurtEWQ",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLnJmVGd6QTZpS1pj",
      "contentDetails": {
        "videoId": "rfTgzA6iKZc",
        "videoPublishedAt": "2019-10-18T05:18:47Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "ZMR6gy7bkIV_ZPOu09wUQSx_Zzg",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLjR3YkNWTjF5THlB",
      "contentDetails": {
        "videoId": "4wbCVN1yLyA",
        "videoPublishedAt": "2019-09-18T18:59:46Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "wvuMT-tXbGxNom1iVr-5JD09xsE",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLkdrbkd6dzBOcXB3",
      "contentDetails": {
        "videoId": "GknGzw0Nqpw",
        "videoPublishedAt": "2019-09-12T19:01:51Z"
      }
    },
    {
      "kind": "youtube#playlistItem",
      "etag": "nelHdeq2SEVcqG4ABAu1h-HaeeM",
      "id": "VVVJQmdZZkRqdFdsYkpoZy0tWjRzT2dRLmZDRkh5UWFnMjFR",
      "contentDetails": {
        "videoId": "fCFHyQag21Q",
        "videoPublishedAt": "2019-09-06T09:01:39Z"
      }
    }
  ],
  "pageInfo": {
    "totalResults": 88,
    "resultsPerPage": 50
  }
}

YOUTUBE_VIDEOS_RESPONSE_1 = {
  "kind": "youtube#videoListResponse",
  "etag": "OVwS3foqqP0mIvjyawCs73Wdtb0",
  "items": [
    {
      "kind": "youtube#video",
      "etag": "sbjTFECAdRg_zweKqShGZFr192Q",
      "id": "JbHip4tjEcg",
      "snippet": {
        "publishedAt": "2021-06-16T14:30:02Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Do It Yourself. Here's how REAL movements start, then change the world.",
        "description": "The moment you realize the world can be influenced by your actions, everything changes. But how do you make a real dent in the world? You don't need fancy tools. Just do it yourself.\n\n0:00 Intro\n1:58 How a movement emerges\n3:56 How Apple was born\n6:43 How to Attract a Following\n8:55 Doing It Yourself\n10:52: Kurt Cobain on DIY\n13:03 Secrets to life & making your mark.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1Bin assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/JbHip4tjEcg/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/JbHip4tjEcg/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/JbHip4tjEcg/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/JbHip4tjEcg/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/JbHip4tjEcg/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "punk rock",
          "fuck perfection",
          "how real movements start",
          "do it yourself",
          "solo founder",
          "building a company by yourself",
          "should I find a cofounder",
          "how to find a cofounder",
          "ramones",
          "kurt cobain",
          "nirvana interviews",
          "punk rock interviews",
          "startup founder",
          "best investors",
          "startup investors",
          "Silicon Valley"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Do It Yourself. Here's how REAL movements start, then change the world.",
          "description": "The moment you realize the world can be influenced by your actions, everything changes. But how do you make a real dent in the world? You don't need fancy tools. Just do it yourself.\n\n0:00 Intro\n1:58 How a movement emerges\n3:56 How Apple was born\n6:43 How to Attract a Following\n8:55 Doing It Yourself\n10:52: Kurt Cobain on DIY\n13:03 Secrets to life & making your mark.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1Bin assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "39299",
        "likeCount": "3148",
        "dislikeCount": "24",
        "favoriteCount": "0",
        "commentCount": "461"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "A8RixKq-6QmiyEu1aUG7FUSKLtk",
      "id": "jo58qEoThSs",
      "snippet": {
        "publishedAt": "2021-06-09T15:21:01Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Why $ETH is Ultrasound Money going to $10K, | Why Buffett and Berkshire Hathaway are WRONG on Crypto",
        "description": "Buffett might be brilliant, but they're totally wrong on Crypto. Here is why I believe Ethereum is ultrasound money and why I believe $ETH is going to $10K.\n\n0:00: Intro\n0:42 Why Warren Buffet is wrong on Crypto\n2:55 Why Ethereum is programmable money\n6:21 Why $ETH is ultrasound money\n9:16 How Ethereum will drop energy usage by 99%\n\nThis video is an opinion and is for information purposes only. It is not intended to be investment advice. Seek a duly licensed professional for investment advice.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1B in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/jo58qEoThSs/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/jo58qEoThSs/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/jo58qEoThSs/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/jo58qEoThSs/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/jo58qEoThSs/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "eth",
          "ethereum",
          "eth vs btc",
          "bitcoin",
          "how to invest in crypto",
          "crypto investing",
          "bitcoin investing",
          "how to buy bitcoin",
          "how to buy ethereum",
          "how to buy NFTs",
          "ethereum price prediction",
          "eth price 2021",
          "crypto prices"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Why $ETH is Ultrasound Money going to $10K, | Why Buffett and Berkshire Hathaway are WRONG on Crypto",
          "description": "Buffett might be brilliant, but they're totally wrong on Crypto. Here is why I believe Ethereum is ultrasound money and why I believe $ETH is going to $10K.\n\n0:00: Intro\n0:42 Why Warren Buffet is wrong on Crypto\n2:55 Why Ethereum is programmable money\n6:21 Why $ETH is ultrasound money\n9:16 How Ethereum will drop energy usage by 99%\n\nThis video is an opinion and is for information purposes only. It is not intended to be investment advice. Seek a duly licensed professional for investment advice.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1B in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "65835",
        "likeCount": "4080",
        "dislikeCount": "119",
        "favoriteCount": "0",
        "commentCount": "785"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "Q8YkP-pCRE6SqVilVcW1lv1frTc",
      "id": "yUgUs-MOxE0",
      "snippet": {
        "publishedAt": "2021-06-01T14:30:08Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "What is Mimetic Theory? Philosophies of René Girard with Luke Burgis",
        "description": "Ever wondered why humans often act like sheep? Learn why in this video where we explore the philosophies of René Girard with Luke Burgis - one of my favorite experts in the space.\n\nLuke’s book Wanting is now available everywhere you can buy books. Pick up a copy at https://lukeburgis.com/wanting/\n\n0:00 Intro\n0:28 Starting a traditional career path\n3:59 What is mimetic theory?\n6:49 Applying mimetic theory to your own life\n9:39 Manifesting change in your own life\n13:07 Mimesis in social media\n17:34 Managing mimetic impulses\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/yUgUs-MOxE0/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/yUgUs-MOxE0/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/yUgUs-MOxE0/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/yUgUs-MOxE0/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/yUgUs-MOxE0/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "What is Mimetic Theory? Philosophies of René Girard with Luke Burgis",
          "description": "Ever wondered why humans often act like sheep? Learn why in this video where we explore the philosophies of René Girard with Luke Burgis - one of my favorite experts in the space.\n\nLuke’s book Wanting is now available everywhere you can buy books. Pick up a copy at https://lukeburgis.com/wanting/\n\n0:00 Intro\n0:28 Starting a traditional career path\n3:59 What is mimetic theory?\n6:49 Applying mimetic theory to your own life\n9:39 Manifesting change in your own life\n13:07 Mimesis in social media\n17:34 Managing mimetic impulses\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "19111",
        "likeCount": "799",
        "dislikeCount": "16",
        "favoriteCount": "0",
        "commentCount": "149"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "B4U-kMa8FMSmwgyl8jFvCWDptQM",
      "id": "MJ-8TSrn0Js",
      "snippet": {
        "publishedAt": "2021-05-26T14:30:02Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "My 8 Best Startup tips from 10+ Years of Investing",
        "description": "Check out report cards by Tydo here - https://app.tydo.com/signup\nHere are 8 Startup tips containing some of my favorite advice I have for startup founders taken from 10+ Years of Investing at my VC fund, Initialized Capital.\n\nWhat is good retention? https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:00 Intro\n1:08 Be Contrarian AND Right\n1:33 Learn AND EARN\n2:05 Realize Time is Finite\n2:33 Check out Report Cards from Tydo!\n3:50 How to SAVE Your Startup from a death spiral\n4:32 Be a NINJA in the face of Chaos\n5:11 The secret to Designing anything\n6:12 The secret growth engine behind most companies\n7:13 Everything to know about User retention and growth\n8:34 Thanks for watching.\n\nREADY TO EXPAND? HERE ARE THE FULL LESSONS— \n\nLearn or Earn https://youtu.be/eLelgy5zRv4\nHow to design https://youtu.be/K3Ctc1UN0bQ\nFunding Coinbase: Turning $300K into $2 billion https://youtu.be/x5YApjnTG10 \nStop chasing money, chase wealth https://youtu.be/7Hdu4DlnLIk\nStartup next steps after raising millions https://youtu.be/khY3REOst-A \nStartup growth strategies https://youtu.be/zG_IXr6NXAg\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/MJ-8TSrn0Js/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/MJ-8TSrn0Js/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/MJ-8TSrn0Js/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/MJ-8TSrn0Js/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/MJ-8TSrn0Js/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "best startup tips",
          "startup investing",
          "startup investing tips",
          "crypto investing tips",
          "stock market investing tips",
          "coinbase IPO",
          "coinbase investors",
          "how to raise money for your startup",
          "how to raise money for startup",
          "raise money startup",
          "how to get funded",
          "best vcs",
          "best startup investors"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "My 8 Best Startup tips from 10+ Years of Investing",
          "description": "Check out report cards by Tydo here - https://app.tydo.com/signup\nHere are 8 Startup tips containing some of my favorite advice I have for startup founders taken from 10+ Years of Investing at my VC fund, Initialized Capital.\n\nWhat is good retention? https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:00 Intro\n1:08 Be Contrarian AND Right\n1:33 Learn AND EARN\n2:05 Realize Time is Finite\n2:33 Check out Report Cards from Tydo!\n3:50 How to SAVE Your Startup from a death spiral\n4:32 Be a NINJA in the face of Chaos\n5:11 The secret to Designing anything\n6:12 The secret growth engine behind most companies\n7:13 Everything to know about User retention and growth\n8:34 Thanks for watching.\n\nREADY TO EXPAND? HERE ARE THE FULL LESSONS— \n\nLearn or Earn https://youtu.be/eLelgy5zRv4\nHow to design https://youtu.be/K3Ctc1UN0bQ\nFunding Coinbase: Turning $300K into $2 billion https://youtu.be/x5YApjnTG10 \nStop chasing money, chase wealth https://youtu.be/7Hdu4DlnLIk\nStartup next steps after raising millions https://youtu.be/khY3REOst-A \nStartup growth strategies https://youtu.be/zG_IXr6NXAg\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "27002",
        "likeCount": "1713",
        "dislikeCount": "26",
        "favoriteCount": "0",
        "commentCount": "278"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "3glLhN4B1JqWrsJrjQP12MOvvHw",
      "id": "SlY8p7qWgsk",
      "snippet": {
        "publishedAt": "2021-05-20T14:30:07Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Funding the Next Unicorn: Building Initialized Capital with Jen Wolf",
        "description": "Today we sit down with Jen Wolf, who is now President & Partner at Initialized. In addition to being a talented product and user experience executive, she is also an incredible investor helping shape the next generation of founders and companies.\n\n1:21 How Garry met Jen\n4:50 How Jen got into tech\n7:37 Design Thinking as a framework\n10:55 Getting exponential value out of your team\n12:17 How to build a diversity flywheel\n20:10 Finding the next great investor\n24:35 How to find flexibility in traditional structure\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/SlY8p7qWgsk/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/SlY8p7qWgsk/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/SlY8p7qWgsk/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/SlY8p7qWgsk/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/SlY8p7qWgsk/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "female vc",
          "diversity in investing",
          "diverse investors",
          "Jen wolf VC",
          "Jen wolf initialized capital",
          "investor female",
          "best female investors",
          "best female vc"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Funding the Next Unicorn: Building Initialized Capital with Jen Wolf",
          "description": "Today we sit down with Jen Wolf, who is now President & Partner at Initialized. In addition to being a talented product and user experience executive, she is also an incredible investor helping shape the next generation of founders and companies.\n\n1:21 How Garry met Jen\n4:50 How Jen got into tech\n7:37 Design Thinking as a framework\n10:55 Getting exponential value out of your team\n12:17 How to build a diversity flywheel\n20:10 Finding the next great investor\n24:35 How to find flexibility in traditional structure\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "11205",
        "likeCount": "385",
        "dislikeCount": "10",
        "favoriteCount": "0",
        "commentCount": "87"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "PPKhrm11d1gAwTTTC7rQt8Z2Sy8",
      "id": "UpbGbKQsTjc",
      "snippet": {
        "publishedAt": "2021-05-19T14:30:07Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Coinbase CEO Brian Armstrong on Cryptocurrency and the Future of Decentralization",
        "description": "Today we sit down the Co-founder & CEO of Coinbase, Brian Armstrong. Brian started Coinbase in 2012 when bitcoin was $2 and almost no one knew what it was, let alone believed in it. Today, it is the largest cryptocurrency exchange in the USA that just went public at a nearly $100B market cap.\n\nhttps://www.coinbase.com\nhttps://www.twitter.com/brian_armstrong\n\n0:00 Intro\n2:18 How Brian got discovered Bitcoin\n9:45 How Coinbase grew from idea to reality\n11:58 What the early days of bitcoin were like\n15:31 How Brian got into tech\n18:57 Democratizing information and money\n21:15 The three ages of crypto\n24:42 How Blockchain will change science and research\n32:18 Decentralization in a centralized world\n42:11 The IPO is 1% of the Coinbase journey\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\n\nProduced by Chris Hall\nFilmed by Michael Bliss & Oliver Covrett",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/UpbGbKQsTjc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/UpbGbKQsTjc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/UpbGbKQsTjc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/UpbGbKQsTjc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/UpbGbKQsTjc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "Brian Armstrong",
          "coinbase IPO",
          "Coinbase ceo",
          "coinbase ceo interview",
          "Brian Armstrong interview",
          "billionaire interview",
          "startup ceos",
          "fireside chat",
          "coinbase crypto",
          "bitcoin interviews",
          "bitcoin billionaires",
          "startup ipo",
          "how to use coinbase",
          "how coinbase started",
          "fred ehrsam",
          "fred coinbase"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Coinbase CEO Brian Armstrong on Cryptocurrency and the Future of Decentralization",
          "description": "Today we sit down the Co-founder & CEO of Coinbase, Brian Armstrong. Brian started Coinbase in 2012 when bitcoin was $2 and almost no one knew what it was, let alone believed in it. Today, it is the largest cryptocurrency exchange in the USA that just went public at a nearly $100B market cap.\n\nhttps://www.coinbase.com\nhttps://www.twitter.com/brian_armstrong\n\n0:00 Intro\n2:18 How Brian got discovered Bitcoin\n9:45 How Coinbase grew from idea to reality\n11:58 What the early days of bitcoin were like\n15:31 How Brian got into tech\n18:57 Democratizing information and money\n21:15 The three ages of crypto\n24:42 How Blockchain will change science and research\n32:18 Decentralization in a centralized world\n42:11 The IPO is 1% of the Coinbase journey\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\n\nProduced by Chris Hall\nFilmed by Michael Bliss & Oliver Covrett"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "110273",
        "likeCount": "4241",
        "dislikeCount": "39",
        "favoriteCount": "0",
        "commentCount": "621"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "wzgKMufth1_TXy9_piM0NkIv72k",
      "id": "mJ7z-5Kjz44",
      "snippet": {
        "publishedAt": "2021-05-04T14:30:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How High-Res Space Imagery Will Change Everything | 10CM Satellite Imagery Explained",
        "description": "It's not every day a company has the potential to improve something by 10X or more. Albedo has done just this by creating affordable, high resolution satellite cameras that are game-changing for multiple major industries across the world. Today, learn how the space tech revolution is taking off and how Albedo's work is changing the world.\n\nhttps://www.albedo.space\nhttps://jobs.lever.co/albedo\n \n0:00 Intro\n1:02 What is Albedo?\n4:13 How is this technology used?\n12:30 Why now for Space tech?\n14:11 How YOU can be a part\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/mJ7z-5Kjz44/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/mJ7z-5Kjz44/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/mJ7z-5Kjz44/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/mJ7z-5Kjz44/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/mJ7z-5Kjz44/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "satellite imagery",
          "10cm satellite imagery",
          "albedo",
          "topher haddad",
          "space imagery",
          "spacex satellite",
          "aerial imagery",
          "how to launch satellites",
          "future of satellite imagery",
          "albedo space",
          "space travel",
          "high res satellite",
          "space telescope",
          "Hubble telescope",
          "aerial video",
          "drone photography",
          "classified"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How High-Res Space Imagery Will Change Everything | 10CM Satellite Imagery Explained",
          "description": "It's not every day a company has the potential to improve something by 10X or more. Albedo has done just this by creating affordable, high resolution satellite cameras that are game-changing for multiple major industries across the world. Today, learn how the space tech revolution is taking off and how Albedo's work is changing the world.\n\nhttps://www.albedo.space\nhttps://jobs.lever.co/albedo\n \n0:00 Intro\n1:02 What is Albedo?\n4:13 How is this technology used?\n12:30 Why now for Space tech?\n14:11 How YOU can be a part\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "18509",
        "likeCount": "841",
        "dislikeCount": "3",
        "favoriteCount": "0",
        "commentCount": "167"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "TMaEpOI50ffC9hQynrdHgaAFTsU",
      "id": "SwvoR9mLTXM",
      "snippet": {
        "publishedAt": "2021-04-22T14:30:05Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "You can be a VC (I’m hiring): How venture works & what it takes to fund billion dollar startups",
        "description": "Venture Capitalists make billions on startup investments, but how does it actually work? Find out in this video. Also, we're hiring! Apply to work with me at Initialized at the links below.\n\nHow I Funded Coinbase and returned $300K into $2.4B - https://youtu.be/x5YApjnTG10\n\nApply to work at my VC fund—\nWhat is it like to work at Initialized? https://blog.initialized.com/2021/04/working-at-initialized/\nPartner: https://jobs.lever.co/initialized/3aa236dd-dd71-4ba7-9832-59023819a41d\nPrincipal: https://jobs.lever.co/initialized/5e9e3bb1-c4e0-402c-975b-aef32c1482c8\n\nHow I Funded Coinbase and turned $300K into $2.4B - https://youtu.be/x5YApjnTG10\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\u200b\nStop wasting your life: https://youtu.be/eLelgy5zRv4\u200b\n\n0:00 Intro\n1:16 How money and returns work\n4:49 How to build the future\n9:02 Why you need to think for yourself\n12:45 How personal traits of VC's and startup investors \n16:39 Special announcement - we're hiring!\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://www.instagram.com/garrytan\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/SwvoR9mLTXM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/SwvoR9mLTXM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/SwvoR9mLTXM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/SwvoR9mLTXM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/SwvoR9mLTXM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "how vc works",
          "how venture capital works",
          "how to invest in startups",
          "how to become a vc",
          "venture capitalist",
          "venture capital",
          "something ventured",
          "best investors",
          "marc andreesen",
          "ben horowitz",
          "fred wilson",
          "coinbase investors",
          "Brian Armstrong coinbase",
          "coinbase ceo",
          "coinbase ipo",
          "startup investors"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "You can be a VC (I’m hiring): How venture works & what it takes to fund billion dollar startups",
          "description": "Venture Capitalists make billions on startup investments, but how does it actually work? Find out in this video. Also, we're hiring! Apply to work with me at Initialized at the links below.\n\nHow I Funded Coinbase and returned $300K into $2.4B - https://youtu.be/x5YApjnTG10\n\nApply to work at my VC fund—\nWhat is it like to work at Initialized? https://blog.initialized.com/2021/04/working-at-initialized/\nPartner: https://jobs.lever.co/initialized/3aa236dd-dd71-4ba7-9832-59023819a41d\nPrincipal: https://jobs.lever.co/initialized/5e9e3bb1-c4e0-402c-975b-aef32c1482c8\n\nHow I Funded Coinbase and turned $300K into $2.4B - https://youtu.be/x5YApjnTG10\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\u200b\nStop wasting your life: https://youtu.be/eLelgy5zRv4\u200b\n\n0:00 Intro\n1:16 How money and returns work\n4:49 How to build the future\n9:02 Why you need to think for yourself\n12:45 How personal traits of VC's and startup investors \n16:39 Special announcement - we're hiring!\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://www.instagram.com/garrytan\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "143453",
        "likeCount": "6189",
        "dislikeCount": "58",
        "favoriteCount": "0",
        "commentCount": "810"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "9XmcFNCsxxy8YX5qJRd7NTNATUs",
      "id": "x5YApjnTG10",
      "snippet": {
        "publishedAt": "2021-04-14T14:30:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "I funded Coinbase in 2012. Making 6000x on my best startup investment yet",
        "description": "I was lucky enough to be the first investor in a company called Coinbase. Today they are a Cryptocurrency exchange worth billions that just went public. But when I met Brian, Bitcoin was $5 and Coinbase was just him. Here's the story behind my best investment yet.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\n0:49 How I met Brain Armstrong (Coinbase Co-Founder & CEO)\n2:54 No one believed in bitcoin in 2012\n3:42 Why I said YES when most said NO\n6:18 What YOU should learn from this\n7:00 Roll your way to a Startup Unicorn: Lessons for Founders\n\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\nNote: 6000X at the $100B private secondary valuation, blended Initialized and Y Combinator early investments at seed/pre-seed.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/x5YApjnTG10/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/x5YApjnTG10/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/x5YApjnTG10/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/x5YApjnTG10/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/x5YApjnTG10/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "coinbase ipo",
          "coinbase investor",
          "Garry Tan coinbase",
          "best startup investments",
          "how to invest in crypto",
          "midas list vc",
          "Forbes Midas list 2021",
          "best bitcoin exchange",
          "how to buy bitcoin",
          "where to buy bitcoin",
          "best investment of all time",
          "bitcoin price",
          "ethereum price",
          "btc",
          "$COIN",
          "coinbase"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "I funded Coinbase in 2012. Making 6000x on my best startup investment yet",
          "description": "I was lucky enough to be the first investor in a company called Coinbase. Today they are a Cryptocurrency exchange worth billions that just went public. But when I met Brian, Bitcoin was $5 and Coinbase was just him. Here's the story behind my best investment yet.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\n0:49 How I met Brain Armstrong (Coinbase Co-Founder & CEO)\n2:54 No one believed in bitcoin in 2012\n3:42 Why I said YES when most said NO\n6:18 What YOU should learn from this\n7:00 Roll your way to a Startup Unicorn: Lessons for Founders\n\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\nNote: 6000X at the $100B private secondary valuation, blended Initialized and Y Combinator early investments at seed/pre-seed.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "304192",
        "likeCount": "11433",
        "dislikeCount": "104",
        "favoriteCount": "0",
        "commentCount": "1774"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "mFoj3HTh7778o3-N3M_0rwr7VOk",
      "id": "eLelgy5zRv4",
      "snippet": {
        "publishedAt": "2021-04-07T14:30:03Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "LEARN, EARN or QUIT | My job/career advice for 2021 | Garry Tan's Founders Journey Ep. 4",
        "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nIf you're not learning AND earning... it's time to move on. You're wasting your life. Learn how to  master BOTH and maximize your time on earth to make build something great in this week's video.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YApjnTG10\nWhat to do after raising money: https://youtu.be/khY3REOst-A\n\nBill Gates on expertise: https://www.youtube.com/watch?v=CsGihiSE6sM\nAlan Watts on Work: https://www.youtube.com/watch?v=1v1NZgE170w\nDavid Graeber on Bullshit Jobs - https://www.youtube.com/watch?v=XIctCDYv7Yg\n\n0:00 Intro\n0:40 Examples of LEARNING\n4:41 Examples to EARNING\n9:03 Examples of LEARNING and EARNING at the same time\n10:59 Examples of neither learning or earning\n12:06 HOW TO DO IT BEST - (learn, then earn)\n13:01 Outro\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/eLelgy5zRv4/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/eLelgy5zRv4/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/eLelgy5zRv4/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/eLelgy5zRv4/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/eLelgy5zRv4/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "startup advice",
          "best startup advice",
          "best career advice",
          "how to quit your job",
          "when to quit your job",
          "how to find a new job",
          "best startup jobs",
          "how to join a startup",
          "how to become an entreprenur",
          "best career tips",
          "how to get rich",
          "how to get wealthy",
          "Garry Tan startup advice",
          "naval",
          "chamath",
          "Paul g"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "LEARN, EARN or QUIT | My job/career advice for 2021 | Garry Tan's Founders Journey Ep. 4",
          "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nIf you're not learning AND earning... it's time to move on. You're wasting your life. Learn how to  master BOTH and maximize your time on earth to make build something great in this week's video.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YApjnTG10\nWhat to do after raising money: https://youtu.be/khY3REOst-A\n\nBill Gates on expertise: https://www.youtube.com/watch?v=CsGihiSE6sM\nAlan Watts on Work: https://www.youtube.com/watch?v=1v1NZgE170w\nDavid Graeber on Bullshit Jobs - https://www.youtube.com/watch?v=XIctCDYv7Yg\n\n0:00 Intro\n0:40 Examples of LEARNING\n4:41 Examples to EARNING\n9:03 Examples of LEARNING and EARNING at the same time\n10:59 Examples of neither learning or earning\n12:06 HOW TO DO IT BEST - (learn, then earn)\n13:01 Outro\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "185316",
        "likeCount": "9891",
        "dislikeCount": "86",
        "favoriteCount": "0",
        "commentCount": "952"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "pxaXSl2aq-1ZyMWudftt3V0Ghlw",
      "id": "7Hdu4DlnLIk",
      "snippet": {
        "publishedAt": "2021-03-31T14:30:04Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "STOP Chasing Money -- Chase WEALTH. | How To get RICH | Garry Tan's Office Hours Ep. 4",
        "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nA lot of you say you want to get rich. But what you really want is wealth. Find out why in this video.\n\nhttp://www.paulgraham.com/articles.html\nhttps://nav.al/rich\n\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YAp...\u200b\nWhat to do after raising money: https://youtu.be/khY3REOst-A\u200b\n\n0:00 Overview\n1:04 What is wealth?\n3:07 Focus on skills\n8:29 Don't sell your time!\n11:45 Create a wealth machine\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/7Hdu4DlnLIk/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/7Hdu4DlnLIk/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/7Hdu4DlnLIk/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/7Hdu4DlnLIk/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/7Hdu4DlnLIk/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "how to get rich",
          "how to get wealthy",
          "how to build wealth",
          "investment advice",
          "startup investors",
          "best startup investors",
          "how to invest long term",
          "Paul graham",
          "naval ravikant",
          "Justin Kan twitch",
          "naval podcast",
          "Paul graham essays",
          "YC demo day tips",
          "how to get rich the real way",
          "get rich quick scheme"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "STOP Chasing Money -- Chase WEALTH. | How To get RICH | Garry Tan's Office Hours Ep. 4",
          "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nA lot of you say you want to get rich. But what you really want is wealth. Find out why in this video.\n\nhttp://www.paulgraham.com/articles.html\nhttps://nav.al/rich\n\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YAp...\u200b\nWhat to do after raising money: https://youtu.be/khY3REOst-A\u200b\n\n0:00 Overview\n1:04 What is wealth?\n3:07 Focus on skills\n8:29 Don't sell your time!\n11:45 Create a wealth machine\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "359031",
        "likeCount": "20679",
        "dislikeCount": "282",
        "favoriteCount": "0",
        "commentCount": "1406"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "9U25y8aimGPaAVCc5UUTn2s79KI",
      "id": "SFPQlKcePu8",
      "snippet": {
        "publishedAt": "2021-03-23T14:30:16Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How to find your true self and fight your own battles. (my failure as a manager)",
        "description": "Do you ever find yourself wanting different things at the same time? You're not alone. \n\n0:00 Intro\n1:02 My FAILURE As A Manager\n3:34 The Many Minds\n7:19 The 5 Stages of Development\n8:48 Why You must INTEGRATE yourself\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/SFPQlKcePu8/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/SFPQlKcePu8/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/SFPQlKcePu8/hqdefault.jpg",
            "width": 480,
            "height": 360
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "mastering your many minds",
          "multiple personalities",
          "executive coaching",
          "do I have multiple personalities",
          "inside the mind",
          "psychology videos",
          "best investors in the world",
          "how do become wealthy",
          "how to clear your mind",
          "how to be successful",
          "meditation for business",
          "how to meditate",
          "how to focus better"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to find your true self and fight your own battles. (my failure as a manager)",
          "description": "Do you ever find yourself wanting different things at the same time? You're not alone. \n\n0:00 Intro\n1:02 My FAILURE As A Manager\n3:34 The Many Minds\n7:19 The 5 Stages of Development\n8:48 Why You must INTEGRATE yourself\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "37261",
        "likeCount": "1966",
        "dislikeCount": "11",
        "favoriteCount": "0",
        "commentCount": "173"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "TeISHGMLEUivCVFaXmal5IWP9EU",
      "id": "KD2bkgoxSSI",
      "snippet": {
        "publishedAt": "2021-03-17T13:00:18Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Get Users to Do What You Want | The Future of Customer Service & Onboarding Customers w/ Cohere",
        "description": "Both startups and corporate giants alike wish they could be next to users in the moment when they are going through a problem. Cohere lets you do actually do that and avoid losing that person forever. Today, join me as I dive into the future of user onboarding and customer service with Cohere.\n\nhttps://cohere.so/\n\n0:00 Intro\n1:01 What is Cohere?\n2:38 See it in Action\n5:39 Lessons from Superhuman\n10:36 Industry Trends\n11:46 Cohere Success Stories\n15:04 The Silver Bullet for CS, PM and Sales\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/KD2bkgoxSSI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/KD2bkgoxSSI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/KD2bkgoxSSI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/KD2bkgoxSSI/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/KD2bkgoxSSI/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "user onboarding",
          "startup customer service tools",
          "how to guide users through problems",
          "onboarding customers",
          "startup CRM tools",
          "how to onboard users quicker",
          "how to onboard users fast",
          "startup user onboarding",
          "cohere",
          "the future of user onboarding",
          "the future of customer service",
          "customer service tips"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Get Users to Do What You Want | The Future of Customer Service & Onboarding Customers w/ Cohere",
          "description": "Both startups and corporate giants alike wish they could be next to users in the moment when they are going through a problem. Cohere lets you do actually do that and avoid losing that person forever. Today, join me as I dive into the future of user onboarding and customer service with Cohere.\n\nhttps://cohere.so/\n\n0:00 Intro\n1:01 What is Cohere?\n2:38 See it in Action\n5:39 Lessons from Superhuman\n10:36 Industry Trends\n11:46 Cohere Success Stories\n15:04 The Silver Bullet for CS, PM and Sales\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "14701",
        "likeCount": "530",
        "dislikeCount": "9",
        "favoriteCount": "0",
        "commentCount": "42"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "aAKjjTou5dqr2-nAa0vNfPm-Ilg",
      "id": "aAbw1IL96V8",
      "snippet": {
        "publishedAt": "2021-02-26T17:00:01Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Coinbase’s Liftoff Moment: Why I love seed investing and being a CEO coach #shorts",
        "description": "I was lucky enough to be the earliest investor in Coinbase. In a recent interview on Clubhouse, Brian Armstrong recounts with me the early moments of finding product market fit. This is what being a seed investor is all about. #shorts",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/aAbw1IL96V8/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/aAbw1IL96V8/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/aAbw1IL96V8/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/aAbw1IL96V8/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/aAbw1IL96V8/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Coinbase’s Liftoff Moment: Why I love seed investing and being a CEO coach #shorts",
          "description": "I was lucky enough to be the earliest investor in Coinbase. In a recent interview on Clubhouse, Brian Armstrong recounts with me the early moments of finding product market fit. This is what being a seed investor is all about. #shorts"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "12487",
        "likeCount": "387",
        "dislikeCount": "2",
        "favoriteCount": "0",
        "commentCount": "31"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "_sVQgAGScJC9SA4LDeJlFGo-4WU",
      "id": "5ls-FQA5Uis",
      "snippet": {
        "publishedAt": "2021-02-26T15:30:14Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Cutting Edge Climate Tech | Fighting Global Warming & Greenhouse Gasses with Kairos Aerospace",
        "description": "Reversing climate change is a huge problem to solve for the world, and Ari Gesher is actually doing something about it. Today, learn about a company you need to know about - Kairos Aerospace, and Ari Gesher. Ari and I were both early employees at Palantir, working for Peter Thiel. Enjoy!\n\n0:00 Intro\n1:06 How Kairos Aerospace Works\n9:18 How Kairos landed enterprise contracts\n15:39 How Kairos Unit Economics\n18:09 Our Lessons from Palantir (working for Peter Thiel)\n31:49 How to Predict the Next Huge Startup\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/5ls-FQA5Uis/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/5ls-FQA5Uis/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/5ls-FQA5Uis/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/5ls-FQA5Uis/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/5ls-FQA5Uis/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "best Silicon Valley vcs",
          "climate change",
          "global warming",
          "aerial imaging",
          "kairos aerospace",
          "aerial technology",
          "oil surverying",
          "how to stop global warming",
          "global warming technology",
          "ways to solve global warlomg",
          "climate change technology",
          "slow down climate change",
          "Peter Thiel palantir",
          "plantir early employees",
          "Peter thiel"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Cutting Edge Climate Tech | Fighting Global Warming & Greenhouse Gasses with Kairos Aerospace",
          "description": "Reversing climate change is a huge problem to solve for the world, and Ari Gesher is actually doing something about it. Today, learn about a company you need to know about - Kairos Aerospace, and Ari Gesher. Ari and I were both early employees at Palantir, working for Peter Thiel. Enjoy!\n\n0:00 Intro\n1:06 How Kairos Aerospace Works\n9:18 How Kairos landed enterprise contracts\n15:39 How Kairos Unit Economics\n18:09 Our Lessons from Palantir (working for Peter Thiel)\n31:49 How to Predict the Next Huge Startup\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "4102",
        "likeCount": "140",
        "dislikeCount": "5",
        "favoriteCount": "0",
        "commentCount": "17"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "emkRNTSZFY0XVV2loDxeLQTdz8c",
      "id": "zG_IXr6NXAg",
      "snippet": {
        "publishedAt": "2021-02-22T15:30:14Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "6 Startup Growth Strategies from a Forbes Top VC | Seed to Billion Dollar IPO | Office Hours Ep.3",
        "description": "Ok, you just got funded. But how do you grow your startup to a billion dollar IPO? It turns out, there's a formula. Learn what it is, along with 6 tips from me on how to do that in this week's episode of Office Hours.\n\nJust raised money? Make sure you watch my last video here: https://www.youtube.com/watch?v=khY3REOst-A\n\nFounding Sales -- https://www.foundingsales.com\nPredictable Revenue -- http://www.amazon.com/dp/0984380213/\nFollow Nik Sharma for tips on Influencers - https://twitter.com/mrsharma\n\n0:00 -- Intro --\n0:46 Intro: How Do You Grow?\n1:24 Tip 1 -- PR\n1:56 How Soylent used PR\n0:55 Tip 2 -- Sales and Marketing\n2:35 Controversy Sells\n3:06 Tip 3 -- Content Marketing\n3:18 How OkCupid used Content Marketing \n3:47 Tip 4 -- SEO\n4:08 How Zapier's used SEO\n5:02 Google Ads\n6:00 Tip 5 -- Influencers\n6:17 How DTC Brands use Influencers\n6:34 Tip 6: Partnerships\n6:42 How Microsoft & IBM used partnerships\n7:10 -- Recap! --\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/zG_IXr6NXAg/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/zG_IXr6NXAg/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/zG_IXr6NXAg/hqdefault.jpg",
            "width": 480,
            "height": 360
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Garry Tan",
          "Garry Tan vc",
          "initialized capital",
          "Garry Tan initialized",
          "Garry Tan initialized capital",
          "Silicon Valley vc",
          "sf vc",
          "a16z",
          "Andreessen horowitz",
          "how to grow your startup",
          "startup growth tips",
          "how to ipo",
          "what is an ipo",
          "what is an exit",
          "6 Startup Growth Tips from a Midas List VC",
          "billion dollar exit",
          "how to build a startup",
          "how to get funded",
          "how to raise money",
          "how to raise money for a startup",
          "startup growth hacks",
          "how to pitch your startup",
          "best Silicon Valley vc"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "6 Startup Growth Strategies from a Forbes Top VC | Seed to Billion Dollar IPO | Office Hours Ep.3",
          "description": "Ok, you just got funded. But how do you grow your startup to a billion dollar IPO? It turns out, there's a formula. Learn what it is, along with 6 tips from me on how to do that in this week's episode of Office Hours.\n\nJust raised money? Make sure you watch my last video here: https://www.youtube.com/watch?v=khY3REOst-A\n\nFounding Sales -- https://www.foundingsales.com\nPredictable Revenue -- http://www.amazon.com/dp/0984380213/\nFollow Nik Sharma for tips on Influencers - https://twitter.com/mrsharma\n\n0:00 -- Intro --\n0:46 Intro: How Do You Grow?\n1:24 Tip 1 -- PR\n1:56 How Soylent used PR\n0:55 Tip 2 -- Sales and Marketing\n2:35 Controversy Sells\n3:06 Tip 3 -- Content Marketing\n3:18 How OkCupid used Content Marketing \n3:47 Tip 4 -- SEO\n4:08 How Zapier's used SEO\n5:02 Google Ads\n6:00 Tip 5 -- Influencers\n6:17 How DTC Brands use Influencers\n6:34 Tip 6: Partnerships\n6:42 How Microsoft & IBM used partnerships\n7:10 -- Recap! --\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "21449",
        "likeCount": "1293",
        "dislikeCount": "13",
        "favoriteCount": "0",
        "commentCount": "66"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "3orRn3ZZ_0XokSprDa8f2K4I-0E",
      "id": "khY3REOst-A",
      "snippet": {
        "publishedAt": "2021-02-15T15:30:15Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Startup Next Steps after Raising Your First Million | from a Forbes Top 100 VC | Office Hours Ep. 2",
        "description": "We hear about startups raising millions, or billions of dollars all the time now -- but what should founders actually do the day the check hits? Find out why in this freshly branded version of Office Hours. \n\nWhat is good retention? -- https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:42 -- Overview --\n1:02 Part 1: Get Your Head Right\n3:25 Part 2: Make a Plan\n8:27 Part 3: Retention Before Growth\n12:08 Part 4: When the Plan goes Wrong\n15:26 -- Recap --\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/khY3REOst-A/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/khY3REOst-A/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/khY3REOst-A/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/khY3REOst-A/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/khY3REOst-A/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Silicon Valley vc",
          "how do I raise money for my startup?",
          "how to raise a seed round",
          "how to raise a series a",
          "how to raise an angel round",
          "how to raise money for startups",
          "how to raise money for your startup",
          "ltv cac",
          "operating plan",
          "raised series A",
          "roi",
          "startup hiring",
          "startup plan",
          "startup seed funding",
          "startup series a funding",
          "startups",
          "t2d3",
          "venture backed startup",
          "what to do after you raise money for your startup",
          "startup retention",
          "retention",
          "good retention",
          "founder advice"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Startup Next Steps after Raising Your First Million | from a Forbes Top 100 VC | Office Hours Ep. 2",
          "description": "We hear about startups raising millions, or billions of dollars all the time now -- but what should founders actually do the day the check hits? Find out why in this freshly branded version of Office Hours. \n\nWhat is good retention? -- https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:42 -- Overview --\n1:02 Part 1: Get Your Head Right\n3:25 Part 2: Make a Plan\n8:27 Part 3: Retention Before Growth\n12:08 Part 4: When the Plan goes Wrong\n15:26 -- Recap --\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "23480",
        "likeCount": "1409",
        "dislikeCount": "10",
        "favoriteCount": "0",
        "commentCount": "141"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "655Xo1dYrVR-Ue5aJbI6OhF8ZTQ",
      "id": "6cW2IdY6Hhc",
      "snippet": {
        "publishedAt": "2021-02-06T15:30:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Master DTC Marketing | Learn Organic vs. Paid Acquisition | with Nik Sharma, CEO Sharma Brands",
        "description": "Everyone wants to start a DTC (direct-to-consumer) brand, but how do you ACTUALLY grow it from scratch? Meet Nik Sharma, who has done just that for some of customer's favorite DTC brands like JUDY (https://judy.co), Caraway (https://www.carawayhome.com), and Hint Water. The key here is content marketing strategy, influencer marketing, proper use of marketing analytics, and social media marketing. \n\nNik's Twitter for DTC Advice: https://twitter.com/mrsharma\nNik's 40 page deck on how to launch brands -- https://nik.co/subscribe\nNik's New Youtube Channel! — https://www.youtube.com/channel/UCaXLpESAYEX6pAHvYgYKYZw\n\n0:57 Meet Nik Sharma!\n2:12 Nik’s first job\n3:05 Working at Hint\n3:55 Focus on WHY\n5:13 How Nik Reduced CAC by 70%\n6:37 Origin of “DTC”\n7:14 Product first, then brand\n8:27 Personification of brands\n9:33 How to Start\n10:50 Solve a REAL problem\n12:49 Focus on product\n13:35 How to Validate an Idea\n14:34 Organic vs Paid Marketing\n15:25 How Haus focused on organic\n17:56 How JUDY used TV Marketing \n19:04 How to grow a product organically\n20:41 SEO and content is underrated\n21:19 Best marketing channels?\n22:36 Why to focus on 2 channels only\n23:32 Consistent branding is key\n25:11 Key DTC metrics\n25:55 Caraway - First purchase profitability\n27:23 What did Nik wish he knew earlier?\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/6cW2IdY6Hhc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/6cW2IdY6Hhc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/6cW2IdY6Hhc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/6cW2IdY6Hhc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/6cW2IdY6Hhc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "dtc",
          "nik sharma",
          "dtc brand",
          "branding",
          "marketing",
          "digital marketing",
          "d2c",
          "direct to consumer",
          "brand",
          "consumer brand",
          "facebook marketing",
          "influencer marketing",
          "content marketing",
          "social media marketing",
          "brands",
          "content marketing strategy",
          "internet marketing",
          "marketing strategy",
          "what is digital marketing",
          "instagram influencer marketing",
          "digital marketing tutorial for beginners",
          "instagram marketing",
          "marketing analytics"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Master DTC Marketing | Learn Organic vs. Paid Acquisition | with Nik Sharma, CEO Sharma Brands",
          "description": "Everyone wants to start a DTC (direct-to-consumer) brand, but how do you ACTUALLY grow it from scratch? Meet Nik Sharma, who has done just that for some of customer's favorite DTC brands like JUDY (https://judy.co), Caraway (https://www.carawayhome.com), and Hint Water. The key here is content marketing strategy, influencer marketing, proper use of marketing analytics, and social media marketing. \n\nNik's Twitter for DTC Advice: https://twitter.com/mrsharma\nNik's 40 page deck on how to launch brands -- https://nik.co/subscribe\nNik's New Youtube Channel! — https://www.youtube.com/channel/UCaXLpESAYEX6pAHvYgYKYZw\n\n0:57 Meet Nik Sharma!\n2:12 Nik’s first job\n3:05 Working at Hint\n3:55 Focus on WHY\n5:13 How Nik Reduced CAC by 70%\n6:37 Origin of “DTC”\n7:14 Product first, then brand\n8:27 Personification of brands\n9:33 How to Start\n10:50 Solve a REAL problem\n12:49 Focus on product\n13:35 How to Validate an Idea\n14:34 Organic vs Paid Marketing\n15:25 How Haus focused on organic\n17:56 How JUDY used TV Marketing \n19:04 How to grow a product organically\n20:41 SEO and content is underrated\n21:19 Best marketing channels?\n22:36 Why to focus on 2 channels only\n23:32 Consistent branding is key\n25:11 Key DTC metrics\n25:55 Caraway - First purchase profitability\n27:23 What did Nik wish he knew earlier?\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "22353",
        "likeCount": "987",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "125"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "nNcAjJCAd32-GI6qIgj20mM84Ww",
      "id": "o2GuOvO-Mns",
      "snippet": {
        "publishedAt": "2021-01-30T15:30:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Co-Founder Conflict & Why I Quit my Startup | How to Fix a Dying Company | Founder’s Journey: Ep.2",
        "description": "Co-founder disputes are the No. 1 early startup killer, but it doesn’t have to be that way. Learn how success masks conflict, how too little AND too much conflict create an issue, and how to ACTUALLY get help and SAVE your company.\n\nExecutive Coaching - https://torch.io/leadership-coaching/\n\n1:05 How Success Masks Co-Founder Conflict\n1:42 Too Little Conflict\n4:14 The Four Horsemen\n5:49 Too Much Conflict\n10:47 Solution Get Help!\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/o2GuOvO-Mns/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/o2GuOvO-Mns/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/o2GuOvO-Mns/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/o2GuOvO-Mns/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/o2GuOvO-Mns/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Co-Founder Conflict & Why I Quit my Startup | How to Fix a Dying Company | Founder’s Journey: Ep.2",
          "description": "Co-founder disputes are the No. 1 early startup killer, but it doesn’t have to be that way. Learn how success masks conflict, how too little AND too much conflict create an issue, and how to ACTUALLY get help and SAVE your company.\n\nExecutive Coaching - https://torch.io/leadership-coaching/\n\n1:05 How Success Masks Co-Founder Conflict\n1:42 Too Little Conflict\n4:14 The Four Horsemen\n5:49 Too Much Conflict\n10:47 Solution Get Help!\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "29392",
        "likeCount": "1423",
        "dislikeCount": "16",
        "favoriteCount": "0",
        "commentCount": "200"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "9ms0lMsaAwCKG4DzciUdzsXMF8k",
      "id": "y29ZlhuEIyc",
      "snippet": {
        "publishedAt": "2021-01-26T15:30:30Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "The Future of Software Development | Build Software with NO CODE | with Michael Skelly from Stacker",
        "description": "Not knowing how to code is no longer a limitation on building the perfect software for your company. Today I'm introducing you to Michael Skelly, co founder of Stacker. With Stacker, you can easily get software built with no technical knowledge required. It’s a game changer, and over 500 companies already utilize it to dramatically optimize their workflows and unlock new capabilities in software they never thought possible.\n\nAlso... Stacker is hiring!\nhttps://www.stackerhq.com/jobs\n\nLearn more:\nhttps://www.stackerhq.com\n\n1:11 What is Stacker?\n3:07 Who’s using it now? 500 teams!\n3:44 Stacker’s origin story: Scratch your own itch\n7:41 Solving problems at marketplace startups\n10:09 True no-code: non-technical teams don’t have to wait for engineering!\n10:55 Stacker was not built for prototyping!\n12:00 Stacker is multi user\n12:45 Example customer: Project N95\n14:36 Any process that a person is doing can be replaced\n16:41 Stacker enables Kaizen\n18:14 What does Michael wish he knew when he started in tech?\n20:34 Advice: Use the minimum amount of tech possible to solve a problem\n21:08 Stacker is hiring - stackerhq.com/jobs\n21:52 Use Stacker now for all of your projects!\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/y29ZlhuEIyc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/y29ZlhuEIyc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/y29ZlhuEIyc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/y29ZlhuEIyc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/y29ZlhuEIyc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The Future of Software Development | Build Software with NO CODE | with Michael Skelly from Stacker",
          "description": "Not knowing how to code is no longer a limitation on building the perfect software for your company. Today I'm introducing you to Michael Skelly, co founder of Stacker. With Stacker, you can easily get software built with no technical knowledge required. It’s a game changer, and over 500 companies already utilize it to dramatically optimize their workflows and unlock new capabilities in software they never thought possible.\n\nAlso... Stacker is hiring!\nhttps://www.stackerhq.com/jobs\n\nLearn more:\nhttps://www.stackerhq.com\n\n1:11 What is Stacker?\n3:07 Who’s using it now? 500 teams!\n3:44 Stacker’s origin story: Scratch your own itch\n7:41 Solving problems at marketplace startups\n10:09 True no-code: non-technical teams don’t have to wait for engineering!\n10:55 Stacker was not built for prototyping!\n12:00 Stacker is multi user\n12:45 Example customer: Project N95\n14:36 Any process that a person is doing can be replaced\n16:41 Stacker enables Kaizen\n18:14 What does Michael wish he knew when he started in tech?\n20:34 Advice: Use the minimum amount of tech possible to solve a problem\n21:08 Stacker is hiring - stackerhq.com/jobs\n21:52 Use Stacker now for all of your projects!\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "30290",
        "likeCount": "1053",
        "dislikeCount": "20",
        "favoriteCount": "0",
        "commentCount": "85"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "R2rllV0V8SM-GlDKf09okr1Tyg8",
      "id": "K3Ctc1UN0bQ",
      "snippet": {
        "publishedAt": "2021-01-20T15:30:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How To DESIGN EVERYTHING if you don't know ANYTHING | Design for Startups | Office Hours Ep. 1",
        "description": "Today we walk through 5 steps so that you can start designing anything, anywhere, regardless of whether or not you've ever done it before. The fact that you clicked on this video means you don't think of yourself as a designer. Stop that right now.\n\nYou can use interaction design, UX design, and visual design to reach product market fit. Minimalism is a big piece of it. Here's how—\n\n0:00 Intro\n0:43 Overview\n1:13 Step 1: De-label Yourself\n2:30 Step 2: Empathize With Your User\n6:24 Step 3: Illuminate The Path for Users\n8:21 Step 4: Minimize the Non-essential\n10:08 Step 5: Create Something Great'\n12:50 Recap! What You Need to Know\n\nWatch my Y Combinator Talk: \"Design for Startups\"\nhttps://www.youtube.com/watch?v=9urYWGx2uNk\n\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/K3Ctc1UN0bQ/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/K3Ctc1UN0bQ/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/K3Ctc1UN0bQ/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/K3Ctc1UN0bQ/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/K3Ctc1UN0bQ/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "design",
          "startup design",
          "y combinator",
          "design for startups",
          "user experience design",
          "ux design",
          "visual design",
          "how to do design",
          "design tutorial",
          "design for beginners",
          "ux for beginners",
          "visual design for beginners",
          "visual design for startups",
          "interaction design",
          "user experience",
          "how to design",
          "minimalism",
          "design minimalism",
          "adolf loos",
          "edward tufte",
          "chartjunk",
          "initialized capital",
          "startups",
          "product market fit",
          "garry tan initialized capital",
          "how to design products"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How To DESIGN EVERYTHING if you don't know ANYTHING | Design for Startups | Office Hours Ep. 1",
          "description": "Today we walk through 5 steps so that you can start designing anything, anywhere, regardless of whether or not you've ever done it before. The fact that you clicked on this video means you don't think of yourself as a designer. Stop that right now.\n\nYou can use interaction design, UX design, and visual design to reach product market fit. Minimalism is a big piece of it. Here's how—\n\n0:00 Intro\n0:43 Overview\n1:13 Step 1: De-label Yourself\n2:30 Step 2: Empathize With Your User\n6:24 Step 3: Illuminate The Path for Users\n8:21 Step 4: Minimize the Non-essential\n10:08 Step 5: Create Something Great'\n12:50 Recap! What You Need to Know\n\nWatch my Y Combinator Talk: \"Design for Startups\"\nhttps://www.youtube.com/watch?v=9urYWGx2uNk\n\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "51032",
        "likeCount": "3433",
        "dislikeCount": "21",
        "favoriteCount": "0",
        "commentCount": "216"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "vbie634D3tgqrCsYG-s6oiSY-ko",
      "id": "8MjWCkcLf1w",
      "snippet": {
        "publishedAt": "2021-01-18T15:30:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "The $1 TRILLION Market - How to File Crypto Taxes & Manage Holdings Chandan Lodha of CoinTracker",
        "description": "Managing your crypto holdings can be complicated with the space gaining popularity, but there are many entrepreneurs working to make it simpler.\n\nToday I want to introduce you Chandan Lodha, Co-Founder of Cointracker, which helps you track your entire crypto portfolio seamlessly. We cover Crypto Taxes, how it relates to the stock market, and even how each of us got our first Bitcoin. Companies like Cointracker are paving the way for the Bitcoin ecosystem to be bigger than we ever could have imagined, so I’m so excited to be a part of companies like Chandans.\n\nLinks:\n\nhttps://twitter.com/cglodha\nhttps://twitter.com/CoinTracker\n\nhttps://www.cointracker.io/a/initialized - 10% Off Cointracker\n\n0:00 Intro\n1:11 What is Cointracker?\n1:57 An always up to date crypto dashboard\n2:55 The best crypto tax solution\n3:21 One-click setup from Coinbase\n4:14 10,000 bugs to fix for product-market fit\n5:35 Decentralized tracking is 100x harder\n6:24 Tax loss harvesting is free money\n8:10 How he got into Crypto\n9:25 Getting into crypto: Tinkering helps!\n11:59 Monetary policy rules everything around me\n13:30 Crypto is in the 1st inning: It’s just getting started\n14:46 Crypto can still 100x or more\n15:16 Advice for crypto newbies\n17:14 How to decide how much to buy, and when?\n18:02 Warning: If you sell, set aside for taxes\n19:26 Cointracker is designed to help you, the user\n21:32 Supporting hundreds of exchanges and thousands of blockchains\n22:22 What did Chandan wish he knew at 18 or 22?\n23:00 Just do a startup! Get into it.\n23:55 Going from zero to one\n24:58 Lessons from the idea search\n25:35 The 2 things that matter when building from 0 to 1\n27:11 Surviving the 2017 crypto rollercoaster\n28:45 The silver lining from the crypto winter\n30:06 Why price doesn’t matter\n30:45 Institutional demand driving 2021’s bull run\n33:31 Sign up for Cointracker\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/8MjWCkcLf1w/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/8MjWCkcLf1w/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/8MjWCkcLf1w/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/8MjWCkcLf1w/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/8MjWCkcLf1w/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "cryptocurrency",
          "bitcoin",
          "ethereum",
          "coinbase",
          "intro to cryptocurrency",
          "crypto",
          "buying cryptocurrency",
          "cointracker",
          "cryptocurrency tax return",
          "cryptocurrency trading",
          "chandan lodha",
          "garry tan",
          "initialized capital",
          "cryptocurrency prices",
          "crypto news",
          "btc",
          "crypto taxes",
          "bitcoin taxes",
          "bitcoin tax",
          "cryptocurrency tax",
          "bitcoin analysis today",
          "how to do crypto taxes"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The $1 TRILLION Market - How to File Crypto Taxes & Manage Holdings Chandan Lodha of CoinTracker",
          "description": "Managing your crypto holdings can be complicated with the space gaining popularity, but there are many entrepreneurs working to make it simpler.\n\nToday I want to introduce you Chandan Lodha, Co-Founder of Cointracker, which helps you track your entire crypto portfolio seamlessly. We cover Crypto Taxes, how it relates to the stock market, and even how each of us got our first Bitcoin. Companies like Cointracker are paving the way for the Bitcoin ecosystem to be bigger than we ever could have imagined, so I’m so excited to be a part of companies like Chandans.\n\nLinks:\n\nhttps://twitter.com/cglodha\nhttps://twitter.com/CoinTracker\n\nhttps://www.cointracker.io/a/initialized - 10% Off Cointracker\n\n0:00 Intro\n1:11 What is Cointracker?\n1:57 An always up to date crypto dashboard\n2:55 The best crypto tax solution\n3:21 One-click setup from Coinbase\n4:14 10,000 bugs to fix for product-market fit\n5:35 Decentralized tracking is 100x harder\n6:24 Tax loss harvesting is free money\n8:10 How he got into Crypto\n9:25 Getting into crypto: Tinkering helps!\n11:59 Monetary policy rules everything around me\n13:30 Crypto is in the 1st inning: It’s just getting started\n14:46 Crypto can still 100x or more\n15:16 Advice for crypto newbies\n17:14 How to decide how much to buy, and when?\n18:02 Warning: If you sell, set aside for taxes\n19:26 Cointracker is designed to help you, the user\n21:32 Supporting hundreds of exchanges and thousands of blockchains\n22:22 What did Chandan wish he knew at 18 or 22?\n23:00 Just do a startup! Get into it.\n23:55 Going from zero to one\n24:58 Lessons from the idea search\n25:35 The 2 things that matter when building from 0 to 1\n27:11 Surviving the 2017 crypto rollercoaster\n28:45 The silver lining from the crypto winter\n30:06 Why price doesn’t matter\n30:45 Institutional demand driving 2021’s bull run\n33:31 Sign up for Cointracker\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "7267",
        "likeCount": "256",
        "dislikeCount": "3",
        "favoriteCount": "0",
        "commentCount": "55"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "-WA1E4kQJlNSJmk6fv161lIulUk",
      "id": "cRDOj4EZ9qo",
      "snippet": {
        "publishedAt": "2021-01-12T15:30:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "The Elon Musk of Baseball: How Startups Hit Grand Slams | Founder’s Journey: Ep. 1",
        "description": "Startups are not like baseball at all. They are snowballs. Just like a snowball, the more you pack on, the easier it is for them to get big. And once you get going with the product-market fit, they have so much momentum that they take on a life of their own.\n\n0:00 Intro: The Legend\n1:27 4 Topics Covered\n1:54 Part 1: Point Well\n2:20 Part 2: Put Points on the Board\n6:46 Part 3: Make The Myth\n8:29 Part 4: Win the Series\n10:09 The Takeaways for YOU\n11:26 Outro: Why I'm doing this, thanks and subscribe! :)\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/cRDOj4EZ9qo/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/cRDOj4EZ9qo/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/cRDOj4EZ9qo/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/cRDOj4EZ9qo/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/cRDOj4EZ9qo/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "babe ruth",
          "elon musk",
          "initialized capital",
          "called shot",
          "product market fit",
          "startups",
          "venture capital",
          "startup advice",
          "billion dollar startups",
          "entrepreneur motivation",
          "founder advice"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The Elon Musk of Baseball: How Startups Hit Grand Slams | Founder’s Journey: Ep. 1",
          "description": "Startups are not like baseball at all. They are snowballs. Just like a snowball, the more you pack on, the easier it is for them to get big. And once you get going with the product-market fit, they have so much momentum that they take on a life of their own.\n\n0:00 Intro: The Legend\n1:27 4 Topics Covered\n1:54 Part 1: Point Well\n2:20 Part 2: Put Points on the Board\n6:46 Part 3: Make The Myth\n8:29 Part 4: Win the Series\n10:09 The Takeaways for YOU\n11:26 Outro: Why I'm doing this, thanks and subscribe! :)\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "6085",
        "likeCount": "532",
        "dislikeCount": "2",
        "favoriteCount": "0",
        "commentCount": "104"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "k0WXq409qO4Sn9dExEVyLldBrDw",
      "id": "kJ4o0ONH7W8",
      "snippet": {
        "publishedAt": "2021-01-09T20:00:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Masterclass: How to hire engineers remotely (10X more remote work candidates)",
        "description": "Today I wanted to welcome back Ammon Bartram from Triplebyte. He’s here to talk about all the things you need to know to be able to properly run your software engineering interviews, but completely remotely. The good news about remote hiring? The number of people who could work at your company just went up by 10X to 100X.\n\n00:00 Intro\n01:30 How to hire remotely\n02:30 Interview process\n04:37 Brainteasers are noise\n07:17 Low noise questions\n07:37 Triplebyte Screen: Rank and filter applicants quickly\n09:33 10X larger candidate pool\n10:39 No need to go to Harvard or Stanford\n12:01 Remote interviewing great for diversity\n13:34 How to close candidates remotely\n16:06 Soft skills assessment remotely\n18:03 Now is the best hiring market in 10 years\n\nSign up for Triplebyte Screen now at https://triplebyte.com/screen\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/kJ4o0ONH7W8/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/kJ4o0ONH7W8/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/kJ4o0ONH7W8/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/kJ4o0ONH7W8/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/kJ4o0ONH7W8/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "remote interviews",
          "remote software engineering",
          "remote software",
          "remote engineering",
          "remote software engineering jobs",
          "remote jobs software",
          "remote job interviewing",
          "interviewing questions",
          "software engineering",
          "hiring",
          "software engineer hiring",
          "triplebyte",
          "garry tan",
          "ammon bartram",
          "initialized capital",
          "remote software developer",
          "remote software engineer",
          "remote software engineer jobs",
          "remote work"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: How to hire engineers remotely (10X more remote work candidates)",
          "description": "Today I wanted to welcome back Ammon Bartram from Triplebyte. He’s here to talk about all the things you need to know to be able to properly run your software engineering interviews, but completely remotely. The good news about remote hiring? The number of people who could work at your company just went up by 10X to 100X.\n\n00:00 Intro\n01:30 How to hire remotely\n02:30 Interview process\n04:37 Brainteasers are noise\n07:17 Low noise questions\n07:37 Triplebyte Screen: Rank and filter applicants quickly\n09:33 10X larger candidate pool\n10:39 No need to go to Harvard or Stanford\n12:01 Remote interviewing great for diversity\n13:34 How to close candidates remotely\n16:06 Soft skills assessment remotely\n18:03 Now is the best hiring market in 10 years\n\nSign up for Triplebyte Screen now at https://triplebyte.com/screen\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "6963",
        "likeCount": "266",
        "dislikeCount": "3",
        "favoriteCount": "0",
        "commentCount": "32"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "7qFm8DGzcueHxiKuSDn3qaCyj-0",
      "id": "dmv-nooN43U",
      "snippet": {
        "publishedAt": "2020-12-29T15:00:27Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Masterclass: Product Market Fit 10X Faster with Amy Jo Kim",
        "description": "Today I sat down with Amy Jo Kim, creator of @Game Thinking TV and early designer of world-class products including Rock Band, The Sims, Ultima Online, eBay and Netflix. We talk about her journey into tech, game dev, and how founders have a lot to learn from game thinking. \n\nAlong the way, we learn a number of secrets on how to get to product-market fit 10x faster. \n\n00:00 Intro\n01:06 Meet Amy Jo Kim\n01:47 Engineer, then designer, then the product\n02:48 Finding her tribe\n04:30 Game Thinking: PMF 10X faster\n06:22 Core loops driving retention\n08:31 Lessons from Ultima Online\n11:48 Test with core super fans first\n13:38 Pick your users\n15:26 Slack's customer journey\n19:04 Storyboarding skips you ahead\n23:38 The most important story: Your user's\n26:51 Overcoming founder ego\n30:15 What founders can learn from game design\n32:03 Amy Jo Kim's lessons for those starting out\n37:20 Learning more about Game Thinking\n\n5 Game Design Tips from Will Wright, Sims Designer\nhttps://youtu.be/scS3f_YSYO0\n\nStill Logged In: What AR & VR can learn from MMOs (Ralph Koster)\nhttps://youtu.be/kgw8RLHv1j4\n\nFollow and subscribe to Amy Jo Kim\nhttps://www.youtube.com/c/GameThinkingTV\nhttps://twitter.com/amyjokim\n\nBuy Amy Jo Kim's book Game Thinking https://amzn.to/37VA5Uj \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dmv-nooN43U/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dmv-nooN43U/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dmv-nooN43U/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dmv-nooN43U/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dmv-nooN43U/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "amy jo kim",
          "game thinking",
          "product market fit",
          "product management",
          "journey",
          "ultima online",
          "game design",
          "will wright",
          "design for founders",
          "game design for founders",
          "game design theory",
          "garry tan",
          "initialized capital",
          "product management training",
          "what is product management",
          "video game design",
          "will wright game design",
          "product management basics",
          "product",
          "product school",
          "amy jo kim game thinking",
          "game thinking design product",
          "game dev",
          "advice for entrepreneurs",
          "gamification",
          "design"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: Product Market Fit 10X Faster with Amy Jo Kim",
          "description": "Today I sat down with Amy Jo Kim, creator of @Game Thinking TV and early designer of world-class products including Rock Band, The Sims, Ultima Online, eBay and Netflix. We talk about her journey into tech, game dev, and how founders have a lot to learn from game thinking. \n\nAlong the way, we learn a number of secrets on how to get to product-market fit 10x faster. \n\n00:00 Intro\n01:06 Meet Amy Jo Kim\n01:47 Engineer, then designer, then the product\n02:48 Finding her tribe\n04:30 Game Thinking: PMF 10X faster\n06:22 Core loops driving retention\n08:31 Lessons from Ultima Online\n11:48 Test with core super fans first\n13:38 Pick your users\n15:26 Slack's customer journey\n19:04 Storyboarding skips you ahead\n23:38 The most important story: Your user's\n26:51 Overcoming founder ego\n30:15 What founders can learn from game design\n32:03 Amy Jo Kim's lessons for those starting out\n37:20 Learning more about Game Thinking\n\n5 Game Design Tips from Will Wright, Sims Designer\nhttps://youtu.be/scS3f_YSYO0\n\nStill Logged In: What AR & VR can learn from MMOs (Ralph Koster)\nhttps://youtu.be/kgw8RLHv1j4\n\nFollow and subscribe to Amy Jo Kim\nhttps://www.youtube.com/c/GameThinkingTV\nhttps://twitter.com/amyjokim\n\nBuy Amy Jo Kim's book Game Thinking https://amzn.to/37VA5Uj \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "30048",
        "likeCount": "1243",
        "dislikeCount": "18",
        "favoriteCount": "0",
        "commentCount": "133"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "7MiqPlWFHjXnUwvXrPhf_y1O2VA",
      "id": "9NYrsE_yW64",
      "snippet": {
        "publishedAt": "2020-12-22T17:45:31Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "The founder mindset you need - (Bill Gates’s lesson on time frame)",
        "description": "If you want to make something truly big, you'll realize you have to be a short term pessimist, and long term optimist. This is the opposite of what most would-be founders actually have. And that, I think, is why most startups fail. \n\nMost founders pursue ideas that are very short term, and they're wildly optimistic about these short term goals. And they ignore obvious things in front of them that should cause them to change course. But as Bill Gates says, they wildly overestimate what can be done in one year, and wildly underestimate what can be done in 10. \n\n00:00 Intro\n00:48 Short term pessimism\n02:43 Short term optimism\n05:30 Long term optimism\n\nWatch my video on the Stockdale Paradox if you haven't seen it yet: https://www.youtube.com/watch?v=dLicgt04hHY&ab_channel=GarryTan\n\nMusic - \"Always Forward\" - Instrumental by Homage - https://youtu.be/0vKyeJboYxE\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/9NYrsE_yW64/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/9NYrsE_yW64/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/9NYrsE_yW64/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/9NYrsE_yW64/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/9NYrsE_yW64/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "initialized capital",
          "founder advice",
          "pessimism",
          "optimism",
          "startup optimism",
          "startup pessimism",
          "startup success",
          "venture capital",
          "startups",
          "startup advice",
          "who is garry tan",
          "startup school",
          "silicon valley",
          "entrepreneur",
          "lean startup",
          "entrepreneurship"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The founder mindset you need - (Bill Gates’s lesson on time frame)",
          "description": "If you want to make something truly big, you'll realize you have to be a short term pessimist, and long term optimist. This is the opposite of what most would-be founders actually have. And that, I think, is why most startups fail. \n\nMost founders pursue ideas that are very short term, and they're wildly optimistic about these short term goals. And they ignore obvious things in front of them that should cause them to change course. But as Bill Gates says, they wildly overestimate what can be done in one year, and wildly underestimate what can be done in 10. \n\n00:00 Intro\n00:48 Short term pessimism\n02:43 Short term optimism\n05:30 Long term optimism\n\nWatch my video on the Stockdale Paradox if you haven't seen it yet: https://www.youtube.com/watch?v=dLicgt04hHY&ab_channel=GarryTan\n\nMusic - \"Always Forward\" - Instrumental by Homage - https://youtu.be/0vKyeJboYxE\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "42541",
        "likeCount": "2594",
        "dislikeCount": "16",
        "favoriteCount": "0",
        "commentCount": "164"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "tu4g8loDiFxRrtjN8Q_epzyZ-GA",
      "id": "vhTicEJTstA",
      "snippet": {
        "publishedAt": "2020-12-21T18:30:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Trillions of dollars in real estate— How do we fund the future we want to live in?",
        "description": "Today’s a big day for Initialized. Opendoor, a company we invested in at the earliest stage, begins trading on Nasdaq. Startups like Opendoor and Airbnb are transforming one of the world’s biggest markets. \n\nLet’s go hang out with my colleague and Initialized Capital partner Kim-Mai Cutler to talk about what’s the future of housing and real estate? How can tech, finance, and cities work together to bring about a society we want to live in? She’s led our real-estate related investments in Landed, Abodu, and Culdesac. \n\nLearn more about Abodu with our interview with the founders here:\nhttps://www.youtube.com/watch?v=P-Igj27Rh_k\n\n00:00 Intro\n00:57 Opendoor's key insight\n02:21 Making housing more accessible\n06:14 Millions of lots = trillions of dollars\n10:20 SF Bay Area is our home\n12:43 Funding the future we want to live in\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/vhTicEJTstA/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/vhTicEJTstA/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/vhTicEJTstA/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/vhTicEJTstA/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/vhTicEJTstA/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "real estate",
          "opendoor",
          "startup",
          "kim-mai cutler",
          "garry tan",
          "initialized capital",
          "initialized",
          "culdesac",
          "abodu",
          "adu laws",
          "california housing",
          "adu laws 2020",
          "housing",
          "startups",
          "venture capital",
          "product hunt cto",
          "silicon valley",
          "entrepreneurship"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Trillions of dollars in real estate— How do we fund the future we want to live in?",
          "description": "Today’s a big day for Initialized. Opendoor, a company we invested in at the earliest stage, begins trading on Nasdaq. Startups like Opendoor and Airbnb are transforming one of the world’s biggest markets. \n\nLet’s go hang out with my colleague and Initialized Capital partner Kim-Mai Cutler to talk about what’s the future of housing and real estate? How can tech, finance, and cities work together to bring about a society we want to live in? She’s led our real-estate related investments in Landed, Abodu, and Culdesac. \n\nLearn more about Abodu with our interview with the founders here:\nhttps://www.youtube.com/watch?v=P-Igj27Rh_k\n\n00:00 Intro\n00:57 Opendoor's key insight\n02:21 Making housing more accessible\n06:14 Millions of lots = trillions of dollars\n10:20 SF Bay Area is our home\n12:43 Funding the future we want to live in\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "6352",
        "likeCount": "247",
        "dislikeCount": "9",
        "favoriteCount": "0",
        "commentCount": "44"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "8Xud_CE4ZJzvTPG96LZ7FuQsKhw",
      "id": "kt9ScQvTqcU",
      "snippet": {
        "publishedAt": "2020-12-18T15:00:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Silicon Valley is still for future billionaires (with Elad Gil, legendary investor)",
        "description": "Re-posting a panel talk we did with Elad Gil, legendary investor in Stripe, Instacart and 14 other multi-billion-dollar startups, and Bloomberg Reporter Katie Roof at Web Summit last week. If you're keeping score I've only funded 10!\n\nThe water is fine. Silicon Valley is doing great. What's going on with all these crazy rounds? We talk through all of those things. \n\n00:00 Intro\n00:28 What does remote work mean for tech?\n05:16 The market for tech is now 10x to 20x bigger than web 1.0\n05:44 Global unicorns: 50% were us, 25% were in Silicon Valley\n07:08 Elad on global growth tech investing\n08:25 Is there too much Venture Capital now?\n09:15 No software? Mr. Market says maybe no future cash flow\n10:17 Anti-Amazon strategy\n11:19 No value investing in Unicorn hunting\n12:26 What's actually overvalued?\n14:11 How to find future sectors\n15:08 Don't ask a VC what's hot - that's 9 to 12 mo too late\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan\n\nFollow Elad Gil on Twitter - @eladgil https://twitter.com/eladgil\n\nFollow me on Twitter— @garrytan https://twitter.com/garrytan\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/kt9ScQvTqcU/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/kt9ScQvTqcU/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/kt9ScQvTqcU/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/kt9ScQvTqcU/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/kt9ScQvTqcU/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "silicon valley",
          "entrepreneurship",
          "ai",
          "elad gil",
          "garry tan",
          "initialized capital",
          "ipo",
          "startup unicorns",
          "unicorns",
          "leaving silicon valley",
          "startups",
          "venture capital"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Silicon Valley is still for future billionaires (with Elad Gil, legendary investor)",
          "description": "Re-posting a panel talk we did with Elad Gil, legendary investor in Stripe, Instacart and 14 other multi-billion-dollar startups, and Bloomberg Reporter Katie Roof at Web Summit last week. If you're keeping score I've only funded 10!\n\nThe water is fine. Silicon Valley is doing great. What's going on with all these crazy rounds? We talk through all of those things. \n\n00:00 Intro\n00:28 What does remote work mean for tech?\n05:16 The market for tech is now 10x to 20x bigger than web 1.0\n05:44 Global unicorns: 50% were us, 25% were in Silicon Valley\n07:08 Elad on global growth tech investing\n08:25 Is there too much Venture Capital now?\n09:15 No software? Mr. Market says maybe no future cash flow\n10:17 Anti-Amazon strategy\n11:19 No value investing in Unicorn hunting\n12:26 What's actually overvalued?\n14:11 How to find future sectors\n15:08 Don't ask a VC what's hot - that's 9 to 12 mo too late\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan\n\nFollow Elad Gil on Twitter - @eladgil https://twitter.com/eladgil\n\nFollow me on Twitter— @garrytan https://twitter.com/garrytan\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "20337",
        "likeCount": "587",
        "dislikeCount": "9",
        "favoriteCount": "0",
        "commentCount": "76"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "Dravg_rX6tPNltvM-DqSMC-nrDg",
      "id": "Mb5Fk1GXuEM",
      "snippet": {
        "publishedAt": "2020-12-15T15:00:12Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How to build the next great startup with remote work, with Andreas Klinger (fmr CTO of Product Hunt)",
        "description": "Andreas Klinger was CTO of Product Hunt and now runs Remote First Capital. He's a great engineer turned investor and we just sat down in May of 2020 for a hang session. Posting it now in December, the things we talked about have become only more relevant. Hear lessons about building a remote team, practical guidelines on how to compensate and hire these teams, and also what the future of San Francisco might be as a state of mind.\n\n00:00 Intro\n00:39 Meet Andreas Klinger\n01:42 Remote engineer compensation\n03:18 Regional differences (how to handle time zones)\n04:33 Andreas's recommendations on how to build a remote team\n05:52 Remote work is now normal\n11:08 What is Andreas investing in?\n12:29 Macro effects of remote work are just beginning\n16:21 What does Andreas wish he knew when he was just entering tech?\n17:06 Sell a product, not your hours\n21:53 Write down a personal vision for yourself\n24:04 How to reach out to Andreas\n\nYou should follow Andreas on Twitter here — https://twitter.com/andreasklinger \n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/Mb5Fk1GXuEM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/Mb5Fk1GXuEM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/Mb5Fk1GXuEM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/Mb5Fk1GXuEM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/Mb5Fk1GXuEM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "remote work",
          "andreas klinger",
          "product hunt",
          "product hunt cto",
          "remote work capital",
          "garry tan",
          "initialized capital",
          "remote",
          "startups",
          "remote teams",
          "startup",
          "work from home",
          "venture capital"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to build the next great startup with remote work, with Andreas Klinger (fmr CTO of Product Hunt)",
          "description": "Andreas Klinger was CTO of Product Hunt and now runs Remote First Capital. He's a great engineer turned investor and we just sat down in May of 2020 for a hang session. Posting it now in December, the things we talked about have become only more relevant. Hear lessons about building a remote team, practical guidelines on how to compensate and hire these teams, and also what the future of San Francisco might be as a state of mind.\n\n00:00 Intro\n00:39 Meet Andreas Klinger\n01:42 Remote engineer compensation\n03:18 Regional differences (how to handle time zones)\n04:33 Andreas's recommendations on how to build a remote team\n05:52 Remote work is now normal\n11:08 What is Andreas investing in?\n12:29 Macro effects of remote work are just beginning\n16:21 What does Andreas wish he knew when he was just entering tech?\n17:06 Sell a product, not your hours\n21:53 Write down a personal vision for yourself\n24:04 How to reach out to Andreas\n\nYou should follow Andreas on Twitter here — https://twitter.com/andreasklinger \n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "17541",
        "likeCount": "731",
        "dislikeCount": "3",
        "favoriteCount": "0",
        "commentCount": "78"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "p3GQefztDKOiO1pct1GYzZ0vAkA",
      "id": "viNZK9P40kY",
      "snippet": {
        "publishedAt": "2020-12-12T17:33:40Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How Airbnb & DoorDash Succeeded: First Principles (3 steps for billion dollar startups)",
        "description": "What society imposes on us is the wrong kind of thinking: one that is based on analogy and credentialism instead of first principles thinking. But how do we break free? Here are 3 ways to do it. \n\nThe third one is the most surprising! What does serenity have to do with building a billion dollar startup? In DoorDash's case: EVERYTHING. \n\nIf you like lifelong learning, try one of our startups we funded called Knowable. They're the best app for audio courses. https://knowable.fyi, 25% off discount code GARRY\n\nI'm donating all proceeds for 2020 from my YouTube channel (ad revenue and affiliate links) to https://code2040.org and I hope you'll consider joining me in pushing for more racial equality in tech.\n\nMusic: \"Azygous\" Instrumental by Homage https://youtu.be/bNesMW_m6lg\n\n—\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this\n\nHOW TO TALK TO ME\n\nI can answer your startup questions on my comments section, or you can DM me on Instagram https://Instagram.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/viNZK9P40kY/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/viNZK9P40kY/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/viNZK9P40kY/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/viNZK9P40kY/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/viNZK9P40kY/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "airbnb",
          "doordash",
          "airbnb stock",
          "abnb",
          "dash",
          "tony xu",
          "brian chesky",
          "y combinator",
          "garry tan",
          "initialized capital",
          "eric ries",
          "five whys",
          "first principles thinking",
          "first principles",
          "billion dollar startup",
          "lean startup",
          "airbnb ipo",
          "airbnb business",
          "airbnb stock analysis",
          "airbnb tips",
          "airbnb ipo analysis",
          "should i buy airbnb stock",
          "doordash ipo"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How Airbnb & DoorDash Succeeded: First Principles (3 steps for billion dollar startups)",
          "description": "What society imposes on us is the wrong kind of thinking: one that is based on analogy and credentialism instead of first principles thinking. But how do we break free? Here are 3 ways to do it. \n\nThe third one is the most surprising! What does serenity have to do with building a billion dollar startup? In DoorDash's case: EVERYTHING. \n\nIf you like lifelong learning, try one of our startups we funded called Knowable. They're the best app for audio courses. https://knowable.fyi, 25% off discount code GARRY\n\nI'm donating all proceeds for 2020 from my YouTube channel (ad revenue and affiliate links) to https://code2040.org and I hope you'll consider joining me in pushing for more racial equality in tech.\n\nMusic: \"Azygous\" Instrumental by Homage https://youtu.be/bNesMW_m6lg\n\n—\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this\n\nHOW TO TALK TO ME\n\nI can answer your startup questions on my comments section, or you can DM me on Instagram https://Instagram.com/garrytan"
        }
      },
      "statistics": {
        "viewCount": "50751",
        "likeCount": "3577",
        "dislikeCount": "44",
        "favoriteCount": "0",
        "commentCount": "281"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "jeHL1cKEvCIwNZK45nYalHchdYo",
      "id": "decDIkDbS3Q",
      "snippet": {
        "publishedAt": "2020-12-09T15:20:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "To become a billionaire, help a billion people | Career Karma helps you find a coding bootcamp",
        "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nToday I'm proud to announce my VC firm Initialized Capital led a $10 million Series A in Career Karma. Their community is helping people find the right coding bootcamp or university for their needs, while also helping them meet other people with similar backgrounds who have done what they want to do: get six figure jobs in tech and help their families, friends, and neighborhoods. Ruben Harris and his cofounders broke into tech themselves, and now they are helping a new generation of people get into tech.\n\nY Combinator showed that it takes a village to achieve success: watching Brian Chesky speak at weekly dinners made it clear you as a founder could succeed too. The YC Community has created $150 billion worth of value and that's thousands of people. In the same way, Career Karma will work with millions of people who need job retraining— building a community of people who will help each other over their entire careers. They've built a fast growing profitable startup on this idea: Colleges spend $10 billion per year on just recruiting and marketing. Job retraining is a key piece of helping people bridge the digital divide while also reducing income inequality. \n\nSign up and try Career Karma now: https://smarturl.it/garry-tan-yt-1\n\n00:00 Intro\n01:54 What is Career Karma?\n03:38 How Ruben broke into startups\n04:26 Four year colleges are not the only way to learn\n05:13 Software eating the world means tech job training is even more important\n06:21 55M Americans are unemployed\n07:01 People want high paying jobs but need networks to help them\n09:46 How Career Karma reached product market fit\n10:03 $1 trillion spent on workforce training per year\n10:29 Colleges spend $10 billion per year on enrollment/recruiting\n11:29 There is no one-size-fits-all education— which is why Career Karma is needed\n16:21 Taking YC's community and bringing it to everyone\n17:22 How Ruben and his cofounders broke into tech: Founder-market fit\n22:19 Success story: Kesha Lake got a software engineering job at Stitch Fix, bought a house, now helping 20 friends through squads\n24:45 Career Karma is hiring\n25:58 Ruben's lessons for people starting out at 20 years old\n28:16 If you want to be a billionaire, help a billion people\n\nCareer Karma is Hiring — https://careerkarma.com/jobs\n\nIf you're interested in finding the right school and community for you to help you get into tech and learn to code, sign up at https://careerkarma.com\n\nYou should follow Ruben on Twitter here https://twitter.com/rubenharris\n\nSubscribe to Career Karma's YouTube channel: https://www.youtube.com/channel/UCkCwv67-HTCzIhxLG8aHOQQ",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/decDIkDbS3Q/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/decDIkDbS3Q/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/decDIkDbS3Q/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/decDIkDbS3Q/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/decDIkDbS3Q/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "career karma",
          "job retraining",
          "y combinator",
          "ruben harris",
          "how to get a job in tech",
          "learn to code",
          "coding bootcamp",
          "which isa is best",
          "which coding bootcamp is the best",
          "yc",
          "yc community",
          "career karma community",
          "coding",
          "career karma review",
          "how good is career karma",
          "how to get jobs with career karma",
          "is it worth going to coding bootcamp",
          "is it worth",
          "should you go",
          "learn how to code",
          "coding bootcamp 2021"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "To become a billionaire, help a billion people | Career Karma helps you find a coding bootcamp",
          "description": "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nToday I'm proud to announce my VC firm Initialized Capital led a $10 million Series A in Career Karma. Their community is helping people find the right coding bootcamp or university for their needs, while also helping them meet other people with similar backgrounds who have done what they want to do: get six figure jobs in tech and help their families, friends, and neighborhoods. Ruben Harris and his cofounders broke into tech themselves, and now they are helping a new generation of people get into tech.\n\nY Combinator showed that it takes a village to achieve success: watching Brian Chesky speak at weekly dinners made it clear you as a founder could succeed too. The YC Community has created $150 billion worth of value and that's thousands of people. In the same way, Career Karma will work with millions of people who need job retraining— building a community of people who will help each other over their entire careers. They've built a fast growing profitable startup on this idea: Colleges spend $10 billion per year on just recruiting and marketing. Job retraining is a key piece of helping people bridge the digital divide while also reducing income inequality. \n\nSign up and try Career Karma now: https://smarturl.it/garry-tan-yt-1\n\n00:00 Intro\n01:54 What is Career Karma?\n03:38 How Ruben broke into startups\n04:26 Four year colleges are not the only way to learn\n05:13 Software eating the world means tech job training is even more important\n06:21 55M Americans are unemployed\n07:01 People want high paying jobs but need networks to help them\n09:46 How Career Karma reached product market fit\n10:03 $1 trillion spent on workforce training per year\n10:29 Colleges spend $10 billion per year on enrollment/recruiting\n11:29 There is no one-size-fits-all education— which is why Career Karma is needed\n16:21 Taking YC's community and bringing it to everyone\n17:22 How Ruben and his cofounders broke into tech: Founder-market fit\n22:19 Success story: Kesha Lake got a software engineering job at Stitch Fix, bought a house, now helping 20 friends through squads\n24:45 Career Karma is hiring\n25:58 Ruben's lessons for people starting out at 20 years old\n28:16 If you want to be a billionaire, help a billion people\n\nCareer Karma is Hiring — https://careerkarma.com/jobs\n\nIf you're interested in finding the right school and community for you to help you get into tech and learn to code, sign up at https://careerkarma.com\n\nYou should follow Ruben on Twitter here https://twitter.com/rubenharris\n\nSubscribe to Career Karma's YouTube channel: https://www.youtube.com/channel/UCkCwv67-HTCzIhxLG8aHOQQ"
        }
      },
      "statistics": {
        "viewCount": "16356",
        "likeCount": "692",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "63"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "mQ0od8BNqdAf3yr4Q8P0JyxQdRE",
      "id": "dBULOo03cys",
      "snippet": {
        "publishedAt": "2020-12-07T15:00:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "This 26 year old investor funded Nadeshot and 100 Thieves: A conversation with Blake Robbins",
        "description": "Blake Robbins is a venture capitalist is a case study in being helpful up front. He's a great example of what great things can happen when you combine what you are genuinely interested in with an ability to show value even through a cold email. It was a pleasure to sit down with him to talk about his path into venture, his experience with helping build top e-sports startup 100 Thieves, and how YouTube creators and startups can work together to build the next generation of consumer startup.\n\n00:00 Intro\n01:51 Getting started cold-emailing every startup on TechCrunch\n02:36 Learning about Venture Capital for the first time\n03:14 Pick your internships well\n04:29 Cold-email to Ludlow Ventures to get his first VC job\n05:55 Cold emails can work but you have to be persistent and helpful\n08:25 The 100 Thieves creation story\n12:01 Cold DM made 100 Thieves happen\n14:04 Key early hire at 100 Thieves: Jackson Dahl\n15:50 Nadeshot is a creator who built something bigger than a personal brand\n16:45 How Blake met Mr. Beast\n18:22 Influencer strategy is key for new consumer startups\n20:08 Casey Neistat & Beme\n21:13 How startups can work with creators\n22:37 Rule of thumb for creators thinking about equity\n24:14 How creators can work with startups\n28:16 Creators as cofounders of startups\n29:25 How do you align creators with cofounders?\n33:42 What Blake wishes he knew when he first started\n\nYou should follow Blake on Twitter here: https://twitter.com/blakeir\n\nBONUS! Blake just started a YouTube channel and you should subscribe to him here https://www.youtube.com/channel/UCjKsEp4qBJFCKWWnh6zfAxw",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dBULOo03cys/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dBULOo03cys/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dBULOo03cys/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dBULOo03cys/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dBULOo03cys/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "blake robbins",
          "ludlow ventures",
          "ludlow",
          "detroit startup",
          "100 thieves",
          "esports",
          "how to get a job in vc",
          "how to get a job at a venture capital firm",
          "youtube billionaire",
          "influencer startup",
          "nadeshot",
          "jackson dahl",
          "venture capital",
          "carpool.vc"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "This 26 year old investor funded Nadeshot and 100 Thieves: A conversation with Blake Robbins",
          "description": "Blake Robbins is a venture capitalist is a case study in being helpful up front. He's a great example of what great things can happen when you combine what you are genuinely interested in with an ability to show value even through a cold email. It was a pleasure to sit down with him to talk about his path into venture, his experience with helping build top e-sports startup 100 Thieves, and how YouTube creators and startups can work together to build the next generation of consumer startup.\n\n00:00 Intro\n01:51 Getting started cold-emailing every startup on TechCrunch\n02:36 Learning about Venture Capital for the first time\n03:14 Pick your internships well\n04:29 Cold-email to Ludlow Ventures to get his first VC job\n05:55 Cold emails can work but you have to be persistent and helpful\n08:25 The 100 Thieves creation story\n12:01 Cold DM made 100 Thieves happen\n14:04 Key early hire at 100 Thieves: Jackson Dahl\n15:50 Nadeshot is a creator who built something bigger than a personal brand\n16:45 How Blake met Mr. Beast\n18:22 Influencer strategy is key for new consumer startups\n20:08 Casey Neistat & Beme\n21:13 How startups can work with creators\n22:37 Rule of thumb for creators thinking about equity\n24:14 How creators can work with startups\n28:16 Creators as cofounders of startups\n29:25 How do you align creators with cofounders?\n33:42 What Blake wishes he knew when he first started\n\nYou should follow Blake on Twitter here: https://twitter.com/blakeir\n\nBONUS! Blake just started a YouTube channel and you should subscribe to him here https://www.youtube.com/channel/UCjKsEp4qBJFCKWWnh6zfAxw"
        }
      },
      "statistics": {
        "viewCount": "18124",
        "likeCount": "781",
        "dislikeCount": "13",
        "favoriteCount": "0",
        "commentCount": "54"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "JZOEr5Q3XV32VOPQF_ccz28nSa4",
      "id": "PBmT30aUd2s",
      "snippet": {
        "publishedAt": "2020-12-01T15:30:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Delivering Founder Happiness",
        "description": "“Happiness is really just about four things: perceived control, perceived progress, connectedness (number and depth of your relationships), and vision/meaning (being part of something bigger than yourself).” —Tony Hsieh 1973 - 2020\n\n\"Coast to Coast\" Instrumental by Homage https://youtu.be/VlXBIWeuACE\n\"Old Soul\" Instrumental by Homage https://youtu.be/O5GyeZrRfL8",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/PBmT30aUd2s/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/PBmT30aUd2s/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/PBmT30aUd2s/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/PBmT30aUd2s/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/PBmT30aUd2s/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "tony hsieh",
          "zappos",
          "delivering happiness",
          "founder",
          "starting companies",
          "founder motivation",
          "founder happiness",
          "zappos.com",
          "tony hsieh zappos",
          "zappos culture",
          "delivering happiness by tony hsieh",
          "zappos delivering happiness"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Delivering Founder Happiness",
          "description": "“Happiness is really just about four things: perceived control, perceived progress, connectedness (number and depth of your relationships), and vision/meaning (being part of something bigger than yourself).” —Tony Hsieh 1973 - 2020\n\n\"Coast to Coast\" Instrumental by Homage https://youtu.be/VlXBIWeuACE\n\"Old Soul\" Instrumental by Homage https://youtu.be/O5GyeZrRfL8"
        }
      },
      "statistics": {
        "viewCount": "11684",
        "likeCount": "793",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "95"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "CHDGD07DNt983J83atWdMjtSUWo",
      "id": "ROw1GMEP_14",
      "snippet": {
        "publishedAt": "2020-11-28T16:00:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Getting hired at Google after 20 rejections — Hacking the process",
        "description": "Viewer interview! Michael Brown hacked the Google hiring process. He tried the jobs page and couldn't get in through the front door. He found another way, asking the right questions, and hacking the system by figuring out what people at HQ wanted. Cool story from someone from the YouTube community. \n\n00:00 Intro\n00:38 Meet Michael Brown\n01:30 20 applications, 100% rejections\n02:15 The window: staffing agencies for Google projects\n02:52 Becoming a grocery picker for Google Shopping\n03:09 Going above and beyond\n04:35 First try: no response\n06:30 2nd try: Full time offer\n07:36 Michael's parting advice\n\nWatch my video How to Win on how to win career tournaments—\nhttps://www.youtube.com/watch?v=5siggWM8faY",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/ROw1GMEP_14/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/ROw1GMEP_14/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/ROw1GMEP_14/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/ROw1GMEP_14/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/ROw1GMEP_14/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "google",
          "get a job at google",
          "google hiring",
          "google application",
          "google hiring process",
          "how to get ahead",
          "career opportunities",
          "first job",
          "first job at google",
          "google's hiring process",
          "life at google",
          "google jobs",
          "work at google",
          "google recruiting process",
          "google recruitment",
          "google recruitment process",
          "how google hires",
          "how to get a job at google",
          "how to get job in google",
          "google recruiting"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Getting hired at Google after 20 rejections — Hacking the process",
          "description": "Viewer interview! Michael Brown hacked the Google hiring process. He tried the jobs page and couldn't get in through the front door. He found another way, asking the right questions, and hacking the system by figuring out what people at HQ wanted. Cool story from someone from the YouTube community. \n\n00:00 Intro\n00:38 Meet Michael Brown\n01:30 20 applications, 100% rejections\n02:15 The window: staffing agencies for Google projects\n02:52 Becoming a grocery picker for Google Shopping\n03:09 Going above and beyond\n04:35 First try: no response\n06:30 2nd try: Full time offer\n07:36 Michael's parting advice\n\nWatch my video How to Win on how to win career tournaments—\nhttps://www.youtube.com/watch?v=5siggWM8faY"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "12601",
        "likeCount": "646",
        "dislikeCount": "11",
        "favoriteCount": "0",
        "commentCount": "74"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "eM1dMZ9S7IEhMf2yW-JoSPqTxV0",
      "id": "0pzphszE704",
      "snippet": {
        "publishedAt": "2020-11-19T15:00:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "AptDeco is eating the $14B furniture market: How they got product market fit",
        "description": "Today we're sitting down with Reham Fagiri and Kalam Dennis, cofounders of AptDeco, the next great marketplace startup. \n\nEveryone's got some furniture that doesn't quite fit their room. You bought it, you thought it would work, but it's not quite right. What if I told you there's a managed marketplace now that will let you get a lot of your money back, and then find your next piece, all at once? \n\nIt's here and it's called AptDeco. They're a managed marketplace that is expanding the market for used furniture. It's my favorite way software can create new behaviors in the same way Instacart let you get your groceries or Uber made it easier to get around. And it's in a $14billion market that can only get bigger, because this is supply and demand that didn't exist before.\n\nThis team is a case study in being data driven, doing unscalable things, and then scaling them in one of the world's biggest possible markets.\n\n00:00 Intro\n00:51 Meet Reham and Kalam\n02:07 AptDeco is the next great managed marketplace\n04:38 Sustainability: AptDeco saves 9M pounds of CO2\n05:24 How they solved demand\n06:51 Key learning: Do your ad buys in-house\n07:43 Key learning: Do your logistics in-house\n09:55 Best software-based logistics = no warehousing\n12:16 Product market fit = supply constrained, unlimited demand\n13:13 Diversity and inclusion in Tech\n17:28 Lessons from Reham and Kalam wish they knew when they started\n19:40 Lesson: Do the unscalable and scale them\n24:06 AptDeco is live now for NYC\n24:52 AptDeco is hiring remotely for engineers and product",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/0pzphszE704/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/0pzphszE704/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/0pzphszE704/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/0pzphszE704/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/0pzphszE704/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "aptdeco",
          "y combinator",
          "initialized",
          "garry tan",
          "startup",
          "billion dollar startup",
          "furniture startup",
          "furniture business",
          "furniture reseller",
          "used furniture",
          "used furniture marketplace",
          "used furniture startup",
          "managed marketplace",
          "aptdeco.com",
          "furniture",
          "selling furniture",
          "nyc",
          "early stage startup",
          "new york startup",
          "reham fagiri",
          "kalam dennis",
          "startups",
          "buying used furniture",
          "selling furniture in new york",
          "buying furniture in new york",
          "entrepreneur",
          "second hand furniture"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "AptDeco is eating the $14B furniture market: How they got product market fit",
          "description": "Today we're sitting down with Reham Fagiri and Kalam Dennis, cofounders of AptDeco, the next great marketplace startup. \n\nEveryone's got some furniture that doesn't quite fit their room. You bought it, you thought it would work, but it's not quite right. What if I told you there's a managed marketplace now that will let you get a lot of your money back, and then find your next piece, all at once? \n\nIt's here and it's called AptDeco. They're a managed marketplace that is expanding the market for used furniture. It's my favorite way software can create new behaviors in the same way Instacart let you get your groceries or Uber made it easier to get around. And it's in a $14billion market that can only get bigger, because this is supply and demand that didn't exist before.\n\nThis team is a case study in being data driven, doing unscalable things, and then scaling them in one of the world's biggest possible markets.\n\n00:00 Intro\n00:51 Meet Reham and Kalam\n02:07 AptDeco is the next great managed marketplace\n04:38 Sustainability: AptDeco saves 9M pounds of CO2\n05:24 How they solved demand\n06:51 Key learning: Do your ad buys in-house\n07:43 Key learning: Do your logistics in-house\n09:55 Best software-based logistics = no warehousing\n12:16 Product market fit = supply constrained, unlimited demand\n13:13 Diversity and inclusion in Tech\n17:28 Lessons from Reham and Kalam wish they knew when they started\n19:40 Lesson: Do the unscalable and scale them\n24:06 AptDeco is live now for NYC\n24:52 AptDeco is hiring remotely for engineers and product"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "12988",
        "likeCount": "520",
        "dislikeCount": "8",
        "favoriteCount": "0",
        "commentCount": "69"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "5TPsblNm6M9nHvKUqIgV5-GxCXE",
      "id": "3YKNr-LiblI",
      "snippet": {
        "publishedAt": "2020-11-14T15:30:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Billion dollar startup ideas",
        "description": "What makes you new, different, and unique in the world will often be the exact thing that makes you succeed. Embrace your differences, and you’ll find something amazing in there. Learn how founders like Steve Jobs, Bill Gates, Ryan Petersen of Flexport, and Jack Conte of Patreon did exactly that: learn things from unique personal experiences that put them on the right path. \n\nAnd if you can do that, you avoid infinite competition. You can make something truly awesome. \n\n00:00 Intro\n01:52 Steve Jobs on Unique Experiences\n03:04 How Ryan Petersen's experiences led him to make Flexport\n04:26 How Jack Conte's experiences led him to make Patreon\n05:24 You don't have to be novel\n06:01 Great startups start as toys\n06:43 Paul Graham on startup ideas\n\nRead How to get Startup Ideas by Paul Graham — http://paulgraham.com/startupideas.html \n\nBeats by Homage Beats - 5am https://www.youtube.com/watch?v=on4gBvEyfCg&ab_channel=HomageBeats\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM - https://instagram.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/3YKNr-LiblI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/3YKNr-LiblI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/3YKNr-LiblI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/3YKNr-LiblI/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/3YKNr-LiblI/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "y combinator",
          "startup school",
          "garry tan",
          "initialized",
          "initialized capital",
          "flexport",
          "patreon",
          "ryan petersen",
          "jack conte",
          "steve jobs",
          "steve jobs motivational",
          "startup ideas",
          "billion dollar startups",
          "tech startups",
          "startup",
          "best startups 2020",
          "best startups 2021",
          "best startups in america",
          "most valued unicorn startups in usa",
          "startups in america",
          "entrepreneurship",
          "startups 2020",
          "unicorn startups",
          "venture capital",
          "entrepreneurs",
          "unicorn startup",
          "founding story",
          "top startups"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Billion dollar startup ideas",
          "description": "What makes you new, different, and unique in the world will often be the exact thing that makes you succeed. Embrace your differences, and you’ll find something amazing in there. Learn how founders like Steve Jobs, Bill Gates, Ryan Petersen of Flexport, and Jack Conte of Patreon did exactly that: learn things from unique personal experiences that put them on the right path. \n\nAnd if you can do that, you avoid infinite competition. You can make something truly awesome. \n\n00:00 Intro\n01:52 Steve Jobs on Unique Experiences\n03:04 How Ryan Petersen's experiences led him to make Flexport\n04:26 How Jack Conte's experiences led him to make Patreon\n05:24 You don't have to be novel\n06:01 Great startups start as toys\n06:43 Paul Graham on startup ideas\n\nRead How to get Startup Ideas by Paul Graham — http://paulgraham.com/startupideas.html \n\nBeats by Homage Beats - 5am https://www.youtube.com/watch?v=on4gBvEyfCg&ab_channel=HomageBeats\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM - https://instagram.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "193015",
        "likeCount": "10736",
        "dislikeCount": "149",
        "favoriteCount": "0",
        "commentCount": "514"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "QrPhtX2nNzcqRjM6EgPY9lP2fGo",
      "id": "qlP4bnZ_izk",
      "snippet": {
        "publishedAt": "2020-11-09T15:00:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Solving the $160 billion food waste problem: Meet Stefan Kalb of Shelf Engine",
        "description": "Stefan Kalb started a food brand at age 23, and that's when he discovered 30% of all perishable food goes in the garbage. He teamed up with a technical cofounder Bede Jordan in 2016 and now they're solving the problem with software. They were solving a problem they saw first hand. They had their twists and turns, and it was something I worked with Stefan and Bede on for years as they followed their own internal north star. Along the way, they figured it out.\n\nNow with product market fit, and signed expanding contracts with 4 of the top 10 US national grocers, they're on track to solve the $160B annual US food waste problem and build the next billion dollar startup in the process.\n\n00:00 Intro\n00:50 Meet Stefan Kalb, cofounder of Shelf Engine\n01:06 What is Shelf Engine?\n02:00 Coming up with the Shelf Engine idea\n04:51 Stefan's direct experience building a food brand\n05:35 Business cofounder + great tech cofounder\n06:30 Early validation of product market fit\n07:00 Raising first money in\n07:54 How Stefan met Initialized\n08:27 First principles approach to building a startup\n09:59 What is it like to sell to grocery store incumbents?\n11:20 How to do enterprise sales\n12:20 A go-to-market pivot led to product market fit\n14:20 Startup founders must be like water (Bruce Lee)\n15:38 Product market fit is unrelated to investor pattern match\n17:25 How do you know when you have product market fit?\n18:22 What is it like to work at Shelf Engine?\n21:21 Shelf Engine is working with 4 of the top 10 US national grocers\n23:10 What does Stefan wish he knew when he started in tech?\n\nLearn more about Shelf Engine at https://www.shelfengine.com/\n\nFollow Stefan Kalb on Twitter at https://twitter.com/stefankalb",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/qlP4bnZ_izk/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/qlP4bnZ_izk/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/qlP4bnZ_izk/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/qlP4bnZ_izk/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/qlP4bnZ_izk/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "food waste",
          "shelf engine",
          "stefan kalb",
          "kroger",
          "safeway",
          "walmart",
          "y combinator",
          "garry tan",
          "initialized capital",
          "food supply chain startup",
          "food supply chain",
          "supply chain",
          "enterprise startup",
          "climate startup",
          "climate change startup",
          "stop food waste",
          "yc",
          "y combinator application",
          "startup school",
          "supply chain management",
          "startup",
          "startup company",
          "vc",
          "sustainability",
          "environment",
          "entrepreneur",
          "climate change",
          "startups",
          "entrepreneurship",
          "ycombinator",
          "food thrown away",
          "donate leftover food"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Solving the $160 billion food waste problem: Meet Stefan Kalb of Shelf Engine",
          "description": "Stefan Kalb started a food brand at age 23, and that's when he discovered 30% of all perishable food goes in the garbage. He teamed up with a technical cofounder Bede Jordan in 2016 and now they're solving the problem with software. They were solving a problem they saw first hand. They had their twists and turns, and it was something I worked with Stefan and Bede on for years as they followed their own internal north star. Along the way, they figured it out.\n\nNow with product market fit, and signed expanding contracts with 4 of the top 10 US national grocers, they're on track to solve the $160B annual US food waste problem and build the next billion dollar startup in the process.\n\n00:00 Intro\n00:50 Meet Stefan Kalb, cofounder of Shelf Engine\n01:06 What is Shelf Engine?\n02:00 Coming up with the Shelf Engine idea\n04:51 Stefan's direct experience building a food brand\n05:35 Business cofounder + great tech cofounder\n06:30 Early validation of product market fit\n07:00 Raising first money in\n07:54 How Stefan met Initialized\n08:27 First principles approach to building a startup\n09:59 What is it like to sell to grocery store incumbents?\n11:20 How to do enterprise sales\n12:20 A go-to-market pivot led to product market fit\n14:20 Startup founders must be like water (Bruce Lee)\n15:38 Product market fit is unrelated to investor pattern match\n17:25 How do you know when you have product market fit?\n18:22 What is it like to work at Shelf Engine?\n21:21 Shelf Engine is working with 4 of the top 10 US national grocers\n23:10 What does Stefan wish he knew when he started in tech?\n\nLearn more about Shelf Engine at https://www.shelfengine.com/\n\nFollow Stefan Kalb on Twitter at https://twitter.com/stefankalb"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "13215",
        "likeCount": "634",
        "dislikeCount": "4",
        "favoriteCount": "0",
        "commentCount": "84"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "C5pDwAXs0IgVCO_Sa2XEJhps5Gg",
      "id": "uQWk7T9Vz_k",
      "snippet": {
        "publishedAt": "2020-11-05T15:00:13Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "How I got into Y Combinator",
        "description": "Don't look for a mentor. Find people who you believe in who can help you, and help them first. That's how I got into Y Combinator, and this is my story.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nBeats by HomageBeats: https://www.youtube.com/watch?v=j3b7Yf-z52g\n\nFollow me on Twitter at https://twitter.com/garrytan\n\nInstagram DMs are open for startup advice at https://instagram.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/uQWk7T9Vz_k/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/uQWk7T9Vz_k/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/uQWk7T9Vz_k/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/uQWk7T9Vz_k/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/uQWk7T9Vz_k/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "y combinator",
          "y combinator application",
          "y combinator process",
          "y combinator application 2020",
          "y combinator application 2021",
          "applying to y combinator",
          "applying to yc",
          "yc interview",
          "yc application",
          "garry tan",
          "initialized capital",
          "startup school",
          "startup school 2008",
          "posterous",
          "incubators",
          "accelerators",
          "how to get into y combinator",
          "yc",
          "yc video",
          "y combinator application video",
          "startup",
          "startups",
          "entrepreneur",
          "entrepreneurship",
          "ycombinator",
          "business ideas"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How I got into Y Combinator",
          "description": "Don't look for a mentor. Find people who you believe in who can help you, and help them first. That's how I got into Y Combinator, and this is my story.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nBeats by HomageBeats: https://www.youtube.com/watch?v=j3b7Yf-z52g\n\nFollow me on Twitter at https://twitter.com/garrytan\n\nInstagram DMs are open for startup advice at https://instagram.com/garrytan"
        }
      },
      "statistics": {
        "viewCount": "25724",
        "likeCount": "1466",
        "dislikeCount": "15",
        "favoriteCount": "0",
        "commentCount": "191"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "DKWTS6C1sokNuc_3Bic4tZqpJ00",
      "id": "MnuFEvI7EGw",
      "snippet": {
        "publishedAt": "2020-10-28T14:00:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Drip Capital went from zero to $1 billion in transactions: Fintech meets global trade finance",
        "description": "Drip Capital started as two people with an idea in 2015. Today, they have over 138 employees, raised more than $45M, and will do over $1 billion in trade volume since inception this year. They have over 2,000 importers and exporters on the platform and have built an enduring fintech startup. They're on track to be as impactful as the World Bank in terms of helping businesses worldwide get access to credit.\n\nThey had to search the space to find one that could grow quickly with high LTV and low CAC. It was really impressive to see as an investor, and I'm so glad we could sit down with cofounder Neil Kothari today.\n\n00:00 Intro\n00:38 Meet Neil Kothari\n02:50 Starting the founder journey\n03:56 First iteration: Lending in the US\n05:52 Pivot to global trade finance, finding product market fit\n07:18 More data = better lending (key insight)\n09:37 Why does net 30 exist when interest rates are zero?\n11:48 Incumbent banks aren't doing their job, which lets Drip create a new cross-border trade model\n13:46 Why is lending hard?\n14:35 What they learned in India\n18:00 Do unscalable things, then scale them\n21:10 Beware advice\n23:31 How to invest, or work at Drip\n\nHi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/MnuFEvI7EGw/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/MnuFEvI7EGw/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/MnuFEvI7EGw/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/MnuFEvI7EGw/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/MnuFEvI7EGw/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "trade finance",
          "neil kothari",
          "fintech",
          "lending startup",
          "fintech startup",
          "zero interest rates",
          "world bank",
          "india cross border trade",
          "india trade",
          "mexico usa trade",
          "cross border trade",
          "global manufacturers",
          "manufacturer",
          "manufacturer lending",
          "international trade finance",
          "trade finance methods",
          "trade finance tutorial",
          "trade finance in banking",
          "supply chain finance",
          "finance",
          "trade finance basics",
          "drip capital",
          "accel india",
          "garry tan",
          "initialized capital",
          "y combinator"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Drip Capital went from zero to $1 billion in transactions: Fintech meets global trade finance",
          "description": "Drip Capital started as two people with an idea in 2015. Today, they have over 138 employees, raised more than $45M, and will do over $1 billion in trade volume since inception this year. They have over 2,000 importers and exporters on the platform and have built an enduring fintech startup. They're on track to be as impactful as the World Bank in terms of helping businesses worldwide get access to credit.\n\nThey had to search the space to find one that could grow quickly with high LTV and low CAC. It was really impressive to see as an investor, and I'm so glad we could sit down with cofounder Neil Kothari today.\n\n00:00 Intro\n00:38 Meet Neil Kothari\n02:50 Starting the founder journey\n03:56 First iteration: Lending in the US\n05:52 Pivot to global trade finance, finding product market fit\n07:18 More data = better lending (key insight)\n09:37 Why does net 30 exist when interest rates are zero?\n11:48 Incumbent banks aren't doing their job, which lets Drip create a new cross-border trade model\n13:46 Why is lending hard?\n14:35 What they learned in India\n18:00 Do unscalable things, then scale them\n21:10 Beware advice\n23:31 How to invest, or work at Drip\n\nHi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        }
      },
      "statistics": {
        "viewCount": "19296",
        "likeCount": "801",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "92"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "poa2CeMml_iCrOlIbBlT-q9zWR0",
      "id": "5siggWM8faY",
      "snippet": {
        "publishedAt": "2020-10-27T14:00:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "5 Steps to Winning Career Tournaments: Competitive jobs, admissions, promotions, or startup funding",
        "description": "There are tournaments everywhere. Careers are often one tournament after another. Meritocracy (fair tournaments) are something society continues to strive for, but we're not there. \n\nHere are 5 steps to getting that job, raising that money, getting into that program or college, getting tenure, getting that press coverage, getting those followers, or whatever it is you are setting out to do. \n\n00:00 Intro\n00:36 My tournament failure story: Stanford Mayfield Fellows Program\n01:49 Two lessons from my own story\n02:18 5 steps to winning any career tournament\n02:57 Step 1 - Identify the tournament\n03:27 Step 2 - Learn the criteria early\n04:56 Step 3 - Apply to many to up your odds\n05:48 Step 4 - Get advice from those before you\n06:26 Step 5 - Door locked? Try the window.\n07:21 If you win enough tournaments, you get to create them\n\nApplying to Y Combinator? Watch these:\nHow to nail your application https://youtu.be/4wbCVN1yLyA\nHow to nail your interview https://youtu.be/rfTgzA6iKZc\n--\n\nHi! I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/5siggWM8faY/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/5siggWM8faY/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/5siggWM8faY/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/5siggWM8faY/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/5siggWM8faY/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "getting a job",
          "getting press coverage",
          "career tournament",
          "getting into college",
          "how to get ahead",
          "competition",
          "winning",
          "tournaments",
          "startup",
          "how to get a job",
          "job",
          "job interview",
          "getting a job in college",
          "how to find a job",
          "career",
          "work",
          "how to get a job with no experience",
          "how to get a job as a teen",
          "career coach",
          "how to make money",
          "getting promoted",
          "work competition",
          "how to get promoted",
          "garry tan",
          "meritocracy",
          "careers",
          "startups",
          "career advice"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "5 Steps to Winning Career Tournaments: Competitive jobs, admissions, promotions, or startup funding",
          "description": "There are tournaments everywhere. Careers are often one tournament after another. Meritocracy (fair tournaments) are something society continues to strive for, but we're not there. \n\nHere are 5 steps to getting that job, raising that money, getting into that program or college, getting tenure, getting that press coverage, getting those followers, or whatever it is you are setting out to do. \n\n00:00 Intro\n00:36 My tournament failure story: Stanford Mayfield Fellows Program\n01:49 Two lessons from my own story\n02:18 5 steps to winning any career tournament\n02:57 Step 1 - Identify the tournament\n03:27 Step 2 - Learn the criteria early\n04:56 Step 3 - Apply to many to up your odds\n05:48 Step 4 - Get advice from those before you\n06:26 Step 5 - Door locked? Try the window.\n07:21 If you win enough tournaments, you get to create them\n\nApplying to Y Combinator? Watch these:\nHow to nail your application https://youtu.be/4wbCVN1yLyA\nHow to nail your interview https://youtu.be/rfTgzA6iKZc\n--\n\nHi! I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "15435",
        "likeCount": "1508",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "202"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "QtzWaQdbwAt_hIxf12Pk7TqwrZA",
      "id": "P-Igj27Rh_k",
      "snippet": {
        "publishedAt": "2020-10-20T14:00:11Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Build housing in weeks, not years: Fight the California housing crisis with Abodu",
        "description": "If you've ever tried to build housing, you'll learn that it takes often YEARS to get something built. In California, this has become a deep societal problem that is affecting quality of life and it's one of the most important things to address. This is why Initialized funded Abodu, a startup that can help you build a studio, 1, or 2 bedroom apartment in your backyard (as an ADU or accessory dwelling unit) in 12 weeks, instead of getting stuck for years in planning department fights.\n\n00:00 Intro\n00:48 Meet Kim-Mai Cutler, housing activist and VC\n02:04 Meet John Geary and Eric McInerney, founders of Abodu\n02:29 Why accessory dwelling units?\n03:51 Palo Alto real customer story\n04:41 What's it like to work with Abodu?\n06:02 Where can I get an Abodu?\n07:09 How does factory built compare to stick built?\n08:12 Taking the guesswork and risk out of building\n08:40 How much does it cost?\n09:53 Financing\n10:47 Studio, 1BR and 2BR options\n11:46 Visit the showroom\n13:02 What's it like to visit?\n13:53 Abodu customer stories\n14:58 Housing is a key societal issue\n\nLearn more about Abodu at https://abodu.com\n\nFollow Kim-Mai Cutler on Twitter at https://twitter.com/kimmaicutler",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/P-Igj27Rh_k/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/P-Igj27Rh_k/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/P-Igj27Rh_k/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/P-Igj27Rh_k/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/P-Igj27Rh_k/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "housing",
          "california housing crisis",
          "housing market 2020",
          "california housing",
          "adu",
          "tiny home",
          "build tiny home",
          "planning department",
          "abodu",
          "initialized capital",
          "kim-mai cutler",
          "garry tan",
          "john geary",
          "eric mcinerney",
          "accessory dwelling unit",
          "building an adu",
          "building adu",
          "building adu in california",
          "building housing",
          "palo alto housing",
          "san jose housing",
          "redwood city housing",
          "granny flat",
          "housing crisis",
          "housing market",
          "housing startup",
          "san jose adu",
          "palo alto adu"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Build housing in weeks, not years: Fight the California housing crisis with Abodu",
          "description": "If you've ever tried to build housing, you'll learn that it takes often YEARS to get something built. In California, this has become a deep societal problem that is affecting quality of life and it's one of the most important things to address. This is why Initialized funded Abodu, a startup that can help you build a studio, 1, or 2 bedroom apartment in your backyard (as an ADU or accessory dwelling unit) in 12 weeks, instead of getting stuck for years in planning department fights.\n\n00:00 Intro\n00:48 Meet Kim-Mai Cutler, housing activist and VC\n02:04 Meet John Geary and Eric McInerney, founders of Abodu\n02:29 Why accessory dwelling units?\n03:51 Palo Alto real customer story\n04:41 What's it like to work with Abodu?\n06:02 Where can I get an Abodu?\n07:09 How does factory built compare to stick built?\n08:12 Taking the guesswork and risk out of building\n08:40 How much does it cost?\n09:53 Financing\n10:47 Studio, 1BR and 2BR options\n11:46 Visit the showroom\n13:02 What's it like to visit?\n13:53 Abodu customer stories\n14:58 Housing is a key societal issue\n\nLearn more about Abodu at https://abodu.com\n\nFollow Kim-Mai Cutler on Twitter at https://twitter.com/kimmaicutler"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "12713",
        "likeCount": "271",
        "dislikeCount": "10",
        "favoriteCount": "0",
        "commentCount": "67"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "4RL37f3YA7hno9z3oobHeNSVZHg",
      "id": "opkHJLVAM4A",
      "snippet": {
        "publishedAt": "2020-10-19T14:00:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Billion dollar startups are better faster cheaper— Lessons from Amazon, Instacart, Uber & Shopify",
        "description": "Facebook wasn't the first social network. So maybe being first doesn't matter. But, being better, faster, or cheaper? That matters a lot.\n\nLessons from Airbnb, Amazon, Robinhood, Coinbase, Instacart, Uber, Lyft, Discord, Flexport and Shopify.\n\nI'm getting a lot of questions from founders and future founders. I'm realizing a lot of folks are not thinking about their startups in a way that will convince people to switch to use their products or services... and if that doesn't seem like it will happen, they won't be able to build teams, or raise money either. So the self-fulfilling prophecy cannot work. This video is for those founders. Can you describe what you are doing in classic better, faster, cheaper terms? If not, you need to.\n\n00:00 Intro\n01:23 Why investors like novelty so much\n02:30 How to be Better\n04:41 How to be Faster\n06:30 How to be Cheaper\n09:06 You can figure this out\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. I was earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/opkHJLVAM4A/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/opkHJLVAM4A/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/opkHJLVAM4A/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/opkHJLVAM4A/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/opkHJLVAM4A/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Better faster cheaper",
          "startup ideas",
          "startup pitch",
          "y combinator",
          "y combinator pitch",
          "the business of agile: better faster cheaper",
          "strategy and tactics",
          "exponential growth",
          "yc",
          "startup",
          "startup school",
          "business ideas",
          "entrepreneur",
          "startups",
          "entrepreneurship",
          "how to start a business",
          "venture capital",
          "silicon valley",
          "best business ideas",
          "new business ideas",
          "startup company",
          "garry tan",
          "initialized capital"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Billion dollar startups are better faster cheaper— Lessons from Amazon, Instacart, Uber & Shopify",
          "description": "Facebook wasn't the first social network. So maybe being first doesn't matter. But, being better, faster, or cheaper? That matters a lot.\n\nLessons from Airbnb, Amazon, Robinhood, Coinbase, Instacart, Uber, Lyft, Discord, Flexport and Shopify.\n\nI'm getting a lot of questions from founders and future founders. I'm realizing a lot of folks are not thinking about their startups in a way that will convince people to switch to use their products or services... and if that doesn't seem like it will happen, they won't be able to build teams, or raise money either. So the self-fulfilling prophecy cannot work. This video is for those founders. Can you describe what you are doing in classic better, faster, cheaper terms? If not, you need to.\n\n00:00 Intro\n01:23 Why investors like novelty so much\n02:30 How to be Better\n04:41 How to be Faster\n06:30 How to be Cheaper\n09:06 You can figure this out\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. I was earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "73558",
        "likeCount": "4675",
        "dislikeCount": "34",
        "favoriteCount": "0",
        "commentCount": "499"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "1vuUZgRTy93PSNJH6qpPOOqX6VY",
      "id": "wxsDnQKcq4Y",
      "snippet": {
        "publishedAt": "2020-10-13T14:40:30Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "20,000 YouTube subscribers in 2 weeks: What I learned",
        "description": "My YouTube channel grew more than 20,000 subscribers in two weeks. I hit some sort of crazy if-else statement in the YouTube algorithm. It took about a year to do this after posting weekly for more than a year. This is the stuff I learned along the way, and how I learned to push through Ira Glass's \"Creativity Gap\" which is the gulf between your taste and what are capable of making right now. \n\nI also discovered how not to compare myself to others, and instead think about metrics internal to me. That helped me move beyond comparing my views to other videos and helped me think more internally about it.\n\nMusic: \"Union Station\", Instrumental by Homage https://youtu.be/T947cjXSb94",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/wxsDnQKcq4Y/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/wxsDnQKcq4Y/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/wxsDnQKcq4Y/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/wxsDnQKcq4Y/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/wxsDnQKcq4Y/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "growing youtube",
          "growing youtube channel 2020",
          "growing subscribers",
          "youtube subscribers",
          "how to get youtube subscribers",
          "building youtube channel",
          "ira glass",
          "the gap",
          "creators mind",
          "how to start a youtube channel",
          "grow your youtube channel",
          "how to grow your youtube chann",
          "grow on youtube",
          "how to grow your youtube channel fast",
          "youtube channel growth tips",
          "youtube channel growth strategy",
          "how to get subscribers on youtube",
          "how to get subscribers",
          "garry tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "20,000 YouTube subscribers in 2 weeks: What I learned",
          "description": "My YouTube channel grew more than 20,000 subscribers in two weeks. I hit some sort of crazy if-else statement in the YouTube algorithm. It took about a year to do this after posting weekly for more than a year. This is the stuff I learned along the way, and how I learned to push through Ira Glass's \"Creativity Gap\" which is the gulf between your taste and what are capable of making right now. \n\nI also discovered how not to compare myself to others, and instead think about metrics internal to me. That helped me move beyond comparing my views to other videos and helped me think more internally about it.\n\nMusic: \"Union Station\", Instrumental by Homage https://youtu.be/T947cjXSb94"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "10572",
        "likeCount": "921",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "377"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "gs8OI8ubieLbkh56ifEh5woGM40",
      "id": "V0eY2nBtsjw",
      "snippet": {
        "publishedAt": "2020-10-08T14:37:29Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Autonomous checkout is now a reality: Standard Cognition's camera-only computer vision stores launch",
        "description": "Today, Standard Cognition launches its first few stores nationwide, including one at the University of Houston. Today we'll get to see one of their first locations and chat with founder and CEO Jordan Fisher who shares what it took to get here, and what makes Standard Cognition ready for mass rollout to thousands of stores.\n\nAmazon Go couldn't do what this team has done. And they'll be able to arm the rebels (thousands of convenience stores and grocery stores) before Amazon eats their lunch. I'm an investor and on the board of Standard Cognition — so it's a big moment today for me too.\n\nWatch the live footage from their first Houston location here: https://www.youtube.com/watch?v=Ll-YJz5f8IU\n\nCoverage of this launch here at VentureBeat: https://venturebeat.com/2020/10/08/standard-launches-cashierless-store-at-the-university-of-houston/\n\nLearn more about Standard Cognition here: https://standard.ai/",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/V0eY2nBtsjw/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/V0eY2nBtsjw/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/V0eY2nBtsjw/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/V0eY2nBtsjw/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/V0eY2nBtsjw/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "standard cognition",
          "cashierless checkout",
          "amazon go",
          "computer vision",
          "amazon go grocery store",
          "retail technology",
          "retail startup",
          "future of retail",
          "future of retail stores",
          "startup",
          "san francisco startup",
          "garry tan",
          "initialized capital",
          "y combinator",
          "yc startup",
          "autonomous checkout",
          "machine learning",
          "cashierless store",
          "deep learning",
          "ai",
          "battle of the cashierless store",
          "startups",
          "ai store",
          "retail"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Autonomous checkout is now a reality: Standard Cognition's camera-only computer vision stores launch",
          "description": "Today, Standard Cognition launches its first few stores nationwide, including one at the University of Houston. Today we'll get to see one of their first locations and chat with founder and CEO Jordan Fisher who shares what it took to get here, and what makes Standard Cognition ready for mass rollout to thousands of stores.\n\nAmazon Go couldn't do what this team has done. And they'll be able to arm the rebels (thousands of convenience stores and grocery stores) before Amazon eats their lunch. I'm an investor and on the board of Standard Cognition — so it's a big moment today for me too.\n\nWatch the live footage from their first Houston location here: https://www.youtube.com/watch?v=Ll-YJz5f8IU\n\nCoverage of this launch here at VentureBeat: https://venturebeat.com/2020/10/08/standard-launches-cashierless-store-at-the-university-of-houston/\n\nLearn more about Standard Cognition here: https://standard.ai/"
        }
      },
      "statistics": {
        "viewCount": "13413",
        "likeCount": "630",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "114"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "mylVc1WTt3PpmWXWGbfbqdD46BE",
      "id": "Xa6zsbXGQ5M",
      "snippet": {
        "publishedAt": "2020-10-06T14:00:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Should you be the CEO?",
        "description": "We were pitching A16z, with Marc Andreessen and Ben Horowitz— final Monday morning meeting on Sand Hill Road. They asked us who was CEO. We said both. It was the wrong answer.\n\nThis video is about why people try to do shared leadership, and how you can tell if you should be CEO or not. It might seem like its not important... but it is.\n\nMusic— \"Always with You\" - Instrumental by Homage - https://youtu.be/4Gn_-0AX0Yo",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/Xa6zsbXGQ5M/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/Xa6zsbXGQ5M/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/Xa6zsbXGQ5M/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/Xa6zsbXGQ5M/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/Xa6zsbXGQ5M/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "ceo",
          "co-ceo",
          "shared leadership",
          "andreessen horowitz",
          "ben horowitz",
          "marc andreessen",
          "initialized capital",
          "initialized",
          "garry tan",
          "posterous",
          "how to be a ceo",
          "day in the life of a ceo",
          "how to become ceo of a company",
          "19 qualities of a great ceo",
          "entrepreneur",
          "world's richest",
          "business",
          "entrepreneurship",
          "ceo qualities",
          "successful ceo",
          "tech ceos",
          "tech ceo",
          "qualities of a ceo",
          "how to become a ceo",
          "entrepreneur motivation",
          "startup entrepreneurs",
          "great ceo",
          "y combinator",
          "innovation"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Should you be the CEO?",
          "description": "We were pitching A16z, with Marc Andreessen and Ben Horowitz— final Monday morning meeting on Sand Hill Road. They asked us who was CEO. We said both. It was the wrong answer.\n\nThis video is about why people try to do shared leadership, and how you can tell if you should be CEO or not. It might seem like its not important... but it is.\n\nMusic— \"Always with You\" - Instrumental by Homage - https://youtu.be/4Gn_-0AX0Yo"
        }
      },
      "statistics": {
        "viewCount": "66801",
        "likeCount": "4183",
        "dislikeCount": "35",
        "favoriteCount": "0",
        "commentCount": "416"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "zvHyfdgiVyXoxUOJ9eCRnfOV4YA",
      "id": "8pG6golMHPo",
      "snippet": {
        "publishedAt": "2020-09-30T13:43:55Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "I said the wrong thing and suddenly our Series A pitch meeting was over",
        "description": "In 2010 I was pitching my startup for Series A to top tier investors like Peter Fenton at Benchmark. He asked me a question, and a very simple one. We said the wrong thing, and it was the end of the meeting. In this video I walk you through what the question was that ended the meeting, and why that was wrong.\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/8pG6golMHPo/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/8pG6golMHPo/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/8pG6golMHPo/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/8pG6golMHPo/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/8pG6golMHPo/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "series a pitch",
          "pitch story",
          "benchmark capital",
          "peter fenton",
          "garry tan",
          "startup pitch",
          "startup fundraising",
          "startup fundraising 101",
          "startups",
          "pitch meeting",
          "pitch advice",
          "how to raise money",
          "fundraising for your startup",
          "fundraising for startups",
          "initialized capital",
          "initialized",
          "instagram startup"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "I said the wrong thing and suddenly our Series A pitch meeting was over",
          "description": "In 2010 I was pitching my startup for Series A to top tier investors like Peter Fenton at Benchmark. He asked me a question, and a very simple one. We said the wrong thing, and it was the end of the meeting. In this video I walk you through what the question was that ended the meeting, and why that was wrong.\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        }
      },
      "statistics": {
        "viewCount": "70488",
        "likeCount": "4324",
        "dislikeCount": "44",
        "favoriteCount": "0",
        "commentCount": "562"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "-aeyQCmMWgEbG72UrC2qSNaSkFY",
      "id": "xy4WdRfDyTs",
      "snippet": {
        "publishedAt": "2020-09-23T14:00:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Successful billion dollar startups build long-lived teams that beat all competition",
        "description": "How long have you been working with your team? How long do you think it takes to win championships? It takes a pro-level basketball team an average of 4.5 years to maximize their odds of winning a championship. \n\nThe right culture, the highest and best culture, is a seamless web of deserved trust. Not much procedure, just totally reliable people correctly trusting one another. —Charlie Munger\n\nIf you could get all the people in an organization rowing in the same direction, you could dominate any industry, in any market, against any competition, at any time. —Patrick Lencioni / Five Dysfunctions of a Team\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/xy4WdRfDyTs/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/xy4WdRfDyTs/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/xy4WdRfDyTs/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/xy4WdRfDyTs/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/xy4WdRfDyTs/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startup",
          "startups",
          "garry tan",
          "team building",
          "hiring",
          "startup founders",
          "finding cofounders",
          "alignment",
          "startup values",
          "patrick lencioni 5 dysfunctions of a team",
          "business culture",
          "building trust",
          "winning championships",
          "initialized capital",
          "initialized",
          "team resilience",
          "long lived teams",
          "lean startup",
          "finding co founders"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Successful billion dollar startups build long-lived teams that beat all competition",
          "description": "How long have you been working with your team? How long do you think it takes to win championships? It takes a pro-level basketball team an average of 4.5 years to maximize their odds of winning a championship. \n\nThe right culture, the highest and best culture, is a seamless web of deserved trust. Not much procedure, just totally reliable people correctly trusting one another. —Charlie Munger\n\nIf you could get all the people in an organization rowing in the same direction, you could dominate any industry, in any market, against any competition, at any time. —Patrick Lencioni / Five Dysfunctions of a Team\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        }
      },
      "statistics": {
        "viewCount": "30964",
        "likeCount": "2190",
        "dislikeCount": "15",
        "favoriteCount": "0",
        "commentCount": "163"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "z9yF9EFv9awYrSp1RYdYlGUGBtc",
      "id": "RhYZECR2Ru8",
      "snippet": {
        "publishedAt": "2020-09-09T14:00:10Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Quit and join that risky tech startup? A guide to learning, earning & minimizing regret at startups",
        "description": "Should you quit or job? What skills do you want to acquire? How do you evaluate how much you'll actually earn? If you're great at what you do, it turns out that the only kind of risk is not taking risk at all. This is my quick guide on how to think through different opportunities that come your way. Learning early on is more important, but earning is absolutely something you need to focus on mid-career. \n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own one man YouTube channel with no staff or crew — I'm going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/RhYZECR2Ru8/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/RhYZECR2Ru8/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/RhYZECR2Ru8/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/RhYZECR2Ru8/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/RhYZECR2Ru8/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "technology",
          "startup job",
          "tech job",
          "tech interview",
          "risk taking",
          "quit my job",
          "start a startup",
          "quitting a stable job",
          "stable job",
          "initialized capital",
          "garry tan",
          "y combinator",
          "join a startup",
          "offer letter",
          "should i join a startup",
          "startup equity",
          "startup vesting",
          "comparing offer letters",
          "series a startup",
          "series b startup",
          "tech startups",
          "top startups",
          "risk minimization",
          "should i quit my job?",
          "quit my job to start a business",
          "quit my job to join a startup"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Quit and join that risky tech startup? A guide to learning, earning & minimizing regret at startups",
          "description": "Should you quit or job? What skills do you want to acquire? How do you evaluate how much you'll actually earn? If you're great at what you do, it turns out that the only kind of risk is not taking risk at all. This is my quick guide on how to think through different opportunities that come your way. Learning early on is more important, but earning is absolutely something you need to focus on mid-career. \n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own one man YouTube channel with no staff or crew — I'm going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        }
      },
      "statistics": {
        "viewCount": "40333",
        "likeCount": "2805",
        "dislikeCount": "14",
        "favoriteCount": "0",
        "commentCount": "201"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "PDW87zhvGYYZG7lcYS2uyOxFrAo",
      "id": "dRBDz-flKs0",
      "snippet": {
        "publishedAt": "2020-09-01T15:59:20Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "Shame and blame in entrepreneurship. How to channel your rage before you destroy your team.",
        "description": "Do you feel shame? I do all the time. It's one of the things that can really torture me, and if I'm not careful, I'll pass it on to my team and the people I work with. Nobody is immune to this. Even Steve Jobs fell for this when Mobile Me had a disastrous launch in 2008.\n\nRather than avoid it, let's acknowledge and talk about it. Shame is a thing many creators feel. If we can be more conscious and aware of it, we can make better choices about what to do with it.\n\n--\n\nHi, I'm Garry. I'm a former Y Combinator partner turned Forbes Midas List Top VC in the world, and I'm here to help you build your dreams. Please hit subscribe to see more videos like this.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dRBDz-flKs0/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dRBDz-flKs0/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dRBDz-flKs0/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dRBDz-flKs0/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dRBDz-flKs0/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "entrepreneurship",
          "entrepreneur",
          "startup",
          "entrepreneur motivation",
          "startup advice",
          "business startup advice",
          "y combinator",
          "startup entrepreneurs",
          "entrepreneurship motivation",
          "entrepreneur advice",
          "business",
          "entrepreneurs",
          "startups",
          "business motivation",
          "starting a business",
          "entrepreneur motivational video",
          "business advice",
          "entrepreneur mindset",
          "success",
          "shame",
          "channeling shame",
          "steve jobs",
          "mobile me",
          "steve jobs mobile me",
          "steve jobs rage",
          "steve jobs angry",
          "berating employees"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Shame and blame in entrepreneurship. How to channel your rage before you destroy your team.",
          "description": "Do you feel shame? I do all the time. It's one of the things that can really torture me, and if I'm not careful, I'll pass it on to my team and the people I work with. Nobody is immune to this. Even Steve Jobs fell for this when Mobile Me had a disastrous launch in 2008.\n\nRather than avoid it, let's acknowledge and talk about it. Shame is a thing many creators feel. If we can be more conscious and aware of it, we can make better choices about what to do with it.\n\n--\n\nHi, I'm Garry. I'm a former Y Combinator partner turned Forbes Midas List Top VC in the world, and I'm here to help you build your dreams. Please hit subscribe to see more videos like this."
        }
      },
      "statistics": {
        "viewCount": "8875",
        "likeCount": "464",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "38"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "oOePgMWidgiFdizLwT5nmuijOsY",
      "id": "dLicgt04hHY",
      "snippet": {
        "publishedAt": "2020-08-18T14:30:09Z",
        "channelId": settings.YOUTUBE_CHANNEL_ID,
        "title": "See reality as it is first, before you try to bend it — The Stockdale Paradox",
        "description": "Jim Stockdale was the highest ranking captive POW in the Hanoi Hilton. He spent 8 years as a prisoner of war and lived to tell about it. His lesson, the Stockdale Paradox, is an important one for all founders. You must know you will prevail while also accepting total reality. Jim Collins talks about it in Good to Great, and it comes up all the time in startup land.\n\nI talk about how both parts of the paradox were a part of my own startup journey. In fact, violation of the 2nd part (being able to see reality as it is) led to my own personal failure. Today I spend a lot of time trying to help founders navigate their own minefields in this world. \n\nSorry for the hiatus! I've been busy closing our newest $230M seed-stage venture capital fund, and so now I'm back to making videos again.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dLicgt04hHY/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dLicgt04hHY/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dLicgt04hHY/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dLicgt04hHY/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dLicgt04hHY/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "stockdale paradox",
          "jim stockdale",
          "good to great",
          "Venture Capital",
          "venture capitalist",
          "investing",
          "early stage venture capital",
          "seed round",
          "raising seed round",
          "initialized",
          "garry tan",
          "bending reality",
          "jim collins",
          "the stockdale paradox",
          "stockdale paradox good to great",
          "raising venture capital",
          "startup board",
          "startup board member",
          "board members",
          "initialized capital"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "See reality as it is first, before you try to bend it — The Stockdale Paradox",
          "description": "Jim Stockdale was the highest ranking captive POW in the Hanoi Hilton. He spent 8 years as a prisoner of war and lived to tell about it. His lesson, the Stockdale Paradox, is an important one for all founders. You must know you will prevail while also accepting total reality. Jim Collins talks about it in Good to Great, and it comes up all the time in startup land.\n\nI talk about how both parts of the paradox were a part of my own startup journey. In fact, violation of the 2nd part (being able to see reality as it is) led to my own personal failure. Today I spend a lot of time trying to help founders navigate their own minefields in this world. \n\nSorry for the hiatus! I've been busy closing our newest $230M seed-stage venture capital fund, and so now I'm back to making videos again."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "21700",
        "likeCount": "1542",
        "dislikeCount": "9",
        "favoriteCount": "0",
        "commentCount": "126"
      }
    }
  ],
  "pageInfo": {
    "totalResults": 50,
    "resultsPerPage": 50
  }
}

YOUTUBE_VIDEOS_RESPONSE_2 = {
  "kind": "youtube#videoListResponse",
  "etag": "Gn2JCebje43KsXSP5PThl1xXj0U",
  "items": [
    {
      "kind": "youtube#video",
      "etag": "BRP369duqG9YtV4cDnpj3-Ucgk4",
      "id": "bORgMI5-oZE",
      "snippet": {
        "publishedAt": "2020-07-29T14:30:03Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Masterclass: Fighting scammers online (How founders can build Trust and Safety teams)",
        "description": "Steve Kirkham and Eric Levine were the early builders at Airbnb that created the Trust and Safety team. They fought for billions of dollars of transactions and beat scammers day in and day out. Learn how they built that team and how you can use their newest product Berbix to protect your online platforms and build marketplaces like Airbnb that matter. \n\nEvery business in the world is moving online— and the smarter founders can be about building Trust and Safety within their companies, the faster that will happen.\n\nLearn more about berbix at https://berbix.com\n\n00:00 Intro\n01:25 Building Trust and Safety at Airbnb\n03:01 Knowing who you're doing business with is the first step\n04:52 Empathy for fraudsters\n06:08 Verify identity around key points\n07:11 Why the incumbents like Jumio are terrible\n07:48 Why Steve and Eric started Berbix\n09:13 Instant software-only verification unlocks all new marketplaces\n11:03 Fundamental need: Sybil resistance\n11:39 When founders should build Trust & Safety teams\n12:50 Types of bad actors\n14:36 How to thwart bad actors\n16:34 ID checks are a strong deterrent\n17:31 Berbix can help you with your trust and safety problems\n18:11 Advice for future founders\n20:45 Conclusion: You can't do business with people if you don't know who they are!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/bORgMI5-oZE/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/bORgMI5-oZE/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/bORgMI5-oZE/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/bORgMI5-oZE/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/bORgMI5-oZE/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "trust and safety",
          "fighting scammers",
          "scammer",
          "spammer",
          "fighting spam",
          "airbnb trust and safety",
          "berbix",
          "garry tan",
          "y combinator",
          "initialized",
          "initialized capital",
          "eric levine",
          "steve kirkham",
          "masterclass",
          "trust and safety council",
          "trust",
          "suspicious login",
          "phishing",
          "stealing user credentials",
          "online protection",
          "hacking",
          "website hacking",
          "online fraud",
          "fraud prevention",
          "identity theft",
          "identity verification",
          "jumio",
          "start up founders"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: Fighting scammers online (How founders can build Trust and Safety teams)",
          "description": "Steve Kirkham and Eric Levine were the early builders at Airbnb that created the Trust and Safety team. They fought for billions of dollars of transactions and beat scammers day in and day out. Learn how they built that team and how you can use their newest product Berbix to protect your online platforms and build marketplaces like Airbnb that matter. \n\nEvery business in the world is moving online— and the smarter founders can be about building Trust and Safety within their companies, the faster that will happen.\n\nLearn more about berbix at https://berbix.com\n\n00:00 Intro\n01:25 Building Trust and Safety at Airbnb\n03:01 Knowing who you're doing business with is the first step\n04:52 Empathy for fraudsters\n06:08 Verify identity around key points\n07:11 Why the incumbents like Jumio are terrible\n07:48 Why Steve and Eric started Berbix\n09:13 Instant software-only verification unlocks all new marketplaces\n11:03 Fundamental need: Sybil resistance\n11:39 When founders should build Trust & Safety teams\n12:50 Types of bad actors\n14:36 How to thwart bad actors\n16:34 ID checks are a strong deterrent\n17:31 Berbix can help you with your trust and safety problems\n18:11 Advice for future founders\n20:45 Conclusion: You can't do business with people if you don't know who they are!"
        }
      },
      "statistics": {
        "viewCount": "3864",
        "likeCount": "122",
        "dislikeCount": "2",
        "favoriteCount": "0",
        "commentCount": "20"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "0dlksT1VaCyTGlyEhKu5TvK6JFw",
      "id": "6XUZrWzFmIE",
      "snippet": {
        "publishedAt": "2020-07-21T13:13:08Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Micromanagement is toxic: Delegation is the cure (6 simple steps)",
        "description": "One of my direct reports once told me in a 1:1 early in my career as a founder: \"I've never been so disrespected in my career.\" This was a wake-up call. I wasn't delegating, and we weren't aligned on goals and values. I was micromanaging, and second guessing everything. In this video we talk about what it takes to be a great leader: a conductor of people who drive greatness.\n\n00:00 Intro: My management mistake\n00:58 Hire and delegate to get more time\n02:59 How to delegate in 6 simple steps\n04:38 Delegation's shadow: Micromanagement\n05:39 If delegation fails, ask the deeper questions\n06:21 I was a terrible manager at times when I failed to delegate or align\n07:04 Conclusion: Be a conductor of people\n\nMusic - \"JUSTICE\" Instrumental by Homage https://youtu.be/sbZgwPZGPs8\n\n--\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed and seek to be the investor of record at that stage.\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things. Forbes Midas List 2019, 2020 🚀",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/6XUZrWzFmIE/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/6XUZrWzFmIE/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/6XUZrWzFmIE/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/6XUZrWzFmIE/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/6XUZrWzFmIE/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "delegation",
          "micromanagement",
          "leadership",
          "bad manager",
          "manager training",
          "management styles",
          "management skills",
          "management",
          "management stories",
          "teamwork",
          "avoiding micromanagement",
          "first time manager",
          "founder",
          "startup stories",
          "startup",
          "founder lessons",
          "startup lessons",
          "y combinator",
          "garry tan",
          "project management",
          "principles of delegation",
          "what is delegation in management",
          "garry tan initialized capital",
          "venture capital"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Micromanagement is toxic: Delegation is the cure (6 simple steps)",
          "description": "One of my direct reports once told me in a 1:1 early in my career as a founder: \"I've never been so disrespected in my career.\" This was a wake-up call. I wasn't delegating, and we weren't aligned on goals and values. I was micromanaging, and second guessing everything. In this video we talk about what it takes to be a great leader: a conductor of people who drive greatness.\n\n00:00 Intro: My management mistake\n00:58 Hire and delegate to get more time\n02:59 How to delegate in 6 simple steps\n04:38 Delegation's shadow: Micromanagement\n05:39 If delegation fails, ask the deeper questions\n06:21 I was a terrible manager at times when I failed to delegate or align\n07:04 Conclusion: Be a conductor of people\n\nMusic - \"JUSTICE\" Instrumental by Homage https://youtu.be/sbZgwPZGPs8\n\n--\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed and seek to be the investor of record at that stage.\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things. Forbes Midas List 2019, 2020 🚀"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "20500",
        "likeCount": "1487",
        "dislikeCount": "11",
        "favoriteCount": "0",
        "commentCount": "112"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "-Hv9Rl-Qh5UmzHnL835ZwCNy1ro",
      "id": "96HCCnhbKYM",
      "snippet": {
        "publishedAt": "2020-07-14T16:00:09Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Sell? Die? No. Grow profitably. How Ooshma Garg and Gobble did it",
        "description": "Ooshma Garg went from spending more than a million dollars a month to putting nearly that much into her bank account every month. How? She chose to go profitable. Learn about how she found product market fit, how she coped with the implosion of Blue Apron in the public markets, and why Gobble is poised to be the best food technology startup growing profitably and independently today.\n\nWant to try Gobble? Use my referral code for $50 in free Gobble credit — https://www.gobble.com/garry50\n\n00:00 Intro\n01:35 How Gobble got started in 2011\n02:04 First idea: peer to peer lasagna\n03:08 The breakthrough\n03:40 Food tech must nail the food too\n05:13 The secret of long term retention for food IP\n07:50 Easier and tastier = best lifetime value\n08:33 Gobble's 1000 days to innovation: How they got product market fit\n08:50 $6M ARR overnight\n09:47 What to do when a worse competitor raises more money\n11:44 Markets are voting machines early and weighing machines later\n12:15 On burning $1M/mo to earning nearly that much\n13:54 When you can't raise, get profitable\n14:57 Digital ad buying lessons: ROI and market depth\n17:12 You don't know if you can be profitable until you try\n18:36 Default alive gives you control\n20:49 The future of Gobble after profitability: re-investment\n22:56 Great food businesses generate cash\n24:37 Ooshma's hardest lessons she wishes she knew when she was 22\n\n--\n\nHi, I'm Garry Tan — I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/96HCCnhbKYM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/96HCCnhbKYM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/96HCCnhbKYM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/96HCCnhbKYM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/96HCCnhbKYM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "gobble",
          "blue apron",
          "meal kits",
          "food tech",
          "food technology",
          "food startup",
          "profitability",
          "startup lessons",
          "startup war stories",
          "founders",
          "ooshma garg",
          "garry tan",
          "y combinator",
          "startup stories",
          "startup school",
          "default alive",
          "startup advice",
          "low runway",
          "out of runway"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Sell? Die? No. Grow profitably. How Ooshma Garg and Gobble did it",
          "description": "Ooshma Garg went from spending more than a million dollars a month to putting nearly that much into her bank account every month. How? She chose to go profitable. Learn about how she found product market fit, how she coped with the implosion of Blue Apron in the public markets, and why Gobble is poised to be the best food technology startup growing profitably and independently today.\n\nWant to try Gobble? Use my referral code for $50 in free Gobble credit — https://www.gobble.com/garry50\n\n00:00 Intro\n01:35 How Gobble got started in 2011\n02:04 First idea: peer to peer lasagna\n03:08 The breakthrough\n03:40 Food tech must nail the food too\n05:13 The secret of long term retention for food IP\n07:50 Easier and tastier = best lifetime value\n08:33 Gobble's 1000 days to innovation: How they got product market fit\n08:50 $6M ARR overnight\n09:47 What to do when a worse competitor raises more money\n11:44 Markets are voting machines early and weighing machines later\n12:15 On burning $1M/mo to earning nearly that much\n13:54 When you can't raise, get profitable\n14:57 Digital ad buying lessons: ROI and market depth\n17:12 You don't know if you can be profitable until you try\n18:36 Default alive gives you control\n20:49 The future of Gobble after profitability: re-investment\n22:56 Great food businesses generate cash\n24:37 Ooshma's hardest lessons she wishes she knew when she was 22\n\n--\n\nHi, I'm Garry Tan — I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things."
        }
      },
      "statistics": {
        "viewCount": "24282",
        "likeCount": "811",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "78"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "Iq4r0OirPUVFo23qDc-i6i00RDs",
      "id": "zHZxURFgK-4",
      "snippet": {
        "publishedAt": "2020-07-09T14:30:10Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Hans Tung funded 16 unicorns: Billion dollar war stories and hard lessons learned",
        "description": "Hans Tung has funded some of the most iconic billion dollar startups: Bytedance, Airbnb, Wish and Affirm just to name a few. He's been on the Forbes Midas List for 7 years straight now, this year at #10. He has seen it all, and we discuss war stories (don't miss the discussion of Alibaba in a global financial crisis and pandemic!), founders, and the future of food tech, software, and robotics. \n\n00:00 Intro\n00:50 Hans's journey to venture capital\n03:18 Hans met iconic founders very early\n05:00 The Alibaba story in 2003: Pandemic and global financial crisis with 5 months of runway (MUST WATCH)\n08:18 The perseverance of Brian Chesky and Airbnb\n09:23 The perseverance of Peloton\n10:36 Global consumer trends now first emerge outside of the US\n13:06 Software eats the world is a one-time shift\n13:45 Foodtech in China: Darkstores and clean data\n16:14 Global food tech trends: Automation\n17:06 All food distribution will be automated and tech enabled: better, faster, cheaper\n18:20 What Uber did to consolidate taxis (a fragmented, no-tech business) will happen everywhere\n20:19 Software in business: first back-office, then front-office, now whole-office\n21:10 Each successive wave of software is more powerful than the last. We're on the 5th or 6th wave now.\n21:59 On founder-market fit\n23:36 Max Levchin and founder-distribution fit at Slide\n24:16 Platform shifts create distribution opps that open, then close quickly\n25:02 5G and the future of distributed computing\n25:33 Timing your startup idea\n26:22 WeChat as a growth channel for Pinduoduo\n27:55 Autonomy and our robotic future\n28:53 Characteristics of great founders\n31:24 What Hans wishes he knew at 18 or 22\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/zHZxURFgK-4/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/zHZxURFgK-4/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/zHZxURFgK-4/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/zHZxURFgK-4/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/zHZxURFgK-4/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "Hans Tung",
          "GGV",
          "GGV Capital",
          "Venture Capital",
          "startups",
          "alibaba",
          "wish",
          "wechat",
          "global venture capital",
          "global vc",
          "vc",
          "billion dollar startups",
          "unicorns",
          "stanford",
          "ms&e",
          "forbes midas list",
          "midas list",
          "ggv capital",
          "silicon valley",
          "stanford university",
          "venture capital",
          "entrepreneurship",
          "innovation",
          "asian high-tech",
          "entrepreneur",
          "garry tan",
          "brian chesky",
          "max levchin"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Hans Tung funded 16 unicorns: Billion dollar war stories and hard lessons learned",
          "description": "Hans Tung has funded some of the most iconic billion dollar startups: Bytedance, Airbnb, Wish and Affirm just to name a few. He's been on the Forbes Midas List for 7 years straight now, this year at #10. He has seen it all, and we discuss war stories (don't miss the discussion of Alibaba in a global financial crisis and pandemic!), founders, and the future of food tech, software, and robotics. \n\n00:00 Intro\n00:50 Hans's journey to venture capital\n03:18 Hans met iconic founders very early\n05:00 The Alibaba story in 2003: Pandemic and global financial crisis with 5 months of runway (MUST WATCH)\n08:18 The perseverance of Brian Chesky and Airbnb\n09:23 The perseverance of Peloton\n10:36 Global consumer trends now first emerge outside of the US\n13:06 Software eats the world is a one-time shift\n13:45 Foodtech in China: Darkstores and clean data\n16:14 Global food tech trends: Automation\n17:06 All food distribution will be automated and tech enabled: better, faster, cheaper\n18:20 What Uber did to consolidate taxis (a fragmented, no-tech business) will happen everywhere\n20:19 Software in business: first back-office, then front-office, now whole-office\n21:10 Each successive wave of software is more powerful than the last. We're on the 5th or 6th wave now.\n21:59 On founder-market fit\n23:36 Max Levchin and founder-distribution fit at Slide\n24:16 Platform shifts create distribution opps that open, then close quickly\n25:02 5G and the future of distributed computing\n25:33 Timing your startup idea\n26:22 WeChat as a growth channel for Pinduoduo\n27:55 Autonomy and our robotic future\n28:53 Characteristics of great founders\n31:24 What Hans wishes he knew at 18 or 22\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "52450",
        "likeCount": "1936",
        "dislikeCount": "12",
        "favoriteCount": "0",
        "commentCount": "142"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "wnlsGeDxeHtrQoU7BBUgHttNM0k",
      "id": "AVGeA-7WbeQ",
      "snippet": {
        "publishedAt": "2020-06-30T14:30:11Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "The God of the Internet's Law is better than The Golden Rule",
        "description": "Everyone knows the golden rule. But do you know Postel's Law? Postel's Law is the guiding principle of the Internet. The Internet is rewriting everything we know about human relationships, so it's helpful to keep this in mind as we build the future.\n\nThis is how Postel's Law enabled the proto-Internet and set the culture for the future of humanity.\n\n00:00 Intro\n00:37 What is Postel's Law?\n01:39 How Jon Postel became the god of the Internet\n02:03 How did Postel's Law create the Internet as we know it?\n02:57 Postel's Law in Product Design\n04:03 How to apply Postel's Law in your own life (remote work-edition)\n06:00 Postel's Law for managers: How to make a user manual for your own protocols\n05:56 Conclusion: You need Postel's Law in your life\n\nFull transcript at https://blog.garrytan.com/how-postels-law-enabled-the-proto-internet-and-set-the-culture-for-the-future-of-humanity\n\nSongs in this include\n* \"Morrison\" - Instrumental by Homage https://youtu.be/dCZU91MeMLY\n* Varsity by Utah \n* Rhea by 9 Moons\n* Mukanshin by Yume\n* \"My Brother's Keeper\"- Instrumental by Homage https://youtu.be/Zl4XYzkiz88\n* Infinite Sustain by Amaranth Cove\n\n--\n\nHi, I'm Garry Tan — I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/AVGeA-7WbeQ/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/AVGeA-7WbeQ/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/AVGeA-7WbeQ/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/AVGeA-7WbeQ/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/AVGeA-7WbeQ/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "golden rule",
          "internet history",
          "jon postel",
          "tcp/ip",
          "postel's law",
          "startup advice",
          "psychology",
          "strategy",
          "business strategy",
          "remote work",
          "the golden rule",
          "what is the golden rule",
          "do unto others",
          "do unto others as you would have them do unto you",
          "garry tan",
          "gary tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The God of the Internet's Law is better than The Golden Rule",
          "description": "Everyone knows the golden rule. But do you know Postel's Law? Postel's Law is the guiding principle of the Internet. The Internet is rewriting everything we know about human relationships, so it's helpful to keep this in mind as we build the future.\n\nThis is how Postel's Law enabled the proto-Internet and set the culture for the future of humanity.\n\n00:00 Intro\n00:37 What is Postel's Law?\n01:39 How Jon Postel became the god of the Internet\n02:03 How did Postel's Law create the Internet as we know it?\n02:57 Postel's Law in Product Design\n04:03 How to apply Postel's Law in your own life (remote work-edition)\n06:00 Postel's Law for managers: How to make a user manual for your own protocols\n05:56 Conclusion: You need Postel's Law in your life\n\nFull transcript at https://blog.garrytan.com/how-postels-law-enabled-the-proto-internet-and-set-the-culture-for-the-future-of-humanity\n\nSongs in this include\n* \"Morrison\" - Instrumental by Homage https://youtu.be/dCZU91MeMLY\n* Varsity by Utah \n* Rhea by 9 Moons\n* Mukanshin by Yume\n* \"My Brother's Keeper\"- Instrumental by Homage https://youtu.be/Zl4XYzkiz88\n* Infinite Sustain by Amaranth Cove\n\n--\n\nHi, I'm Garry Tan — I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "8334",
        "likeCount": "512",
        "dislikeCount": "5",
        "favoriteCount": "0",
        "commentCount": "44"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "a1LDqDuFMcCVC9QDaTyiPjxpeiI",
      "id": "r8hhvw-1b-M",
      "snippet": {
        "publishedAt": "2020-06-15T14:30:31Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Roll your way to a Startup Unicorn: Lessons for Founders",
        "description": "What does an obscure Japanese video game have to teach us about building a startup that takes over the world? Actually a lot. This is the story of Katamari Damacy which can teach you how to build the next billion dollar startup. \n\nSpoiler: it's about getting customers, colleagues and capital. Rinse and repeat. All you have to do is get rolling.\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/r8hhvw-1b-M/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/r8hhvw-1b-M/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/r8hhvw-1b-M/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/r8hhvw-1b-M/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/r8hhvw-1b-M/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "entrepreneurship",
          "entrepreneur",
          "startup",
          "entrepreneur motivation",
          "startup advice",
          "business startup advice",
          "startup stories",
          "y combinator",
          "y combinator application",
          "startup entrepreneurs",
          "entrepreneurship motivation",
          "entrepreneur advice",
          "business",
          "entrepreneurs",
          "how to start a business",
          "startups",
          "business ideas",
          "business motivation",
          "starting a business",
          "entrepreneur motivational video",
          "business advice",
          "entrepreneur mindset",
          "success",
          "business tips",
          "motivation for entrepreneurs",
          "unicorn"
        ],
        "categoryId": "27",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Roll your way to a Startup Unicorn: Lessons for Founders",
          "description": "What does an obscure Japanese video game have to teach us about building a startup that takes over the world? Actually a lot. This is the story of Katamari Damacy which can teach you how to build the next billion dollar startup. \n\nSpoiler: it's about getting customers, colleagues and capital. Rinse and repeat. All you have to do is get rolling.\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "15891",
        "likeCount": "951",
        "dislikeCount": "11",
        "favoriteCount": "0",
        "commentCount": "97"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "cRkxP_D0CUMLkg9y4xGX0ITRXvU",
      "id": "apP62c8GHiM",
      "snippet": {
        "publishedAt": "2020-05-20T00:00:28Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "How to fight instant pushback to your ideas",
        "description": "You can change the world, but it sometimes comes at a steep cost. It did for Ignaz Semmelweis, the first doctor to discover handwashing saved lives, especially mothers in his maternity ward. He made 5 key mistakes that you can avoid. And in doing so, I hope you'll be able to avoid the insane asylum. \n\nEveryone who makes something new gets hit back quickly with what is known now as the Semmelweis Reflex- people reject anything that challenges them out of hand. You can be prepared to face this. Here's the five ways steps:\n\n0:00 Intro\n01:07 History of Ignaz Semmelweis\n03:05 Speak for yourself\n04:26 Persevere, and don't ragequit\n05:34 Speak truth when you know you're right\n06:09 When they go low, we go high\n07:13 The breakthrough is only half the work\n\nThanks for watching! I made this video last night in about 3 hours- I'm going for a video a week, and my goal is to just help future founders and people who are making things get to the next stage.\n\nPlease like or subscribe to this and as always you can read the full transcript at https://blog.garrytan.com\n\n#nosmallcreator",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/apP62c8GHiM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/apP62c8GHiM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/apP62c8GHiM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/apP62c8GHiM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/apP62c8GHiM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "semmelweis",
          "insane asylum",
          "creator",
          "change the world",
          "communication",
          "science",
          "handwashing",
          "discovery",
          "shunned",
          "entrepreneurship",
          "y combinator",
          "startups",
          "ragequit",
          "speak up",
          "breakthroughs",
          "ignaz semmelweis",
          "semmelweis reflex",
          "rejection",
          "speak for yourself"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to fight instant pushback to your ideas",
          "description": "You can change the world, but it sometimes comes at a steep cost. It did for Ignaz Semmelweis, the first doctor to discover handwashing saved lives, especially mothers in his maternity ward. He made 5 key mistakes that you can avoid. And in doing so, I hope you'll be able to avoid the insane asylum. \n\nEveryone who makes something new gets hit back quickly with what is known now as the Semmelweis Reflex- people reject anything that challenges them out of hand. You can be prepared to face this. Here's the five ways steps:\n\n0:00 Intro\n01:07 History of Ignaz Semmelweis\n03:05 Speak for yourself\n04:26 Persevere, and don't ragequit\n05:34 Speak truth when you know you're right\n06:09 When they go low, we go high\n07:13 The breakthrough is only half the work\n\nThanks for watching! I made this video last night in about 3 hours- I'm going for a video a week, and my goal is to just help future founders and people who are making things get to the next stage.\n\nPlease like or subscribe to this and as always you can read the full transcript at https://blog.garrytan.com\n\n#nosmallcreator"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "9792",
        "likeCount": "775",
        "dislikeCount": "4",
        "favoriteCount": "0",
        "commentCount": "96"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "GPWSapa3G2q50LyLl7-IpA70Fx4",
      "id": "35UOGYlm7F8",
      "snippet": {
        "publishedAt": "2020-05-13T16:00:09Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Reprogram your mind with exec coaching",
        "description": "Is it possible to reprogram your own mind? Yes, it is. Coaching, therapy, meditation, and group work are the keys to unlocking incredible potential. Trauma is a surprising common trait for many founders, and those who can overcome their past can truly build the future. #withme\n\nCameron Yarbrough was my coach for years before starting Torch.io, the best way to get exec coaching. We discuss the problems we've faced over the years, and how you're not alone. There is absolutely a way forward.\n\nI wish I worked on these things 10 years before I did— doing deep work will change your life if you let it. \n\n00:00 Intro\n00:59 Many founders have difficult childhoods that set them on the path to founderhood\n02:19 How difficult pasts can uniquely prepare founders for startup life \n03:26 Cameron was Garry's coach through many difficult periods in his business life \n03:53 Critical moments in your business career are driven by your own mental health \n04:49 Cameron could coach Garry because he had to overcome many of the same challenges \n07:10 Cameron's childhood impact on his early founder experiences \n07:59 Cameron's past leadership challenges drove him to seek therapy, meditation, and coaching\n08:47 It's extremely valuable for repeat founders to do deep internal work before diving back in\n10:05 The horse and rider allegory: Deep work lets the rider can better steer the horse\n10:37 You can work on mental health right now.\n11:01 YOU CAN DO THIS NOW.\n11:46 Executives who get their own mental health right will radiate this health to the whole org.\n12:47 Bibliotherapy\n13:37 Proper deep work is metaprogramming\n14:41 How to get behavior change\n15:25 Regular coaching and therapy enables breakthroughs\n16:45 On finding the right coach or therapist\n17:41 Even coaches have coaches\n18:30 Radical candor enables winning leadership style\n20:12 What is the optimal organization?\n21:04 Your own experience is not universal\n22:02 Leadership and mental health in the time of COVID-19\n24:17 Wartime leadership requires more empathy, not less\n25:01 Cameron’s toughest COVID-19 crisis decision\n26:54 Why coaches are important even if you have lots of friends\n28:13 How to get help in group sessions\n29:46 You are not alone\n\nView the full transcript here: https://blog.garrytan.com/metaprogram-your-own-mind-a-conversation-with-cameron-yarbrough-of-torch-dot-io\n\nTorch.io is an amazing place to get a coach. https://torch.io/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nTORCH SMALL GROUP COACHING https://lp.torch.io/group-coaching/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nLearn more about Radical Candor from Kim Scott in virtual sessions (May 2020): https://lp.torch.io/the-radically-candid-coach/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/35UOGYlm7F8/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/35UOGYlm7F8/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/35UOGYlm7F8/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/35UOGYlm7F8/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/35UOGYlm7F8/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "exec coaching",
          "withme",
          "#withme",
          "coaching",
          "therapy",
          "group therapy",
          "group sessions",
          "counseling",
          "stanford t-group",
          "stanford t-groups",
          "radical candor",
          "kim scott",
          "torch",
          "initialized",
          "garry tan",
          "cameron yarbrough",
          "business coaching",
          "advisor",
          "startup",
          "startup advice",
          "mental health",
          "psychotherapy",
          "meditation"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Reprogram your mind with exec coaching",
          "description": "Is it possible to reprogram your own mind? Yes, it is. Coaching, therapy, meditation, and group work are the keys to unlocking incredible potential. Trauma is a surprising common trait for many founders, and those who can overcome their past can truly build the future. #withme\n\nCameron Yarbrough was my coach for years before starting Torch.io, the best way to get exec coaching. We discuss the problems we've faced over the years, and how you're not alone. There is absolutely a way forward.\n\nI wish I worked on these things 10 years before I did— doing deep work will change your life if you let it. \n\n00:00 Intro\n00:59 Many founders have difficult childhoods that set them on the path to founderhood\n02:19 How difficult pasts can uniquely prepare founders for startup life \n03:26 Cameron was Garry's coach through many difficult periods in his business life \n03:53 Critical moments in your business career are driven by your own mental health \n04:49 Cameron could coach Garry because he had to overcome many of the same challenges \n07:10 Cameron's childhood impact on his early founder experiences \n07:59 Cameron's past leadership challenges drove him to seek therapy, meditation, and coaching\n08:47 It's extremely valuable for repeat founders to do deep internal work before diving back in\n10:05 The horse and rider allegory: Deep work lets the rider can better steer the horse\n10:37 You can work on mental health right now.\n11:01 YOU CAN DO THIS NOW.\n11:46 Executives who get their own mental health right will radiate this health to the whole org.\n12:47 Bibliotherapy\n13:37 Proper deep work is metaprogramming\n14:41 How to get behavior change\n15:25 Regular coaching and therapy enables breakthroughs\n16:45 On finding the right coach or therapist\n17:41 Even coaches have coaches\n18:30 Radical candor enables winning leadership style\n20:12 What is the optimal organization?\n21:04 Your own experience is not universal\n22:02 Leadership and mental health in the time of COVID-19\n24:17 Wartime leadership requires more empathy, not less\n25:01 Cameron’s toughest COVID-19 crisis decision\n26:54 Why coaches are important even if you have lots of friends\n28:13 How to get help in group sessions\n29:46 You are not alone\n\nView the full transcript here: https://blog.garrytan.com/metaprogram-your-own-mind-a-conversation-with-cameron-yarbrough-of-torch-dot-io\n\nTorch.io is an amazing place to get a coach. https://torch.io/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nTORCH SMALL GROUP COACHING https://lp.torch.io/group-coaching/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nLearn more about Radical Candor from Kim Scott in virtual sessions (May 2020): https://lp.torch.io/the-radically-candid-coach/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan"
        }
      },
      "statistics": {
        "viewCount": "13058",
        "likeCount": "522",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "54"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "ijg8dHHByKEauBmhBt70AMSZXu0",
      "id": "hOMZebDXQsU",
      "snippet": {
        "publishedAt": "2020-05-06T17:55:14Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "The $1 TRILLION Artificial Intelligence Problem",
        "description": "Can computers think like humans? Can they learn like us? $1 trillion is spent yearly on tasks a general artificial intelligence can do. D Scott Phoenix of AI startup Vicarious is building exactly that.\n\nSCOTT'S HISTORY\n00:00 Intro\n00:59 How Garry and Scott met\n02:02 How Scott came up with the idea to work on AGI\n\nON ARTIFICIAL GENERAL INTELLIGENCE\n02:41 The time to build AGI is now\n03:10 Why work on AGI?\n04:26 What are the building blocks to building a general AI?\n04:49 What is a human-like learning system?\n06:15 Vicarious vs Deep Learning\n08:08 Traditional AI methods resemble insectoid or reptilian brain approaches\n09:43 New methods and models are more important than more money on training existing models\n11:52 Limits of narrow AI\n12:48 History and origins of the AI debate in philosophy and neuroscience\n14:45 Brute force methods require 14,000 years of training to do what children only need 2 years to learn\n15:28 Lessons from biology\n16:24 How do systems layer to generate more complex behavior?\n17:30 Is an ambitious project like AGI composable and iterable like SaaS software?\n\nON VICARIOUS\n20:01 Long term ambition is great, but what do you do along the way?\n20:38 Vicarious's first applied use case in robotics\n22:16 Vicarious vs other robotics approaches\n23:47 Building learning systems, not one-off point solutions\n\nFOR FUTURE FOUNDERS\n24:51 Advice for builders just starting out\n25:17 How to tackle large problems and ambitious projects\n26:57 Technology is the ultimate lever for humans to create a better world\n29:14 How to be prepared for the long hard road\n\nFull transcript at https://blog.garrytan.com/building-general-ai-with-d-scott-phoenix-founder-of-vicarious\n\nLearn more about Vicarious: https://vicarious.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/hOMZebDXQsU/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/hOMZebDXQsU/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/hOMZebDXQsU/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/hOMZebDXQsU/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/hOMZebDXQsU/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "artificial intelligence",
          "ai",
          "strong ai",
          "general ai",
          "robotics",
          "deep learning",
          "open ai",
          "vicarious",
          "hard tech",
          "hard technology",
          "garry tan",
          "ai debate",
          "machine learning documentary",
          "venture capital",
          "ai startup",
          "how does ai work"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "The $1 TRILLION Artificial Intelligence Problem",
          "description": "Can computers think like humans? Can they learn like us? $1 trillion is spent yearly on tasks a general artificial intelligence can do. D Scott Phoenix of AI startup Vicarious is building exactly that.\n\nSCOTT'S HISTORY\n00:00 Intro\n00:59 How Garry and Scott met\n02:02 How Scott came up with the idea to work on AGI\n\nON ARTIFICIAL GENERAL INTELLIGENCE\n02:41 The time to build AGI is now\n03:10 Why work on AGI?\n04:26 What are the building blocks to building a general AI?\n04:49 What is a human-like learning system?\n06:15 Vicarious vs Deep Learning\n08:08 Traditional AI methods resemble insectoid or reptilian brain approaches\n09:43 New methods and models are more important than more money on training existing models\n11:52 Limits of narrow AI\n12:48 History and origins of the AI debate in philosophy and neuroscience\n14:45 Brute force methods require 14,000 years of training to do what children only need 2 years to learn\n15:28 Lessons from biology\n16:24 How do systems layer to generate more complex behavior?\n17:30 Is an ambitious project like AGI composable and iterable like SaaS software?\n\nON VICARIOUS\n20:01 Long term ambition is great, but what do you do along the way?\n20:38 Vicarious's first applied use case in robotics\n22:16 Vicarious vs other robotics approaches\n23:47 Building learning systems, not one-off point solutions\n\nFOR FUTURE FOUNDERS\n24:51 Advice for builders just starting out\n25:17 How to tackle large problems and ambitious projects\n26:57 Technology is the ultimate lever for humans to create a better world\n29:14 How to be prepared for the long hard road\n\nFull transcript at https://blog.garrytan.com/building-general-ai-with-d-scott-phoenix-founder-of-vicarious\n\nLearn more about Vicarious: https://vicarious.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "18446",
        "likeCount": "674",
        "dislikeCount": "13",
        "favoriteCount": "0",
        "commentCount": "80"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "gwJ_fUeDdLJXnahcJS7odPq0tk0",
      "id": "WK_XwjhJl3k",
      "snippet": {
        "publishedAt": "2020-04-28T23:42:37Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "6 Skills for Successful Startup Founders: Maximize your chances",
        "description": "You can't guarantee success, but you can increase the chances of it by working on these six skills for startup founders. \n\n00:00 Intro\n02:36 Engineering\n04:44 Product\n06:15 Design\n08:16 Sales & Marketing\n08:54 Finance\n10:00 Management and leadership\n\nFull transcript and sign up for email updates at https://blog.garrytan.com\n\nKey resources mentioned\n* Codecademy https://codecademy.com\n* Insight Fellows https://blog.garrytan.com/my-newest-youtube-video-meet-jake-klamka-who-made-insight-fellows-the-worlds-next-applied-graduate-school-for-data-and-engineering \n* Career Karma https://careerkarma.com\n* Garry's YC Startup School: Design Part 1 https://www.youtube.com/watch?v=9urYWGx2uNk and Part 2 https://www.youtube.com/watch?v=O6uREh3G3sQ\n\nMusic 04:44 to 08:16 by Sudo, aka Suhail Doshi https://soundcloud.com/sudo\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/WK_XwjhJl3k/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/WK_XwjhJl3k/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/WK_XwjhJl3k/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/WK_XwjhJl3k/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/WK_XwjhJl3k/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "startup skills",
          "level up",
          "business",
          "life goals",
          "development",
          "engineering",
          "sales",
          "marketing",
          "garry tan",
          "y combinator",
          "startup school",
          "goals",
          "entrepreneurship",
          "quitting your job",
          "how i made it into venture capital",
          "tech startups",
          "startup"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "6 Skills for Successful Startup Founders: Maximize your chances",
          "description": "You can't guarantee success, but you can increase the chances of it by working on these six skills for startup founders. \n\n00:00 Intro\n02:36 Engineering\n04:44 Product\n06:15 Design\n08:16 Sales & Marketing\n08:54 Finance\n10:00 Management and leadership\n\nFull transcript and sign up for email updates at https://blog.garrytan.com\n\nKey resources mentioned\n* Codecademy https://codecademy.com\n* Insight Fellows https://blog.garrytan.com/my-newest-youtube-video-meet-jake-klamka-who-made-insight-fellows-the-worlds-next-applied-graduate-school-for-data-and-engineering \n* Career Karma https://careerkarma.com\n* Garry's YC Startup School: Design Part 1 https://www.youtube.com/watch?v=9urYWGx2uNk and Part 2 https://www.youtube.com/watch?v=O6uREh3G3sQ\n\nMusic 04:44 to 08:16 by Sudo, aka Suhail Doshi https://soundcloud.com/sudo\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "85774",
        "likeCount": "4235",
        "dislikeCount": "32",
        "favoriteCount": "0",
        "commentCount": "325"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "XckIDnLuVs3OoznUDEGaN8JD5h4",
      "id": "F-r6jL-oFXE",
      "snippet": {
        "publishedAt": "2020-04-22T14:28:34Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Timebox your way to startup product market fit",
        "description": "If at first you don't succeed, try try again. Then quit. It's no use being a damn fool about it. WC Fields said that, and that's true for startups and founders as they make decisions about what customers to pursue, and what problems to solve. These are all linked. In this video we cover concrete ways to use timeboxing to improve your chances of finding startup gold.\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/F-r6jL-oFXE/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/F-r6jL-oFXE/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/F-r6jL-oFXE/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/F-r6jL-oFXE/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/F-r6jL-oFXE/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "lean startup",
          "management",
          "leadership",
          "decision making",
          "project management",
          "intrapraneurship",
          "product market fit",
          "customers",
          "user driven design",
          "user centered design",
          "founder",
          "mistake",
          "startup failure",
          "garry tan",
          "gary tan",
          "my way",
          "timebox",
          "timeboxing"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Timebox your way to startup product market fit",
          "description": "If at first you don't succeed, try try again. Then quit. It's no use being a damn fool about it. WC Fields said that, and that's true for startups and founders as they make decisions about what customers to pursue, and what problems to solve. These are all linked. In this video we cover concrete ways to use timeboxing to improve your chances of finding startup gold.\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "31100",
        "likeCount": "1706",
        "dislikeCount": "21",
        "favoriteCount": "0",
        "commentCount": "152"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "oU-TbKaz9qfsykRcpuTS_Q32rmg",
      "id": "ZjTaFB3mo5w",
      "snippet": {
        "publishedAt": "2020-04-14T14:15:00Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Masterclass: Remote work and management with empathy (Part 2)",
        "description": "How do you build a fast growing startup team that builds a world-class product? This is part two, focusing on management with empathy and how to build an organic remote work culture from scratch that is truly collaborative. \n\nHere is what we cover in this masterclass:\n\n00:00 Intro\n01:00 Managing culture remotely\n01:54 Weekly check-ins and 1:1s with leadership\n02:29 Remote work requires trust by default\n03:45 How to run a 1:1 remotely\n04:52 Forming an organic remote culture\n08:53 How to communicate values\n11:39 How to handle management when things go wrong\n13:17 How to handle performance issues\n15:02 How to handle misalignment in values\n16:39 Rewards and recognition\n17:42 Approaching diversity and inclusion\n19:52 Remote work in the time of coronavirus\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com/masterclass-on-remote-work-management-with-empathy-fostering-a-values-based-culture-part-2-of-2\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/ZjTaFB3mo5w/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/ZjTaFB3mo5w/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/ZjTaFB3mo5w/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/ZjTaFB3mo5w/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/ZjTaFB3mo5w/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "recruiting",
          "remote work",
          "agile",
          "startup",
          "upwork",
          "hiring engineers",
          "engineering",
          "software engineering",
          "interviewing",
          "interviews",
          "garry tan",
          "y combinator"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: Remote work and management with empathy (Part 2)",
          "description": "How do you build a fast growing startup team that builds a world-class product? This is part two, focusing on management with empathy and how to build an organic remote work culture from scratch that is truly collaborative. \n\nHere is what we cover in this masterclass:\n\n00:00 Intro\n01:00 Managing culture remotely\n01:54 Weekly check-ins and 1:1s with leadership\n02:29 Remote work requires trust by default\n03:45 How to run a 1:1 remotely\n04:52 Forming an organic remote culture\n08:53 How to communicate values\n11:39 How to handle management when things go wrong\n13:17 How to handle performance issues\n15:02 How to handle misalignment in values\n16:39 Rewards and recognition\n17:42 Approaching diversity and inclusion\n19:52 Remote work in the time of coronavirus\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com/masterclass-on-remote-work-management-with-empathy-fostering-a-values-based-culture-part-2-of-2\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "1901",
        "likeCount": "60",
        "dislikeCount": "0",
        "favoriteCount": "0",
        "commentCount": "11"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "ReQEXwReB4h343kpRGi4YLl9WqE",
      "id": "2gccAOuGRdU",
      "snippet": {
        "publishedAt": "2020-04-07T14:57:07Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Should you work on that startup idea?",
        "description": "It's not enough to be pointed at a particular direction. You need to have a very clear reason why you will win. This is a list of questions you should ask yourself before picking a startup idea. \n\nWeak answers to these questions might mean you need to change ideas... on the other hand, they focus you to figure out the things you need to solve so your startup becomes a winning one. \n\nRead the full transcript at https://blog.garrytan.com/should-you-work-on-that-startup-idea-ask-why-me-why-now\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/2gccAOuGRdU/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/2gccAOuGRdU/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/2gccAOuGRdU/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/2gccAOuGRdU/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/2gccAOuGRdU/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "startup ideas",
          "garry tan",
          "y combinator",
          "initialized",
          "why now",
          "moats",
          "competitive advantage"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Should you work on that startup idea?",
          "description": "It's not enough to be pointed at a particular direction. You need to have a very clear reason why you will win. This is a list of questions you should ask yourself before picking a startup idea. \n\nWeak answers to these questions might mean you need to change ideas... on the other hand, they focus you to figure out the things you need to solve so your startup becomes a winning one. \n\nRead the full transcript at https://blog.garrytan.com/should-you-work-on-that-startup-idea-ask-why-me-why-now\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "41239",
        "likeCount": "1858",
        "dislikeCount": "17",
        "favoriteCount": "0",
        "commentCount": "108"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "uhz3Kj1IkPJ9iY45cD65TRX6NqE",
      "id": "3pP5RJ-dE74",
      "snippet": {
        "publishedAt": "2020-04-01T14:56:06Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Masterclass: How to build a fast growing startup team entirely remotely (Part 1)",
        "description": "How do you build a fast growing startup team that builds a world-class product? \n\nHere is what we cover in this masterclass:\n00:00 Intro\n01:46 Meet Shogun, the e-commerce page builder\n02:58 They started fully remote as a side project that became profitable\n04:23 The Tools: Hardware and software that enables remote work\n07:40 How to do learning & development\n08:29 On recruiting for remote teams\n11:35 Remote recruiting process\n19:43 How to legally hire remote candidates\n22:05 Onboarding remote employees\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nThis is part one. Part two gets posted next week! Stay tuned.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/3pP5RJ-dE74/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/3pP5RJ-dE74/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/3pP5RJ-dE74/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/3pP5RJ-dE74/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/3pP5RJ-dE74/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "recruiting",
          "remote work",
          "agile",
          "startup",
          "upwork",
          "hiring engineers",
          "engineering",
          "software engineering",
          "interviewing",
          "interviews",
          "garry tan",
          "masterclass"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: How to build a fast growing startup team entirely remotely (Part 1)",
          "description": "How do you build a fast growing startup team that builds a world-class product? \n\nHere is what we cover in this masterclass:\n00:00 Intro\n01:46 Meet Shogun, the e-commerce page builder\n02:58 They started fully remote as a side project that became profitable\n04:23 The Tools: Hardware and software that enables remote work\n07:40 How to do learning & development\n08:29 On recruiting for remote teams\n11:35 Remote recruiting process\n19:43 How to legally hire remote candidates\n22:05 Onboarding remote employees\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nThis is part one. Part two gets posted next week! Stay tuned.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "5708",
        "likeCount": "157",
        "dislikeCount": "2",
        "favoriteCount": "0",
        "commentCount": "12"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "JJvihsbfHZOqaY5QY8Y5O6joAfo",
      "id": "hd9DD4t85fM",
      "snippet": {
        "publishedAt": "2020-03-25T15:48:26Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Software engineer to product manager? How?",
        "description": "What’s up everyone? I thought I would do some questions from the community. Here’s a quick sample of what we talk about:\n\n🤹\u200d♀️ How does an engineer switch to a product role?\n📸 Google vs Apple vs Amazon \n💸 How D2C requires intense focus on CAC\n\nStill stuck at home in San Francisco. We are all going through a lot. But I believe in us.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/hd9DD4t85fM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/hd9DD4t85fM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/hd9DD4t85fM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/hd9DD4t85fM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/hd9DD4t85fM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "design",
          "product management",
          "engineering",
          "software engineering",
          "apple",
          "google",
          "amazon",
          "startups",
          "founders",
          "career planning",
          "garry tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Software engineer to product manager? How?",
          "description": "What’s up everyone? I thought I would do some questions from the community. Here’s a quick sample of what we talk about:\n\n🤹\u200d♀️ How does an engineer switch to a product role?\n📸 Google vs Apple vs Amazon \n💸 How D2C requires intense focus on CAC\n\nStill stuck at home in San Francisco. We are all going through a lot. But I believe in us."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "11079",
        "likeCount": "438",
        "dislikeCount": "4",
        "favoriteCount": "0",
        "commentCount": "54"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "iif2k8OPpVjS-bg55hyHyx4i0Ag",
      "id": "FcjbpX7cnEI",
      "snippet": {
        "publishedAt": "2020-03-19T02:46:53Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "How to get the absolute best telework setup—Elgato Camlink 4K + Sigma 35mm f1.2 for Zoom Tutorial",
        "description": "Here's a basic tutorial on how to use a Sony mirrorless camera as your main webcam setup for Zoom. We also sit down with the creator and founder of new teleconferencing startup Around, an Initialized portfolio company. They just launched today and have a brand new approach compared to Zoom. \n\nSign up for early access: https://www.around.co\n\nApril 2020 update: Canon camera owners may not need to buy a capture card. If you already have a newer Canon camera you can just download this utility (PC only sadly) - http://canon.us/livestream \n\nHere are my recommendations:\n\nCAMERA\nBudget option - Sony A6100 - https://amzn.to/2QtLSAj\nMid-level option - Sony A6600 — https://amzn.to/2WogMO3\nLuxury option - Sony A7R4 — https://amzn.to/2waHEXr\n\nLENS\nBudget - Sigma 16mm f1.4 for Sony E mount https://amzn.to/2xNPZR3\nLuxury option - Sigma 35mm f1.2 for Sony E mount\n\nCAPTURE CARD\nElgato Camlink 4k (Sold out on Amazon as of today but you can find on eBay or direct from Elgato)\n\nElgato HD60 S+ also works and is still in stock\n\nChert 4KC works fine on PC but flickers on Mac\n\nOTHER THINGS YOU NEED\nGonine AC dummy battery and power supply (works with either Sony A6600 or A7R4 camera above) — https://www.amazon.com/gp/product/B07D5V8KY5/ref=ppx_yo_dt_b_search_asin_title\n\nDummy Battery for budget option Sony A6100 — https://amzn.to/2WuUY3E\n\nCheap Tripod — https://amzn.to/3bdY0gN\n6 ft AmazonBasics Micro HDMI to HDMI cable — https://amzn.to/2vBoZDM\n\n(NOTE: As an Amazon Associate I earn from qualifying purchases.)\n\nHi, I'm Garry Tan. I'm a venture capitalist (Forbes Midas List #21 for 2019) and early investor in startups like Coinbase and Instacart. This is my one man run and gun channel— everything you see is filmed, edited, and posted by yours truly.\n\nYou can follow me on Twitter at https://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/FcjbpX7cnEI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/FcjbpX7cnEI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/FcjbpX7cnEI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/FcjbpX7cnEI/sddefault.jpg",
            "width": 640,
            "height": 480
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "telecommuting",
          "telework",
          "zoom",
          "camera",
          "webcam",
          "garry tan",
          "camlink",
          "elgato cam link 4k",
          "gary tan",
          "cam link 4k",
          "elgato camlink",
          "camlink 4k",
          "elgato cam link",
          "elgato cam link zoom",
          "a7r4",
          "elgato camlink 4k",
          "sony a6100",
          "sony a6600",
          "sony a7r4",
          "cam link",
          "cam link 4k setup",
          "best zoom setup"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to get the absolute best telework setup—Elgato Camlink 4K + Sigma 35mm f1.2 for Zoom Tutorial",
          "description": "Here's a basic tutorial on how to use a Sony mirrorless camera as your main webcam setup for Zoom. We also sit down with the creator and founder of new teleconferencing startup Around, an Initialized portfolio company. They just launched today and have a brand new approach compared to Zoom. \n\nSign up for early access: https://www.around.co\n\nApril 2020 update: Canon camera owners may not need to buy a capture card. If you already have a newer Canon camera you can just download this utility (PC only sadly) - http://canon.us/livestream \n\nHere are my recommendations:\n\nCAMERA\nBudget option - Sony A6100 - https://amzn.to/2QtLSAj\nMid-level option - Sony A6600 — https://amzn.to/2WogMO3\nLuxury option - Sony A7R4 — https://amzn.to/2waHEXr\n\nLENS\nBudget - Sigma 16mm f1.4 for Sony E mount https://amzn.to/2xNPZR3\nLuxury option - Sigma 35mm f1.2 for Sony E mount\n\nCAPTURE CARD\nElgato Camlink 4k (Sold out on Amazon as of today but you can find on eBay or direct from Elgato)\n\nElgato HD60 S+ also works and is still in stock\n\nChert 4KC works fine on PC but flickers on Mac\n\nOTHER THINGS YOU NEED\nGonine AC dummy battery and power supply (works with either Sony A6600 or A7R4 camera above) — https://www.amazon.com/gp/product/B07D5V8KY5/ref=ppx_yo_dt_b_search_asin_title\n\nDummy Battery for budget option Sony A6100 — https://amzn.to/2WuUY3E\n\nCheap Tripod — https://amzn.to/3bdY0gN\n6 ft AmazonBasics Micro HDMI to HDMI cable — https://amzn.to/2vBoZDM\n\n(NOTE: As an Amazon Associate I earn from qualifying purchases.)\n\nHi, I'm Garry Tan. I'm a venture capitalist (Forbes Midas List #21 for 2019) and early investor in startups like Coinbase and Instacart. This is my one man run and gun channel— everything you see is filmed, edited, and posted by yours truly.\n\nYou can follow me on Twitter at https://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "17754",
        "likeCount": "400",
        "dislikeCount": "22",
        "favoriteCount": "0",
        "commentCount": "69"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "yqF62gYWUturZZPXqFzI5s5mV1c",
      "id": "idyfHs3DWmc",
      "snippet": {
        "publishedAt": "2020-03-11T15:17:14Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Masterclass: How to sell to 20M software developers with an amazing onboarding experience",
        "description": "The world is being eaten by software, and that makes selling to software engineers the one thing to nail for all infrastructure startups. Come learn from Sylvain Utard, VP of Engineering from Algolia, and how they created one of the world's best developer onboarding experiences. \n\n00:00 Intro\n01:15 Algolia is realtime fast customizable search, both backend and frontend\n02:02 Tech giants can put 100s of devs on search. Algolia democratizes that.\n03:34 Walking through the developer experience\n03:45 The homepage\n04:01 Evolving from self-serve to enterprise\n04:16 Even if you sell by enterprise, showing value to developers immediately is still key\n05:51 Garry's magical experience using Algolia\n06:12 The docs drive you directly to the tutorial: Interactive is better\n09:53 Design tip: Strong call to actions\n10:42 Developer docs are only effective when they make devs feel powerful\n11:56 How did Algolia implement their interactive developer docs?\n13:01 How to support 9 runtimes? 15 engineers out of 100 are devoted to dev xp\n14:43 Low bar high ceiling: the holy grail for products and services\n16:56 How Sylvain got into tech\n18:56 When your competition is 20 year old tech: Lucene and Sphinx\n20:05 Career advice for engineers starting out\n20:50 Algolia is hiring for remote and roles in SF and Paris\n\nFull transcript available at https://blog.garrytan.com/\n\nThanks to Sylvain and the whole Algolia team for making an amazing product and coming to sit with me.\n\nYou can follow Sylvain at https://twitter.com/sylvainutard\nVisit https://algolia.com to try their world-class search product\n\nYou can follow me on Twitter at https://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/idyfHs3DWmc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/idyfHs3DWmc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/idyfHs3DWmc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/idyfHs3DWmc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/idyfHs3DWmc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "developers",
          "software developers",
          "software engineering",
          "engineering",
          "cloud",
          "saas",
          "startups",
          "venture capital",
          "masterclass",
          "vp of engineering",
          "dev docs",
          "ruby",
          "rails",
          "python",
          "java",
          "react",
          "search",
          "lucene",
          "algolia",
          "sphinx",
          "full text search",
          "garry tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Masterclass: How to sell to 20M software developers with an amazing onboarding experience",
          "description": "The world is being eaten by software, and that makes selling to software engineers the one thing to nail for all infrastructure startups. Come learn from Sylvain Utard, VP of Engineering from Algolia, and how they created one of the world's best developer onboarding experiences. \n\n00:00 Intro\n01:15 Algolia is realtime fast customizable search, both backend and frontend\n02:02 Tech giants can put 100s of devs on search. Algolia democratizes that.\n03:34 Walking through the developer experience\n03:45 The homepage\n04:01 Evolving from self-serve to enterprise\n04:16 Even if you sell by enterprise, showing value to developers immediately is still key\n05:51 Garry's magical experience using Algolia\n06:12 The docs drive you directly to the tutorial: Interactive is better\n09:53 Design tip: Strong call to actions\n10:42 Developer docs are only effective when they make devs feel powerful\n11:56 How did Algolia implement their interactive developer docs?\n13:01 How to support 9 runtimes? 15 engineers out of 100 are devoted to dev xp\n14:43 Low bar high ceiling: the holy grail for products and services\n16:56 How Sylvain got into tech\n18:56 When your competition is 20 year old tech: Lucene and Sphinx\n20:05 Career advice for engineers starting out\n20:50 Algolia is hiring for remote and roles in SF and Paris\n\nFull transcript available at https://blog.garrytan.com/\n\nThanks to Sylvain and the whole Algolia team for making an amazing product and coming to sit with me.\n\nYou can follow Sylvain at https://twitter.com/sylvainutard\nVisit https://algolia.com to try their world-class search product\n\nYou can follow me on Twitter at https://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "11385",
        "likeCount": "323",
        "dislikeCount": "4",
        "favoriteCount": "0",
        "commentCount": "29"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "z4nrdDEMYTss8U_FgngBWX0R6FY",
      "id": "dhnzBQjsgGU",
      "snippet": {
        "publishedAt": "2020-03-04T14:42:53Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Stories from China Tech that don't get reported: A conversation with Rui Ma of Tech Buzz China",
        "description": "Table of contents with timestamps below. There's a large divide between Chinese Tech and the rest of the world. Co-Host of Tech Buzz China, Rui Ma, is an old friend of mine who I interviewed since she is one of my first resources when trying to understand all things China. \n\n00:00 Intro\n00:30 Meet Rui Ma, Co-host of Tech Buzz China\n00:58 Tech in the West doesn't pay enough attention to China\n01:18 Why Rui started Tech Buzz China\n02:13 Luckin Coffee is an example of US tech media getting it wrong\n02:43 Media's false equivalence: Luckin = Starbucks\n03:39 Smartphone growth drove more greenfield new consumer cos in China than US\n04:20 Mobile drove growth in China- Now that growth is over\n04:55 What do founders in the West need to understand about China tech?\n05:23 2 China Tech narratives - they're the future or they steal the future\n07:02 US companies can't walk and chew gum. Chinese tech cos build super apps.\n07:48 Why superapps? WeChat and Meituan are transaction oriented\n08:51 Superapps provide superior UX\n09:12 Obligatory coronavirus discussion\n10:02 Economic output is still only 1/6th of normal levels as China \n10:42 The markets didn't catch up to COVID-19 until the last week of Feb, months later\n11:24 Money-printing will help large firms but not SMBs who need it the most\n12:15 Chinese gov't has asked landlords and lenders to forgive payments\n12:31 Chinese cos with software automation were least hit by the COVID-19 lockdown\n13:29 Ant Financial's Supply Chain Blockchain is one of the largest production blockchains in use\n14:48 Covid 19's impact on remote work\n15:31 China's 996 Work Schedule: 9AM to 9PM, 6 Days a Week\n16:45 On-demand is far more viable in densely urban China\n17:10 Surprising: China's on-demand economy can't hire enough blue collar service workers\n17:34 Even China can't hire enough workers, so they are turning to robotic automation\n18:17 Nvidia mobile GPU chips are a key \"Why Now?\" for robotics\n18:37 Freedom Robotics is building the telematics/analytics platform that lets fleets manage 1000s of robots\n18:52 Robotics is going to see its iPhone moment thanks to realtime computer vision + GPU compute\n19:21 On EHang (autonomous air taxi)\n20:27 How China deals with hard tech startups \"PPT\" deals\n21:53 In China, the govt will define entire tech waves for investment\n23:27 How are China's bike share companies doing in 2020?\n25:02 As a Chinese startup, do you have to be allies with Alibaba or Tencent?\n26:14 A startup's product market fit sometimes becomes just a tech giant's moat\n26:36 How Chinese megatech cos invest in startups\n27:19 How can US startups enter China?\n28:00 How to build a presence in China? Just go.\n28:19 Top tier cities are like Manhattan, and not representative of the whole country\n28:49 Over 300 cities with more than 1M people\n30:09 City dwellers make up over 1B of 1.4B people\n30:24 Rural China is over 400M people\n31:20 Qutoutiao pays rural Chinese users pennies per day to use it for even an hour\n\nFull transcript of this interview is also available on my blog here:\nhttps://blog.garrytan.com/stories-from-china-tech-that-dont-get-reported-a-conversation-with-rui-ma-of-tech-buzz-china\n\nSubscribe to Tech Buzz China here: https://techbuzzchina.com\n\nYou should follow Rui Ma on Twitter here: https://twitter.com/ruima",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dhnzBQjsgGU/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dhnzBQjsgGU/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dhnzBQjsgGU/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dhnzBQjsgGU/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dhnzBQjsgGU/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "china tech",
          "china",
          "china technology",
          "china economy",
          "alibaba stock",
          "luckin coffee",
          "meituan dianping",
          "luckin",
          "qutoutiao",
          "rui ma"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Stories from China Tech that don't get reported: A conversation with Rui Ma of Tech Buzz China",
          "description": "Table of contents with timestamps below. There's a large divide between Chinese Tech and the rest of the world. Co-Host of Tech Buzz China, Rui Ma, is an old friend of mine who I interviewed since she is one of my first resources when trying to understand all things China. \n\n00:00 Intro\n00:30 Meet Rui Ma, Co-host of Tech Buzz China\n00:58 Tech in the West doesn't pay enough attention to China\n01:18 Why Rui started Tech Buzz China\n02:13 Luckin Coffee is an example of US tech media getting it wrong\n02:43 Media's false equivalence: Luckin = Starbucks\n03:39 Smartphone growth drove more greenfield new consumer cos in China than US\n04:20 Mobile drove growth in China- Now that growth is over\n04:55 What do founders in the West need to understand about China tech?\n05:23 2 China Tech narratives - they're the future or they steal the future\n07:02 US companies can't walk and chew gum. Chinese tech cos build super apps.\n07:48 Why superapps? WeChat and Meituan are transaction oriented\n08:51 Superapps provide superior UX\n09:12 Obligatory coronavirus discussion\n10:02 Economic output is still only 1/6th of normal levels as China \n10:42 The markets didn't catch up to COVID-19 until the last week of Feb, months later\n11:24 Money-printing will help large firms but not SMBs who need it the most\n12:15 Chinese gov't has asked landlords and lenders to forgive payments\n12:31 Chinese cos with software automation were least hit by the COVID-19 lockdown\n13:29 Ant Financial's Supply Chain Blockchain is one of the largest production blockchains in use\n14:48 Covid 19's impact on remote work\n15:31 China's 996 Work Schedule: 9AM to 9PM, 6 Days a Week\n16:45 On-demand is far more viable in densely urban China\n17:10 Surprising: China's on-demand economy can't hire enough blue collar service workers\n17:34 Even China can't hire enough workers, so they are turning to robotic automation\n18:17 Nvidia mobile GPU chips are a key \"Why Now?\" for robotics\n18:37 Freedom Robotics is building the telematics/analytics platform that lets fleets manage 1000s of robots\n18:52 Robotics is going to see its iPhone moment thanks to realtime computer vision + GPU compute\n19:21 On EHang (autonomous air taxi)\n20:27 How China deals with hard tech startups \"PPT\" deals\n21:53 In China, the govt will define entire tech waves for investment\n23:27 How are China's bike share companies doing in 2020?\n25:02 As a Chinese startup, do you have to be allies with Alibaba or Tencent?\n26:14 A startup's product market fit sometimes becomes just a tech giant's moat\n26:36 How Chinese megatech cos invest in startups\n27:19 How can US startups enter China?\n28:00 How to build a presence in China? Just go.\n28:19 Top tier cities are like Manhattan, and not representative of the whole country\n28:49 Over 300 cities with more than 1M people\n30:09 City dwellers make up over 1B of 1.4B people\n30:24 Rural China is over 400M people\n31:20 Qutoutiao pays rural Chinese users pennies per day to use it for even an hour\n\nFull transcript of this interview is also available on my blog here:\nhttps://blog.garrytan.com/stories-from-china-tech-that-dont-get-reported-a-conversation-with-rui-ma-of-tech-buzz-china\n\nSubscribe to Tech Buzz China here: https://techbuzzchina.com\n\nYou should follow Rui Ma on Twitter here: https://twitter.com/ruima"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "7500",
        "likeCount": "265",
        "dislikeCount": "6",
        "favoriteCount": "0",
        "commentCount": "41"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "iBvF2wk5rWUx_B-laCUuJ7E8Qk8",
      "id": "BJo_7-Rn1wc",
      "snippet": {
        "publishedAt": "2020-02-28T14:48:44Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "How to sell your startup for millions",
        "description": "When and how should you sell your startup? M&A (mergers & acquisitions) are always a high stress process, and there are a lot of things you have to consider when you are trying to get a startup exit executed in the right way.\n\nRead Paul Graham's essay on Default Alive vs Default Dead here: http://www.paulgraham.com/aord.html\n\nGarry's startup Posterous was sold to Twitter for $20M and Andrew Lee's startup was sold to Zynga too. As venture capitalists today we work with more than a hundred startups from the earliest possible stage to now a total market value of over $36 billion. We've seen nearly every kind of startup exit and regularly try to help founders as they navigate these problems.\n\nThis short video encompasses a lot of the advice we end up giving frequently to our community.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/BJo_7-Rn1wc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/BJo_7-Rn1wc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/BJo_7-Rn1wc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/BJo_7-Rn1wc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/BJo_7-Rn1wc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "how to sell your startup for millions",
          "startup",
          "paul graham"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to sell your startup for millions",
          "description": "When and how should you sell your startup? M&A (mergers & acquisitions) are always a high stress process, and there are a lot of things you have to consider when you are trying to get a startup exit executed in the right way.\n\nRead Paul Graham's essay on Default Alive vs Default Dead here: http://www.paulgraham.com/aord.html\n\nGarry's startup Posterous was sold to Twitter for $20M and Andrew Lee's startup was sold to Zynga too. As venture capitalists today we work with more than a hundred startups from the earliest possible stage to now a total market value of over $36 billion. We've seen nearly every kind of startup exit and regularly try to help founders as they navigate these problems.\n\nThis short video encompasses a lot of the advice we end up giving frequently to our community."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "52431",
        "likeCount": "1675",
        "dislikeCount": "14",
        "favoriteCount": "0",
        "commentCount": "88"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "jNaMPe8DTpqMC8UrhiXUmxdgbj8",
      "id": "btRHm7z61oo",
      "snippet": {
        "publishedAt": "2020-02-21T14:34:24Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Drowning in email is the first sign you aren't scaling.",
        "description": "If you're too busy to answer email then that's the first sign you aren't scaling. Here's what you need to do get out of that mess.\n\nSurprisingly what got you here won't get you there. You need to be open and creative early on, but at some point every startup switches over to a focus on scaling and conscientiousness. \n\nThe link to the 2004 Bucknell Study is here — it turns out openness is anti-correlated with long term startup success, while conscientiousness predictably is positively correlated. \nhttps://www.sciencedirect.com/science/article/abs/pii/S0883902603000934\n\nFull transcript of this video here:\nhttps://blog.garrytan.com/email-overload-is-the-first-sign-you-need-to-scale-yourself",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/btRHm7z61oo/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/btRHm7z61oo/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/btRHm7z61oo/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/btRHm7z61oo/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/btRHm7z61oo/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "management",
          "productivity",
          "founder",
          "garry tan",
          "gary tan",
          "email"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Drowning in email is the first sign you aren't scaling.",
          "description": "If you're too busy to answer email then that's the first sign you aren't scaling. Here's what you need to do get out of that mess.\n\nSurprisingly what got you here won't get you there. You need to be open and creative early on, but at some point every startup switches over to a focus on scaling and conscientiousness. \n\nThe link to the 2004 Bucknell Study is here — it turns out openness is anti-correlated with long term startup success, while conscientiousness predictably is positively correlated. \nhttps://www.sciencedirect.com/science/article/abs/pii/S0883902603000934\n\nFull transcript of this video here:\nhttps://blog.garrytan.com/email-overload-is-the-first-sign-you-need-to-scale-yourself"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "2438",
        "likeCount": "115",
        "dislikeCount": "2",
        "favoriteCount": "0",
        "commentCount": "11"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "KQGz_Q6S5JiWfTx5AnPvjemxdc0",
      "id": "emoauFlhXA0",
      "snippet": {
        "publishedAt": "2020-02-12T13:45:01Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "How to bend a spoon with your mind",
        "description": "Supreme alchemy is manifestation of ideas into reality. Biggie Smalls did it, and so can you. What can we learn from Spoon Bending parties? What role does luck play in getting to our destination?\n\nRead the full transcript and subscribe to the blog here: https://blog.garrytan.com/how-to-bend-a-spoon-with-your-mind\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. Just the stuff I think is interesting or valuable for mainly founders and future founders.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nYou can follow me on Twitter here: https://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/emoauFlhXA0/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/emoauFlhXA0/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/emoauFlhXA0/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/emoauFlhXA0/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/emoauFlhXA0/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "manifestation",
          "supreme alchemy",
          "spoon bending",
          "the matrix",
          "bend reality",
          "how to bend reality to your will",
          "garry tan",
          "how to bend"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to bend a spoon with your mind",
          "description": "Supreme alchemy is manifestation of ideas into reality. Biggie Smalls did it, and so can you. What can we learn from Spoon Bending parties? What role does luck play in getting to our destination?\n\nRead the full transcript and subscribe to the blog here: https://blog.garrytan.com/how-to-bend-a-spoon-with-your-mind\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. Just the stuff I think is interesting or valuable for mainly founders and future founders.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nYou can follow me on Twitter here: https://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "10073",
        "likeCount": "393",
        "dislikeCount": "29",
        "favoriteCount": "0",
        "commentCount": "49"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "_Dri85Om7nKbDsD1nRXuYEiv6GY",
      "id": "pGbWynzWauQ",
      "snippet": {
        "publishedAt": "2020-02-05T15:00:25Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "What to do when someone steals your idea",
        "description": "Someone stole your idea. What do you do next? if you execute faster, you go to market faster, you're innovating and running out ahead of everyone else, then even the people who are trying to copy you can only copy the version of you from six or nine months ago. That's what it takes. You can beat the copycats, but you've got to outrun them.\n\nFull transcript here: https://blog.garrytan.com/what-to-do-when-someone-steals-your-idea\n\n\nHi, I'm Garry Tan and I do this YouTube channel for fun in my spare time. By day, I am a venture capitalist who started Initialized Capital, an early stage investor with over $500M in assets under management. We were the earliest investors in Coinbase, Instacart and 100+ more startups now worth over $36B in startup market cap so far. \n\nForbes Midas List 2019 #21 🚀\n\nYou can follow me on Twitter here: https://twitter.com/garrytan",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/pGbWynzWauQ/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/pGbWynzWauQ/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/pGbWynzWauQ/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/pGbWynzWauQ/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/pGbWynzWauQ/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "ideas",
          "entrepreneurship",
          "ycombinator",
          "garry tan",
          "when someone stole youre business idea"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "What to do when someone steals your idea",
          "description": "Someone stole your idea. What do you do next? if you execute faster, you go to market faster, you're innovating and running out ahead of everyone else, then even the people who are trying to copy you can only copy the version of you from six or nine months ago. That's what it takes. You can beat the copycats, but you've got to outrun them.\n\nFull transcript here: https://blog.garrytan.com/what-to-do-when-someone-steals-your-idea\n\n\nHi, I'm Garry Tan and I do this YouTube channel for fun in my spare time. By day, I am a venture capitalist who started Initialized Capital, an early stage investor with over $500M in assets under management. We were the earliest investors in Coinbase, Instacart and 100+ more startups now worth over $36B in startup market cap so far. \n\nForbes Midas List 2019 #21 🚀\n\nYou can follow me on Twitter here: https://twitter.com/garrytan"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "30931",
        "likeCount": "1515",
        "dislikeCount": "14",
        "favoriteCount": "0",
        "commentCount": "105"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "eACKq8SvcHpqhg3OpaQsjAqrbUs",
      "id": "lIlguzWxEiI",
      "snippet": {
        "publishedAt": "2020-01-29T15:00:08Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Parker Conrad's Billion Dollar Lessons on Remote Work and Customer Support (Part 2)",
        "description": "\"Remote work is the worst way to build a company, except for all the others.\" —Parker Conrad\n\nSee part 1 of the video here: https://www.youtube.com/watch?v=M93xlPxQADE\n\nPrefer to read? Full transcript of this video: https://blog.garrytan.com/parker-conrad-on-building-eng-teams-outside-of-sf-and-why-engineers-must-do-customer-support-part-2",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/lIlguzWxEiI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/lIlguzWxEiI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/lIlguzWxEiI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/lIlguzWxEiI/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/lIlguzWxEiI/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "2019-11 Rippling",
          "parker conrad",
          "remote work",
          "garry tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Parker Conrad's Billion Dollar Lessons on Remote Work and Customer Support (Part 2)",
          "description": "\"Remote work is the worst way to build a company, except for all the others.\" —Parker Conrad\n\nSee part 1 of the video here: https://www.youtube.com/watch?v=M93xlPxQADE\n\nPrefer to read? Full transcript of this video: https://blog.garrytan.com/parker-conrad-on-building-eng-teams-outside-of-sf-and-why-engineers-must-do-customer-support-part-2"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "13285",
        "likeCount": "404",
        "dislikeCount": "8",
        "favoriteCount": "0",
        "commentCount": "28"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "AqLcjNDDcmqLskaUuPVw48MFTWo",
      "id": "0a3eM7lcVTI",
      "snippet": {
        "publishedAt": "2020-01-20T21:10:21Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "How to apply the artistry of Death Stranding to creating as an auteur",
        "description": "Hideo Kojima created a true masterpiece. Find out what practices helped this amazing game come together. \n\n🛡 A mission to cure loneliness in society\n♟ Resolute decisionmaking, not just doing the popular thing\n💥 Action: knowing what to delegate, outsource, and do yourself\n🎭 Showmanship that sets the tone\n\nThese things taken together allow teams to create something that has never existed before. Death Stranding is no exception.\n\nHere's the full transcript if you prefer to read: https://blog.garrytan.com/the-4-habits-of-auteurs-that-made-hideo-kojimas-video-game-masterpiece-death-stranding\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/0a3eM7lcVTI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/0a3eM7lcVTI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/0a3eM7lcVTI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/0a3eM7lcVTI/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/0a3eM7lcVTI/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "hideo kojima",
          "kojima"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "How to apply the artistry of Death Stranding to creating as an auteur",
          "description": "Hideo Kojima created a true masterpiece. Find out what practices helped this amazing game come together. \n\n🛡 A mission to cure loneliness in society\n♟ Resolute decisionmaking, not just doing the popular thing\n💥 Action: knowing what to delegate, outsource, and do yourself\n🎭 Showmanship that sets the tone\n\nThese things taken together allow teams to create something that has never existed before. Death Stranding is no exception.\n\nHere's the full transcript if you prefer to read: https://blog.garrytan.com/the-4-habits-of-auteurs-that-made-hideo-kojimas-video-game-masterpiece-death-stranding\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "1373",
        "likeCount": "87",
        "dislikeCount": "0",
        "favoriteCount": "0",
        "commentCount": "25"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "Jm0EeDmSxu0H7mgmX6i2OLU6wO0",
      "id": "M93xlPxQADE",
      "snippet": {
        "publishedAt": "2020-01-13T14:54:15Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Parker Conrad's Billion Dollar Startup Lessons: Do unscalable things, then scale them (part 1)",
        "description": "Today we're going to hang out with Parker Conrad. He created Rippling. He's one of the best product-focused CEO's I've ever met. Today he's going to share some of the most hard-won startup lessons that I've ever heard. And he hasn't talked about it anywhere else.\n\nHear about his startup journey\n☠️ Spending 7 years grinding it out on a startup that wasn't working\n🧨 Finding product market fit by doing the unscalable\n💎 Getting it right with Rippling: Using software to actually scale operations\n\nRead the full transcript here: https://blog.garrytan.com/parker-conrads-hard-won-billion-dollar-startup-lessons-do-unscalable-things-but-then-actually-scale-it-part-1\n\nLearn more about Rippling at https://rippling.com\n\nFollow Parker on Twitter at https://twitter.com/parkerconrad\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/M93xlPxQADE/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/M93xlPxQADE/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/M93xlPxQADE/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/M93xlPxQADE/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/M93xlPxQADE/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startup",
          "ycombinator",
          "rippling company",
          "garry tan",
          "conrad parker",
          "parker conrad",
          "scale",
          "venture capital",
          "zenefits",
          "zenefits demo"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Parker Conrad's Billion Dollar Startup Lessons: Do unscalable things, then scale them (part 1)",
          "description": "Today we're going to hang out with Parker Conrad. He created Rippling. He's one of the best product-focused CEO's I've ever met. Today he's going to share some of the most hard-won startup lessons that I've ever heard. And he hasn't talked about it anywhere else.\n\nHear about his startup journey\n☠️ Spending 7 years grinding it out on a startup that wasn't working\n🧨 Finding product market fit by doing the unscalable\n💎 Getting it right with Rippling: Using software to actually scale operations\n\nRead the full transcript here: https://blog.garrytan.com/parker-conrads-hard-won-billion-dollar-startup-lessons-do-unscalable-things-but-then-actually-scale-it-part-1\n\nLearn more about Rippling at https://rippling.com\n\nFollow Parker on Twitter at https://twitter.com/parkerconrad\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "52794",
        "likeCount": "1471",
        "dislikeCount": "29",
        "favoriteCount": "0",
        "commentCount": "68"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "nF8HC0u6muP_YOTIYoZsPvrbUSo",
      "id": "t4wmxx0xWP0",
      "snippet": {
        "publishedAt": "2020-01-06T15:49:55Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Storytelling for your Series A Fundraise with Andrew Lee",
        "description": "I asked my friend and colleague Andrew Lee to sit down with me and talk through some of the advice we give very frequently to our founders. This isn't intended to be a comprehensive guide, but most everything we discuss here is stuff that comes up as a part of our experience working with hundreds of startups at Initialized.\n\nHere are Andrew's four specific tips on your Series A fundraise to consider:\n🏃🏻\u200d♀️ Brevity is the soul of wit\n🤫 Secrets create friends\n🚄 Transport investors into the future\n🦸🏻\u200d♀️ You are the right hero for this journey\n\nYou can follow Andrew Lee on Twitter here: https://twitter.com/ndrewlee",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/t4wmxx0xWP0/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/t4wmxx0xWP0/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/t4wmxx0xWP0/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/t4wmxx0xWP0/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/t4wmxx0xWP0/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "andrew lee",
          "fundraise",
          "series a",
          "fundraising",
          "startup fundraising",
          "series b",
          "seed round",
          "series a round",
          "startup pitch",
          "pitch practice",
          "y combinator interview",
          "incubator interview",
          "pitch deck",
          "storytelling"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Storytelling for your Series A Fundraise with Andrew Lee",
          "description": "I asked my friend and colleague Andrew Lee to sit down with me and talk through some of the advice we give very frequently to our founders. This isn't intended to be a comprehensive guide, but most everything we discuss here is stuff that comes up as a part of our experience working with hundreds of startups at Initialized.\n\nHere are Andrew's four specific tips on your Series A fundraise to consider:\n🏃🏻\u200d♀️ Brevity is the soul of wit\n🤫 Secrets create friends\n🚄 Transport investors into the future\n🦸🏻\u200d♀️ You are the right hero for this journey\n\nYou can follow Andrew Lee on Twitter here: https://twitter.com/ndrewlee"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "15213",
        "likeCount": "761",
        "dislikeCount": "3",
        "favoriteCount": "0",
        "commentCount": "53"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "KVhB3fIXYSMity0bvlihK834L5k",
      "id": "lbJnMmfH4-U",
      "snippet": {
        "publishedAt": "2019-12-30T15:05:43Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Nikola Tesla predicted the smartphone",
        "description": "Nikola Tesla predicted the smartphone in 1926\n\n\"Culture based on the Internet is just beginning\" —@pmarca\n\"7.5B unique people = No one can compete with you on being you.\" —@naval\n\n📱5B people have smartphones\n🌎 The world has become a global brain\n💎 Only 1 in 300 people know how to code\n✍️ All the world is software, and you may contribute a line of code\n\nThe world has become a global brain. What line of code will you write?\n\nMore videos you should watch—\nHow to Get Rich by Naval Ravikant https://www.youtube.com/watch?v=1-TZqOsVCNM&t=2878s\nWhy you should be optimistic about the future, with Marc Andreessen and Kevin Kelly https://www.youtube.com/watch?v=UnU5Dikdr2U&t=214s",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/lbJnMmfH4-U/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/lbJnMmfH4-U/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/lbJnMmfH4-U/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/lbJnMmfH4-U/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/lbJnMmfH4-U/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "technology",
          "global brain",
          "internet culture",
          "nikola tesla",
          "naval ravikant",
          "garry tan"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Nikola Tesla predicted the smartphone",
          "description": "Nikola Tesla predicted the smartphone in 1926\n\n\"Culture based on the Internet is just beginning\" —@pmarca\n\"7.5B unique people = No one can compete with you on being you.\" —@naval\n\n📱5B people have smartphones\n🌎 The world has become a global brain\n💎 Only 1 in 300 people know how to code\n✍️ All the world is software, and you may contribute a line of code\n\nThe world has become a global brain. What line of code will you write?\n\nMore videos you should watch—\nHow to Get Rich by Naval Ravikant https://www.youtube.com/watch?v=1-TZqOsVCNM&t=2878s\nWhy you should be optimistic about the future, with Marc Andreessen and Kevin Kelly https://www.youtube.com/watch?v=UnU5Dikdr2U&t=214s"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "2430",
        "likeCount": "132",
        "dislikeCount": "1",
        "favoriteCount": "0",
        "commentCount": "42"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "S_Czqij0E7JpnYNDg6iGv15dzYU",
      "id": "dtnG0ELjvcM",
      "snippet": {
        "publishedAt": "2019-12-23T15:26:07Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "My $200 million startup mistake: Peter Thiel asked and I said no",
        "description": "I was 23 and didn't know anything about startups. Peter Thiel offered me a big equity stake and a full year's salary to quit my stable job at Microsoft and join a startup he was starting. It wasn't even a risky decision. I still said no, and it cost me $200M. \n\nWhen opportunity knocks, you should think about taking the risk. If you're good, it's often the only way you can actually get a larger piece of the kind of value you can create when making software. There are lots of good reasons to work at a big tech giant, but there are also downsides. We talk through those things.\n\nDon't make my mistake. Make all new mistakes.\n\n00:00 Intro\n00:32 How it went down\n02:32 How to make what you're worth\n03:26 Big company upside\n03:50 How to avoid the downsides\n04:27 Work at a startup or start yourself\n\nEDIT NOTE: Google actually makes $1.6M annualized net revenue per employee per year, not profit. Apologies for the imprecision.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/dtnG0ELjvcM/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/dtnG0ELjvcM/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/dtnG0ELjvcM/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/dtnG0ELjvcM/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/dtnG0ELjvcM/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "risk",
          "venture capital",
          "peter thiel",
          "microsoft",
          "technology",
          "tech giant",
          "garry tan",
          "my $200m mistake",
          "how one decision cost me 200",
          "truepill",
          "y combinator",
          "ycombinator",
          "palantir"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "My $200 million startup mistake: Peter Thiel asked and I said no",
          "description": "I was 23 and didn't know anything about startups. Peter Thiel offered me a big equity stake and a full year's salary to quit my stable job at Microsoft and join a startup he was starting. It wasn't even a risky decision. I still said no, and it cost me $200M. \n\nWhen opportunity knocks, you should think about taking the risk. If you're good, it's often the only way you can actually get a larger piece of the kind of value you can create when making software. There are lots of good reasons to work at a big tech giant, but there are also downsides. We talk through those things.\n\nDon't make my mistake. Make all new mistakes.\n\n00:00 Intro\n00:32 How it went down\n02:32 How to make what you're worth\n03:26 Big company upside\n03:50 How to avoid the downsides\n04:27 Work at a startup or start yourself\n\nEDIT NOTE: Google actually makes $1.6M annualized net revenue per employee per year, not profit. Apologies for the imprecision."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "278924",
        "likeCount": "9389",
        "dislikeCount": "231",
        "favoriteCount": "0",
        "commentCount": "616"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "1fdtzyWJ6PfqkhlywcXJwPu7QZo",
      "id": "9FpIZ3n7PMA",
      "snippet": {
        "publishedAt": "2019-12-19T19:20:51Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "5 years into the 10 year overnight success",
        "description": "Meet Ram Jayaraman. He is co-founder of Plate IQ. He’s helping restaurants actually understand how much money they’re spending on their food bills. This is an $860 billion market that has no pricing transparency until now. Plate IQ is used by 3% of restaurants in major cities today, and just getting started. \n\nA lot of companies claim to use AI to automate manual tasks, but Plate IQ has done it. They process now over 1M invoices per month. They took an unprofitable process and scaled it with pure software, and in the process are building something that gives pricing transparency that never existed before. \n\nFull transcript here: https://blog.garrytan.com/how-to-double-profit-margins-for-the-most-competitive-industry-in-the-world-with-software-a-conversation-with-plate-iq-cofounder-ram-jayaraman\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about Plate IQ at https://plateiq.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/9FpIZ3n7PMA/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/9FpIZ3n7PMA/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/9FpIZ3n7PMA/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/9FpIZ3n7PMA/sddefault.jpg",
            "width": 640,
            "height": 480
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "overnight success"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "5 years into the 10 year overnight success",
          "description": "Meet Ram Jayaraman. He is co-founder of Plate IQ. He’s helping restaurants actually understand how much money they’re spending on their food bills. This is an $860 billion market that has no pricing transparency until now. Plate IQ is used by 3% of restaurants in major cities today, and just getting started. \n\nA lot of companies claim to use AI to automate manual tasks, but Plate IQ has done it. They process now over 1M invoices per month. They took an unprofitable process and scaled it with pure software, and in the process are building something that gives pricing transparency that never existed before. \n\nFull transcript here: https://blog.garrytan.com/how-to-double-profit-margins-for-the-most-competitive-industry-in-the-world-with-software-a-conversation-with-plate-iq-cofounder-ram-jayaraman\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about Plate IQ at https://plateiq.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "10979",
        "likeCount": "319",
        "dislikeCount": "5",
        "favoriteCount": "0",
        "commentCount": "23"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "7LGfFgz-QLI6wyi-LFhNgswfjYg",
      "id": "gX4sxHRo12U",
      "snippet": {
        "publishedAt": "2019-12-03T15:00:10Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Why planes crash (surprising)",
        "description": "Whether you’re a pilot flying a plane or a CEO running a company, it’s clear that there are ways you can minimize the chances of crashing into the ground. \n\nWhy do they happen? What do the copilots say? It all comes back to the life or death 10 second decisions. We're lucky as managers we get more than 10 seconds, but there are so many things we should get right to maximize our chance of avoiding total catastrophe.\n\n* Dont panic - things will go wrong! Know they will. \n* Identify errors: we are going to crash. \n* Communicate the right way: be understood or be actionable\n* Minimize pulling rank\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/gX4sxHRo12U/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/gX4sxHRo12U/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/gX4sxHRo12U/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/gX4sxHRo12U/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/gX4sxHRo12U/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "garry tan",
          "plane crash"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Why planes crash (surprising)",
          "description": "Whether you’re a pilot flying a plane or a CEO running a company, it’s clear that there are ways you can minimize the chances of crashing into the ground. \n\nWhy do they happen? What do the copilots say? It all comes back to the life or death 10 second decisions. We're lucky as managers we get more than 10 seconds, but there are so many things we should get right to maximize our chance of avoiding total catastrophe.\n\n* Dont panic - things will go wrong! Know they will. \n* Identify errors: we are going to crash. \n* Communicate the right way: be understood or be actionable\n* Minimize pulling rank\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "2261",
        "likeCount": "158",
        "dislikeCount": "1",
        "favoriteCount": "0",
        "commentCount": "16"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "K6BFFQF0F6n59-2u8baNNakDGIA",
      "id": "NBd6yJBzyis",
      "snippet": {
        "publishedAt": "2019-11-14T08:29:34Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Don’t suffer from the “idea disease”",
        "description": "Execs and new founders frequently make a big mistake: overvaluing the big idea and undervaluing the execution\n\nHere are 3 questions that let you skip that noise:\n\n🧥What should we build?\n🧵How should we build it?\n🌨Why should we build it?\n\nA great idea is not 90% of the work. Here are the 3 questions all creators need to ask in order to actually create new things that are amazing. \n\nMy COO Jen Wolf told me about the Disney Method. I wanted to share how these 3 questions are essential for people to create anything. Dreamers need realists and critics to create something great.",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/NBd6yJBzyis/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/NBd6yJBzyis/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/NBd6yJBzyis/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/NBd6yJBzyis/sddefault.jpg",
            "width": 640,
            "height": 480
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "disney method",
          "creativity",
          "imagineering",
          "startups",
          "steve jobs",
          "apple",
          "creation",
          "teamwork",
          "garry tan",
          "venture capital",
          "y combinator",
          "initialized capital",
          "startup",
          "entrepreneur",
          "inspiration",
          "walt disney",
          "silicon valley",
          "yc",
          "tech",
          "startup school",
          "entrepreneurship",
          "entrepreneur motivation",
          "entrepreneur mindset",
          "entrepreneur ideas",
          "motivational video"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Don’t suffer from the “idea disease”",
          "description": "Execs and new founders frequently make a big mistake: overvaluing the big idea and undervaluing the execution\n\nHere are 3 questions that let you skip that noise:\n\n🧥What should we build?\n🧵How should we build it?\n🌨Why should we build it?\n\nA great idea is not 90% of the work. Here are the 3 questions all creators need to ask in order to actually create new things that are amazing. \n\nMy COO Jen Wolf told me about the Disney Method. I wanted to share how these 3 questions are essential for people to create anything. Dreamers need realists and critics to create something great."
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "59971",
        "likeCount": "3035",
        "dislikeCount": "68",
        "favoriteCount": "0",
        "commentCount": "121"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "UvHsq6QkyHYkA5sYjSRxp78pef0",
      "id": "LMDP6TCv1pI",
      "snippet": {
        "publishedAt": "2019-11-05T15:06:49Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Moms shouldn't have to choose between family and career",
        "description": "Allison Robinson created The Mom Project to help talented moms get family-friendly jobs. It works with dads too, and it's free to join— they serve as your job search assistant when parents are ready to find a family-friendly job that is tailored to their expertise and needs. \n\n43% of all women who have children end up leaving the work force. Solving this problem is worth over $570 billion— and has deep implications for gender equality and the pay gap. It’s not just a mission and a cause, it’s also one of the most important business problems to solve too.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about The Mom Project at https://themomproject.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/LMDP6TCv1pI/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/LMDP6TCv1pI/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/LMDP6TCv1pI/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/LMDP6TCv1pI/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/LMDP6TCv1pI/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "tartups",
          "venture capital",
          "software engineering",
          "tech",
          "tech jobs",
          "credentialism",
          "new credentials",
          "recruiting",
          "linkedin",
          "moms",
          "jobs for moms",
          "parental leave",
          "paid family leave",
          "gender pay gap",
          "gender wage gap",
          "allison robinson the mom project"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Moms shouldn't have to choose between family and career",
          "description": "Allison Robinson created The Mom Project to help talented moms get family-friendly jobs. It works with dads too, and it's free to join— they serve as your job search assistant when parents are ready to find a family-friendly job that is tailored to their expertise and needs. \n\n43% of all women who have children end up leaving the work force. Solving this problem is worth over $570 billion— and has deep implications for gender equality and the pay gap. It’s not just a mission and a cause, it’s also one of the most important business problems to solve too.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about The Mom Project at https://themomproject.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "2125",
        "likeCount": "64",
        "dislikeCount": "0",
        "favoriteCount": "0",
        "commentCount": "9"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "wzS5JqbiFx2kPg0oWI3smBzsY58",
      "id": "UWwtXfaldl4",
      "snippet": {
        "publishedAt": "2019-10-29T03:41:18Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Control your shadow, control your fate",
        "description": "Emotion drives our decisionmaking (see USC professor Antonio Damasio's groundbreaking research here: https://www.youtube.com/watch?v=1wup_K2WN0I), but what do we do about emotions that we refuse to even acknowledge? Our Jungian shadows.\n\nI headed to the Big Island of Hawaii for the Lobby Conference 2019- the time away helped me think about my relationship with my own shadow: the parts of myself that drive me that I don't even think about. This is something everyone faces, but not everyone is aware of these internal forces. \n\nThe shadow is a moral problem that challenges the whole ego-personality, for no one can become conscious of the shadow without considerable moral effort. To become conscious of it involves recognizing the dark aspects of the personality as present and real. This act is the essential condition for any kind of self-knowledge.\" - Carl Jung, Aion (1951)\n\nIf you hate a person, you hate something in him that is a part of yourself. What isn't a part of ourselves doesn't disturb us - Herman Hesse\n\n\"I don't like that man, I must get to know him better.\" - Abraham Lincoln\n\nThanks to Cameron Yarbrough, Stephanie Lim, Tony C Yang, and Dani Metz Shuval for watching drafts of this.\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/UWwtXfaldl4/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/UWwtXfaldl4/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/UWwtXfaldl4/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/UWwtXfaldl4/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/UWwtXfaldl4/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "venture capital",
          "executive coaching",
          "psychology",
          "founders",
          "founder stress",
          "psyche",
          "persona",
          "carl jung",
          "decisionmaking",
          "control your fate",
          "garry tan",
          "antonio damasio"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Control your shadow, control your fate",
          "description": "Emotion drives our decisionmaking (see USC professor Antonio Damasio's groundbreaking research here: https://www.youtube.com/watch?v=1wup_K2WN0I), but what do we do about emotions that we refuse to even acknowledge? Our Jungian shadows.\n\nI headed to the Big Island of Hawaii for the Lobby Conference 2019- the time away helped me think about my relationship with my own shadow: the parts of myself that drive me that I don't even think about. This is something everyone faces, but not everyone is aware of these internal forces. \n\nThe shadow is a moral problem that challenges the whole ego-personality, for no one can become conscious of the shadow without considerable moral effort. To become conscious of it involves recognizing the dark aspects of the personality as present and real. This act is the essential condition for any kind of self-knowledge.\" - Carl Jung, Aion (1951)\n\nIf you hate a person, you hate something in him that is a part of yourself. What isn't a part of ourselves doesn't disturb us - Herman Hesse\n\n\"I don't like that man, I must get to know him better.\" - Abraham Lincoln\n\nThanks to Cameron Yarbrough, Stephanie Lim, Tony C Yang, and Dani Metz Shuval for watching drafts of this.\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "11461",
        "likeCount": "674",
        "dislikeCount": "8",
        "favoriteCount": "0",
        "commentCount": "59"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "nRtaP5xII2JhC_HtNxX8744H5d8",
      "id": "bk-r0zCP59M",
      "snippet": {
        "publishedAt": "2019-10-22T16:30:25Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Rejected from every college and eng job, but now a successful founder: How he did it",
        "description": "Ammon Bartram is the cofounder of Triplebyte, a powerful force in how the best software engineers in the world are getting identified today. At triplebyte.com anyone can take a test to get their skills certified. The best candidates get top paying software engineering jobs like at Dropbox, Flexport and Cruise. Tech jobs have for too long been the domain of privileged credentials—resumes and expensive degrees from a small set of top universities are terribly limited compared to the magnitude of need for great software in the world. Triplebyte circles that square by helping firms identify and hire truly great talent through objective tests that anyone can take. \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/bk-r0zCP59M/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/bk-r0zCP59M/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/bk-r0zCP59M/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/bk-r0zCP59M/sddefault.jpg",
            "width": 640,
            "height": 480
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "venture capital",
          "software engineering",
          "tech",
          "tech jobs",
          "credentialism",
          "new credentials",
          "recruiting",
          "linkedin",
          "triplebyte",
          "garry tan",
          "i got rejected from every college",
          "rejected from every college",
          "triplebyte interview",
          "ammon bartram",
          "triplebyte founder",
          "y combinator application"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Rejected from every college and eng job, but now a successful founder: How he did it",
          "description": "Ammon Bartram is the cofounder of Triplebyte, a powerful force in how the best software engineers in the world are getting identified today. At triplebyte.com anyone can take a test to get their skills certified. The best candidates get top paying software engineering jobs like at Dropbox, Flexport and Cruise. Tech jobs have for too long been the domain of privileged credentials—resumes and expensive degrees from a small set of top universities are terribly limited compared to the magnitude of need for great software in the world. Triplebyte circles that square by helping firms identify and hire truly great talent through objective tests that anyone can take. \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "15440",
        "likeCount": "625",
        "dislikeCount": "8",
        "favoriteCount": "0",
        "commentCount": "58"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "EbOAwVyc3rGufomqgtc5X5UM4Os",
      "id": "rfTgzA6iKZc",
      "snippet": {
        "publishedAt": "2019-10-18T05:18:47Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "3 Tips to Nail the Y Combinator Interview",
        "description": "These were the 3 things I looked for when I was a Y Combinator partner. It's only 10 minutes, so make them count. The YC Interview is just a conversation. Take a deep breath, prepare, and practice.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/rfTgzA6iKZc/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/rfTgzA6iKZc/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/rfTgzA6iKZc/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/rfTgzA6iKZc/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/rfTgzA6iKZc/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "venture capital",
          "tech",
          "tech jobs",
          "dayinthelife",
          "initialized",
          "y combinator",
          "yc interview",
          "y combinator interview",
          "garry tan",
          "yc",
          "yc application"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "3 Tips to Nail the Y Combinator Interview",
          "description": "These were the 3 things I looked for when I was a Y Combinator partner. It's only 10 minutes, so make them count. The YC Interview is just a conversation. Take a deep breath, prepare, and practice.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "26116",
        "likeCount": "1130",
        "dislikeCount": "7",
        "favoriteCount": "0",
        "commentCount": "78"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "0XvEMj1mKdom9VldZnwXrx11TiI",
      "id": "4wbCVN1yLyA",
      "snippet": {
        "publishedAt": "2019-09-18T18:59:46Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Applying to Y Combinator? 3 tips from a former YC partner",
        "description": "Hi, I'm Garry Tan, former Y Combinator partner, Forbes Midas List top venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered and high signal in order to help you with your building your world-changing startup.\n\nIn this episode I want to talk about what advice I always give to people writing their YC applications. Watch to the end you'll find a link to submit your YC app to me for comments and review. I can guarantee I'll read the first 100 responses from YouTube, but honestly I'll try my best overall just because I really want to help. \n\nY Combinator changed my life and I hope it changes yours. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/4wbCVN1yLyA/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/4wbCVN1yLyA/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/4wbCVN1yLyA/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/4wbCVN1yLyA/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/4wbCVN1yLyA/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "y combinator",
          "yc application",
          "venture capital",
          "tech",
          "silicon valley",
          "yc interview prep",
          "yc interview",
          "yc",
          "garry tan",
          "yc w2020",
          "gary tan",
          "y combinator application videos successful",
          "yc founder video",
          "yc demo day"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Applying to Y Combinator? 3 tips from a former YC partner",
          "description": "Hi, I'm Garry Tan, former Y Combinator partner, Forbes Midas List top venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered and high signal in order to help you with your building your world-changing startup.\n\nIn this episode I want to talk about what advice I always give to people writing their YC applications. Watch to the end you'll find a link to submit your YC app to me for comments and review. I can guarantee I'll read the first 100 responses from YouTube, but honestly I'll try my best overall just because I really want to help. \n\nY Combinator changed my life and I hope it changes yours. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "31975",
        "likeCount": "1248",
        "dislikeCount": "9",
        "favoriteCount": "0",
        "commentCount": "106"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "Z3IM1kW67-tCSyzL_SbTolTgaGQ",
      "id": "GknGzw0Nqpw",
      "snippet": {
        "publishedAt": "2019-09-12T19:01:51Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Why I started a VC firm- \"Too much money chasing too few good people and ideas\" is totally wrong",
        "description": "Hi, I'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building.\n\nToday, I was at home on paternity leave, so I thought I'd sit down in my backyard and talk about what drove me to start a venture capital firm that funds early stage startups, and why I'm doing my own YouTube channel with no staff or crew.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/GknGzw0Nqpw/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/GknGzw0Nqpw/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/GknGzw0Nqpw/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/GknGzw0Nqpw/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/GknGzw0Nqpw/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "One Person",
          "garry tan",
          "venture capital",
          "startups",
          "startup advice",
          "new business creation",
          "creative destruction"
        ],
        "categoryId": "28",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Why I started a VC firm- \"Too much money chasing too few good people and ideas\" is totally wrong",
          "description": "Hi, I'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building.\n\nToday, I was at home on paternity leave, so I thought I'd sit down in my backyard and talk about what drove me to start a venture capital firm that funds early stage startups, and why I'm doing my own YouTube channel with no staff or crew.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "19160",
        "likeCount": "1087",
        "dislikeCount": "13",
        "favoriteCount": "0",
        "commentCount": "148"
      }
    },
    {
      "kind": "youtube#video",
      "etag": "T3wJYMjfK_I5y7eeIz5u6g0OPTU",
      "id": "fCFHyQag21Q",
      "snippet": {
        "publishedAt": "2019-09-06T09:01:39Z",
        "channelId": "UCIBgYfDjtWlbJhg--Z4sOgQ",
        "title": "Meet Truepill cofounder Sid Viswanathan: Telemedicine will make medicine 10x better/cheaper/faster",
        "description": "Hi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building.\n\nToday I sat down with Sid Viswanathan, cofounder of Truepill, an API for all needs for telemedicine. Telemedicine has the potential to bring down costs and make high quality care more accessible for every person on the planet. We’re headed to Hayward, California, their west coast HQ and fulfillment center out of which they provide pharmacy services for dozens of telemedicine startups and practices large and small, shipping to all 50 states. \n\nCome learn about how as a founder, you need to choose a problem space that you could want to work on for 10 years or more. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this with top founders. \n\nFind Sid on Twitter at https://twitter.com/sidviswanathan\nFind Garry on Twitter at https://twitter.com/garrytan\nLearn more about Truepill at https://truepill.com\n \nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/fCFHyQag21Q/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/fCFHyQag21Q/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/fCFHyQag21Q/hqdefault.jpg",
            "width": 480,
            "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/fCFHyQag21Q/sddefault.jpg",
            "width": 640,
            "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/fCFHyQag21Q/maxresdefault.jpg",
            "width": 1280,
            "height": 720
          }
        },
        "channelTitle": "Garry Tan",
        "tags": [
          "startups",
          "funding",
          "telemedicine",
          "venture capital",
          "full stack startup",
          "medicine",
          "doctors",
          "pharmacy",
          "garry tan",
          "truepill"
        ],
        "categoryId": "22",
        "liveBroadcastContent": "none",
        "localized": {
          "title": "Meet Truepill cofounder Sid Viswanathan: Telemedicine will make medicine 10x better/cheaper/faster",
          "description": "Hi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building.\n\nToday I sat down with Sid Viswanathan, cofounder of Truepill, an API for all needs for telemedicine. Telemedicine has the potential to bring down costs and make high quality care more accessible for every person on the planet. We’re headed to Hayward, California, their west coast HQ and fulfillment center out of which they provide pharmacy services for dozens of telemedicine startups and practices large and small, shipping to all 50 states. \n\nCome learn about how as a founder, you need to choose a problem space that you could want to work on for 10 years or more. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this with top founders. \n\nFind Sid on Twitter at https://twitter.com/sidviswanathan\nFind Garry on Twitter at https://twitter.com/garrytan\nLearn more about Truepill at https://truepill.com\n \nLearn more about the companies we fund, and how we work with them at https://initialized.com"
        },
        "defaultAudioLanguage": "en-US"
      },
      "statistics": {
        "viewCount": "12480",
        "likeCount": "383",
        "dislikeCount": "4",
        "favoriteCount": "0",
        "commentCount": "63"
      }
    }
  ],
  "pageInfo": {
    "totalResults": 38,
    "resultsPerPage": 38
  }
}
FORMATTED_VIDEOS_DATA = [
    {
        'youtube_id': 'JbHip4tjEcg',
        'title': "Do It Yourself. Here's how REAL movements start, then change the world.",
        'description': "The moment you realize the world can be influenced by your actions, everything changes. But how do you make a real dent in the world? You don't need fancy tools. Just do it yourself.\n\n0:00 Intro\n1:58 How a movement emerges\n3:56 How Apple was born\n6:43 How to Attract a Following\n8:55 Doing It Yourself\n10:52: Kurt Cobain on DIY\n13:03 Secrets to life & making your mark.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1Bin assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/JbHip4tjEcg/default.jpg',
        'published_at': datetime.datetime(2021, 6, 16, 14, 30, 2),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'punk rock', 'fuck perfection', 'how real movements start', 'do it yourself', 'solo founder', 'building a company by yourself', 'should I find a cofounder', 'how to find a cofounder', 'ramones', 'kurt cobain', 'nirvana interviews', 'punk rock interviews', 'startup founder', 'best investors', 'startup investors', 'Silicon Valley'],
        'view_count': 39299,
        'like_count': 3148,
        'dislike_count': 24,
        'favorite_count': 0,
        'comment_count': 461
    },
    {
        'youtube_id': 'jo58qEoThSs',
        'title': 'Why $ETH is Ultrasound Money going to $10K, | Why Buffett and Berkshire Hathaway are WRONG on Crypto',
        'description': "Buffett might be brilliant, but they're totally wrong on Crypto. Here is why I believe Ethereum is ultrasound money and why I believe $ETH is going to $10K.\n\n0:00: Intro\n0:42 Why Warren Buffet is wrong on Crypto\n2:55 Why Ethereum is programmable money\n6:21 Why $ETH is ultrasound money\n9:16 How Ethereum will drop energy usage by 99%\n\nThis video is an opinion and is for information purposes only. It is not intended to be investment advice. Seek a duly licensed professional for investment advice.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $1B in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/jo58qEoThSs/default.jpg',
        'published_at': datetime.datetime(2021, 6, 9, 15, 21, 1),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'eth', 'ethereum', 'eth vs btc', 'bitcoin', 'how to invest in crypto', 'crypto investing', 'bitcoin investing', 'how to buy bitcoin', 'how to buy ethereum', 'how to buy NFTs', 'ethereum price prediction', 'eth price 2021', 'crypto prices'],
        'view_count': 65835,
        'like_count': 4080,
        'dislike_count': 119,
        'favorite_count': 0,
        'comment_count': 785
    },
    {
        'youtube_id': 'yUgUs-MOxE0',
        'title': 'What is Mimetic Theory? Philosophies of René Girard with Luke Burgis',
        'description': 'Ever wondered why humans often act like sheep? Learn why in this video where we explore the philosophies of René Girard with Luke Burgis - one of my favorite experts in the space.\n\nLuke’s book Wanting is now available everywhere you can buy books. Pick up a copy at https://lukeburgis.com/wanting/\n\n0:00 Intro\n0:28 Starting a traditional career path\n3:59 What is mimetic theory?\n6:49 Applying mimetic theory to your own life\n9:39 Manifesting change in your own life\n13:07 Mimesis in social media\n17:34 Managing mimetic impulses\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b',
        'thumbnail': 'https://i.ytimg.com/vi/yUgUs-MOxE0/default.jpg',
        'published_at': datetime.datetime(2021, 6, 1, 14, 30, 8),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs'],
        'view_count': 19111,
        'like_count': 799,
        'dislike_count': 16,
        'favorite_count': 0,
        'comment_count': 149
    },
    {
        'youtube_id': 'MJ-8TSrn0Js',
        'title': 'My 8 Best Startup tips from 10+ Years of Investing',
        'description': 'Check out report cards by Tydo here - https://app.tydo.com/signup\nHere are 8 Startup tips containing some of my favorite advice I have for startup founders taken from 10+ Years of Investing at my VC fund, Initialized Capital.\n\nWhat is good retention? https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:00 Intro\n1:08 Be Contrarian AND Right\n1:33 Learn AND EARN\n2:05 Realize Time is Finite\n2:33 Check out Report Cards from Tydo!\n3:50 How to SAVE Your Startup from a death spiral\n4:32 Be a NINJA in the face of Chaos\n5:11 The secret to Designing anything\n6:12 The secret growth engine behind most companies\n7:13 Everything to know about User retention and growth\n8:34 Thanks for watching.\n\nREADY TO EXPAND? HERE ARE THE FULL LESSONS— \n\nLearn or Earn https://youtu.be/eLelgy5zRv4\nHow to design https://youtu.be/K3Ctc1UN0bQ\nFunding Coinbase: Turning $300K into $2 billion https://youtu.be/x5YApjnTG10 \nStop chasing money, chase wealth https://youtu.be/7Hdu4DlnLIk\nStartup next steps after raising millions https://youtu.be/khY3REOst-A \nStartup growth strategies https://youtu.be/zG_IXr6NXAg\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b',
        'thumbnail': 'https://i.ytimg.com/vi/MJ-8TSrn0Js/default.jpg',
        'published_at': datetime.datetime(2021, 5, 26, 14, 30, 2),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'best startup tips', 'startup investing', 'startup investing tips', 'crypto investing tips', 'stock market investing tips', 'coinbase IPO', 'coinbase investors', 'how to raise money for your startup', 'how to raise money for startup', 'raise money startup', 'how to get funded', 'best vcs', 'best startup investors'],
        'view_count': 27002,
        'like_count': 1713,
        'dislike_count': 26,
        'favorite_count': 0,
        'comment_count': 278
    },
    {
        'youtube_id': 'SlY8p7qWgsk',
        'title': 'Funding the Next Unicorn: Building Initialized Capital with Jen Wolf',
        'description': 'Today we sit down with Jen Wolf, who is now President & Partner at Initialized. In addition to being a talented product and user experience executive, she is also an incredible investor helping shape the next generation of founders and companies.\n\n1:21 How Garry met Jen\n4:50 How Jen got into tech\n7:37 Design Thinking as a framework\n10:55 Getting exponential value out of your team\n12:17 How to build a diversity flywheel\n20:10 Finding the next great investor\n24:35 How to find flexibility in traditional structure\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b',
        'thumbnail': 'https://i.ytimg.com/vi/SlY8p7qWgsk/default.jpg',
        'published_at': datetime.datetime(2021, 5, 20, 14, 30, 7),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'female vc', 'diversity in investing', 'diverse investors', 'Jen wolf VC', 'Jen wolf initialized capital', 'investor female', 'best female investors', 'best female vc'],
        'view_count': 11205,
        'like_count': 385,
        'dislike_count': 10,
        'favorite_count': 0,
        'comment_count': 87
    },
    {
        'youtube_id': 'UpbGbKQsTjc',
        'title': 'Coinbase CEO Brian Armstrong on Cryptocurrency and the Future of Decentralization',
        'description': 'Today we sit down the Co-founder & CEO of Coinbase, Brian Armstrong. Brian started Coinbase in 2012 when bitcoin was $2 and almost no one knew what it was, let alone believed in it. Today, it is the largest cryptocurrency exchange in the USA that just went public at a nearly $100B market cap.\n\nhttps://www.coinbase.com\nhttps://www.twitter.com/brian_armstrong\n\n0:00 Intro\n2:18 How Brian got discovered Bitcoin\n9:45 How Coinbase grew from idea to reality\n11:58 What the early days of bitcoin were like\n15:31 How Brian got into tech\n18:57 Democratizing information and money\n21:15 The three ages of crypto\n24:42 How Blockchain will change science and research\n32:18 Decentralization in a centralized world\n42:11 The IPO is 1% of the Coinbase journey\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\n\nProduced by Chris Hall\nFilmed by Michael Bliss & Oliver Covrett',
        'thumbnail': 'https://i.ytimg.com/vi/UpbGbKQsTjc/default.jpg',
        'published_at': datetime.datetime(2021, 5, 19, 14, 30, 7),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'Brian Armstrong', 'coinbase IPO', 'Coinbase ceo', 'coinbase ceo interview', 'Brian Armstrong interview', 'billionaire interview', 'startup ceos', 'fireside chat', 'coinbase crypto', 'bitcoin interviews', 'bitcoin billionaires', 'startup ipo', 'how to use coinbase', 'how coinbase started', 'fred ehrsam', 'fred coinbase'],
        'view_count': 110273,
        'like_count': 4241,
        'dislike_count': 39,
        'favorite_count': 0,
        'comment_count': 621
    },
    {
        'youtube_id': 'mJ7z-5Kjz44',
        'title': 'How High-Res Space Imagery Will Change Everything | 10CM Satellite Imagery Explained',
        'description': "It's not every day a company has the potential to improve something by 10X or more. Albedo has done just this by creating affordable, high resolution satellite cameras that are game-changing for multiple major industries across the world. Today, learn how the space tech revolution is taking off and how Albedo's work is changing the world.\n\nhttps://www.albedo.space\nhttps://jobs.lever.co/albedo\n \n0:00 Intro\n1:02 What is Albedo?\n4:13 How is this technology used?\n12:30 Why now for Space tech?\n14:11 How YOU can be a part\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/mJ7z-5Kjz44/default.jpg',
        'published_at': datetime.datetime(2021, 5, 4, 14, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'satellite imagery', '10cm satellite imagery', 'albedo', 'topher haddad', 'space imagery', 'spacex satellite', 'aerial imagery', 'how to launch satellites', 'future of satellite imagery', 'albedo space', 'space travel', 'high res satellite', 'space telescope', 'Hubble telescope', 'aerial video', 'drone photography', 'classified'],
        'view_count': 18509,
        'like_count': 841,
        'dislike_count': 3,
        'favorite_count': 0,
        'comment_count': 167
    },
    {
        'youtube_id': 'SwvoR9mLTXM',
        'title': 'You can be a VC (I’m hiring): How venture works & what it takes to fund billion dollar startups',
        'description': "Venture Capitalists make billions on startup investments, but how does it actually work? Find out in this video. Also, we're hiring! Apply to work with me at Initialized at the links below.\n\nHow I Funded Coinbase and returned $300K into $2.4B - https://youtu.be/x5YApjnTG10\n\nApply to work at my VC fund—\nWhat is it like to work at Initialized? https://blog.initialized.com/2021/04/working-at-initialized/\nPartner: https://jobs.lever.co/initialized/3aa236dd-dd71-4ba7-9832-59023819a41d\nPrincipal: https://jobs.lever.co/initialized/5e9e3bb1-c4e0-402c-975b-aef32c1482c8\n\nHow I Funded Coinbase and turned $300K into $2.4B - https://youtu.be/x5YApjnTG10\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\u200b\nStop wasting your life: https://youtu.be/eLelgy5zRv4\u200b\n\n0:00 Intro\n1:16 How money and returns work\n4:49 How to build the future\n9:02 Why you need to think for yourself\n12:45 How personal traits of VC's and startup investors \n16:39 Special announcement - we're hiring!\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://www.instagram.com/garrytan\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/SwvoR9mLTXM/default.jpg',
        'published_at': datetime.datetime(2021, 4, 22, 14, 30, 5),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'how vc works', 'how venture capital works', 'how to invest in startups', 'how to become a vc', 'venture capitalist', 'venture capital', 'something ventured', 'best investors', 'marc andreesen', 'ben horowitz', 'fred wilson', 'coinbase investors', 'Brian Armstrong coinbase', 'coinbase ceo', 'coinbase ipo', 'startup investors'],
        'view_count': 143453,
        'like_count': 6189,
        'dislike_count': 58,
        'favorite_count': 0,
        'comment_count': 810
    },
    {
        'youtube_id': 'x5YApjnTG10',
        'title': 'I funded Coinbase in 2012. Making 6000x on my best startup investment yet',
        'description': "I was lucky enough to be the first investor in a company called Coinbase. Today they are a Cryptocurrency exchange worth billions that just went public. But when I met Brian, Bitcoin was $5 and Coinbase was just him. Here's the story behind my best investment yet.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\n0:49 How I met Brain Armstrong (Coinbase Co-Founder & CEO)\n2:54 No one believed in bitcoin in 2012\n3:42 Why I said YES when most said NO\n6:18 What YOU should learn from this\n7:00 Roll your way to a Startup Unicorn: Lessons for Founders\n\nRoll your way to a Startup Unicorn: https://youtu.be/r8hhvw-1b-M\n\nNote: 6000X at the $100B private secondary valuation, blended Initialized and Y Combinator early investments at seed/pre-seed.\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/x5YApjnTG10/default.jpg',
        'published_at': datetime.datetime(2021, 4, 14, 14, 30, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'coinbase ipo', 'coinbase investor', 'Garry Tan coinbase', 'best startup investments', 'how to invest in crypto', 'midas list vc', 'Forbes Midas list 2021', 'best bitcoin exchange', 'how to buy bitcoin', 'where to buy bitcoin', 'best investment of all time', 'bitcoin price', 'ethereum price', 'btc', '$COIN', 'coinbase'],
        'view_count': 304192,
        'like_count': 11433,
        'dislike_count': 104,
        'favorite_count': 0,
        'comment_count': 1774
    },
    {
        'youtube_id': 'eLelgy5zRv4',
        'title': "LEARN, EARN or QUIT | My job/career advice for 2021 | Garry Tan's Founders Journey Ep. 4",
        'description': "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nIf you're not learning AND earning... it's time to move on. You're wasting your life. Learn how to  master BOTH and maximize your time on earth to make build something great in this week's video.\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YApjnTG10\nWhat to do after raising money: https://youtu.be/khY3REOst-A\n\nBill Gates on expertise: https://www.youtube.com/watch?v=CsGihiSE6sM\nAlan Watts on Work: https://www.youtube.com/watch?v=1v1NZgE170w\nDavid Graeber on Bullshit Jobs - https://www.youtube.com/watch?v=XIctCDYv7Yg\n\n0:00 Intro\n0:40 Examples of LEARNING\n4:41 Examples to EARNING\n9:03 Examples of LEARNING and EARNING at the same time\n10:59 Examples of neither learning or earning\n12:06 HOW TO DO IT BEST - (learn, then earn)\n13:01 Outro\n\nHow to get rich: https://youtu.be/7Hdu4DlnLIk\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/eLelgy5zRv4/default.jpg',
        'published_at': datetime.datetime(2021, 4, 7, 14, 30, 3),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'startup advice', 'best startup advice', 'best career advice', 'how to quit your job', 'when to quit your job', 'how to find a new job', 'best startup jobs', 'how to join a startup', 'how to become an entreprenur', 'best career tips', 'how to get rich', 'how to get wealthy', 'Garry Tan startup advice', 'naval', 'chamath', 'Paul g'],
        'view_count': 185316,
        'like_count': 9891,
        'dislike_count': 86,
        'favorite_count': 0,
        'comment_count': 952
    },
    {
        'youtube_id': '7Hdu4DlnLIk',
        'title': "STOP Chasing Money -- Chase WEALTH. | How To get RICH | Garry Tan's Office Hours Ep. 4",
        'description': "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nA lot of you say you want to get rich. But what you really want is wealth. Find out why in this video.\n\nhttp://www.paulgraham.com/articles.html\nhttps://nav.al/rich\n\nStop wasting your life: https://youtu.be/eLelgy5zRv4\nTurning $300K into $2B with Coinbase: https://www.youtube.com/watch?v=x5YAp...\u200b\nWhat to do after raising money: https://youtu.be/khY3REOst-A\u200b\n\n0:00 Overview\n1:04 What is wealth?\n3:07 Focus on skills\n8:29 Don't sell your time!\n11:45 Create a wealth machine\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/7Hdu4DlnLIk/default.jpg',
        'published_at': datetime.datetime(2021, 3, 31, 14, 30, 4),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'how to get rich', 'how to get wealthy', 'how to build wealth', 'investment advice', 'startup investors', 'best startup investors', 'how to invest long term', 'Paul graham', 'naval ravikant', 'Justin Kan twitch', 'naval podcast', 'Paul graham essays', 'YC demo day tips', 'how to get rich the real way', 'get rich quick scheme'],
        'view_count': 359031,
        'like_count': 20679,
        'dislike_count': 282,
        'favorite_count': 0,
        'comment_count': 1406
    },
    {
        'youtube_id': 'SFPQlKcePu8',
        'title': 'How to find your true self and fight your own battles. (my failure as a manager)',
        'description': "Do you ever find yourself wanting different things at the same time? You're not alone. \n\n0:00 Intro\n1:02 My FAILURE As A Manager\n3:34 The Many Minds\n7:19 The 5 Stages of Development\n8:48 Why You must INTEGRATE yourself\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/SFPQlKcePu8/default.jpg',
        'published_at': datetime.datetime(2021, 3, 23, 14, 30, 16),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'mastering your many minds', 'multiple personalities', 'executive coaching', 'do I have multiple personalities', 'inside the mind', 'psychology videos', 'best investors in the world', 'how do become wealthy', 'how to clear your mind', 'how to be successful', 'meditation for business', 'how to meditate', 'how to focus better'],
        'view_count': 37261,
        'like_count': 1966,
        'dislike_count': 11,
        'favorite_count': 0,
        'comment_count': 173
    },
    {
        'youtube_id': 'KD2bkgoxSSI',
        'title': 'Get Users to Do What You Want | The Future of Customer Service & Onboarding Customers w/ Cohere',
        'description': 'Both startups and corporate giants alike wish they could be next to users in the moment when they are going through a problem. Cohere lets you do actually do that and avoid losing that person forever. Today, join me as I dive into the future of user onboarding and customer service with Cohere.\n\nhttps://cohere.so/\n\n0:00 Intro\n1:01 What is Cohere?\n2:38 See it in Action\n5:39 Lessons from Superhuman\n10:36 Industry Trends\n11:46 Cohere Success Stories\n15:04 The Silver Bullet for CS, PM and Sales\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $100 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b',
        'thumbnail': 'https://i.ytimg.com/vi/KD2bkgoxSSI/default.jpg',
        'published_at': datetime.datetime(2021, 3, 17, 13, 0, 18),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'user onboarding', 'startup customer service tools', 'how to guide users through problems', 'onboarding customers', 'startup CRM tools', 'how to onboard users quicker', 'how to onboard users fast', 'startup user onboarding', 'cohere', 'the future of user onboarding', 'the future of customer service', 'customer service tips'],
        'view_count': 14701,
        'like_count': 530,
        'dislike_count': 9,
        'favorite_count': 0,
        'comment_count': 42
    },
    {
        'youtube_id': 'aAbw1IL96V8',
        'title': 'Coinbase’s Liftoff Moment: Why I love seed investing and being a CEO coach #shorts',
        'description': 'I was lucky enough to be the earliest investor in Coinbase. In a recent interview on Clubhouse, Brian Armstrong recounts with me the early moments of finding product market fit. This is what being a seed investor is all about. #shorts',
        'thumbnail': 'https://i.ytimg.com/vi/aAbw1IL96V8/default.jpg',
        'published_at': datetime.datetime(2021, 2, 26, 17, 0, 1),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': [],
        'view_count': 12487,
        'like_count': 387,
        'dislike_count': 2,
        'favorite_count': 0,
        'comment_count': 31
    },
    {
        'youtube_id': '5ls-FQA5Uis',
        'title': 'Cutting Edge Climate Tech | Fighting Global Warming & Greenhouse Gasses with Kairos Aerospace',
        'description': 'Reversing climate change is a huge problem to solve for the world, and Ari Gesher is actually doing something about it. Today, learn about a company you need to know about - Kairos Aerospace, and Ari Gesher. Ari and I were both early employees at Palantir, working for Peter Thiel. Enjoy!\n\n0:00 Intro\n1:06 How Kairos Aerospace Works\n9:18 How Kairos landed enterprise contracts\n15:39 How Kairos Unit Economics\n18:09 Our Lessons from Palantir (working for Peter Thiel)\n31:49 How to Predict the Next Huge Startup\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b',
        'thumbnail': 'https://i.ytimg.com/vi/5ls-FQA5Uis/default.jpg',
        'published_at': datetime.datetime(2021, 2, 26, 15, 30, 14),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'best Silicon Valley vcs', 'climate change', 'global warming', 'aerial imaging', 'kairos aerospace', 'aerial technology', 'oil surverying', 'how to stop global warming', 'global warming technology', 'ways to solve global warlomg', 'climate change technology', 'slow down climate change', 'Peter Thiel palantir', 'plantir early employees', 'Peter thiel'],
        'view_count': 4102,
        'like_count': 140,
        'dislike_count': 5,
        'favorite_count': 0,
        'comment_count': 17
    },
    {
        'youtube_id': 'zG_IXr6NXAg',
        'title': '6 Startup Growth Strategies from a Forbes Top VC | Seed to Billion Dollar IPO | Office Hours Ep.3',
        'description': "Ok, you just got funded. But how do you grow your startup to a billion dollar IPO? It turns out, there's a formula. Learn what it is, along with 6 tips from me on how to do that in this week's episode of Office Hours.\n\nJust raised money? Make sure you watch my last video here: https://www.youtube.com/watch?v=khY3REOst-A\n\nFounding Sales -- https://www.foundingsales.com\nPredictable Revenue -- http://www.amazon.com/dp/0984380213/\nFollow Nik Sharma for tips on Influencers - https://twitter.com/mrsharma\n\n0:00 -- Intro --\n0:46 Intro: How Do You Grow?\n1:24 Tip 1 -- PR\n1:56 How Soylent used PR\n0:55 Tip 2 -- Sales and Marketing\n2:35 Controversy Sells\n3:06 Tip 3 -- Content Marketing\n3:18 How OkCupid used Content Marketing \n3:47 Tip 4 -- SEO\n4:08 How Zapier's used SEO\n5:02 Google Ads\n6:00 Tip 5 -- Influencers\n6:17 How DTC Brands use Influencers\n6:34 Tip 6: Partnerships\n6:42 How Microsoft & IBM used partnerships\n7:10 -- Recap! --\n\n100% of the revenue from this channel is donated to https://www.code2040.org\u200b - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\u200b\nhttps://twitter.com/garrytan\u200b",
        'thumbnail': 'https://i.ytimg.com/vi/zG_IXr6NXAg/default.jpg',
        'published_at': datetime.datetime(2021, 2, 22, 15, 30, 14),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Garry Tan', 'Garry Tan vc', 'initialized capital', 'Garry Tan initialized', 'Garry Tan initialized capital', 'Silicon Valley vc', 'sf vc', 'a16z', 'Andreessen horowitz', 'how to grow your startup', 'startup growth tips', 'how to ipo', 'what is an ipo', 'what is an exit', '6 Startup Growth Tips from a Midas List VC', 'billion dollar exit', 'how to build a startup', 'how to get funded', 'how to raise money', 'how to raise money for a startup', 'startup growth hacks', 'how to pitch your startup', 'best Silicon Valley vc'],
        'view_count': 21449,
        'like_count': 1293,
        'dislike_count': 13,
        'favorite_count': 0,
        'comment_count': 66
    },
    {
        'youtube_id': 'khY3REOst-A',
        'title': 'Startup Next Steps after Raising Your First Million | from a Forbes Top 100 VC | Office Hours Ep. 2',
        'description': 'We hear about startups raising millions, or billions of dollars all the time now -- but what should founders actually do the day the check hits? Find out why in this freshly branded version of Office Hours. \n\nWhat is good retention? -- https://www.lennysnewsletter.com/p/what-is-good-retention-issue-29\n\n0:42 -- Overview --\n1:02 Part 1: Get Your Head Right\n3:25 Part 2: Make a Plan\n8:27 Part 3: Retention Before Growth\n12:08 Part 4: When the Plan goes Wrong\n15:26 -- Recap --\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan',
        'thumbnail': 'https://i.ytimg.com/vi/khY3REOst-A/default.jpg',
        'published_at': datetime.datetime(2021, 2, 15, 15, 30, 15),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Silicon Valley vc', 'how do I raise money for my startup?', 'how to raise a seed round', 'how to raise a series a', 'how to raise an angel round', 'how to raise money for startups', 'how to raise money for your startup', 'ltv cac', 'operating plan', 'raised series A', 'roi', 'startup hiring', 'startup plan', 'startup seed funding', 'startup series a funding', 'startups', 't2d3', 'venture backed startup', 'what to do after you raise money for your startup', 'startup retention', 'retention', 'good retention', 'founder advice'],
        'view_count': 23480,
        'like_count': 1409,
        'dislike_count': 10,
        'favorite_count': 0,
        'comment_count': 141
    },
    {
        'youtube_id': '6cW2IdY6Hhc',
        'title': 'Master DTC Marketing | Learn Organic vs. Paid Acquisition | with Nik Sharma, CEO Sharma Brands',
        'description': "Everyone wants to start a DTC (direct-to-consumer) brand, but how do you ACTUALLY grow it from scratch? Meet Nik Sharma, who has done just that for some of customer's favorite DTC brands like JUDY (https://judy.co), Caraway (https://www.carawayhome.com), and Hint Water. The key here is content marketing strategy, influencer marketing, proper use of marketing analytics, and social media marketing. \n\nNik's Twitter for DTC Advice: https://twitter.com/mrsharma\nNik's 40 page deck on how to launch brands -- https://nik.co/subscribe\nNik's New Youtube Channel! — https://www.youtube.com/channel/UCaXLpESAYEX6pAHvYgYKYZw\n\n0:57 Meet Nik Sharma!\n2:12 Nik’s first job\n3:05 Working at Hint\n3:55 Focus on WHY\n5:13 How Nik Reduced CAC by 70%\n6:37 Origin of “DTC”\n7:14 Product first, then brand\n8:27 Personification of brands\n9:33 How to Start\n10:50 Solve a REAL problem\n12:49 Focus on product\n13:35 How to Validate an Idea\n14:34 Organic vs Paid Marketing\n15:25 How Haus focused on organic\n17:56 How JUDY used TV Marketing \n19:04 How to grow a product organically\n20:41 SEO and content is underrated\n21:19 Best marketing channels?\n22:36 Why to focus on 2 channels only\n23:32 Consistent branding is key\n25:11 Key DTC metrics\n25:55 Caraway - First purchase profitability\n27:23 What did Nik wish he knew earlier?\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/6cW2IdY6Hhc/default.jpg',
        'published_at': datetime.datetime(2021, 2, 6, 15, 30, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['dtc', 'nik sharma', 'dtc brand', 'branding', 'marketing', 'digital marketing', 'd2c', 'direct to consumer', 'brand', 'consumer brand', 'facebook marketing', 'influencer marketing', 'content marketing', 'social media marketing', 'brands', 'content marketing strategy', 'internet marketing', 'marketing strategy', 'what is digital marketing', 'instagram influencer marketing', 'digital marketing tutorial for beginners', 'instagram marketing', 'marketing analytics'],
        'view_count': 22353,
        'like_count': 987,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 125
    },
    {
        'youtube_id': 'o2GuOvO-Mns',
        'title': 'Co-Founder Conflict & Why I Quit my Startup | How to Fix a Dying Company | Founder’s Journey: Ep.2',
        'description': 'Co-founder disputes are the No. 1 early startup killer, but it doesn’t have to be that way. Learn how success masks conflict, how too little AND too much conflict create an issue, and how to ACTUALLY get help and SAVE your company.\n\nExecutive Coaching - https://torch.io/leadership-coaching/\n\n1:05 How Success Masks Co-Founder Conflict\n1:42 Too Little Conflict\n4:14 The Four Horsemen\n5:49 Too Much Conflict\n10:47 Solution Get Help!\n\n100% of the revenue from this channel is donated to https://www.code2040.org - thank you for helping me help the engineers of tomorrow.\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan',
        'thumbnail': 'https://i.ytimg.com/vi/o2GuOvO-Mns/default.jpg',
        'published_at': datetime.datetime(2021, 1, 30, 15, 30, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': [],
        'view_count': 29392,
        'like_count': 1423,
        'dislike_count': 16,
        'favorite_count': 0,
        'comment_count': 200
    },
    {
        'youtube_id': 'y29ZlhuEIyc',
        'title': 'The Future of Software Development | Build Software with NO CODE | with Michael Skelly from Stacker',
        'description': "Not knowing how to code is no longer a limitation on building the perfect software for your company. Today I'm introducing you to Michael Skelly, co founder of Stacker. With Stacker, you can easily get software built with no technical knowledge required. It’s a game changer, and over 500 companies already utilize it to dramatically optimize their workflows and unlock new capabilities in software they never thought possible.\n\nAlso... Stacker is hiring!\nhttps://www.stackerhq.com/jobs\n\nLearn more:\nhttps://www.stackerhq.com\n\n1:11 What is Stacker?\n3:07 Who’s using it now? 500 teams!\n3:44 Stacker’s origin story: Scratch your own itch\n7:41 Solving problems at marketplace startups\n10:09 True no-code: non-technical teams don’t have to wait for engineering!\n10:55 Stacker was not built for prototyping!\n12:00 Stacker is multi user\n12:45 Example customer: Project N95\n14:36 Any process that a person is doing can be replaced\n16:41 Stacker enables Kaizen\n18:14 What does Michael wish he knew when he started in tech?\n20:34 Advice: Use the minimum amount of tech possible to solve a problem\n21:08 Stacker is hiring - stackerhq.com/jobs\n21:52 Use Stacker now for all of your projects!\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/y29ZlhuEIyc/default.jpg',
        'published_at': datetime.datetime(2021, 1, 26, 15, 30, 30),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': [],
        'view_count': 30290,
        'like_count': 1053,
        'dislike_count': 20,
        'favorite_count': 0,
        'comment_count': 85
    },
    {
        'youtube_id': 'K3Ctc1UN0bQ',
        'title': "How To DESIGN EVERYTHING if you don't know ANYTHING | Design for Startups | Office Hours Ep. 1",
        'description': 'Today we walk through 5 steps so that you can start designing anything, anywhere, regardless of whether or not you\'ve ever done it before. The fact that you clicked on this video means you don\'t think of yourself as a designer. Stop that right now.\n\nYou can use interaction design, UX design, and visual design to reach product market fit. Minimalism is a big piece of it. Here\'s how—\n\n0:00 Intro\n0:43 Overview\n1:13 Step 1: De-label Yourself\n2:30 Step 2: Empathize With Your User\n6:24 Step 3: Illuminate The Path for Users\n8:21 Step 4: Minimize the Non-essential\n10:08 Step 5: Create Something Great\'\n12:50 Recap! What You Need to Know\n\nWatch my Y Combinator Talk: "Design for Startups"\nhttps://www.youtube.com/watch?v=9urYWGx2uNk\n\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan',
        'thumbnail': 'https://i.ytimg.com/vi/K3Ctc1UN0bQ/default.jpg',
        'published_at': datetime.datetime(2021, 1, 20, 15, 30, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'design', 'startup design', 'y combinator', 'design for startups', 'user experience design', 'ux design', 'visual design', 'how to do design', 'design tutorial', 'design for beginners', 'ux for beginners', 'visual design for beginners', 'visual design for startups', 'interaction design', 'user experience', 'how to design', 'minimalism', 'design minimalism', 'adolf loos', 'edward tufte', 'chartjunk', 'initialized capital', 'startups', 'product market fit', 'garry tan initialized capital', 'how to design products'],
        'view_count': 51032,
        'like_count': 3433,
        'dislike_count': 21,
        'favorite_count': 0,
        'comment_count': 216
    },
    {
        'youtube_id': '8MjWCkcLf1w',
        'title': 'The $1 TRILLION Market - How to File Crypto Taxes & Manage Holdings Chandan Lodha of CoinTracker',
        'description': 'Managing your crypto holdings can be complicated with the space gaining popularity, but there are many entrepreneurs working to make it simpler.\n\nToday I want to introduce you Chandan Lodha, Co-Founder of Cointracker, which helps you track your entire crypto portfolio seamlessly. We cover Crypto Taxes, how it relates to the stock market, and even how each of us got our first Bitcoin. Companies like Cointracker are paving the way for the Bitcoin ecosystem to be bigger than we ever could have imagined, so I’m so excited to be a part of companies like Chandans.\n\nLinks:\n\nhttps://twitter.com/cglodha\nhttps://twitter.com/CoinTracker\n\nhttps://www.cointracker.io/a/initialized - 10% Off Cointracker\n\n0:00 Intro\n1:11 What is Cointracker?\n1:57 An always up to date crypto dashboard\n2:55 The best crypto tax solution\n3:21 One-click setup from Coinbase\n4:14 10,000 bugs to fix for product-market fit\n5:35 Decentralized tracking is 100x harder\n6:24 Tax loss harvesting is free money\n8:10 How he got into Crypto\n9:25 Getting into crypto: Tinkering helps!\n11:59 Monetary policy rules everything around me\n13:30 Crypto is in the 1st inning: It’s just getting started\n14:46 Crypto can still 100x or more\n15:16 Advice for crypto newbies\n17:14 How to decide how much to buy, and when?\n18:02 Warning: If you sell, set aside for taxes\n19:26 Cointracker is designed to help you, the user\n21:32 Supporting hundreds of exchanges and thousands of blockchains\n22:22 What did Chandan wish he knew at 18 or 22?\n23:00 Just do a startup! Get into it.\n23:55 Going from zero to one\n24:58 Lessons from the idea search\n25:35 The 2 things that matter when building from 0 to 1\n27:11 Surviving the 2017 crypto rollercoaster\n28:45 The silver lining from the crypto winter\n30:06 Why price doesn’t matter\n30:45 Institutional demand driving 2021’s bull run\n33:31 Sign up for Cointracker\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan',
        'thumbnail': 'https://i.ytimg.com/vi/8MjWCkcLf1w/default.jpg',
        'published_at': datetime.datetime(2021, 1, 18, 15, 30, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['cryptocurrency', 'bitcoin', 'ethereum', 'coinbase', 'intro to cryptocurrency', 'crypto', 'buying cryptocurrency', 'cointracker', 'cryptocurrency tax return', 'cryptocurrency trading', 'chandan lodha', 'garry tan', 'initialized capital', 'cryptocurrency prices', 'crypto news', 'btc', 'crypto taxes', 'bitcoin taxes', 'bitcoin tax', 'cryptocurrency tax', 'bitcoin analysis today', 'how to do crypto taxes'],
        'view_count': 7267,
        'like_count': 256,
        'dislike_count': 3,
        'favorite_count': 0,
        'comment_count': 55
    },
    {
        'youtube_id': 'cRDOj4EZ9qo',
        'title': 'The Elon Musk of Baseball: How Startups Hit Grand Slams | Founder’s Journey: Ep. 1',
        'description': "Startups are not like baseball at all. They are snowballs. Just like a snowball, the more you pack on, the easier it is for them to get big. And once you get going with the product-market fit, they have so much momentum that they take on a life of their own.\n\n0:00 Intro: The Legend\n1:27 4 Topics Covered\n1:54 Part 1: Point Well\n2:20 Part 2: Put Points on the Board\n6:46 Part 3: Make The Myth\n8:29 Part 4: Win the Series\n10:09 The Takeaways for YOU\n11:26 Outro: Why I'm doing this, thanks and subscribe! :)\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM -\n\nhttps://instagram.com/garrytan\nhttps://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/cRDOj4EZ9qo/default.jpg',
        'published_at': datetime.datetime(2021, 1, 12, 15, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'babe ruth', 'elon musk', 'initialized capital', 'called shot', 'product market fit', 'startups', 'venture capital', 'startup advice', 'billion dollar startups', 'entrepreneur motivation', 'founder advice'],
        'view_count': 6085,
        'like_count': 532,
        'dislike_count': 2,
        'favorite_count': 0,
        'comment_count': 104
    },
    {
        'youtube_id': 'kJ4o0ONH7W8',
        'title': 'Masterclass: How to hire engineers remotely (10X more remote work candidates)',
        'description': "Today I wanted to welcome back Ammon Bartram from Triplebyte. He’s here to talk about all the things you need to know to be able to properly run your software engineering interviews, but completely remotely. The good news about remote hiring? The number of people who could work at your company just went up by 10X to 100X.\n\n00:00 Intro\n01:30 How to hire remotely\n02:30 Interview process\n04:37 Brainteasers are noise\n07:17 Low noise questions\n07:37 Triplebyte Screen: Rank and filter applicants quickly\n09:33 10X larger candidate pool\n10:39 No need to go to Harvard or Stanford\n12:01 Remote interviewing great for diversity\n13:34 How to close candidates remotely\n16:06 Soft skills assessment remotely\n18:03 Now is the best hiring market in 10 years\n\nSign up for Triplebyte Screen now at https://triplebyte.com/screen\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/kJ4o0ONH7W8/default.jpg',
        'published_at': datetime.datetime(2021, 1, 9, 20, 0, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['remote interviews', 'remote software engineering', 'remote software', 'remote engineering', 'remote software engineering jobs', 'remote jobs software', 'remote job interviewing', 'interviewing questions', 'software engineering', 'hiring', 'software engineer hiring', 'triplebyte', 'garry tan', 'ammon bartram', 'initialized capital', 'remote software developer', 'remote software engineer', 'remote software engineer jobs', 'remote work'],
        'view_count': 6963,
        'like_count': 266,
        'dislike_count': 3,
        'favorite_count': 0,
        'comment_count': 32
    },
    {
        'youtube_id': 'dmv-nooN43U',
        'title': 'Masterclass: Product Market Fit 10X Faster with Amy Jo Kim',
        'description': "Today I sat down with Amy Jo Kim, creator of @Game Thinking TV and early designer of world-class products including Rock Band, The Sims, Ultima Online, eBay and Netflix. We talk about her journey into tech, game dev, and how founders have a lot to learn from game thinking. \n\nAlong the way, we learn a number of secrets on how to get to product-market fit 10x faster. \n\n00:00 Intro\n01:06 Meet Amy Jo Kim\n01:47 Engineer, then designer, then the product\n02:48 Finding her tribe\n04:30 Game Thinking: PMF 10X faster\n06:22 Core loops driving retention\n08:31 Lessons from Ultima Online\n11:48 Test with core super fans first\n13:38 Pick your users\n15:26 Slack's customer journey\n19:04 Storyboarding skips you ahead\n23:38 The most important story: Your user's\n26:51 Overcoming founder ego\n30:15 What founders can learn from game design\n32:03 Amy Jo Kim's lessons for those starting out\n37:20 Learning more about Game Thinking\n\n5 Game Design Tips from Will Wright, Sims Designer\nhttps://youtu.be/scS3f_YSYO0\n\nStill Logged In: What AR & VR can learn from MMOs (Ralph Koster)\nhttps://youtu.be/kgw8RLHv1j4\n\nFollow and subscribe to Amy Jo Kim\nhttps://www.youtube.com/c/GameThinkingTV\nhttps://twitter.com/amyjokim\n\nBuy Amy Jo Kim's book Game Thinking https://amzn.to/37VA5Uj \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/dmv-nooN43U/default.jpg',
        'published_at': datetime.datetime(2020, 12, 29, 15, 0, 27),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['amy jo kim', 'game thinking', 'product market fit', 'product management', 'journey', 'ultima online', 'game design', 'will wright', 'design for founders', 'game design for founders', 'game design theory', 'garry tan', 'initialized capital', 'product management training', 'what is product management', 'video game design', 'will wright game design', 'product management basics', 'product', 'product school', 'amy jo kim game thinking', 'game thinking design product', 'game dev', 'advice for entrepreneurs', 'gamification', 'design'],
        'view_count': 30048,
        'like_count': 1243,
        'dislike_count': 18,
        'favorite_count': 0,
        'comment_count': 133
    },
    {
        'youtube_id': '9NYrsE_yW64',
        'title': 'The founder mindset you need - (Bill Gates’s lesson on time frame)',
        'description': 'If you want to make something truly big, you\'ll realize you have to be a short term pessimist, and long term optimist. This is the opposite of what most would-be founders actually have. And that, I think, is why most startups fail. \n\nMost founders pursue ideas that are very short term, and they\'re wildly optimistic about these short term goals. And they ignore obvious things in front of them that should cause them to change course. But as Bill Gates says, they wildly overestimate what can be done in one year, and wildly underestimate what can be done in 10. \n\n00:00 Intro\n00:48 Short term pessimism\n02:43 Short term optimism\n05:30 Long term optimism\n\nWatch my video on the Stockdale Paradox if you haven\'t seen it yet: https://www.youtube.com/watch?v=dLicgt04hHY&ab_channel=GarryTan\n\nMusic - "Always Forward" - Instrumental by Homage - https://youtu.be/0vKyeJboYxE\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we\'re going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!',
        'thumbnail': 'https://i.ytimg.com/vi/9NYrsE_yW64/default.jpg',
        'published_at': datetime.datetime(2020, 12, 22, 17, 45, 31),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'initialized capital', 'founder advice', 'pessimism', 'optimism', 'startup optimism', 'startup pessimism', 'startup success', 'venture capital', 'startups', 'startup advice', 'who is garry tan', 'startup school', 'silicon valley', 'entrepreneur', 'lean startup', 'entrepreneurship'],
        'view_count': 42541,
        'like_count': 2594,
        'dislike_count': 16,
        'favorite_count': 0,
        'comment_count': 164
    },
    {
        'youtube_id': 'vhTicEJTstA',
        'title': 'Trillions of dollars in real estate— How do we fund the future we want to live in?',
        'description': "Today’s a big day for Initialized. Opendoor, a company we invested in at the earliest stage, begins trading on Nasdaq. Startups like Opendoor and Airbnb are transforming one of the world’s biggest markets. \n\nLet’s go hang out with my colleague and Initialized Capital partner Kim-Mai Cutler to talk about what’s the future of housing and real estate? How can tech, finance, and cities work together to bring about a society we want to live in? She’s led our real-estate related investments in Landed, Abodu, and Culdesac. \n\nLearn more about Abodu with our interview with the founders here:\nhttps://www.youtube.com/watch?v=P-Igj27Rh_k\n\n00:00 Intro\n00:57 Opendoor's key insight\n02:21 Making housing more accessible\n06:14 Millions of lots = trillions of dollars\n10:20 SF Bay Area is our home\n12:43 Funding the future we want to live in\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan \nFollow me on Twitter— @garrytan https://twitter.com/garrytan\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/vhTicEJTstA/default.jpg',
        'published_at': datetime.datetime(2020, 12, 21, 18, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['real estate', 'opendoor', 'startup', 'kim-mai cutler', 'garry tan', 'initialized capital', 'initialized', 'culdesac', 'abodu', 'adu laws', 'california housing', 'adu laws 2020', 'housing', 'startups', 'venture capital', 'product hunt cto', 'silicon valley', 'entrepreneurship'],
        'view_count': 6352,
        'like_count': 247,
        'dislike_count': 9,
        'favorite_count': 0,
        'comment_count': 44
    },
    {
        'youtube_id': 'kt9ScQvTqcU',
        'title': 'Silicon Valley is still for future billionaires (with Elad Gil, legendary investor)',
        'description': "Re-posting a panel talk we did with Elad Gil, legendary investor in Stripe, Instacart and 14 other multi-billion-dollar startups, and Bloomberg Reporter Katie Roof at Web Summit last week. If you're keeping score I've only funded 10!\n\nThe water is fine. Silicon Valley is doing great. What's going on with all these crazy rounds? We talk through all of those things. \n\n00:00 Intro\n00:28 What does remote work mean for tech?\n05:16 The market for tech is now 10x to 20x bigger than web 1.0\n05:44 Global unicorns: 50% were us, 25% were in Silicon Valley\n07:08 Elad on global growth tech investing\n08:25 Is there too much Venture Capital now?\n09:15 No software? Mr. Market says maybe no future cash flow\n10:17 Anti-Amazon strategy\n11:19 No value investing in Unicorn hunting\n12:26 What's actually overvalued?\n14:11 How to find future sectors\n15:08 Don't ask a VC what's hot - that's 9 to 12 mo too late\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nDo you like my audio and video setup? My full kit is here: https://kit.co/garrytan\n\nFollow Elad Gil on Twitter - @eladgil https://twitter.com/eladgil\n\nFollow me on Twitter— @garrytan https://twitter.com/garrytan\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/kt9ScQvTqcU/default.jpg',
        'published_at': datetime.datetime(2020, 12, 18, 15, 0, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['silicon valley', 'entrepreneurship', 'ai', 'elad gil', 'garry tan', 'initialized capital', 'ipo', 'startup unicorns', 'unicorns', 'leaving silicon valley', 'startups', 'venture capital'],
        'view_count': 20337,
        'like_count': 587,
        'dislike_count': 9,
        'favorite_count': 0,
        'comment_count': 76
    },
    {
        'youtube_id': 'Mb5Fk1GXuEM',
        'title': 'How to build the next great startup with remote work, with Andreas Klinger (fmr CTO of Product Hunt)',
        'description': "Andreas Klinger was CTO of Product Hunt and now runs Remote First Capital. He's a great engineer turned investor and we just sat down in May of 2020 for a hang session. Posting it now in December, the things we talked about have become only more relevant. Hear lessons about building a remote team, practical guidelines on how to compensate and hire these teams, and also what the future of San Francisco might be as a state of mind.\n\n00:00 Intro\n00:39 Meet Andreas Klinger\n01:42 Remote engineer compensation\n03:18 Regional differences (how to handle time zones)\n04:33 Andreas's recommendations on how to build a remote team\n05:52 Remote work is now normal\n11:08 What is Andreas investing in?\n12:29 Macro effects of remote work are just beginning\n16:21 What does Andreas wish he knew when he was just entering tech?\n17:06 Sell a product, not your hours\n21:53 Write down a personal vision for yourself\n24:04 How to reach out to Andreas\n\nYou should follow Andreas on Twitter here — https://twitter.com/andreasklinger \n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nFor 2020 I’m donating all proceeds from my YouTube channel (Ads + affiliate links) to Code2040 to fight for racial equality in tech. Join me in donating at https://code2040.org\n\nFor startup advice - Follow and DM me on Instagram https://instagram.com/garrytan \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/Mb5Fk1GXuEM/default.jpg',
        'published_at': datetime.datetime(2020, 12, 15, 15, 0, 12),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['remote work', 'andreas klinger', 'product hunt', 'product hunt cto', 'remote work capital', 'garry tan', 'initialized capital', 'remote', 'startups', 'remote teams', 'startup', 'work from home', 'venture capital'],
        'view_count': 17541,
        'like_count': 731,
        'dislike_count': 3,
        'favorite_count': 0,
        'comment_count': 78
    },
    {
        'youtube_id': 'viNZK9P40kY',
        'title': 'How Airbnb & DoorDash Succeeded: First Principles (3 steps for billion dollar startups)',
        'description': 'What society imposes on us is the wrong kind of thinking: one that is based on analogy and credentialism instead of first principles thinking. But how do we break free? Here are 3 ways to do it. \n\nThe third one is the most surprising! What does serenity have to do with building a billion dollar startup? In DoorDash\'s case: EVERYTHING. \n\nIf you like lifelong learning, try one of our startups we funded called Knowable. They\'re the best app for audio courses. https://knowable.fyi, 25% off discount code GARRY\n\nI\'m donating all proceeds for 2020 from my YouTube channel (ad revenue and affiliate links) to https://code2040.org and I hope you\'ll consider joining me in pushing for more racial equality in tech.\n\nMusic: "Azygous" Instrumental by Homage https://youtu.be/bNesMW_m6lg\n\n—\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we\'re going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this\n\nHOW TO TALK TO ME\n\nI can answer your startup questions on my comments section, or you can DM me on Instagram https://Instagram.com/garrytan',
        'thumbnail': 'https://i.ytimg.com/vi/viNZK9P40kY/default.jpg',
        'published_at': datetime.datetime(2020, 12, 12, 17, 33, 40),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['airbnb', 'doordash', 'airbnb stock', 'abnb', 'dash', 'tony xu', 'brian chesky', 'y combinator', 'garry tan', 'initialized capital', 'eric ries', 'five whys', 'first principles thinking', 'first principles', 'billion dollar startup', 'lean startup', 'airbnb ipo', 'airbnb business', 'airbnb stock analysis', 'airbnb tips', 'airbnb ipo analysis', 'should i buy airbnb stock', 'doordash ipo'],
        'view_count': 50751,
        'like_count': 3577,
        'dislike_count': 44,
        'favorite_count': 0,
        'comment_count': 281
    },
    {
        'youtube_id': 'decDIkDbS3Q',
        'title': 'To become a billionaire, help a billion people | Career Karma helps you find a coding bootcamp',
        'description': "Want to jumpstart your career in tech? Click the link below to match with the best programs for you and schedule your first coaching session.\n\nThe first 1,000 people to sign up will get a free coaching session!\nhttps://careerkarma.com\n\nToday I'm proud to announce my VC firm Initialized Capital led a $10 million Series A in Career Karma. Their community is helping people find the right coding bootcamp or university for their needs, while also helping them meet other people with similar backgrounds who have done what they want to do: get six figure jobs in tech and help their families, friends, and neighborhoods. Ruben Harris and his cofounders broke into tech themselves, and now they are helping a new generation of people get into tech.\n\nY Combinator showed that it takes a village to achieve success: watching Brian Chesky speak at weekly dinners made it clear you as a founder could succeed too. The YC Community has created $150 billion worth of value and that's thousands of people. In the same way, Career Karma will work with millions of people who need job retraining— building a community of people who will help each other over their entire careers. They've built a fast growing profitable startup on this idea: Colleges spend $10 billion per year on just recruiting and marketing. Job retraining is a key piece of helping people bridge the digital divide while also reducing income inequality. \n\nSign up and try Career Karma now: https://smarturl.it/garry-tan-yt-1\n\n00:00 Intro\n01:54 What is Career Karma?\n03:38 How Ruben broke into startups\n04:26 Four year colleges are not the only way to learn\n05:13 Software eating the world means tech job training is even more important\n06:21 55M Americans are unemployed\n07:01 People want high paying jobs but need networks to help them\n09:46 How Career Karma reached product market fit\n10:03 $1 trillion spent on workforce training per year\n10:29 Colleges spend $10 billion per year on enrollment/recruiting\n11:29 There is no one-size-fits-all education— which is why Career Karma is needed\n16:21 Taking YC's community and bringing it to everyone\n17:22 How Ruben and his cofounders broke into tech: Founder-market fit\n22:19 Success story: Kesha Lake got a software engineering job at Stitch Fix, bought a house, now helping 20 friends through squads\n24:45 Career Karma is hiring\n25:58 Ruben's lessons for people starting out at 20 years old\n28:16 If you want to be a billionaire, help a billion people\n\nCareer Karma is Hiring — https://careerkarma.com/jobs\n\nIf you're interested in finding the right school and community for you to help you get into tech and learn to code, sign up at https://careerkarma.com\n\nYou should follow Ruben on Twitter here https://twitter.com/rubenharris\n\nSubscribe to Career Karma's YouTube channel: https://www.youtube.com/channel/UCkCwv67-HTCzIhxLG8aHOQQ",
        'thumbnail': 'https://i.ytimg.com/vi/decDIkDbS3Q/default.jpg',
        'published_at': datetime.datetime(2020, 12, 9, 15, 20, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['career karma', 'job retraining', 'y combinator', 'ruben harris', 'how to get a job in tech', 'learn to code', 'coding bootcamp', 'which isa is best', 'which coding bootcamp is the best', 'yc', 'yc community', 'career karma community', 'coding', 'career karma review', 'how good is career karma', 'how to get jobs with career karma', 'is it worth going to coding bootcamp', 'is it worth', 'should you go', 'learn how to code', 'coding bootcamp 2021'],
        'view_count': 16356,
        'like_count': 692,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 63
    },
    {
        'youtube_id': 'dBULOo03cys',
        'title': 'This 26 year old investor funded Nadeshot and 100 Thieves: A conversation with Blake Robbins',
        'description': "Blake Robbins is a venture capitalist is a case study in being helpful up front. He's a great example of what great things can happen when you combine what you are genuinely interested in with an ability to show value even through a cold email. It was a pleasure to sit down with him to talk about his path into venture, his experience with helping build top e-sports startup 100 Thieves, and how YouTube creators and startups can work together to build the next generation of consumer startup.\n\n00:00 Intro\n01:51 Getting started cold-emailing every startup on TechCrunch\n02:36 Learning about Venture Capital for the first time\n03:14 Pick your internships well\n04:29 Cold-email to Ludlow Ventures to get his first VC job\n05:55 Cold emails can work but you have to be persistent and helpful\n08:25 The 100 Thieves creation story\n12:01 Cold DM made 100 Thieves happen\n14:04 Key early hire at 100 Thieves: Jackson Dahl\n15:50 Nadeshot is a creator who built something bigger than a personal brand\n16:45 How Blake met Mr. Beast\n18:22 Influencer strategy is key for new consumer startups\n20:08 Casey Neistat & Beme\n21:13 How startups can work with creators\n22:37 Rule of thumb for creators thinking about equity\n24:14 How creators can work with startups\n28:16 Creators as cofounders of startups\n29:25 How do you align creators with cofounders?\n33:42 What Blake wishes he knew when he first started\n\nYou should follow Blake on Twitter here: https://twitter.com/blakeir\n\nBONUS! Blake just started a YouTube channel and you should subscribe to him here https://www.youtube.com/channel/UCjKsEp4qBJFCKWWnh6zfAxw",
        'thumbnail': 'https://i.ytimg.com/vi/dBULOo03cys/default.jpg',
        'published_at': datetime.datetime(2020, 12, 7, 15, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['blake robbins', 'ludlow ventures', 'ludlow', 'detroit startup', '100 thieves', 'esports', 'how to get a job in vc', 'how to get a job at a venture capital firm', 'youtube billionaire', 'influencer startup', 'nadeshot', 'jackson dahl', 'venture capital', 'carpool.vc'],
        'view_count': 18124,
        'like_count': 781,
        'dislike_count': 13,
        'favorite_count': 0,
        'comment_count': 54
    },
    {
        'youtube_id': 'PBmT30aUd2s',
        'title': 'Delivering Founder Happiness',
        'description': '“Happiness is really just about four things: perceived control, perceived progress, connectedness (number and depth of your relationships), and vision/meaning (being part of something bigger than yourself).” —Tony Hsieh 1973 - 2020\n\n"Coast to Coast" Instrumental by Homage https://youtu.be/VlXBIWeuACE\n"Old Soul" Instrumental by Homage https://youtu.be/O5GyeZrRfL8',
        'thumbnail': 'https://i.ytimg.com/vi/PBmT30aUd2s/default.jpg',
        'published_at': datetime.datetime(2020, 12, 1, 15, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['tony hsieh', 'zappos', 'delivering happiness', 'founder', 'starting companies', 'founder motivation', 'founder happiness', 'zappos.com', 'tony hsieh zappos', 'zappos culture', 'delivering happiness by tony hsieh', 'zappos delivering happiness'],
        'view_count': 11684,
        'like_count': 793,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 95
    },
    {
        'youtube_id': 'ROw1GMEP_14',
        'title': 'Getting hired at Google after 20 rejections — Hacking the process',
        'description': "Viewer interview! Michael Brown hacked the Google hiring process. He tried the jobs page and couldn't get in through the front door. He found another way, asking the right questions, and hacking the system by figuring out what people at HQ wanted. Cool story from someone from the YouTube community. \n\n00:00 Intro\n00:38 Meet Michael Brown\n01:30 20 applications, 100% rejections\n02:15 The window: staffing agencies for Google projects\n02:52 Becoming a grocery picker for Google Shopping\n03:09 Going above and beyond\n04:35 First try: no response\n06:30 2nd try: Full time offer\n07:36 Michael's parting advice\n\nWatch my video How to Win on how to win career tournaments—\nhttps://www.youtube.com/watch?v=5siggWM8faY",
        'thumbnail': 'https://i.ytimg.com/vi/ROw1GMEP_14/default.jpg',
        'published_at': datetime.datetime(2020, 11, 28, 16, 0, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['google', 'get a job at google', 'google hiring', 'google application', 'google hiring process', 'how to get ahead', 'career opportunities', 'first job', 'first job at google', "google's hiring process", 'life at google', 'google jobs', 'work at google', 'google recruiting process', 'google recruitment', 'google recruitment process', 'how google hires', 'how to get a job at google', 'how to get job in google', 'google recruiting'],
        'view_count': 12601,
        'like_count': 646,
        'dislike_count': 11,
        'favorite_count': 0,
        'comment_count': 74
    },
    {
        'youtube_id': '0pzphszE704',
        'title': 'AptDeco is eating the $14B furniture market: How they got product market fit',
        'description': "Today we're sitting down with Reham Fagiri and Kalam Dennis, cofounders of AptDeco, the next great marketplace startup. \n\nEveryone's got some furniture that doesn't quite fit their room. You bought it, you thought it would work, but it's not quite right. What if I told you there's a managed marketplace now that will let you get a lot of your money back, and then find your next piece, all at once? \n\nIt's here and it's called AptDeco. They're a managed marketplace that is expanding the market for used furniture. It's my favorite way software can create new behaviors in the same way Instacart let you get your groceries or Uber made it easier to get around. And it's in a $14billion market that can only get bigger, because this is supply and demand that didn't exist before.\n\nThis team is a case study in being data driven, doing unscalable things, and then scaling them in one of the world's biggest possible markets.\n\n00:00 Intro\n00:51 Meet Reham and Kalam\n02:07 AptDeco is the next great managed marketplace\n04:38 Sustainability: AptDeco saves 9M pounds of CO2\n05:24 How they solved demand\n06:51 Key learning: Do your ad buys in-house\n07:43 Key learning: Do your logistics in-house\n09:55 Best software-based logistics = no warehousing\n12:16 Product market fit = supply constrained, unlimited demand\n13:13 Diversity and inclusion in Tech\n17:28 Lessons from Reham and Kalam wish they knew when they started\n19:40 Lesson: Do the unscalable and scale them\n24:06 AptDeco is live now for NYC\n24:52 AptDeco is hiring remotely for engineers and product",
        'thumbnail': 'https://i.ytimg.com/vi/0pzphszE704/default.jpg',
        'published_at': datetime.datetime(2020, 11, 19, 15, 0, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['aptdeco', 'y combinator', 'initialized', 'garry tan', 'startup', 'billion dollar startup', 'furniture startup', 'furniture business', 'furniture reseller', 'used furniture', 'used furniture marketplace', 'used furniture startup', 'managed marketplace', 'aptdeco.com', 'furniture', 'selling furniture', 'nyc', 'early stage startup', 'new york startup', 'reham fagiri', 'kalam dennis', 'startups', 'buying used furniture', 'selling furniture in new york', 'buying furniture in new york', 'entrepreneur', 'second hand furniture'],
        'view_count': 12988,
        'like_count': 520,
        'dislike_count': 8,
        'favorite_count': 0,
        'comment_count': 69
    },
    {
        'youtube_id': '3YKNr-LiblI',
        'title': 'Billion dollar startup ideas',
        'description': "What makes you new, different, and unique in the world will often be the exact thing that makes you succeed. Embrace your differences, and you’ll find something amazing in there. Learn how founders like Steve Jobs, Bill Gates, Ryan Petersen of Flexport, and Jack Conte of Patreon did exactly that: learn things from unique personal experiences that put them on the right path. \n\nAnd if you can do that, you avoid infinite competition. You can make something truly awesome. \n\n00:00 Intro\n01:52 Steve Jobs on Unique Experiences\n03:04 How Ryan Petersen's experiences led him to make Flexport\n04:26 How Jack Conte's experiences led him to make Patreon\n05:24 You don't have to be novel\n06:01 Great startups start as toys\n06:43 Paul Graham on startup ideas\n\nRead How to get Startup Ideas by Paul Graham — http://paulgraham.com/startupideas.html \n\nBeats by Homage Beats - 5am https://www.youtube.com/watch?v=on4gBvEyfCg&ab_channel=HomageBeats\n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind me on Instagram where I can answer your startup advice questions by DM - https://instagram.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/3YKNr-LiblI/default.jpg',
        'published_at': datetime.datetime(2020, 11, 14, 15, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'y combinator', 'startup school', 'garry tan', 'initialized', 'initialized capital', 'flexport', 'patreon', 'ryan petersen', 'jack conte', 'steve jobs', 'steve jobs motivational', 'startup ideas', 'billion dollar startups', 'tech startups', 'startup', 'best startups 2020', 'best startups 2021', 'best startups in america', 'most valued unicorn startups in usa', 'startups in america', 'entrepreneurship', 'startups 2020', 'unicorn startups', 'venture capital', 'entrepreneurs', 'unicorn startup', 'founding story', 'top startups'],
        'view_count': 193015,
        'like_count': 10736,
        'dislike_count': 149,
        'favorite_count': 0,
        'comment_count': 514
    },
    {
        'youtube_id': 'qlP4bnZ_izk',
        'title': 'Solving the $160 billion food waste problem: Meet Stefan Kalb of Shelf Engine',
        'description': "Stefan Kalb started a food brand at age 23, and that's when he discovered 30% of all perishable food goes in the garbage. He teamed up with a technical cofounder Bede Jordan in 2016 and now they're solving the problem with software. They were solving a problem they saw first hand. They had their twists and turns, and it was something I worked with Stefan and Bede on for years as they followed their own internal north star. Along the way, they figured it out.\n\nNow with product market fit, and signed expanding contracts with 4 of the top 10 US national grocers, they're on track to solve the $160B annual US food waste problem and build the next billion dollar startup in the process.\n\n00:00 Intro\n00:50 Meet Stefan Kalb, cofounder of Shelf Engine\n01:06 What is Shelf Engine?\n02:00 Coming up with the Shelf Engine idea\n04:51 Stefan's direct experience building a food brand\n05:35 Business cofounder + great tech cofounder\n06:30 Early validation of product market fit\n07:00 Raising first money in\n07:54 How Stefan met Initialized\n08:27 First principles approach to building a startup\n09:59 What is it like to sell to grocery store incumbents?\n11:20 How to do enterprise sales\n12:20 A go-to-market pivot led to product market fit\n14:20 Startup founders must be like water (Bruce Lee)\n15:38 Product market fit is unrelated to investor pattern match\n17:25 How do you know when you have product market fit?\n18:22 What is it like to work at Shelf Engine?\n21:21 Shelf Engine is working with 4 of the top 10 US national grocers\n23:10 What does Stefan wish he knew when he started in tech?\n\nLearn more about Shelf Engine at https://www.shelfengine.com/\n\nFollow Stefan Kalb on Twitter at https://twitter.com/stefankalb",
        'thumbnail': 'https://i.ytimg.com/vi/qlP4bnZ_izk/default.jpg',
        'published_at': datetime.datetime(2020, 11, 9, 15, 0, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['food waste', 'shelf engine', 'stefan kalb', 'kroger', 'safeway', 'walmart', 'y combinator', 'garry tan', 'initialized capital', 'food supply chain startup', 'food supply chain', 'supply chain', 'enterprise startup', 'climate startup', 'climate change startup', 'stop food waste', 'yc', 'y combinator application', 'startup school', 'supply chain management', 'startup', 'startup company', 'vc', 'sustainability', 'environment', 'entrepreneur', 'climate change', 'startups', 'entrepreneurship', 'ycombinator', 'food thrown away', 'donate leftover food'],
        'view_count': 13215,
        'like_count': 634,
        'dislike_count': 4,
        'favorite_count': 0,
        'comment_count': 84
    },
    {
        'youtube_id': 'uQWk7T9Vz_k',
        'title': 'How I got into Y Combinator',
        'description': "Don't look for a mentor. Find people who you believe in who can help you, and help them first. That's how I got into Y Combinator, and this is my story.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nBeats by HomageBeats: https://www.youtube.com/watch?v=j3b7Yf-z52g\n\nFollow me on Twitter at https://twitter.com/garrytan\n\nInstagram DMs are open for startup advice at https://instagram.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/uQWk7T9Vz_k/default.jpg',
        'published_at': datetime.datetime(2020, 11, 5, 15, 0, 13),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['y combinator', 'y combinator application', 'y combinator process', 'y combinator application 2020', 'y combinator application 2021', 'applying to y combinator', 'applying to yc', 'yc interview', 'yc application', 'garry tan', 'initialized capital', 'startup school', 'startup school 2008', 'posterous', 'incubators', 'accelerators', 'how to get into y combinator', 'yc', 'yc video', 'y combinator application video', 'startup', 'startups', 'entrepreneur', 'entrepreneurship', 'ycombinator', 'business ideas'],
        'view_count': 25724,
        'like_count': 1466,
        'dislike_count': 15,
        'favorite_count': 0,
        'comment_count': 191
    },
    {
        'youtube_id': 'MnuFEvI7EGw',
        'title': 'Drip Capital went from zero to $1 billion in transactions: Fintech meets global trade finance',
        'description': "Drip Capital started as two people with an idea in 2015. Today, they have over 138 employees, raised more than $45M, and will do over $1 billion in trade volume since inception this year. They have over 2,000 importers and exporters on the platform and have built an enduring fintech startup. They're on track to be as impactful as the World Bank in terms of helping businesses worldwide get access to credit.\n\nThey had to search the space to find one that could grow quickly with high LTV and low CAC. It was really impressive to see as an investor, and I'm so glad we could sit down with cofounder Neil Kothari today.\n\n00:00 Intro\n00:38 Meet Neil Kothari\n02:50 Starting the founder journey\n03:56 First iteration: Lending in the US\n05:52 Pivot to global trade finance, finding product market fit\n07:18 More data = better lending (key insight)\n09:37 Why does net 30 exist when interest rates are zero?\n11:48 Incumbent banks aren't doing their job, which lets Drip create a new cross-border trade model\n13:46 Why is lending hard?\n14:35 What they learned in India\n18:00 Do unscalable things, then scale them\n21:10 Beware advice\n23:31 How to invest, or work at Drip\n\nHi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/MnuFEvI7EGw/default.jpg',
        'published_at': datetime.datetime(2020, 10, 28, 14, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['trade finance', 'neil kothari', 'fintech', 'lending startup', 'fintech startup', 'zero interest rates', 'world bank', 'india cross border trade', 'india trade', 'mexico usa trade', 'cross border trade', 'global manufacturers', 'manufacturer', 'manufacturer lending', 'international trade finance', 'trade finance methods', 'trade finance tutorial', 'trade finance in banking', 'supply chain finance', 'finance', 'trade finance basics', 'drip capital', 'accel india', 'garry tan', 'initialized capital', 'y combinator'],
        'view_count': 19296,
        'like_count': 801,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 92
    },
    {
        'youtube_id': '5siggWM8faY',
        'title': '5 Steps to Winning Career Tournaments: Competitive jobs, admissions, promotions, or startup funding',
        'description': "There are tournaments everywhere. Careers are often one tournament after another. Meritocracy (fair tournaments) are something society continues to strive for, but we're not there. \n\nHere are 5 steps to getting that job, raising that money, getting into that program or college, getting tenure, getting that press coverage, getting those followers, or whatever it is you are setting out to do. \n\n00:00 Intro\n00:36 My tournament failure story: Stanford Mayfield Fellows Program\n01:49 Two lessons from my own story\n02:18 5 steps to winning any career tournament\n02:57 Step 1 - Identify the tournament\n03:27 Step 2 - Learn the criteria early\n04:56 Step 3 - Apply to many to up your odds\n05:48 Step 4 - Get advice from those before you\n06:26 Step 5 - Door locked? Try the window.\n07:21 If you win enough tournaments, you get to create them\n\nApplying to Y Combinator? Watch these:\nHow to nail your application https://youtu.be/4wbCVN1yLyA\nHow to nail your interview https://youtu.be/rfTgzA6iKZc\n--\n\nHi! I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/5siggWM8faY/default.jpg',
        'published_at': datetime.datetime(2020, 10, 27, 14, 0, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['getting a job', 'getting press coverage', 'career tournament', 'getting into college', 'how to get ahead', 'competition', 'winning', 'tournaments', 'startup', 'how to get a job', 'job', 'job interview', 'getting a job in college', 'how to find a job', 'career', 'work', 'how to get a job with no experience', 'how to get a job as a teen', 'career coach', 'how to make money', 'getting promoted', 'work competition', 'how to get promoted', 'garry tan', 'meritocracy', 'careers', 'startups', 'career advice'],
        'view_count': 15435,
        'like_count': 1508,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 202
    },
    {
        'youtube_id': 'P-Igj27Rh_k',
        'title': 'Build housing in weeks, not years: Fight the California housing crisis with Abodu',
        'description': "If you've ever tried to build housing, you'll learn that it takes often YEARS to get something built. In California, this has become a deep societal problem that is affecting quality of life and it's one of the most important things to address. This is why Initialized funded Abodu, a startup that can help you build a studio, 1, or 2 bedroom apartment in your backyard (as an ADU or accessory dwelling unit) in 12 weeks, instead of getting stuck for years in planning department fights.\n\n00:00 Intro\n00:48 Meet Kim-Mai Cutler, housing activist and VC\n02:04 Meet John Geary and Eric McInerney, founders of Abodu\n02:29 Why accessory dwelling units?\n03:51 Palo Alto real customer story\n04:41 What's it like to work with Abodu?\n06:02 Where can I get an Abodu?\n07:09 How does factory built compare to stick built?\n08:12 Taking the guesswork and risk out of building\n08:40 How much does it cost?\n09:53 Financing\n10:47 Studio, 1BR and 2BR options\n11:46 Visit the showroom\n13:02 What's it like to visit?\n13:53 Abodu customer stories\n14:58 Housing is a key societal issue\n\nLearn more about Abodu at https://abodu.com\n\nFollow Kim-Mai Cutler on Twitter at https://twitter.com/kimmaicutler",
        'thumbnail': 'https://i.ytimg.com/vi/P-Igj27Rh_k/default.jpg',
        'published_at': datetime.datetime(2020, 10, 20, 14, 0, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['housing', 'california housing crisis', 'housing market 2020', 'california housing', 'adu', 'tiny home', 'build tiny home', 'planning department', 'abodu', 'initialized capital', 'kim-mai cutler', 'garry tan', 'john geary', 'eric mcinerney', 'accessory dwelling unit', 'building an adu', 'building adu', 'building adu in california', 'building housing', 'palo alto housing', 'san jose housing', 'redwood city housing', 'granny flat', 'housing crisis', 'housing market', 'housing startup', 'san jose adu', 'palo alto adu'],
        'view_count': 12713,
        'like_count': 271,
        'dislike_count': 10,
        'favorite_count': 0,
        'comment_count': 67
    },
    {
        'youtube_id': 'opkHJLVAM4A',
        'title': 'Billion dollar startups are better faster cheaper— Lessons from Amazon, Instacart, Uber & Shopify',
        'description': "Facebook wasn't the first social network. So maybe being first doesn't matter. But, being better, faster, or cheaper? That matters a lot.\n\nLessons from Airbnb, Amazon, Robinhood, Coinbase, Instacart, Uber, Lyft, Discord, Flexport and Shopify.\n\nI'm getting a lot of questions from founders and future founders. I'm realizing a lot of folks are not thinking about their startups in a way that will convince people to switch to use their products or services... and if that doesn't seem like it will happen, they won't be able to build teams, or raise money either. So the self-fulfilling prophecy cannot work. This video is for those founders. Can you describe what you are doing in classic better, faster, cheaper terms? If not, you need to.\n\n00:00 Intro\n01:23 Why investors like novelty so much\n02:30 How to be Better\n04:41 How to be Faster\n06:30 How to be Cheaper\n09:06 You can figure this out\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. I was earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/opkHJLVAM4A/default.jpg',
        'published_at': datetime.datetime(2020, 10, 19, 14, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Better faster cheaper', 'startup ideas', 'startup pitch', 'y combinator', 'y combinator pitch', 'the business of agile: better faster cheaper', 'strategy and tactics', 'exponential growth', 'yc', 'startup', 'startup school', 'business ideas', 'entrepreneur', 'startups', 'entrepreneurship', 'how to start a business', 'venture capital', 'silicon valley', 'best business ideas', 'new business ideas', 'startup company', 'garry tan', 'initialized capital'],
        'view_count': 73558,
        'like_count': 4675,
        'dislike_count': 34,
        'favorite_count': 0,
        'comment_count': 499
    },
    {
        'youtube_id': 'wxsDnQKcq4Y',
        'title': '20,000 YouTube subscribers in 2 weeks: What I learned',
        'description': 'My YouTube channel grew more than 20,000 subscribers in two weeks. I hit some sort of crazy if-else statement in the YouTube algorithm. It took about a year to do this after posting weekly for more than a year. This is the stuff I learned along the way, and how I learned to push through Ira Glass\'s "Creativity Gap" which is the gulf between your taste and what are capable of making right now. \n\nI also discovered how not to compare myself to others, and instead think about metrics internal to me. That helped me move beyond comparing my views to other videos and helped me think more internally about it.\n\nMusic: "Union Station", Instrumental by Homage https://youtu.be/T947cjXSb94',
        'thumbnail': 'https://i.ytimg.com/vi/wxsDnQKcq4Y/default.jpg',
        'published_at': datetime.datetime(2020, 10, 13, 14, 40, 30),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['growing youtube', 'growing youtube channel 2020', 'growing subscribers', 'youtube subscribers', 'how to get youtube subscribers', 'building youtube channel', 'ira glass', 'the gap', 'creators mind', 'how to start a youtube channel', 'grow your youtube channel', 'how to grow your youtube chann', 'grow on youtube', 'how to grow your youtube channel fast', 'youtube channel growth tips', 'youtube channel growth strategy', 'how to get subscribers on youtube', 'how to get subscribers', 'garry tan'],
        'view_count': 10572,
        'like_count': 921,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 377
    },
    {
        'youtube_id': 'V0eY2nBtsjw',
        'title': "Autonomous checkout is now a reality: Standard Cognition's camera-only computer vision stores launch",
        'description': "Today, Standard Cognition launches its first few stores nationwide, including one at the University of Houston. Today we'll get to see one of their first locations and chat with founder and CEO Jordan Fisher who shares what it took to get here, and what makes Standard Cognition ready for mass rollout to thousands of stores.\n\nAmazon Go couldn't do what this team has done. And they'll be able to arm the rebels (thousands of convenience stores and grocery stores) before Amazon eats their lunch. I'm an investor and on the board of Standard Cognition — so it's a big moment today for me too.\n\nWatch the live footage from their first Houston location here: https://www.youtube.com/watch?v=Ll-YJz5f8IU\n\nCoverage of this launch here at VentureBeat: https://venturebeat.com/2020/10/08/standard-launches-cashierless-store-at-the-university-of-houston/\n\nLearn more about Standard Cognition here: https://standard.ai/",
        'thumbnail': 'https://i.ytimg.com/vi/V0eY2nBtsjw/default.jpg',
        'published_at': datetime.datetime(2020, 10, 8, 14, 37, 29),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['standard cognition', 'cashierless checkout', 'amazon go', 'computer vision', 'amazon go grocery store', 'retail technology', 'retail startup', 'future of retail', 'future of retail stores', 'startup', 'san francisco startup', 'garry tan', 'initialized capital', 'y combinator', 'yc startup', 'autonomous checkout', 'machine learning', 'cashierless store', 'deep learning', 'ai', 'battle of the cashierless store', 'startups', 'ai store', 'retail'],
        'view_count': 13413,
        'like_count': 630,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 114
    },
    {
        'youtube_id': 'Xa6zsbXGQ5M',
        'title': 'Should you be the CEO?',
        'description': 'We were pitching A16z, with Marc Andreessen and Ben Horowitz— final Monday morning meeting on Sand Hill Road. They asked us who was CEO. We said both. It was the wrong answer.\n\nThis video is about why people try to do shared leadership, and how you can tell if you should be CEO or not. It might seem like its not important... but it is.\n\nMusic— "Always with You" - Instrumental by Homage - https://youtu.be/4Gn_-0AX0Yo',
        'thumbnail': 'https://i.ytimg.com/vi/Xa6zsbXGQ5M/default.jpg',
        'published_at': datetime.datetime(2020, 10, 6, 14, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['ceo', 'co-ceo', 'shared leadership', 'andreessen horowitz', 'ben horowitz', 'marc andreessen', 'initialized capital', 'initialized', 'garry tan', 'posterous', 'how to be a ceo', 'day in the life of a ceo', 'how to become ceo of a company', '19 qualities of a great ceo', 'entrepreneur', "world's richest", 'business', 'entrepreneurship', 'ceo qualities', 'successful ceo', 'tech ceos', 'tech ceo', 'qualities of a ceo', 'how to become a ceo', 'entrepreneur motivation', 'startup entrepreneurs', 'great ceo', 'y combinator', 'innovation'],
        'view_count': 66801,
        'like_count': 4183,
        'dislike_count': 35,
        'favorite_count': 0,
        'comment_count': 416
    },
    {
        'youtube_id': '8pG6golMHPo',
        'title': 'I said the wrong thing and suddenly our Series A pitch meeting was over',
        'description': "In 2010 I was pitching my startup for Series A to top tier investors like Peter Fenton at Benchmark. He asked me a question, and a very simple one. We said the wrong thing, and it was the end of the meeting. In this video I walk you through what the question was that ended the meeting, and why that was wrong.\n\n--\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/8pG6golMHPo/default.jpg',
        'published_at': datetime.datetime(2020, 9, 30, 13, 43, 55),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['series a pitch', 'pitch story', 'benchmark capital', 'peter fenton', 'garry tan', 'startup pitch', 'startup fundraising', 'startup fundraising 101', 'startups', 'pitch meeting', 'pitch advice', 'how to raise money', 'fundraising for your startup', 'fundraising for startups', 'initialized capital', 'initialized', 'instagram startup'],
        'view_count': 70488,
        'like_count': 4324,
        'dislike_count': 44,
        'favorite_count': 0,
        'comment_count': 562
    },
    {
        'youtube_id': 'xy4WdRfDyTs',
        'title': 'Successful billion dollar startups build long-lived teams that beat all competition',
        'description': "How long have you been working with your team? How long do you think it takes to win championships? It takes a pro-level basketball team an average of 4.5 years to maximize their odds of winning a championship. \n\nThe right culture, the highest and best culture, is a seamless web of deserved trust. Not much procedure, just totally reliable people correctly trusting one another. —Charlie Munger\n\nIf you could get all the people in an organization rowing in the same direction, you could dominate any industry, in any market, against any competition, at any time. —Patrick Lencioni / Five Dysfunctions of a Team\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and I’m a Forbes Midas List Top 100 venture capitalist in the world. We want these videos to be about helping people build world-class teams and startups that touch a billion people. Our startups have gone on to create more than $40 billion in market value so far, and Initialized has over $770M in assets under management. I’m doing my own one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/xy4WdRfDyTs/default.jpg',
        'published_at': datetime.datetime(2020, 9, 23, 14, 0, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startup', 'startups', 'garry tan', 'team building', 'hiring', 'startup founders', 'finding cofounders', 'alignment', 'startup values', 'patrick lencioni 5 dysfunctions of a team', 'business culture', 'building trust', 'winning championships', 'initialized capital', 'initialized', 'team resilience', 'long lived teams', 'lean startup', 'finding co founders'],
        'view_count': 30964,
        'like_count': 2190,
        'dislike_count': 15,
        'favorite_count': 0,
        'comment_count': 163
    },
    {
        'youtube_id': 'RhYZECR2Ru8',
        'title': 'Quit and join that risky tech startup? A guide to learning, earning & minimizing regret at startups',
        'description': "Should you quit or job? What skills do you want to acquire? How do you evaluate how much you'll actually earn? If you're great at what you do, it turns out that the only kind of risk is not taking risk at all. This is my quick guide on how to think through different opportunities that come your way. Learning early on is more important, but earning is absolutely something you need to focus on mid-career. \n\nI’m Garry Tan, venture capitalist and founder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own one man YouTube channel with no staff or crew — I'm going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/RhYZECR2Ru8/default.jpg',
        'published_at': datetime.datetime(2020, 9, 9, 14, 0, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'technology', 'startup job', 'tech job', 'tech interview', 'risk taking', 'quit my job', 'start a startup', 'quitting a stable job', 'stable job', 'initialized capital', 'garry tan', 'y combinator', 'join a startup', 'offer letter', 'should i join a startup', 'startup equity', 'startup vesting', 'comparing offer letters', 'series a startup', 'series b startup', 'tech startups', 'top startups', 'risk minimization', 'should i quit my job?', 'quit my job to start a business', 'quit my job to join a startup'],
        'view_count': 40333,
        'like_count': 2805,
        'dislike_count': 14,
        'favorite_count': 0,
        'comment_count': 201
    },
    {
        'youtube_id': 'dRBDz-flKs0',
        'title': 'Shame and blame in entrepreneurship. How to channel your rage before you destroy your team.',
        'description': "Do you feel shame? I do all the time. It's one of the things that can really torture me, and if I'm not careful, I'll pass it on to my team and the people I work with. Nobody is immune to this. Even Steve Jobs fell for this when Mobile Me had a disastrous launch in 2008.\n\nRather than avoid it, let's acknowledge and talk about it. Shame is a thing many creators feel. If we can be more conscious and aware of it, we can make better choices about what to do with it.\n\n--\n\nHi, I'm Garry. I'm a former Y Combinator partner turned Forbes Midas List Top VC in the world, and I'm here to help you build your dreams. Please hit subscribe to see more videos like this.",
        'thumbnail': 'https://i.ytimg.com/vi/dRBDz-flKs0/default.jpg',
        'published_at': datetime.datetime(2020, 9, 1, 15, 59, 20),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['entrepreneurship', 'entrepreneur', 'startup', 'entrepreneur motivation', 'startup advice', 'business startup advice', 'y combinator', 'startup entrepreneurs', 'entrepreneurship motivation', 'entrepreneur advice', 'business', 'entrepreneurs', 'startups', 'business motivation', 'starting a business', 'entrepreneur motivational video', 'business advice', 'entrepreneur mindset', 'success', 'shame', 'channeling shame', 'steve jobs', 'mobile me', 'steve jobs mobile me', 'steve jobs rage', 'steve jobs angry', 'berating employees'],
        'view_count': 8875,
        'like_count': 464,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 38
    },
    {
        'youtube_id': 'dLicgt04hHY',
        'title': 'See reality as it is first, before you try to bend it — The Stockdale Paradox',
        'description': "Jim Stockdale was the highest ranking captive POW in the Hanoi Hilton. He spent 8 years as a prisoner of war and lived to tell about it. His lesson, the Stockdale Paradox, is an important one for all founders. You must know you will prevail while also accepting total reality. Jim Collins talks about it in Good to Great, and it comes up all the time in startup land.\n\nI talk about how both parts of the paradox were a part of my own startup journey. In fact, violation of the 2nd part (being able to see reality as it is) led to my own personal failure. Today I spend a lot of time trying to help founders navigate their own minefields in this world. \n\nSorry for the hiatus! I've been busy closing our newest $230M seed-stage venture capital fund, and so now I'm back to making videos again.",
        'thumbnail': 'https://i.ytimg.com/vi/dLicgt04hHY/default.jpg',
        'published_at': datetime.datetime(2020, 8, 18, 14, 30, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['stockdale paradox', 'jim stockdale', 'good to great', 'Venture Capital', 'venture capitalist', 'investing', 'early stage venture capital', 'seed round', 'raising seed round', 'initialized', 'garry tan', 'bending reality', 'jim collins', 'the stockdale paradox', 'stockdale paradox good to great', 'raising venture capital', 'startup board', 'startup board member', 'board members', 'initialized capital'],
        'view_count': 21700,
        'like_count': 1542,
        'dislike_count': 9,
        'favorite_count': 0,
        'comment_count': 126
    },
    {
        'youtube_id': 'bORgMI5-oZE',
        'title': 'Masterclass: Fighting scammers online (How founders can build Trust and Safety teams)',
        'description': "Steve Kirkham and Eric Levine were the early builders at Airbnb that created the Trust and Safety team. They fought for billions of dollars of transactions and beat scammers day in and day out. Learn how they built that team and how you can use their newest product Berbix to protect your online platforms and build marketplaces like Airbnb that matter. \n\nEvery business in the world is moving online— and the smarter founders can be about building Trust and Safety within their companies, the faster that will happen.\n\nLearn more about berbix at https://berbix.com\n\n00:00 Intro\n01:25 Building Trust and Safety at Airbnb\n03:01 Knowing who you're doing business with is the first step\n04:52 Empathy for fraudsters\n06:08 Verify identity around key points\n07:11 Why the incumbents like Jumio are terrible\n07:48 Why Steve and Eric started Berbix\n09:13 Instant software-only verification unlocks all new marketplaces\n11:03 Fundamental need: Sybil resistance\n11:39 When founders should build Trust & Safety teams\n12:50 Types of bad actors\n14:36 How to thwart bad actors\n16:34 ID checks are a strong deterrent\n17:31 Berbix can help you with your trust and safety problems\n18:11 Advice for future founders\n20:45 Conclusion: You can't do business with people if you don't know who they are!",
        'thumbnail': 'https://i.ytimg.com/vi/bORgMI5-oZE/default.jpg',
        'published_at': datetime.datetime(2020, 7, 29, 14, 30, 3),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['trust and safety', 'fighting scammers', 'scammer', 'spammer', 'fighting spam', 'airbnb trust and safety', 'berbix', 'garry tan', 'y combinator', 'initialized', 'initialized capital', 'eric levine', 'steve kirkham', 'masterclass', 'trust and safety council', 'trust', 'suspicious login', 'phishing', 'stealing user credentials', 'online protection', 'hacking', 'website hacking', 'online fraud', 'fraud prevention', 'identity theft', 'identity verification', 'jumio', 'start up founders'],
        'view_count': 3864,
        'like_count': 122,
        'dislike_count': 2,
        'favorite_count': 0,
        'comment_count': 20
    },
    {
        'youtube_id': '6XUZrWzFmIE',
        'title': 'Micromanagement is toxic: Delegation is the cure (6 simple steps)',
        'description': 'One of my direct reports once told me in a 1:1 early in my career as a founder: "I\'ve never been so disrespected in my career." This was a wake-up call. I wasn\'t delegating, and we weren\'t aligned on goals and values. I was micromanaging, and second guessing everything. In this video we talk about what it takes to be a great leader: a conductor of people who drive greatness.\n\n00:00 Intro: My management mistake\n00:58 Hire and delegate to get more time\n02:59 How to delegate in 6 simple steps\n04:38 Delegation\'s shadow: Micromanagement\n05:39 If delegation fails, ask the deeper questions\n06:21 I was a terrible manager at times when I failed to delegate or align\n07:04 Conclusion: Be a conductor of people\n\nMusic - "JUSTICE" Instrumental by Homage https://youtu.be/sbZgwPZGPs8\n\n--\nHi, I\'m Garry Tan - I\'m a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We\'re a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed and seek to be the investor of record at that stage.\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology\'s quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things. Forbes Midas List 2019, 2020 🚀',
        'thumbnail': 'https://i.ytimg.com/vi/6XUZrWzFmIE/default.jpg',
        'published_at': datetime.datetime(2020, 7, 21, 13, 13, 8),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['delegation', 'micromanagement', 'leadership', 'bad manager', 'manager training', 'management styles', 'management skills', 'management', 'management stories', 'teamwork', 'avoiding micromanagement', 'first time manager', 'founder', 'startup stories', 'startup', 'founder lessons', 'startup lessons', 'y combinator', 'garry tan', 'project management', 'principles of delegation', 'what is delegation in management', 'garry tan initialized capital', 'venture capital'],
        'view_count': 20500,
        'like_count': 1487,
        'dislike_count': 11,
        'favorite_count': 0,
        'comment_count': 112
    },
    {
        'youtube_id': '96HCCnhbKYM',
        'title': 'Sell? Die? No. Grow profitably. How Ooshma Garg and Gobble did it',
        'description': "Ooshma Garg went from spending more than a million dollars a month to putting nearly that much into her bank account every month. How? She chose to go profitable. Learn about how she found product market fit, how she coped with the implosion of Blue Apron in the public markets, and why Gobble is poised to be the best food technology startup growing profitably and independently today.\n\nWant to try Gobble? Use my referral code for $50 in free Gobble credit — https://www.gobble.com/garry50\n\n00:00 Intro\n01:35 How Gobble got started in 2011\n02:04 First idea: peer to peer lasagna\n03:08 The breakthrough\n03:40 Food tech must nail the food too\n05:13 The secret of long term retention for food IP\n07:50 Easier and tastier = best lifetime value\n08:33 Gobble's 1000 days to innovation: How they got product market fit\n08:50 $6M ARR overnight\n09:47 What to do when a worse competitor raises more money\n11:44 Markets are voting machines early and weighing machines later\n12:15 On burning $1M/mo to earning nearly that much\n13:54 When you can't raise, get profitable\n14:57 Digital ad buying lessons: ROI and market depth\n17:12 You don't know if you can be profitable until you try\n18:36 Default alive gives you control\n20:49 The future of Gobble after profitability: re-investment\n22:56 Great food businesses generate cash\n24:37 Ooshma's hardest lessons she wishes she knew when she was 22\n\n--\n\nHi, I'm Garry Tan — I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        'thumbnail': 'https://i.ytimg.com/vi/96HCCnhbKYM/default.jpg',
        'published_at': datetime.datetime(2020, 7, 14, 16, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'gobble', 'blue apron', 'meal kits', 'food tech', 'food technology', 'food startup', 'profitability', 'startup lessons', 'startup war stories', 'founders', 'ooshma garg', 'garry tan', 'y combinator', 'startup stories', 'startup school', 'default alive', 'startup advice', 'low runway', 'out of runway'],
        'view_count': 24282,
        'like_count': 811,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 78
    },
    {
        'youtube_id': 'zHZxURFgK-4',
        'title': 'Hans Tung funded 16 unicorns: Billion dollar war stories and hard lessons learned',
        'description': "Hans Tung has funded some of the most iconic billion dollar startups: Bytedance, Airbnb, Wish and Affirm just to name a few. He's been on the Forbes Midas List for 7 years straight now, this year at #10. He has seen it all, and we discuss war stories (don't miss the discussion of Alibaba in a global financial crisis and pandemic!), founders, and the future of food tech, software, and robotics. \n\n00:00 Intro\n00:50 Hans's journey to venture capital\n03:18 Hans met iconic founders very early\n05:00 The Alibaba story in 2003: Pandemic and global financial crisis with 5 months of runway (MUST WATCH)\n08:18 The perseverance of Brian Chesky and Airbnb\n09:23 The perseverance of Peloton\n10:36 Global consumer trends now first emerge outside of the US\n13:06 Software eats the world is a one-time shift\n13:45 Foodtech in China: Darkstores and clean data\n16:14 Global food tech trends: Automation\n17:06 All food distribution will be automated and tech enabled: better, faster, cheaper\n18:20 What Uber did to consolidate taxis (a fragmented, no-tech business) will happen everywhere\n20:19 Software in business: first back-office, then front-office, now whole-office\n21:10 Each successive wave of software is more powerful than the last. We're on the 5th or 6th wave now.\n21:59 On founder-market fit\n23:36 Max Levchin and founder-distribution fit at Slide\n24:16 Platform shifts create distribution opps that open, then close quickly\n25:02 5G and the future of distributed computing\n25:33 Timing your startup idea\n26:22 WeChat as a growth channel for Pinduoduo\n27:55 Autonomy and our robotic future\n28:53 Characteristics of great founders\n31:24 What Hans wishes he knew at 18 or 22\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        'thumbnail': 'https://i.ytimg.com/vi/zHZxURFgK-4/default.jpg',
        'published_at': datetime.datetime(2020, 7, 9, 14, 30, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['Hans Tung', 'GGV', 'GGV Capital', 'Venture Capital', 'startups', 'alibaba', 'wish', 'wechat', 'global venture capital', 'global vc', 'vc', 'billion dollar startups', 'unicorns', 'stanford', 'ms&e', 'forbes midas list', 'midas list', 'ggv capital', 'silicon valley', 'stanford university', 'venture capital', 'entrepreneurship', 'innovation', 'asian high-tech', 'entrepreneur', 'garry tan', 'brian chesky', 'max levchin'],
        'view_count': 52450,
        'like_count': 1936,
        'dislike_count': 12,
        'favorite_count': 0,
        'comment_count': 142
    },
    {
        'youtube_id': 'AVGeA-7WbeQ',
        'title': "The God of the Internet's Law is better than The Golden Rule",
        'description': 'Everyone knows the golden rule. But do you know Postel\'s Law? Postel\'s Law is the guiding principle of the Internet. The Internet is rewriting everything we know about human relationships, so it\'s helpful to keep this in mind as we build the future.\n\nThis is how Postel\'s Law enabled the proto-Internet and set the culture for the future of humanity.\n\n00:00 Intro\n00:37 What is Postel\'s Law?\n01:39 How Jon Postel became the god of the Internet\n02:03 How did Postel\'s Law create the Internet as we know it?\n02:57 Postel\'s Law in Product Design\n04:03 How to apply Postel\'s Law in your own life (remote work-edition)\n06:00 Postel\'s Law for managers: How to make a user manual for your own protocols\n05:56 Conclusion: You need Postel\'s Law in your life\n\nFull transcript at https://blog.garrytan.com/how-postels-law-enabled-the-proto-internet-and-set-the-culture-for-the-future-of-humanity\n\nSongs in this include\n* "Morrison" - Instrumental by Homage https://youtu.be/dCZU91MeMLY\n* Varsity by Utah \n* Rhea by 9 Moons\n* Mukanshin by Yume\n* "My Brother\'s Keeper"- Instrumental by Homage https://youtu.be/Zl4XYzkiz88\n* Infinite Sustain by Amaranth Cove\n\n--\n\nHi, I\'m Garry Tan — I\'m a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We\'re a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology\'s quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.',
        'thumbnail': 'https://i.ytimg.com/vi/AVGeA-7WbeQ/default.jpg',
        'published_at': datetime.datetime(2020, 6, 30, 14, 30, 11),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['golden rule', 'internet history', 'jon postel', 'tcp/ip', "postel's law", 'startup advice', 'psychology', 'strategy', 'business strategy', 'remote work', 'the golden rule', 'what is the golden rule', 'do unto others', 'do unto others as you would have them do unto you', 'garry tan', 'gary tan'],
        'view_count': 8334,
        'like_count': 512,
        'dislike_count': 5,
        'favorite_count': 0,
        'comment_count': 44
    },
    {
        'youtube_id': 'r8hhvw-1b-M',
        'title': 'Roll your way to a Startup Unicorn: Lessons for Founders',
        'description': "What does an obscure Japanese video game have to teach us about building a startup that takes over the world? Actually a lot. This is the story of Katamari Damacy which can teach you how to build the next billion dollar startup. \n\nSpoiler: it's about getting customers, colleagues and capital. Rinse and repeat. All you have to do is get rolling.\n\n--\n\nHi, I'm Garry Tan - I'm a designer, engineer, and investor in early stage startups. Cofounder and managing partner of Initialized Capital, an early stage venture capital fund that was earliest in Coinbase and Instacart. We're a team of 8 investing partners with over $500M in assets under management and our startups have created more than $45 billion in market value in the past 8 years. We focus on pre-product-market-fit seed. Selected for Forbes Midas List Top 100 Best Global Venture Capitalists for 2019 and 2020. 🚀\n\nPreviously a partner at Y Combinator. Invested in and directly worked with over 700 companies in 5 years from the earliest possible stage, often just an idea.\n\nBefore that, I cofounded Posterous and helped build it to a world-class website used by millions. (Acquired by Twitter) I also cofounded the engineering team for Palantir Technology's quant finance analysis platform, and designed the current Palantir logo and wordmark.\n\nI love building things.",
        'thumbnail': 'https://i.ytimg.com/vi/r8hhvw-1b-M/default.jpg',
        'published_at': datetime.datetime(2020, 6, 15, 14, 30, 31),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['entrepreneurship', 'entrepreneur', 'startup', 'entrepreneur motivation', 'startup advice', 'business startup advice', 'startup stories', 'y combinator', 'y combinator application', 'startup entrepreneurs', 'entrepreneurship motivation', 'entrepreneur advice', 'business', 'entrepreneurs', 'how to start a business', 'startups', 'business ideas', 'business motivation', 'starting a business', 'entrepreneur motivational video', 'business advice', 'entrepreneur mindset', 'success', 'business tips', 'motivation for entrepreneurs', 'unicorn'],
        'view_count': 15891,
        'like_count': 951,
        'dislike_count': 11,
        'favorite_count': 0,
        'comment_count': 97
    },
    {
        'youtube_id': 'apP62c8GHiM',
        'title': 'How to fight instant pushback to your ideas',
        'description': "You can change the world, but it sometimes comes at a steep cost. It did for Ignaz Semmelweis, the first doctor to discover handwashing saved lives, especially mothers in his maternity ward. He made 5 key mistakes that you can avoid. And in doing so, I hope you'll be able to avoid the insane asylum. \n\nEveryone who makes something new gets hit back quickly with what is known now as the Semmelweis Reflex- people reject anything that challenges them out of hand. You can be prepared to face this. Here's the five ways steps:\n\n0:00 Intro\n01:07 History of Ignaz Semmelweis\n03:05 Speak for yourself\n04:26 Persevere, and don't ragequit\n05:34 Speak truth when you know you're right\n06:09 When they go low, we go high\n07:13 The breakthrough is only half the work\n\nThanks for watching! I made this video last night in about 3 hours- I'm going for a video a week, and my goal is to just help future founders and people who are making things get to the next stage.\n\nPlease like or subscribe to this and as always you can read the full transcript at https://blog.garrytan.com\n\n#nosmallcreator",
        'thumbnail': 'https://i.ytimg.com/vi/apP62c8GHiM/default.jpg',
        'published_at': datetime.datetime(2020, 5, 20, 0, 0, 28),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['semmelweis', 'insane asylum', 'creator', 'change the world', 'communication', 'science', 'handwashing', 'discovery', 'shunned', 'entrepreneurship', 'y combinator', 'startups', 'ragequit', 'speak up', 'breakthroughs', 'ignaz semmelweis', 'semmelweis reflex', 'rejection', 'speak for yourself'],
        'view_count': 9792,
        'like_count': 775,
        'dislike_count': 4,
        'favorite_count': 0,
        'comment_count': 96
    },
    {
        'youtube_id': '35UOGYlm7F8',
        'title': 'Reprogram your mind with exec coaching',
        'description': "Is it possible to reprogram your own mind? Yes, it is. Coaching, therapy, meditation, and group work are the keys to unlocking incredible potential. Trauma is a surprising common trait for many founders, and those who can overcome their past can truly build the future. #withme\n\nCameron Yarbrough was my coach for years before starting Torch.io, the best way to get exec coaching. We discuss the problems we've faced over the years, and how you're not alone. There is absolutely a way forward.\n\nI wish I worked on these things 10 years before I did— doing deep work will change your life if you let it. \n\n00:00 Intro\n00:59 Many founders have difficult childhoods that set them on the path to founderhood\n02:19 How difficult pasts can uniquely prepare founders for startup life \n03:26 Cameron was Garry's coach through many difficult periods in his business life \n03:53 Critical moments in your business career are driven by your own mental health \n04:49 Cameron could coach Garry because he had to overcome many of the same challenges \n07:10 Cameron's childhood impact on his early founder experiences \n07:59 Cameron's past leadership challenges drove him to seek therapy, meditation, and coaching\n08:47 It's extremely valuable for repeat founders to do deep internal work before diving back in\n10:05 The horse and rider allegory: Deep work lets the rider can better steer the horse\n10:37 You can work on mental health right now.\n11:01 YOU CAN DO THIS NOW.\n11:46 Executives who get their own mental health right will radiate this health to the whole org.\n12:47 Bibliotherapy\n13:37 Proper deep work is metaprogramming\n14:41 How to get behavior change\n15:25 Regular coaching and therapy enables breakthroughs\n16:45 On finding the right coach or therapist\n17:41 Even coaches have coaches\n18:30 Radical candor enables winning leadership style\n20:12 What is the optimal organization?\n21:04 Your own experience is not universal\n22:02 Leadership and mental health in the time of COVID-19\n24:17 Wartime leadership requires more empathy, not less\n25:01 Cameron’s toughest COVID-19 crisis decision\n26:54 Why coaches are important even if you have lots of friends\n28:13 How to get help in group sessions\n29:46 You are not alone\n\nView the full transcript here: https://blog.garrytan.com/metaprogram-your-own-mind-a-conversation-with-cameron-yarbrough-of-torch-dot-io\n\nTorch.io is an amazing place to get a coach. https://torch.io/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nTORCH SMALL GROUP COACHING https://lp.torch.io/group-coaching/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan\n\nLearn more about Radical Candor from Kim Scott in virtual sessions (May 2020): https://lp.torch.io/the-radically-candid-coach/?utm_campaign=Website+CTA&utm_medium=Website&utm_source=YouTube&utm_content=garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/35UOGYlm7F8/default.jpg',
        'published_at': datetime.datetime(2020, 5, 13, 16, 0, 9),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['exec coaching', 'withme', '#withme', 'coaching', 'therapy', 'group therapy', 'group sessions', 'counseling', 'stanford t-group', 'stanford t-groups', 'radical candor', 'kim scott', 'torch', 'initialized', 'garry tan', 'cameron yarbrough', 'business coaching', 'advisor', 'startup', 'startup advice', 'mental health', 'psychotherapy', 'meditation'],
        'view_count': 13058,
        'like_count': 522,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 54
    },
    {
        'youtube_id': 'hOMZebDXQsU',
        'title': 'The $1 TRILLION Artificial Intelligence Problem',
        'description': "Can computers think like humans? Can they learn like us? $1 trillion is spent yearly on tasks a general artificial intelligence can do. D Scott Phoenix of AI startup Vicarious is building exactly that.\n\nSCOTT'S HISTORY\n00:00 Intro\n00:59 How Garry and Scott met\n02:02 How Scott came up with the idea to work on AGI\n\nON ARTIFICIAL GENERAL INTELLIGENCE\n02:41 The time to build AGI is now\n03:10 Why work on AGI?\n04:26 What are the building blocks to building a general AI?\n04:49 What is a human-like learning system?\n06:15 Vicarious vs Deep Learning\n08:08 Traditional AI methods resemble insectoid or reptilian brain approaches\n09:43 New methods and models are more important than more money on training existing models\n11:52 Limits of narrow AI\n12:48 History and origins of the AI debate in philosophy and neuroscience\n14:45 Brute force methods require 14,000 years of training to do what children only need 2 years to learn\n15:28 Lessons from biology\n16:24 How do systems layer to generate more complex behavior?\n17:30 Is an ambitious project like AGI composable and iterable like SaaS software?\n\nON VICARIOUS\n20:01 Long term ambition is great, but what do you do along the way?\n20:38 Vicarious's first applied use case in robotics\n22:16 Vicarious vs other robotics approaches\n23:47 Building learning systems, not one-off point solutions\n\nFOR FUTURE FOUNDERS\n24:51 Advice for builders just starting out\n25:17 How to tackle large problems and ambitious projects\n26:57 Technology is the ultimate lever for humans to create a better world\n29:14 How to be prepared for the long hard road\n\nFull transcript at https://blog.garrytan.com/building-general-ai-with-d-scott-phoenix-founder-of-vicarious\n\nLearn more about Vicarious: https://vicarious.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/hOMZebDXQsU/default.jpg',
        'published_at': datetime.datetime(2020, 5, 6, 17, 55, 14),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['artificial intelligence', 'ai', 'strong ai', 'general ai', 'robotics', 'deep learning', 'open ai', 'vicarious', 'hard tech', 'hard technology', 'garry tan', 'ai debate', 'machine learning documentary', 'venture capital', 'ai startup', 'how does ai work'],
        'view_count': 18446,
        'like_count': 674,
        'dislike_count': 13,
        'favorite_count': 0,
        'comment_count': 80
    },
    {
        'youtube_id': 'WK_XwjhJl3k',
        'title': '6 Skills for Successful Startup Founders: Maximize your chances',
        'description': "You can't guarantee success, but you can increase the chances of it by working on these six skills for startup founders. \n\n00:00 Intro\n02:36 Engineering\n04:44 Product\n06:15 Design\n08:16 Sales & Marketing\n08:54 Finance\n10:00 Management and leadership\n\nFull transcript and sign up for email updates at https://blog.garrytan.com\n\nKey resources mentioned\n* Codecademy https://codecademy.com\n* Insight Fellows https://blog.garrytan.com/my-newest-youtube-video-meet-jake-klamka-who-made-insight-fellows-the-worlds-next-applied-graduate-school-for-data-and-engineering \n* Career Karma https://careerkarma.com\n* Garry's YC Startup School: Design Part 1 https://www.youtube.com/watch?v=9urYWGx2uNk and Part 2 https://www.youtube.com/watch?v=O6uREh3G3sQ\n\nMusic 04:44 to 08:16 by Sudo, aka Suhail Doshi https://soundcloud.com/sudo\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/WK_XwjhJl3k/default.jpg',
        'published_at': datetime.datetime(2020, 4, 28, 23, 42, 37),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'startup skills', 'level up', 'business', 'life goals', 'development', 'engineering', 'sales', 'marketing', 'garry tan', 'y combinator', 'startup school', 'goals', 'entrepreneurship', 'quitting your job', 'how i made it into venture capital', 'tech startups', 'startup'],
        'view_count': 85774,
        'like_count': 4235,
        'dislike_count': 32,
        'favorite_count': 0,
        'comment_count': 325
    },
    {
        'youtube_id': 'F-r6jL-oFXE',
        'title': 'Timebox your way to startup product market fit',
        'description': "If at first you don't succeed, try try again. Then quit. It's no use being a damn fool about it. WC Fields said that, and that's true for startups and founders as they make decisions about what customers to pursue, and what problems to solve. These are all linked. In this video we cover concrete ways to use timeboxing to improve your chances of finding startup gold.\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/F-r6jL-oFXE/default.jpg',
        'published_at': datetime.datetime(2020, 4, 22, 14, 28, 34),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'lean startup', 'management', 'leadership', 'decision making', 'project management', 'intrapraneurship', 'product market fit', 'customers', 'user driven design', 'user centered design', 'founder', 'mistake', 'startup failure', 'garry tan', 'gary tan', 'my way', 'timebox', 'timeboxing'],
        'view_count': 31100,
        'like_count': 1706,
        'dislike_count': 21,
        'favorite_count': 0,
        'comment_count': 152
    },
    {
        'youtube_id': 'ZjTaFB3mo5w',
        'title': 'Masterclass: Remote work and management with empathy (Part 2)',
        'description': "How do you build a fast growing startup team that builds a world-class product? This is part two, focusing on management with empathy and how to build an organic remote work culture from scratch that is truly collaborative. \n\nHere is what we cover in this masterclass:\n\n00:00 Intro\n01:00 Managing culture remotely\n01:54 Weekly check-ins and 1:1s with leadership\n02:29 Remote work requires trust by default\n03:45 How to run a 1:1 remotely\n04:52 Forming an organic remote culture\n08:53 How to communicate values\n11:39 How to handle management when things go wrong\n13:17 How to handle performance issues\n15:02 How to handle misalignment in values\n16:39 Rewards and recognition\n17:42 Approaching diversity and inclusion\n19:52 Remote work in the time of coronavirus\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com/masterclass-on-remote-work-management-with-empathy-fostering-a-values-based-culture-part-2-of-2\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/ZjTaFB3mo5w/default.jpg',
        'published_at': datetime.datetime(2020, 4, 14, 14, 15),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['recruiting', 'remote work', 'agile', 'startup', 'upwork', 'hiring engineers', 'engineering', 'software engineering', 'interviewing', 'interviews', 'garry tan', 'y combinator'],
        'view_count': 1901,
        'like_count': 60,
        'dislike_count': 0,
        'favorite_count': 0,
        'comment_count': 11
    },
    {
        'youtube_id': '2gccAOuGRdU',
        'title': 'Should you work on that startup idea?',
        'description': "It's not enough to be pointed at a particular direction. You need to have a very clear reason why you will win. This is a list of questions you should ask yourself before picking a startup idea. \n\nWeak answers to these questions might mean you need to change ideas... on the other hand, they focus you to figure out the things you need to solve so your startup becomes a winning one. \n\nRead the full transcript at https://blog.garrytan.com/should-you-work-on-that-startup-idea-ask-why-me-why-now\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/2gccAOuGRdU/default.jpg',
        'published_at': datetime.datetime(2020, 4, 7, 14, 57, 7),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'startup ideas', 'garry tan', 'y combinator', 'initialized', 'why now', 'moats', 'competitive advantage'],
        'view_count': 41239,
        'like_count': 1858,
        'dislike_count': 17,
        'favorite_count': 0,
        'comment_count': 108
    },
    {
        'youtube_id': '3pP5RJ-dE74',
        'title': 'Masterclass: How to build a fast growing startup team entirely remotely (Part 1)',
        'description': "How do you build a fast growing startup team that builds a world-class product? \n\nHere is what we cover in this masterclass:\n00:00 Intro\n01:46 Meet Shogun, the e-commerce page builder\n02:58 They started fully remote as a side project that became profitable\n04:23 The Tools: Hardware and software that enables remote work\n07:40 How to do learning & development\n08:29 On recruiting for remote teams\n11:35 Remote recruiting process\n19:43 How to legally hire remote candidates\n22:05 Onboarding remote employees\n\nShogun is one of the best companies in our portfolio that is doing it completely remotely. They have employees in 21 countries on 6 different continents working on their e-commerce page builder. This is a fast growing company driving real revenue, and building one of the best engineering cultures we've ever seen.\n\nMy colleague Katelin Holloway (partner at Initialized, formerly head of people at multi-billion dollar startup Reddit) sits down with Shogun cofounders Finbarr Taylor (former engineer at Y Combinator) and Nick Raushenbush (former cofounder of famed video production co Glass & Marker) and they talk through all the things that they did to succeed.\n\nThis is part one. Part two gets posted next week! Stay tuned.\n\nRead a full transcript of this at Garry's blog: https://blog.garrytan.com\n\nRead the full Shogun Remote Work guide here: https://getshogun.com/remote-work-guide\n\nLearn more about Shogun at https://getshogun.com\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/3pP5RJ-dE74/default.jpg',
        'published_at': datetime.datetime(2020, 4, 1, 14, 56, 6),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['recruiting', 'remote work', 'agile', 'startup', 'upwork', 'hiring engineers', 'engineering', 'software engineering', 'interviewing', 'interviews', 'garry tan', 'masterclass'],
        'view_count': 5708,
        'like_count': 157,
        'dislike_count': 2,
        'favorite_count': 0,
        'comment_count': 12
    },
    {
        'youtube_id': 'hd9DD4t85fM',
        'title': 'Software engineer to product manager? How?',
        'description': 'What’s up everyone? I thought I would do some questions from the community. Here’s a quick sample of what we talk about:\n\n🤹\u200d♀️ How does an engineer switch to a product role?\n📸 Google vs Apple vs Amazon \n💸 How D2C requires intense focus on CAC\n\nStill stuck at home in San Francisco. We are all going through a lot. But I believe in us.',
        'thumbnail': 'https://i.ytimg.com/vi/hd9DD4t85fM/default.jpg',
        'published_at': datetime.datetime(2020, 3, 25, 15, 48, 26),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['design', 'product management', 'engineering', 'software engineering', 'apple', 'google', 'amazon', 'startups', 'founders', 'career planning', 'garry tan'],
        'view_count': 11079,
        'like_count': 438,
        'dislike_count': 4,
        'favorite_count': 0,
        'comment_count': 54
    },
    {
        'youtube_id': 'FcjbpX7cnEI',
        'title': 'How to get the absolute best telework setup—Elgato Camlink 4K + Sigma 35mm f1.2 for Zoom Tutorial',
        'description': "Here's a basic tutorial on how to use a Sony mirrorless camera as your main webcam setup for Zoom. We also sit down with the creator and founder of new teleconferencing startup Around, an Initialized portfolio company. They just launched today and have a brand new approach compared to Zoom. \n\nSign up for early access: https://www.around.co\n\nApril 2020 update: Canon camera owners may not need to buy a capture card. If you already have a newer Canon camera you can just download this utility (PC only sadly) - http://canon.us/livestream \n\nHere are my recommendations:\n\nCAMERA\nBudget option - Sony A6100 - https://amzn.to/2QtLSAj\nMid-level option - Sony A6600 — https://amzn.to/2WogMO3\nLuxury option - Sony A7R4 — https://amzn.to/2waHEXr\n\nLENS\nBudget - Sigma 16mm f1.4 for Sony E mount https://amzn.to/2xNPZR3\nLuxury option - Sigma 35mm f1.2 for Sony E mount\n\nCAPTURE CARD\nElgato Camlink 4k (Sold out on Amazon as of today but you can find on eBay or direct from Elgato)\n\nElgato HD60 S+ also works and is still in stock\n\nChert 4KC works fine on PC but flickers on Mac\n\nOTHER THINGS YOU NEED\nGonine AC dummy battery and power supply (works with either Sony A6600 or A7R4 camera above) — https://www.amazon.com/gp/product/B07D5V8KY5/ref=ppx_yo_dt_b_search_asin_title\n\nDummy Battery for budget option Sony A6100 — https://amzn.to/2WuUY3E\n\nCheap Tripod — https://amzn.to/3bdY0gN\n6 ft AmazonBasics Micro HDMI to HDMI cable — https://amzn.to/2vBoZDM\n\n(NOTE: As an Amazon Associate I earn from qualifying purchases.)\n\nHi, I'm Garry Tan. I'm a venture capitalist (Forbes Midas List #21 for 2019) and early investor in startups like Coinbase and Instacart. This is my one man run and gun channel— everything you see is filmed, edited, and posted by yours truly.\n\nYou can follow me on Twitter at https://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/FcjbpX7cnEI/default.jpg',
        'published_at': datetime.datetime(2020, 3, 19, 2, 46, 53),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['telecommuting', 'telework', 'zoom', 'camera', 'webcam', 'garry tan', 'camlink', 'elgato cam link 4k', 'gary tan', 'cam link 4k', 'elgato camlink', 'camlink 4k', 'elgato cam link', 'elgato cam link zoom', 'a7r4', 'elgato camlink 4k', 'sony a6100', 'sony a6600', 'sony a7r4', 'cam link', 'cam link 4k setup', 'best zoom setup'],
        'view_count': 17754,
        'like_count': 400,
        'dislike_count': 22,
        'favorite_count': 0,
        'comment_count': 69
    },
    {
        'youtube_id': 'idyfHs3DWmc',
        'title': 'Masterclass: How to sell to 20M software developers with an amazing onboarding experience',
        'description': "The world is being eaten by software, and that makes selling to software engineers the one thing to nail for all infrastructure startups. Come learn from Sylvain Utard, VP of Engineering from Algolia, and how they created one of the world's best developer onboarding experiences. \n\n00:00 Intro\n01:15 Algolia is realtime fast customizable search, both backend and frontend\n02:02 Tech giants can put 100s of devs on search. Algolia democratizes that.\n03:34 Walking through the developer experience\n03:45 The homepage\n04:01 Evolving from self-serve to enterprise\n04:16 Even if you sell by enterprise, showing value to developers immediately is still key\n05:51 Garry's magical experience using Algolia\n06:12 The docs drive you directly to the tutorial: Interactive is better\n09:53 Design tip: Strong call to actions\n10:42 Developer docs are only effective when they make devs feel powerful\n11:56 How did Algolia implement their interactive developer docs?\n13:01 How to support 9 runtimes? 15 engineers out of 100 are devoted to dev xp\n14:43 Low bar high ceiling: the holy grail for products and services\n16:56 How Sylvain got into tech\n18:56 When your competition is 20 year old tech: Lucene and Sphinx\n20:05 Career advice for engineers starting out\n20:50 Algolia is hiring for remote and roles in SF and Paris\n\nFull transcript available at https://blog.garrytan.com/\n\nThanks to Sylvain and the whole Algolia team for making an amazing product and coming to sit with me.\n\nYou can follow Sylvain at https://twitter.com/sylvainutard\nVisit https://algolia.com to try their world-class search product\n\nYou can follow me on Twitter at https://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/idyfHs3DWmc/default.jpg',
        'published_at': datetime.datetime(2020, 3, 11, 15, 17, 14),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['developers', 'software developers', 'software engineering', 'engineering', 'cloud', 'saas', 'startups', 'venture capital', 'masterclass', 'vp of engineering', 'dev docs', 'ruby', 'rails', 'python', 'java', 'react', 'search', 'lucene', 'algolia', 'sphinx', 'full text search', 'garry tan'],
        'view_count': 11385,
        'like_count': 323,
        'dislike_count': 4,
        'favorite_count': 0,
        'comment_count': 29
    },
    {
        'youtube_id': 'dhnzBQjsgGU',
        'title': "Stories from China Tech that don't get reported: A conversation with Rui Ma of Tech Buzz China",
        'description': 'Table of contents with timestamps below. There\'s a large divide between Chinese Tech and the rest of the world. Co-Host of Tech Buzz China, Rui Ma, is an old friend of mine who I interviewed since she is one of my first resources when trying to understand all things China. \n\n00:00 Intro\n00:30 Meet Rui Ma, Co-host of Tech Buzz China\n00:58 Tech in the West doesn\'t pay enough attention to China\n01:18 Why Rui started Tech Buzz China\n02:13 Luckin Coffee is an example of US tech media getting it wrong\n02:43 Media\'s false equivalence: Luckin = Starbucks\n03:39 Smartphone growth drove more greenfield new consumer cos in China than US\n04:20 Mobile drove growth in China- Now that growth is over\n04:55 What do founders in the West need to understand about China tech?\n05:23 2 China Tech narratives - they\'re the future or they steal the future\n07:02 US companies can\'t walk and chew gum. Chinese tech cos build super apps.\n07:48 Why superapps? WeChat and Meituan are transaction oriented\n08:51 Superapps provide superior UX\n09:12 Obligatory coronavirus discussion\n10:02 Economic output is still only 1/6th of normal levels as China \n10:42 The markets didn\'t catch up to COVID-19 until the last week of Feb, months later\n11:24 Money-printing will help large firms but not SMBs who need it the most\n12:15 Chinese gov\'t has asked landlords and lenders to forgive payments\n12:31 Chinese cos with software automation were least hit by the COVID-19 lockdown\n13:29 Ant Financial\'s Supply Chain Blockchain is one of the largest production blockchains in use\n14:48 Covid 19\'s impact on remote work\n15:31 China\'s 996 Work Schedule: 9AM to 9PM, 6 Days a Week\n16:45 On-demand is far more viable in densely urban China\n17:10 Surprising: China\'s on-demand economy can\'t hire enough blue collar service workers\n17:34 Even China can\'t hire enough workers, so they are turning to robotic automation\n18:17 Nvidia mobile GPU chips are a key "Why Now?" for robotics\n18:37 Freedom Robotics is building the telematics/analytics platform that lets fleets manage 1000s of robots\n18:52 Robotics is going to see its iPhone moment thanks to realtime computer vision + GPU compute\n19:21 On EHang (autonomous air taxi)\n20:27 How China deals with hard tech startups "PPT" deals\n21:53 In China, the govt will define entire tech waves for investment\n23:27 How are China\'s bike share companies doing in 2020?\n25:02 As a Chinese startup, do you have to be allies with Alibaba or Tencent?\n26:14 A startup\'s product market fit sometimes becomes just a tech giant\'s moat\n26:36 How Chinese megatech cos invest in startups\n27:19 How can US startups enter China?\n28:00 How to build a presence in China? Just go.\n28:19 Top tier cities are like Manhattan, and not representative of the whole country\n28:49 Over 300 cities with more than 1M people\n30:09 City dwellers make up over 1B of 1.4B people\n30:24 Rural China is over 400M people\n31:20 Qutoutiao pays rural Chinese users pennies per day to use it for even an hour\n\nFull transcript of this interview is also available on my blog here:\nhttps://blog.garrytan.com/stories-from-china-tech-that-dont-get-reported-a-conversation-with-rui-ma-of-tech-buzz-china\n\nSubscribe to Tech Buzz China here: https://techbuzzchina.com\n\nYou should follow Rui Ma on Twitter here: https://twitter.com/ruima',
        'thumbnail': 'https://i.ytimg.com/vi/dhnzBQjsgGU/default.jpg',
        'published_at': datetime.datetime(2020, 3, 4, 14, 42, 53),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'china tech', 'china', 'china technology', 'china economy', 'alibaba stock', 'luckin coffee', 'meituan dianping', 'luckin', 'qutoutiao', 'rui ma'],
        'view_count': 7500,
        'like_count': 265,
        'dislike_count': 6,
        'favorite_count': 0,
        'comment_count': 41
    },
    {
        'youtube_id': 'BJo_7-Rn1wc',
        'title': 'How to sell your startup for millions',
        'description': "When and how should you sell your startup? M&A (mergers & acquisitions) are always a high stress process, and there are a lot of things you have to consider when you are trying to get a startup exit executed in the right way.\n\nRead Paul Graham's essay on Default Alive vs Default Dead here: http://www.paulgraham.com/aord.html\n\nGarry's startup Posterous was sold to Twitter for $20M and Andrew Lee's startup was sold to Zynga too. As venture capitalists today we work with more than a hundred startups from the earliest possible stage to now a total market value of over $36 billion. We've seen nearly every kind of startup exit and regularly try to help founders as they navigate these problems.\n\nThis short video encompasses a lot of the advice we end up giving frequently to our community.",
        'thumbnail': 'https://i.ytimg.com/vi/BJo_7-Rn1wc/default.jpg',
        'published_at': datetime.datetime(2020, 2, 28, 14, 48, 44),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'how to sell your startup for millions', 'startup', 'paul graham'],
        'view_count': 52431,
        'like_count': 1675,
        'dislike_count': 14,
        'favorite_count': 0,
        'comment_count': 88
    },
    {
        'youtube_id': 'btRHm7z61oo',
        'title': "Drowning in email is the first sign you aren't scaling.",
        'description': "If you're too busy to answer email then that's the first sign you aren't scaling. Here's what you need to do get out of that mess.\n\nSurprisingly what got you here won't get you there. You need to be open and creative early on, but at some point every startup switches over to a focus on scaling and conscientiousness. \n\nThe link to the 2004 Bucknell Study is here — it turns out openness is anti-correlated with long term startup success, while conscientiousness predictably is positively correlated. \nhttps://www.sciencedirect.com/science/article/abs/pii/S0883902603000934\n\nFull transcript of this video here:\nhttps://blog.garrytan.com/email-overload-is-the-first-sign-you-need-to-scale-yourself",
        'thumbnail': 'https://i.ytimg.com/vi/btRHm7z61oo/default.jpg',
        'published_at': datetime.datetime(2020, 2, 21, 14, 34, 24),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'management', 'productivity', 'founder', 'garry tan', 'gary tan', 'email'],
        'view_count': 2438,
        'like_count': 115,
        'dislike_count': 2,
        'favorite_count': 0,
        'comment_count': 11
    },
    {
        'youtube_id': 'emoauFlhXA0',
        'title': 'How to bend a spoon with your mind',
        'description': "Supreme alchemy is manifestation of ideas into reality. Biggie Smalls did it, and so can you. What can we learn from Spoon Bending parties? What role does luck play in getting to our destination?\n\nRead the full transcript and subscribe to the blog here: https://blog.garrytan.com/how-to-bend-a-spoon-with-your-mind\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. Just the stuff I think is interesting or valuable for mainly founders and future founders.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nYou can follow me on Twitter here: https://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/emoauFlhXA0/default.jpg',
        'published_at': datetime.datetime(2020, 2, 12, 13, 45, 1),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['manifestation', 'supreme alchemy', 'spoon bending', 'the matrix', 'bend reality', 'how to bend reality to your will', 'garry tan', 'how to bend'],
        'view_count': 10073,
        'like_count': 393,
        'dislike_count': 29,
        'favorite_count': 0,
        'comment_count': 49
    },
    {
        'youtube_id': 'pGbWynzWauQ',
        'title': 'What to do when someone steals your idea',
        'description': "Someone stole your idea. What do you do next? if you execute faster, you go to market faster, you're innovating and running out ahead of everyone else, then even the people who are trying to copy you can only copy the version of you from six or nine months ago. That's what it takes. You can beat the copycats, but you've got to outrun them.\n\nFull transcript here: https://blog.garrytan.com/what-to-do-when-someone-steals-your-idea\n\n\nHi, I'm Garry Tan and I do this YouTube channel for fun in my spare time. By day, I am a venture capitalist who started Initialized Capital, an early stage investor with over $500M in assets under management. We were the earliest investors in Coinbase, Instacart and 100+ more startups now worth over $36B in startup market cap so far. \n\nForbes Midas List 2019 #21 🚀\n\nYou can follow me on Twitter here: https://twitter.com/garrytan",
        'thumbnail': 'https://i.ytimg.com/vi/pGbWynzWauQ/default.jpg',
        'published_at': datetime.datetime(2020, 2, 5, 15, 0, 25),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'ideas', 'entrepreneurship', 'ycombinator', 'garry tan', 'when someone stole youre business idea'],
        'view_count': 30931,
        'like_count': 1515,
        'dislike_count': 14,
        'favorite_count': 0,
        'comment_count': 105
    },
    {
        'youtube_id': 'lIlguzWxEiI',
        'title': "Parker Conrad's Billion Dollar Lessons on Remote Work and Customer Support (Part 2)",
        'description': '"Remote work is the worst way to build a company, except for all the others." —Parker Conrad\n\nSee part 1 of the video here: https://www.youtube.com/watch?v=M93xlPxQADE\n\nPrefer to read? Full transcript of this video: https://blog.garrytan.com/parker-conrad-on-building-eng-teams-outside-of-sf-and-why-engineers-must-do-customer-support-part-2',
        'thumbnail': 'https://i.ytimg.com/vi/lIlguzWxEiI/default.jpg',
        'published_at': datetime.datetime(2020, 1, 29, 15, 0, 8),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['2019-11 Rippling', 'parker conrad', 'remote work', 'garry tan'],
        'view_count': 13285,
        'like_count': 404,
        'dislike_count': 8,
        'favorite_count': 0,
        'comment_count': 28
    },
    {
        'youtube_id': '0a3eM7lcVTI',
        'title': 'How to apply the artistry of Death Stranding to creating as an auteur',
        'description': "Hideo Kojima created a true masterpiece. Find out what practices helped this amazing game come together. \n\n🛡 A mission to cure loneliness in society\n♟ Resolute decisionmaking, not just doing the popular thing\n💥 Action: knowing what to delegate, outsource, and do yourself\n🎭 Showmanship that sets the tone\n\nThese things taken together allow teams to create something that has never existed before. Death Stranding is no exception.\n\nHere's the full transcript if you prefer to read: https://blog.garrytan.com/the-4-habits-of-auteurs-that-made-hideo-kojimas-video-game-masterpiece-death-stranding\n\nI'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/0a3eM7lcVTI/default.jpg',
        'published_at': datetime.datetime(2020, 1, 20, 21, 10, 21),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'hideo kojima', 'kojima'],
        'view_count': 1373,
        'like_count': 87,
        'dislike_count': 0,
        'favorite_count': 0,
        'comment_count': 25
    },
    {
        'youtube_id': 'M93xlPxQADE',
        'title': "Parker Conrad's Billion Dollar Startup Lessons: Do unscalable things, then scale them (part 1)",
        'description': "Today we're going to hang out with Parker Conrad. He created Rippling. He's one of the best product-focused CEO's I've ever met. Today he's going to share some of the most hard-won startup lessons that I've ever heard. And he hasn't talked about it anywhere else.\n\nHear about his startup journey\n☠️ Spending 7 years grinding it out on a startup that wasn't working\n🧨 Finding product market fit by doing the unscalable\n💎 Getting it right with Rippling: Using software to actually scale operations\n\nRead the full transcript here: https://blog.garrytan.com/parker-conrads-hard-won-billion-dollar-startup-lessons-do-unscalable-things-but-then-actually-scale-it-part-1\n\nLearn more about Rippling at https://rippling.com\n\nFollow Parker on Twitter at https://twitter.com/parkerconrad\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/M93xlPxQADE/default.jpg',
        'published_at': datetime.datetime(2020, 1, 13, 14, 54, 15),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startup', 'ycombinator', 'rippling company', 'garry tan', 'conrad parker', 'parker conrad', 'scale', 'venture capital', 'zenefits', 'zenefits demo'],
        'view_count': 52794,
        'like_count': 1471,
        'dislike_count': 29,
        'favorite_count': 0,
        'comment_count': 68
    },
    {
        'youtube_id': 't4wmxx0xWP0',
        'title': 'Storytelling for your Series A Fundraise with Andrew Lee',
        'description': "I asked my friend and colleague Andrew Lee to sit down with me and talk through some of the advice we give very frequently to our founders. This isn't intended to be a comprehensive guide, but most everything we discuss here is stuff that comes up as a part of our experience working with hundreds of startups at Initialized.\n\nHere are Andrew's four specific tips on your Series A fundraise to consider:\n🏃🏻\u200d♀️ Brevity is the soul of wit\n🤫 Secrets create friends\n🚄 Transport investors into the future\n🦸🏻\u200d♀️ You are the right hero for this journey\n\nYou can follow Andrew Lee on Twitter here: https://twitter.com/ndrewlee",
        'thumbnail': 'https://i.ytimg.com/vi/t4wmxx0xWP0/default.jpg',
        'published_at': datetime.datetime(2020, 1, 6, 15, 49, 55),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'andrew lee', 'fundraise', 'series a', 'fundraising', 'startup fundraising', 'series b', 'seed round', 'series a round', 'startup pitch', 'pitch practice', 'y combinator interview', 'incubator interview', 'pitch deck', 'storytelling'],
        'view_count': 15213,
        'like_count': 761,
        'dislike_count': 3,
        'favorite_count': 0,
        'comment_count': 53
    },
    {
        'youtube_id': 'lbJnMmfH4-U',
        'title': 'Nikola Tesla predicted the smartphone',
        'description': 'Nikola Tesla predicted the smartphone in 1926\n\n"Culture based on the Internet is just beginning" —@pmarca\n"7.5B unique people = No one can compete with you on being you." —@naval\n\n📱5B people have smartphones\n🌎 The world has become a global brain\n💎 Only 1 in 300 people know how to code\n✍️ All the world is software, and you may contribute a line of code\n\nThe world has become a global brain. What line of code will you write?\n\nMore videos you should watch—\nHow to Get Rich by Naval Ravikant https://www.youtube.com/watch?v=1-TZqOsVCNM&t=2878s\nWhy you should be optimistic about the future, with Marc Andreessen and Kevin Kelly https://www.youtube.com/watch?v=UnU5Dikdr2U&t=214s',
        'thumbnail': 'https://i.ytimg.com/vi/lbJnMmfH4-U/default.jpg',
        'published_at': datetime.datetime(2019, 12, 30, 15, 5, 43),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['technology', 'global brain', 'internet culture', 'nikola tesla', 'naval ravikant', 'garry tan'],
        'view_count': 2430,
        'like_count': 132,
        'dislike_count': 1,
        'favorite_count': 0,
        'comment_count': 42
    },
    {
        'youtube_id': 'dtnG0ELjvcM',
        'title': 'My $200 million startup mistake: Peter Thiel asked and I said no',
        'description': "I was 23 and didn't know anything about startups. Peter Thiel offered me a big equity stake and a full year's salary to quit my stable job at Microsoft and join a startup he was starting. It wasn't even a risky decision. I still said no, and it cost me $200M. \n\nWhen opportunity knocks, you should think about taking the risk. If you're good, it's often the only way you can actually get a larger piece of the kind of value you can create when making software. There are lots of good reasons to work at a big tech giant, but there are also downsides. We talk through those things.\n\nDon't make my mistake. Make all new mistakes.\n\n00:00 Intro\n00:32 How it went down\n02:32 How to make what you're worth\n03:26 Big company upside\n03:50 How to avoid the downsides\n04:27 Work at a startup or start yourself\n\nEDIT NOTE: Google actually makes $1.6M annualized net revenue per employee per year, not profit. Apologies for the imprecision.",
        'thumbnail': 'https://i.ytimg.com/vi/dtnG0ELjvcM/default.jpg',
        'published_at': datetime.datetime(2019, 12, 23, 15, 26, 7),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'risk', 'venture capital', 'peter thiel', 'microsoft', 'technology', 'tech giant', 'garry tan', 'my $200m mistake', 'how one decision cost me 200', 'truepill', 'y combinator', 'ycombinator', 'palantir'],
        'view_count': 278924,
        'like_count': 9389,
        'dislike_count': 231,
        'favorite_count': 0,
        'comment_count': 616
    },
    {
        'youtube_id': '9FpIZ3n7PMA',
        'title': '5 years into the 10 year overnight success',
        'description': "Meet Ram Jayaraman. He is co-founder of Plate IQ. He’s helping restaurants actually understand how much money they’re spending on their food bills. This is an $860 billion market that has no pricing transparency until now. Plate IQ is used by 3% of restaurants in major cities today, and just getting started. \n\nA lot of companies claim to use AI to automate manual tasks, but Plate IQ has done it. They process now over 1M invoices per month. They took an unprofitable process and scaled it with pure software, and in the process are building something that gives pricing transparency that never existed before. \n\nFull transcript here: https://blog.garrytan.com/how-to-double-profit-margins-for-the-most-competitive-industry-in-the-world-with-software-a-conversation-with-plate-iq-cofounder-ram-jayaraman\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about Plate IQ at https://plateiq.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/9FpIZ3n7PMA/default.jpg',
        'published_at': datetime.datetime(2019, 12, 19, 19, 20, 51),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'overnight success'],
        'view_count': 10979,
        'like_count': 319,
        'dislike_count': 5,
        'favorite_count': 0,
        'comment_count': 23
    },
    {
        'youtube_id': 'gX4sxHRo12U',
        'title': 'Why planes crash (surprising)',
        'description': "Whether you’re a pilot flying a plane or a CEO running a company, it’s clear that there are ways you can minimize the chances of crashing into the ground. \n\nWhy do they happen? What do the copilots say? It all comes back to the life or death 10 second decisions. We're lucky as managers we get more than 10 seconds, but there are so many things we should get right to maximize our chance of avoiding total catastrophe.\n\n* Dont panic - things will go wrong! Know they will. \n* Identify errors: we are going to crash. \n* Communicate the right way: be understood or be actionable\n* Minimize pulling rank\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/gX4sxHRo12U/default.jpg',
        'published_at': datetime.datetime(2019, 12, 3, 15, 0, 10),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['garry tan', 'plane crash'],
        'view_count': 2261,
        'like_count': 158,
        'dislike_count': 1,
        'favorite_count': 0,
        'comment_count': 16
    },
    {
        'youtube_id': 'NBd6yJBzyis',
        'title': 'Don’t suffer from the “idea disease”',
        'description': 'Execs and new founders frequently make a big mistake: overvaluing the big idea and undervaluing the execution\n\nHere are 3 questions that let you skip that noise:\n\n🧥What should we build?\n🧵How should we build it?\n🌨Why should we build it?\n\nA great idea is not 90% of the work. Here are the 3 questions all creators need to ask in order to actually create new things that are amazing. \n\nMy COO Jen Wolf told me about the Disney Method. I wanted to share how these 3 questions are essential for people to create anything. Dreamers need realists and critics to create something great.',
        'thumbnail': 'https://i.ytimg.com/vi/NBd6yJBzyis/default.jpg',
        'published_at': datetime.datetime(2019, 11, 14, 8, 29, 34),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['disney method', 'creativity', 'imagineering', 'startups', 'steve jobs', 'apple', 'creation', 'teamwork', 'garry tan', 'venture capital', 'y combinator', 'initialized capital', 'startup', 'entrepreneur', 'inspiration', 'walt disney', 'silicon valley', 'yc', 'tech', 'startup school', 'entrepreneurship', 'entrepreneur motivation', 'entrepreneur mindset', 'entrepreneur ideas', 'motivational video'],
        'view_count': 59971,
        'like_count': 3035,
        'dislike_count': 68,
        'favorite_count': 0,
        'comment_count': 121
    },
    {
        'youtube_id': 'LMDP6TCv1pI',
        'title': "Moms shouldn't have to choose between family and career",
        'description': "Allison Robinson created The Mom Project to help talented moms get family-friendly jobs. It works with dads too, and it's free to join— they serve as your job search assistant when parents are ready to find a family-friendly job that is tailored to their expertise and needs. \n\n43% of all women who have children end up leaving the work force. Solving this problem is worth over $570 billion— and has deep implications for gender equality and the pay gap. It’s not just a mission and a cause, it’s also one of the most important business problems to solve too.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about The Mom Project at https://themomproject.com\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/LMDP6TCv1pI/default.jpg',
        'published_at': datetime.datetime(2019, 11, 5, 15, 6, 49),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['tartups', 'venture capital', 'software engineering', 'tech', 'tech jobs', 'credentialism', 'new credentials', 'recruiting', 'linkedin', 'moms', 'jobs for moms', 'parental leave', 'paid family leave', 'gender pay gap', 'gender wage gap', 'allison robinson the mom project'],
        'view_count': 2125,
        'like_count': 64,
        'dislike_count': 0,
        'favorite_count': 0,
        'comment_count': 9
    },
    {
        'youtube_id': 'UWwtXfaldl4',
        'title': 'Control your shadow, control your fate',
        'description': 'Emotion drives our decisionmaking (see USC professor Antonio Damasio\'s groundbreaking research here: https://www.youtube.com/watch?v=1wup_K2WN0I), but what do we do about emotions that we refuse to even acknowledge? Our Jungian shadows.\n\nI headed to the Big Island of Hawaii for the Lobby Conference 2019- the time away helped me think about my relationship with my own shadow: the parts of myself that drive me that I don\'t even think about. This is something everyone faces, but not everyone is aware of these internal forces. \n\nThe shadow is a moral problem that challenges the whole ego-personality, for no one can become conscious of the shadow without considerable moral effort. To become conscious of it involves recognizing the dark aspects of the personality as present and real. This act is the essential condition for any kind of self-knowledge." - Carl Jung, Aion (1951)\n\nIf you hate a person, you hate something in him that is a part of yourself. What isn\'t a part of ourselves doesn\'t disturb us - Herman Hesse\n\n"I don\'t like that man, I must get to know him better." - Abraham Lincoln\n\nThanks to Cameron Yarbrough, Stephanie Lim, Tony C Yang, and Dani Metz Shuval for watching drafts of this.\n\nI\'m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we\'re spending time with some of our best founders to learn the secrets of their success and see the future they\'re building. I\'m doing my own run-and-gun one man YouTube channel with no staff or crew - we\'re going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com',
        'thumbnail': 'https://i.ytimg.com/vi/UWwtXfaldl4/default.jpg',
        'published_at': datetime.datetime(2019, 10, 29, 3, 41, 18),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'venture capital', 'executive coaching', 'psychology', 'founders', 'founder stress', 'psyche', 'persona', 'carl jung', 'decisionmaking', 'control your fate', 'garry tan', 'antonio damasio'],
        'view_count': 11461,
        'like_count': 674,
        'dislike_count': 8,
        'favorite_count': 0,
        'comment_count': 59
    },
    {
        'youtube_id': 'bk-r0zCP59M',
        'title': 'Rejected from every college and eng job, but now a successful founder: How he did it',
        'description': "Ammon Bartram is the cofounder of Triplebyte, a powerful force in how the best software engineers in the world are getting identified today. At triplebyte.com anyone can take a test to get their skills certified. The best candidates get top paying software engineering jobs like at Dropbox, Flexport and Cruise. Tech jobs have for too long been the domain of privileged credentials—resumes and expensive degrees from a small set of top universities are terribly limited compared to the magnitude of need for great software in the world. Triplebyte circles that square by helping firms identify and hire truly great talent through objective tests that anyone can take. \n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!",
        'thumbnail': 'https://i.ytimg.com/vi/bk-r0zCP59M/default.jpg',
        'published_at': datetime.datetime(2019, 10, 22, 16, 30, 25),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'venture capital', 'software engineering', 'tech', 'tech jobs', 'credentialism', 'new credentials', 'recruiting', 'linkedin', 'triplebyte', 'garry tan', 'i got rejected from every college', 'rejected from every college', 'triplebyte interview', 'ammon bartram', 'triplebyte founder', 'y combinator application'],
        'view_count': 15440,
        'like_count': 625,
        'dislike_count': 8,
        'favorite_count': 0,
        'comment_count': 58
    },
    {
        'youtube_id': 'rfTgzA6iKZc',
        'title': '3 Tips to Nail the Y Combinator Interview',
        'description': "These were the 3 things I looked for when I was a Y Combinator partner. It's only 10 minutes, so make them count. The YC Interview is just a conversation. Take a deep breath, prepare, and practice.\n\nI’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building. I’m doing my own run-and-gun one man YouTube channel with no staff or crew — we're going for raw and unfiltered, not perfect. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/rfTgzA6iKZc/default.jpg',
        'published_at': datetime.datetime(2019, 10, 18, 5, 18, 47),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'venture capital', 'tech', 'tech jobs', 'dayinthelife', 'initialized', 'y combinator', 'yc interview', 'y combinator interview', 'garry tan', 'yc', 'yc application'],
        'view_count': 26116,
        'like_count': 1130,
        'dislike_count': 7,
        'favorite_count': 0,
        'comment_count': 78
    },
    {
        'youtube_id': '4wbCVN1yLyA',
        'title': 'Applying to Y Combinator? 3 tips from a former YC partner',
        'description': "Hi, I'm Garry Tan, former Y Combinator partner, Forbes Midas List top venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart. I'm doing my own run-and-gun one man YouTube channel with no staff or crew - we're going for raw and unfiltered and high signal in order to help you with your building your world-changing startup.\n\nIn this episode I want to talk about what advice I always give to people writing their YC applications. Watch to the end you'll find a link to submit your YC app to me for comments and review. I can guarantee I'll read the first 100 responses from YouTube, but honestly I'll try my best overall just because I really want to help. \n\nY Combinator changed my life and I hope it changes yours. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan\n\nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/4wbCVN1yLyA/default.jpg',
        'published_at': datetime.datetime(2019, 9, 18, 18, 59, 46),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'y combinator', 'yc application', 'venture capital', 'tech', 'silicon valley', 'yc interview prep', 'yc interview', 'yc', 'garry tan', 'yc w2020', 'gary tan', 'y combinator application videos successful', 'yc founder video', 'yc demo day'],
        'view_count': 31975,
        'like_count': 1248,
        'dislike_count': 9,
        'favorite_count': 0,
        'comment_count': 106
    },
    {
        'youtube_id': 'GknGzw0Nqpw',
        'title': 'Why I started a VC firm- "Too much money chasing too few good people and ideas" is totally wrong',
        'description': "Hi, I'm Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we're spending time with some of our best founders to learn the secrets of their success and see the future they're building.\n\nToday, I was at home on paternity leave, so I thought I'd sit down in my backyard and talk about what drove me to start a venture capital firm that funds early stage startups, and why I'm doing my own YouTube channel with no staff or crew.\n\nPlease like this video and subscribe to my channel if you want to see more videos like this!\n\nFind Garry on Twitter at https://twitter.com/garrytan \nLearn more about the companies we fund, and how we work with them at https://initialized.com",
        'thumbnail': 'https://i.ytimg.com/vi/GknGzw0Nqpw/default.jpg',
        'published_at': datetime.datetime(2019, 9, 12, 19, 1, 51),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['One Person', 'garry tan', 'venture capital', 'startups', 'startup advice', 'new business creation', 'creative destruction'],
        'view_count': 19160,
        'like_count': 1087,
        'dislike_count': 13,
        'favorite_count': 0,
        'comment_count': 148
    },
    {
        'youtube_id': 'fCFHyQag21Q',
        'title': 'Meet Truepill cofounder Sid Viswanathan: Telemedicine will make medicine 10x better/cheaper/faster',
        'description': 'Hi, I’m Garry Tan, venture capitalist and cofounder at Initialized Capital. We were earliest investors in billion dollar startups like Coinbase and Instacart, and we’re spending time with some of our best founders to learn the secrets of their success and see the future they’re building.\n\nToday I sat down with Sid Viswanathan, cofounder of Truepill, an API for all needs for telemedicine. Telemedicine has the potential to bring down costs and make high quality care more accessible for every person on the planet. We’re headed to Hayward, California, their west coast HQ and fulfillment center out of which they provide pharmacy services for dozens of telemedicine startups and practices large and small, shipping to all 50 states. \n\nCome learn about how as a founder, you need to choose a problem space that you could want to work on for 10 years or more. \n\nPlease like this video and subscribe to my channel if you want to see more videos like this with top founders. \n\nFind Sid on Twitter at https://twitter.com/sidviswanathan\nFind Garry on Twitter at https://twitter.com/garrytan\nLearn more about Truepill at https://truepill.com\n \nLearn more about the companies we fund, and how we work with them at https://initialized.com',
        'thumbnail': 'https://i.ytimg.com/vi/fCFHyQag21Q/default.jpg',
        'published_at': datetime.datetime(2019, 9, 6, 9, 1, 39),
        'channel_id': settings.YOUTUBE_CHANNEL_ID,
        'tags': ['startups', 'funding', 'telemedicine', 'venture capital', 'full stack startup', 'medicine', 'doctors', 'pharmacy', 'garry tan', 'truepill'],
        'view_count': 12480,
        'like_count': 383,
        'dislike_count': 4,
        'favorite_count': 0,
        'comment_count': 63
    }
]
