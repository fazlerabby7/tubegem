import pytest

from datetime import timedelta
from statistics import median

from django.conf import settings

from ..models import Video
from ..tasks import fetch_and_update_videos
from .factories import VideoFactory


@pytest.mark.django_db
class TestFetchAndUpdateVideos:

    def test_create_videos(self, youtube_get_all_videos):
        fetched_videos = youtube_get_all_videos.return_value

        views_median = median(
            [fetched_video['view_count'] for fetched_video in fetched_videos]
        )

        fetch_and_update_videos()

        for fetched_video in fetched_videos:
            db_video = Video.objects.get(youtube_id=fetched_video['youtube_id'])

            assert db_video.title == fetched_video['title']
            assert db_video.description == fetched_video['description']
            assert db_video.thumbnail == fetched_video['thumbnail']
            assert db_video.published_at.replace(tzinfo=None) == fetched_video['published_at']
            assert db_video.channel_id == fetched_video['channel_id']
            assert db_video.view_count == fetched_video['view_count']
            assert db_video.like_count == fetched_video['like_count']
            assert db_video.dislike_count == fetched_video['dislike_count']
            assert db_video.favorite_count == fetched_video['favorite_count']
            assert db_video.comment_count == fetched_video['comment_count']
            assert db_video.first_hour_views == fetched_video['view_count']
            assert db_video.performance == db_video.first_hour_views / views_median

            db_tags = db_video.tags.order_by('value')
            fetched_tags = sorted(fetched_video['tags'])
            assert db_video.tags.count() == len(fetched_tags)
            for db_tag, fetched_tag in zip(db_tags, fetched_tags):
                assert db_tag.value == fetched_tag

        youtube_get_all_videos.assert_called_once_with(settings.YOUTUBE_CHANNEL_ID)

    def test_update_videos(self, youtube_get_all_videos):
        fetched_videos = youtube_get_all_videos.return_value

        stored_videos = [
            VideoFactory(youtube_id=fetched_video['youtube_id'])
            for fetched_video in fetched_videos
        ]

        views_median = median(
            [fetched_video['view_count'] for fetched_video in fetched_videos]
        )

        fetch_and_update_videos()

        assert Video.objects.count() == len(stored_videos)

        for fetched_video in fetched_videos:
            db_video = Video.objects.get(youtube_id=fetched_video['youtube_id'])

            assert db_video.title == fetched_video['title']
            assert db_video.description == fetched_video['description']
            assert db_video.thumbnail == fetched_video['thumbnail']
            assert db_video.published_at.replace(tzinfo=None) == fetched_video['published_at']
            assert db_video.channel_id == fetched_video['channel_id']
            assert db_video.view_count == fetched_video['view_count']
            assert db_video.like_count == fetched_video['like_count']
            assert db_video.dislike_count == fetched_video['dislike_count']
            assert db_video.favorite_count == fetched_video['favorite_count']
            assert db_video.comment_count == fetched_video['comment_count']
            assert db_video.first_hour_views == fetched_video['view_count']
            assert db_video.performance == db_video.first_hour_views / views_median

            db_tags = db_video.tags.order_by('value')
            fetched_tags = sorted(fetched_video['tags'])
            assert db_video.tags.count() == len(fetched_tags)
            for db_tag, fetched_tag in zip(db_tags, fetched_tags):
                assert db_tag.value == fetched_tag

        youtube_get_all_videos.assert_called_once_with(settings.YOUTUBE_CHANNEL_ID)

    def test_no_first_hour_views_update_after_1_hour(self, youtube_get_all_videos):
        fetched_videos = youtube_get_all_videos.return_value

        for fetched_video in fetched_videos:
            db_video = VideoFactory(
                youtube_id=fetched_video['youtube_id'],
                first_hour_views=100
            )
            db_video.created_at -= timedelta(hours=1, seconds=1)
            db_video.save()

        fetch_and_update_videos()

        for fetched_video in fetched_videos:
            db_video = Video.objects.get(youtube_id=fetched_video['youtube_id'])

            assert db_video.first_hour_views != fetched_video['view_count']
            assert db_video.first_hour_views == 100

        youtube_get_all_videos.assert_called_once_with(settings.YOUTUBE_CHANNEL_ID)
