import pytest

from datetime import datetime

from django.conf import settings
from rest_framework.test import APIClient

from .factories import TagFactory, VideoFactory, fake


@pytest.fixture
def youtube_get_all_videos(mocker):
    mocked_method = mocker.patch('videos.tasks.youtube.get_all_videos')
    mocked_method.return_value = [
        {
            'youtube_id': 'idyfHs3DWmc',
            'title': 'Masterclass: How to',
            'description': "The world is being eaten by software",
            'thumbnail': 'https://i.ytimg.com/vi/idyfHs3DWmc/default.jpg',
            'published_at': datetime(2020, 3, 11, 15, 17, 14),
            'channel_id': settings.YOUTUBE_CHANNEL_ID,
            'tags': ['developers', 'software developers'],
            'view_count': 11385,
            'like_count': 323,
            'dislike_count': 4,
            'favorite_count': 0,
            'comment_count': 29
        },
        {
            'youtube_id': 'dhnzBQjsgGU',
            'title': "Stories from China",
            'description': 'Table of contents with timestamps below.',
            'thumbnail': 'https://i.ytimg.com/vi/dhnzBQjsgGU/default.jpg',
            'published_at': datetime(2020, 3, 4, 14, 42, 53),
            'channel_id': settings.YOUTUBE_CHANNEL_ID,
            'tags': [],
            'view_count': 7500,
            'like_count': 265,
            'dislike_count': 6,
            'favorite_count': 0,
            'comment_count': 41
        }
    ]
    return mocked_method


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def make_tags():
    def _make_tags(count, **kwargs):
        return [
            TagFactory(value=fake.unique.pystr(max_chars=100), **kwargs)
            for i in range(count)
        ]
    return _make_tags


@pytest.fixture
def make_videos():
    def _make_videos(count, **kwargs):
        return [
            VideoFactory(
                youtube_id=fake.unique.lexify(text='???????????'), **kwargs
            )
            for i in range(count)
        ]
    return _make_videos
