import pytest

from datetime import datetime

from django.urls import reverse


@pytest.mark.django_db
class TestVideoList:

    def test_get_video_list(self, api_client, make_tags, make_videos):
        videos = make_videos(20, tags=make_tags(5))

        url = reverse('videos')
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 20
        assert response.data['next'] is None
        assert response.data['previous'] is None

        for result, video in zip(response.data['results'], videos):
            assert result['id'] == video.id
            assert result['youtube_id'] == video.youtube_id
            assert result['title'] == video.title
            assert result['description'] == video.description
            assert result['thumbnail'] == video.thumbnail
            assert result['published_at'] == datetime.strftime(
                video.published_at, '%Y-%m-%dT%H:%M:%SZ'
            )
            assert result['channel_id'] == video.channel_id
            assert result['tags'] == [tag.value for tag in video.tags.all()]
            assert result['view_count'] == video.view_count
            assert result['like_count'] == video.like_count
            assert result['dislike_count'] == video.dislike_count
            assert result['favorite_count'] == video.favorite_count
            assert result['comment_count'] == video.comment_count
            assert result['first_hour_views'] == video.first_hour_views
            assert result['performance'] == video.performance
            assert result['created_at'] == datetime.strftime(
                video.created_at, '%Y-%m-%dT%H:%M:%S.%fZ'
            )
            assert result['modified_at'] == datetime.strftime(
                video.modified_at, '%Y-%m-%dT%H:%M:%S.%fZ'
            )

    def test_get_video_list_pagination(self, api_client, make_videos):
        videos = make_videos(60)

        url = reverse('videos')
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 60
        assert response.data['next'] == ('http://testserver/api/v1/videos'
                                         '?limit=50&offset=50')
        assert response.data['previous'] is None
        assert len(response.data['results']) == 50

        next_url = f'{url}?limit=50&offset=50'
        response = api_client.get(next_url)

        assert response.status_code == 200
        assert response.data['count'] == 60
        assert response.data['next'] is None
        assert response.data['previous'] == ('http://testserver/api'
                                             '/v1/videos?limit=50')
        assert len(response.data['results']) == 10

    def test_get_video_list_performance_min_filter(self, api_client, make_videos):
        make_videos(9, performance=5)
        videos = make_videos(7, performance=10)

        url = f"{reverse('videos')}?performance_min=10"
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 7
        assert len(response.data['results']) == 7

        for result, video in zip(response.data['results'], videos):
            assert result['id'] == video.id

    def test_get_video_list_performance_max_filter(self, api_client, make_videos):
        videos = make_videos(9, performance=5)
        make_videos(7, performance=10)

        url = f"{reverse('videos')}?performance_max=7"
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 9
        assert len(response.data['results']) == 9

        for result, video in zip(response.data['results'], videos):
            assert result['id'] == video.id

    def test_get_video_list_performance_both_filter(self, api_client, make_videos):
        make_videos(5, performance=1)
        make_videos(5, performance=15)
        videos = make_videos(11, performance=7)

        url = f"{reverse('videos')}?performance_min=5&performance_max=10"
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 11
        assert len(response.data['results']) == 11

        for result, video in zip(response.data['results'], videos):
            assert result['id'] == video.id

    def test_get_video_list_tags_filter(self, api_client, make_tags, make_videos):
        tags = make_tags(3)
        videos = make_videos(12, tags=tags)
        make_videos(7, tags=make_tags(2))

        url = f"{reverse('videos')}?tags={tags[0].value}"
        response = api_client.get(url)

        assert response.status_code == 200
        assert response.data['count'] == 12
        assert len(response.data['results']) == 12

        for result, video in zip(response.data['results'], videos):
            assert result['id'] == video.id

    def test_get_video_list_multiple_tags_filter(self, api_client, make_tags, make_videos):
        tags1 = make_tags(5)
        videos1 = make_videos(2, tags=tags1)
        tags2 = make_tags(3)
        videos2 = make_videos(3, tags=tags2)
        make_videos(4, tags=make_tags(2))

        url = f"{reverse('videos')}?tags={tags1[0].value},{tags2[1].value}"
        response = api_client.get(url)
        assert response.status_code == 200
        assert response.data['count'] == 5
        assert len(response.data['results']) == 5

        for result, video in zip(response.data['results'], videos1+videos2):
            assert result['id'] == video.id
