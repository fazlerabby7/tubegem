import pytest
from django.conf import settings

from ..services import YoutubeV3
from . import test_data


class TestYoutubeV3:

    @pytest.fixture
    def youtube_mocks(self, responses):
        channels_url = ('https://www.googleapis.com/youtube/v3/channels'
                        f'?part=contentDetails&id={settings.YOUTUBE_CHANNEL_ID}'
                        f'&key={settings.YOUTUBE_API_KEY}&maxResults=50')

        playlist_id = test_data.YOUTUBE_CHANNELS_RESPONSE['items'][0]['contentDetails']['relatedPlaylists']['uploads']

        playlists_url_1 = ('https://www.googleapis.com/youtube/v3/playlistItems'
                           f'?part=contentDetails&playlistId={playlist_id}'
                           f'&key={settings.YOUTUBE_API_KEY}&maxResults=50')
        playlists_url_2 = ('https://www.googleapis.com/youtube/v3/playlistItems'
                           f'?part=contentDetails&playlistId={playlist_id}&key='
                           f'{settings.YOUTUBE_API_KEY}&maxResults=50&pageToken='
                           f'{test_data.YOUTUBE_PLAYLISTS_RESPONSE_1["nextPageToken"]}')

        video_ids_1 = ','.join([
            item['contentDetails']['videoId']
            for item in test_data.YOUTUBE_PLAYLISTS_RESPONSE_1['items']
        ])
        videos_url_1 = ('https://www.googleapis.com/youtube/v3/videos'
                        f'?part=snippet,statistics&id={video_ids_1}&key='
                        f'{settings.YOUTUBE_API_KEY}&maxResults=50')

        video_ids_2 = ','.join([
            item['contentDetails']['videoId']
            for item in test_data.YOUTUBE_PLAYLISTS_RESPONSE_2['items']
        ])
        videos_url_2 = ('https://www.googleapis.com/youtube/v3/videos'
                        f'?part=snippet,statistics&id={video_ids_2}&key='
                        f'{settings.YOUTUBE_API_KEY}&maxResults=50')

        responses.add(method=responses.GET, url=channels_url,
                      json=test_data.YOUTUBE_CHANNELS_RESPONSE, status=200)
        responses.add(method=responses.GET, url=playlists_url_1,
                      json=test_data.YOUTUBE_PLAYLISTS_RESPONSE_1, status=200)
        responses.add(method=responses.GET, url=playlists_url_2,
                      json=test_data.YOUTUBE_PLAYLISTS_RESPONSE_2, status=200)
        responses.add(method=responses.GET, url=videos_url_1,
                      json=test_data.YOUTUBE_VIDEOS_RESPONSE_1, status=200)
        responses.add(method=responses.GET, url=videos_url_2,
                      json=test_data.YOUTUBE_VIDEOS_RESPONSE_2, status=200)

    def test_get_all_videos(self, youtube_mocks, responses):
        youtube = YoutubeV3()
        videos = youtube.get_all_videos(settings.YOUTUBE_CHANNEL_ID)
        assert videos == test_data.FORMATTED_VIDEOS_DATA
        assert len(responses.calls) == 5
