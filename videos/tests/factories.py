import factory

from faker import Faker
from django.conf import settings

from ..models import Tag, Video


fake = Faker('en_US')


class TagFactory(factory.django.DjangoModelFactory):
    value = fake.unique.pystr(max_chars=100)

    class Meta:
        model = Tag


class VideoFactory(factory.django.DjangoModelFactory):
    youtube_id = fake.unique.lexify(text='???????????')
    title = fake.pystr(max_chars=100)
    description = fake.paragraph()
    thumbnail = fake.url()
    published_at = fake.date_time()
    channel_id = settings.YOUTUBE_CHANNEL_ID
    view_count = fake.pyint()
    like_count = fake.pyint()
    dislike_count = fake.pyint()
    favorite_count = fake.pyint()
    comment_count = fake.pyint()
    first_hour_views = view_count
    performance = fake.pyfloat(positive=True, max_value=5)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for tag in extracted:
                self.tags.add(tag)

    class Meta:
        model = Video
