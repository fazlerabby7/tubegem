#!/bin/bash
set -xe

(while ! nc -z -w 1 db 3306; do echo "$(date) - waiting for mysql"; sleep 1; done)
echo "$(date) - mysql is up!"

echo "$(date) - applying database migration..."
python manage.py migrate

echo "$(date) - starting the development server..."
python manage.py runserver 0.0.0.0:8000
